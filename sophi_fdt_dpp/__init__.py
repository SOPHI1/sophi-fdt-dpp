# Licensed under a 3-clause BSD style license - see LICENSE
#"""SOPHI_FDT_DPP: SOPHI FDT Data Procesing Pipeline."""

#__all__ = ["__version__"]
#
#import pkg_resources
#
#try:
#    __version__ = pkg_resources.get_distribution(__name__).version
#except pkg_resources.DistributionNotFound:
#    # package is not installed
#    pass

from .src import *
from .src.cmilos import *

# from .src.phi_fits import *
# from .src.phi_gen import *
# from .src.phi_reg import *
# from .src.phi_utils import *
# from .src.phifdt_flat import *
# from .src.phifdt_pipe import *
# from .src.phi_rte import *
# from .src.plot_lib import *
# from .src.tools import *
# from .src.phifdt_pipe_modules import *
# from .src.phi_json_gen import json_generator
# from .src.cog import *
# from .src.phifdt_disk import *
# from .src.polcal_py.polcal_lib_v2 import *
# from .src.phifdt_rte_interactive import *