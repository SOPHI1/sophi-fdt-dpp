import numpy as np
from datetime import datetime
import getopt, sys
sys.path.append('../../../../sophi-fdt-dpp/')
import sophi_fdt_dpp as fdt


def orbit_info(date,frame='SOLO_HEEQ'):

    date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S')
    stats = fdt.phi_orbit(date, 'Solar Orbiter', frame = frame)
    rad_sun = 1391016           #radio sol en km
    dit_sun = 149597870         #definicion de AU
    asiz = np.arctan(rad_sun/dit_sun) #tamaño angular del Sol
    sun_arc = asiz*180/np.pi*60*60  #en arcsec
    res = 3.57#3.61
    # lambda_ref = 6173341    #mAA
    # vlight = 299792.458     #km/s
    # tunconst = 0.3513       #mAA/V
    # line_space = 140        #mAA
    # cont_space = 300        #mAA

    # #+++++++ To determine voltage range for individual FG temerptatures in dependence of S/C velocity+++++
    # #61
    # u_cen61 = (stats.vr * lambda_ref/vlight)/tunconst
    # umin61_r = u_cen61 - line_space/tunconst
    # umax61_r = u_cen61 + cont_space/tunconst
    # umin61_b = u_cen61 - cont_space/tunconst
    # umax61_b= u_cen61 + line_space/tunconst
    # #56
    # u_cen56 = (stats.vr * lambda_ref/vlight+(40.323*5))/tunconst
    # umin56_r = u_cen56 - line_space/tunconst
    # umax56_r = u_cen56 + cont_space/tunconst
    # umin56_b = u_cen56 - cont_space/tunconst
    # umax56_b= u_cen56 + line_space/tunconst
    # #66
    # u_cen66 = (stats.vr * lambda_ref/vlight+(-40.323*5))/tunconst
    # umin66_r = u_cen66 - line_space/tunconst
    # umax66_r = u_cen66 + cont_space/tunconst
    # umin66_b = u_cen66 - cont_space/tunconst
    # umax66_b = u_cen66 + line_space/tunconst
    # #+++ Calculate distance to Sun in which flat can used for the data or which flat distance is needed for the data +++
    # data_dist = stats.r/1.1
    # flat_dist = stats.r*1.1

    strings =['Solo Sun center (lat.):   ',
              'Solo Sun center (long.):  ',
              'Solo radial velocity:     ',
              'Distance:                 ',
              'Solar diameter from solo: ',
              'FDT solar diameter:       ',
              'FDT resolution:           ',
              'HRT FoV:                  ',
              'HRT resolution:           ',
              'Earth Sun (lat.):         ',
              'Earth Sun (long.):        ',
              'Earth Sun Distance:       ']
    data=[stats.lat*180/np.pi , stats.lon*180/np.pi ,stats.vr , stats.r,
            stats.s_size ,stats.s_size/res,  res*stats.r,
            1*stats.r/2. * 2048,  1*stats.r, stats.earth_lat*180/np.pi , stats.earth_lon*180/np.pi, stats.earth_r,]
    units = ['degree', 'degree', 'km/s', 'au', 'arcsec', 'px', 'arcsec', 'arcsec', 'arcsec', 'degree', 'degree', 'AU']
    x = ' '
    print(' ')
    print('Time: ',date,' Frame:', frame)
    print('_________________________________________________________')
    for i,j,k in zip(strings, data,units):
        print('{:<2s}{:<12s}{:>12.4f}{:<3s}{:<9s}'.format(x,i,j,x,k))
    print('_________________________________________________________')
    #+++ Print out voltage range for each FG temperature depending on S/C velocity +++
    # print('  56º, cont. red:  Umin = ' + str(f'{umin56_r:.1f}')+ ', Umax = ' + str(f'{umax56_r:.1f}'))
    # print('  56º, cont. blue: Umin = ' + str(f'{umin56_b:.1f}')+ ', Umax = ' + str(f'{umax56_b:.1f}'))
    # print('  61º, cont. red:  Umin = ' + str(f'{umin61_r:.1f}')+ ', Umax = ' + str(f'{umax61_r:.1f}'))
    # print('  61º, cont. blue: Umin = ' + str(f'{umin61_b:.1f}')+ ', Umax = ' + str(f'{umax61_b:.1f}'))
    # print('  66º, cont. red:  Umin = ' + str(f'{umin66_r:.1f}')+ ', Umax = ' + str(f'{umax66_r:.1f}'))
    # print('  66º, cont. blue: Umin = ' + str(f'{umin66_b:.1f}')+ ', Umax = ' + str(f'{umax66_b:.1f}'))
    # print('_________________________________________________________')
    #+++ Print distance limitations for limitations  +++
    # print('  Flat fits data acquired at ' + str(f'{data_dist:.4f}')+ ' au')
    # print('  Data fits flat acquired at ' + str(f'{flat_dist:.4f}')+ ' au')
    # print('_________________________________________________________')

    print(stats.x,stats.y)

if __name__ == "__main__":

    date = "2021-08-01T00:00:00"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hd:v", ["help", "date="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        print('USE: python phi_orbit.py -d 2021-08-01T00:00:00')
        sys.exit(2)
    output = None
    verbose = False
    for o, a in opts:
        if o == "-v":
            verbose = True
        elif o in ("-h", "--help"):
            print('USE: python phi_orbit_info.py -d 2021-08-01T00:00:00')
            sys.exit()
        elif o in ("-d", "--date"):
            date = a
        else:
            assert False, "unhandled option"

    orbit_info(date)


