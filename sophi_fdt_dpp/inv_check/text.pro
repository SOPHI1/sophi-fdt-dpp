function text,a,format=format

if not(keyword_set(format)) then text=strtrim(string(a),1) else text=strtrim(string(a,format=format),1) 


return,text
end
