;COMO EMPEZAR
; restore,'sunspot_512x512_recont.sav' & merged_recont = shift(merged_recont,0,1,1)
; restore,'sunspot_512x512_recont_20.sav' & merged_recont_20 = shift(merged_recont_20,0,1,1)
; restore,'sunspot_512x512.sav'
;
; view_two,merged_original,merged_recont

pro view_two,data1,data2,zoom=zoom,landa=landa

if not(keyword_set(zoom)) then zoom = 1. 
if not(keyword_set(landa)) then landa = 0.

s = size(data1)
dimx = s(1)
dimy = s(2)
nlam = s(4)

loadct,0
window,2,xsize=dimx/4,ysize=dimy/4
window,3,xsize=201*2+1,ysize=201,title='ZOOM'
window,1,xsize=dimx*2*zoom+1,ysize=dimy*zoom

tvscl,congrid(reform(data1(*,*,0,landa)),dimx*zoom,dimy*zoom),0
tvscl,congrid(reform(data2(*,*,0,landa)),dimx*zoom,dimy*zoom),1
i1 = total(reform(data1(*,*,0,landa)))/dimx/dimy
i2 = total(reform(data2(*,*,0,landa)))/dimx/dimy
i2 = i1
i1 = 1.
i2 = 1.

!mouse.button=0
xb=0
yb=0
device,set_graphics=6
plots,/dev,[xb,xb]*zoom,[0,dimy]*zoom
plots,/dev,[xb+dimx,xb+dimx]*zoom,[0,dimy]*zoom
plots,/dev,[0,dimx*2]*zoom,[yb,yb]*zoom

se = 'I'
sse = ['I','Q','U','V']
x = 0
y = 0
while !mouse.button ne 4 do begin 

   cursor,x,y,2,/device
    x = x / zoom
    y = y / zoom
	print,x,y
	
    if ( ((x ne xb) or (y ne yb)) and ((x gt 0) and (x lt dimx-1) and (y gt 0) and (y lt dimy-1))) then begin
       plots,/dev,[xb,xb]*zoom,[0,dimy]*zoom
       plots,/dev,[xb+dimx,xb+dimx]*zoom,[0,dimy]*zoom
       plots,/dev,[0,dimx*2]*zoom,[yb,yb]*zoom
       
       case strupcase(get_kbrd(0)) of
          'I':	begin
        device,set_graphics=3      
             tvscl,congrid(reform(data1(*,*,0,landa)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,0,landa)),dimx*zoom,dimy*zoom),1
             se = 'I'
        device,set_graphics=6
          endcase
          'Q':	begin
         device,set_graphics=3
            tvscl,congrid(reform(data1(*,*,1,landa)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,1,landa)),dimx*zoom,dimy*zoom),1
             se = 'Q'
        device,set_graphics=6
          endcase
          'U':	begin
         device,set_graphics=3
            tvscl,congrid(reform(data1(*,*,2,landa)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,2,landa)),dimx*zoom,dimy*zoom),1
              se = 'U'
       device,set_graphics=6
          endcase
          'V':	begin
        device,set_graphics=3
             tvscl,congrid(reform(data1(*,*,3,landa)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,3,landa)),dimx*zoom,dimy*zoom),1
             se = 'V'
        device,set_graphics=6
          endcase
          '+':	begin
          landa = (landa + 1 )<(nlam-1)
          print,landa
          iid = where(sse eq se)
        device,set_graphics=3
             tvscl,congrid(reform(data1(*,*,0,landa+nlam)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,0,landa+nlam)),dimx*zoom,dimy*zoom),1
        device,set_graphics=6
          endcase
          '-':	begin
          landa = (landa - 1 )<(nlam-1)
          print,landa
          iid = where(sse eq se)
        device,set_graphics=3
             tvscl,congrid(reform(data1(*,*,0,landa+nlam)),dimx*zoom,dimy*zoom),0
             tvscl,congrid(reform(data2(*,*,0,landa+nlam)),dimx*zoom,dimy*zoom),1
        device,set_graphics=6
          endcase
          else: print,'Hola'
       endcase

       plots,/dev,[x,x]*zoom,[0,dimy]*zoom
       plots,/dev,[x+dimx,x+dimx]*zoom,[0,dimy]*zoom
       plots,/dev,[0,dimx*2]*zoom,[y,y]*zoom

       device,set_graphics=3

         wset,3
        case se of
          'I':	begin
        print,landa,(x-25)>0,(x+25)<(dimx-1),(y-25)>0,(y+25)<(dimy-1)
        tvscl,congrid(data1((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),0,landa),50*4,50*4),0
        tvscl,congrid(data2((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),0,landa),50*4,50*4),1
          endcase
          'Q':	begin
        tvscl,congrid(data1((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),1,landa),50*4,50*4),0
        tvscl,congrid(data2((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),1,landa),50*4,50*4),1
          endcase
          'U':	begin
        tvscl,congrid(data1((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),2,landa),50*4,50*4),0
        tvscl,congrid(data2((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),2,landa),50*4,50*4),1
          endcase
          'V':	begin
        tvscl,congrid(data1((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),3,landa),50*4,50*4),0
        tvscl,congrid(data2((x-25)>0:(x+25)<(dimx-1),(y-25)>0:(y+25)<(dimy-1),3,landa),50*4,50*4),1
          endcase
       endcase
        plots,/dev,[100,100],[0,200]
        plots,/dev,[300,300],[0,200]
        plots,/dev,[0,400],[100,100]

      wset,2 
       colores
       !p.multi=[0,2,2]
        mini = min([data2(x,y,0,*)/i2,data1(x,y,0,*)/i1])
        maxi = max([data2(x,y,0,*)/i2,data1(x,y,0,*)/i1])
        minq = min([data2(x,y,1,*)/i2,data1(x,y,1,*)/i1])
        maxq = max([data2(x,y,1,*)/i2,data1(x,y,1,*)/i1])
        minu = min([data2(x,y,2,*)/i2,data1(x,y,2,*)/i1])
        maxu = max([data2(x,y,2,*)/i2,data1(x,y,2,*)/i1])
        minv = min([data2(x,y,3,*)/i2,data1(x,y,3,*)/i1])
        maxv = max([data2(x,y,3,*)/i2,data1(x,y,3,*)/i1])
        plot,data1(x,y,0,*)/i1,thick=2,yrange=[mini,maxi],ystyle=3
        oplot,data2(x,y,0,*)/i2,color=3,thick=2
        plots,[landa,landa],[mini,maxi]
        plot,data1(x,y,1,*)/i1,thick=2,yrange=[minq,maxq],ystyle=3
        oplot,data2(x,y,1,*)/i2,color=3,thick=2
        plots,[landa,landa],[minq,maxq]
        plot,data1(x,y,2,*)/i1,thick=2,yrange=[minu,maxu],ystyle=3
        oplot,data2(x,y,2,*)/i2,color=3,thick=2
        plots,[landa,landa],[minu,maxu]
        plot,data1(x,y,3,*)/i1,thick=2,yrange=[minv,maxv],ystyle=3
        oplot,data2(x,y,3,*)/i2,color=3,thick=2
        plots,[landa,landa],[minv,maxv]
 



        xb=x & yb=y
        loadct,0,/silent
        wset,1 
        device,set_graphics=6
     endif

    if !mouse.button eq 2 then begin
       print,'HHHH'
    endif
endwhile
device,set_graphics=3

end

