pro check_invs, stokes_data, model_data

    ; USES IDL MILOS VERSION:
    ; https://github.com/vivivum/MilosIDL

    ; it is a toy version that has to be tailored reading the header information
    ; in the future, this will be implemented in python using pymilos.

    ;Read data
    data = readfits(stokes_data)
    ;2048 by 2048 by 4 by 6 array
    sdata = size(data)

    ;Read inversion results
    model = readfits(model_data)
    ;2048 by 2048 by 4 by 6 array
    smodel = size(model)

    !p.multi=[0,2,2]
    tvframe3,model(*,*,8)<2>(-2)
    tvframe3,model(*,*,2) <1000
    tvframe3,model(*,*,3) > 0< 180
    tvframe3,model(*,*,4) > 0< 180

    init_milos,'6173',wl

    sampling = [-265.,-140.,-70.,0.,70.,140.]
    lda = sampling/1e3 + 6173.3356


    fm=fltarr(smodel[1],smodel[2],11)
    ch=fltarr(smodel[1],smodel[2])
    fm(*,*,0) = (model(*,*,5))<10
    fm(*,*,1) = (model(*,*,2))
    fm(*,*,2) = (model(*,*,8))
    fm(*,*,3) = (model(*,*,6))
    fm(*,*,4) = (model(*,*,7))
    fm(*,*,5) = (model(*,*,3))
    fm(*,*,6) = (model(*,*,4))
    fm(*,*,7) = (model(*,*,9))
    fm(*,*,8) = (model(*,*,10))
    fm(*,*,9) = 0
    fm(*,*,10) = 1
    ch[*,*] = (model(*,*,11))<20

    pxi=0
    pxf=sdata(1)-1
    pyi=0
    pyf=sdata(2)-1
    check_ajustes,data[pxi:pxf,pyi:pyf,*,*],lda,fm[pxi:pxf,pyi:pyf,*],ch[pxi:pxf,pyi:pyf],'6173',order='xypl',zoom=0.4,yrange='[-0.02,0.02]',si='[0.1,1.1]';,zoom=zoom,color=color

end

pro test_check_inv

    stokes_data = 'test_data/solo_L2_phi-fdt-stokes_20220408T031502_V1_0244080001.fits.gz'
    model_data = 'test_data/solo_L2_phi-fdt-FullModel_20220408T031502_V1_0244080001.fits.gz'

    check_invs, stokes_data, model_data

end