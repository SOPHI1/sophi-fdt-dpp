"""
``phi_pipe_modules.py`` contains pipeline main reduction modules.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez (orozco@iaa.es)**
   :Contributors: **Alex Feller  (feller@mps.mpg.de) Hanna Strecker (strekerh@iaa.es)**

.. list-table::
   :widths: 25 50
   :header-rows: 1


   * - program
     - Short summary
   * - :py:meth:`zero_level`
     - Image mean level
   * - :py:meth:`phi_correct_dark`
     - TBD
   * - :py:meth:`interpolateImages`
     - TBD
   * - :py:meth:`distortion_correction_model`
     - TBD
   * - :py:meth:`correct_distortion_single`
     - TBD
   * - :py:meth:`phi_correct_distortion`
     - TBD
   * - :py:meth:`phi_correct_prefilter`
     - TBD
   * - :py:meth:`applyPrefilter`
     - TBD
   * - :py:meth:`check_pmp_temp`
     - TBD
   * - :py:meth:`phi_apply_demodulation`
     - TBD
   * - :py:meth:`crosstalk_ItoQUV`
     - TBD
   * - :py:meth:`cross_talk_QUV`
     - TBD
   * - :py:meth:`phi_correct_ghost`
     - TBD
   * - :py:meth:`phi_correct_fringes`
     - TBD
   * - :py:meth:`generate_level2`
     - TBD
   * - :py:meth:`save_level2`
     - TBD
   * - :py:meth:`update_header_stokes`
     - TBD
   * - :py:meth:`crop_calibration_data`
     - TBD
   * - :py:meth:`expand_mask`
     - TBD
   * - :py:meth:`sigma_mask`
     - TBD
   * - :py:meth:`n_coord`
     - TBD
   * - :py:meth:`dmodel`
     - TBD
   * - :py:meth:`cmesh`
     - TBD
"""


import os, time
from random import sample
import statistics
import re
# from platform import node
from sys import exit

import numpy as np
import scipy as sp
from astropy.io import fits as pyfits
from matplotlib import pyplot as plt
from scipy.ndimage import gaussian_filter, map_coordinates
from scipy.ndimage import rotate as sp_rotate
from scipy.optimize import minimize, minimize_scalar
from .processing import MP

from . import plot_lib as plib
from .phi_fits import find_string, fits_get, set_level
from .phi_gen import bin_annulus, generate_circular_mask, shift, gradient
from .phifdt_disk import find_center
from .phi_reg import moments, shift_subp
from .phi_rte import VERS_RTE, phi_rte
from .phi_utils import (
    azimutal_average, genera_2d,
    limb_darkening, newton, debug)
from .phifdt_disk import get_disk_mean
from .tools import bcolors, fix_path, printc, update_header


def zero_level(input: np.ndarray, mask: np.ndarray,FIVE_SIGMA: float = 5):
    """
    zero_level calculate main level of a image discarding pixels that go beyond FIVE_SIGMA

    _extended_summary_

    :param input: image input
    :type input: np.ndarray
    :param mask: mask input
    :type mask: np.ndarray
    :param FIVE_SIGMA: sigma level, defaults to 5
    :type FIVE_SIGMA: float, optional
    :return: mean value
    :rtype: float

    """

    s = input[mask > 0]
    #  calculate average of gain table for normalization
    sm = np.mean(s)
    sm2 = np.mean(s ** 2)
    five_sigma = FIVE_SIGMA * np.sqrt(sm2 - sm * sm)  #TODO: check this expression
    sm = np.mean(s[np.abs(s - sm) < five_sigma])
    return sm


def phi_correct_dark(dark_f, data, header, data_scale, verbose=False, get_dark=False):
    """
    phi_correct_dark correct dark current

    _extended_summary_

    :param dark_f: _description_
    :type dark_f: _type_
    :param data: _description_
    :type data: _type_
    :param header: _description_
    :type header: _type_
    :param data_scale: _description_
    :type data_scale: _type_
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :param get_dark: _description_, defaults to False
    :type get_dark: bool, optional
    :return: _description_
    :rtype: _type_
    """
    printc('-->>>>>>> Reading Darks                   ', color=bcolors.OKGREEN)
    printc('          Input should be [y-dim,x-dim].', color=bcolors.OKGREEN)
    printc('          DARK IS DIVIDED by 256.   ', color=bcolors.OKGREEN)

    try:
        dark, dark_header = fits_get(dark_f)
        DID = dark_header['PHIDATID']
        printc('Dark DID: ', DID, color=bcolors.OKBLUE)
    except Exception:
        printc("ERROR, Unable to open darks file: {}", dark_f, color=bcolors.FAIL)
        raise

    if dark_header['IMGDIRX'] == 'YES':
        printc("Flipping the dark: ", color=bcolors.FAIL)
        dark = np.fliplr(dark)

    # printc('-->>>>>>> Scaling DARK... ',color=bcolors.OKGREEN)
    # dark = scale_data(dark,dark_header)
    # dark = dark / 256.

    dark_scale = fits_get(dark_f, get_scaling=True)
    scaling = 1

    if dark_scale["Present"][0] == data_scale["Present"][0]:
        scaling = dark_scale["scaling"][0] / data_scale["scaling"][0]
    else:
        scaling = dark_scale["scaling"][1] / data_scale["scaling"][1] * dark_scale["scaling"][0]

    if scaling != 1:
        printc('          checking scaling and correcting for it in the dark.', dark_scale, data_scale, scaling,
               color=bcolors.WARNING)
        # dark = dark * scaling
        print(scaling, 'scaling')

    if get_dark:  # for kll
        printc('-->>>>>>> Dark is output in phi_correct_dark()', color=bcolors.OKGREEN)
        return dark, scaling

    printc('-->>>>>>> Correcting dark current.', color=bcolors.OKGREEN)
    PXBEG1 = int(header['PXBEG1']) - 1
    PXEND1 = int(header['PXEND1']) - 1
    PXBEG2 = int(header['PXBEG2']) - 1
    PXEND2 = int(header['PXEND2']) - 1

    # CHECK NACC
    acc = int(header['ACCACCUM']) * int(header['ACCCOLIT'])
    acc_dark = int(dark_header['ACCACCUM']) * int(dark_header['ACCCOLIT'])
    if acc != acc_dark:
        printc('WARNING - NACC NOT IDENTICAL DURING DARK CORRECTION', color=bcolors.WARNING)
        printc('DARK NACC ', acc_dark, ' DATA NACC ', acc, color=bcolors.WARNING)

    # Keep a copy of the data before dark correction, for later
    if verbose:
        if np.ndim(data) == 3:
            dummy = data[0, :, :]
        else:
            dummy = data[0, 0, :, :]

    dark = crop_calibration_data(dark, dark_header, data, header)
    data = data - np.broadcast_to(dark, data.shape)

    if 'CAL_DARK' in header:  # Check for existence
        header['CAL_DARK'] = DID

    if verbose:
        md = np.mean(dark)
        mds = np.std(dark)
        if verbose:
            if np.ndim(data) == 3:
                md2 = np.mean(data[0, 0:100, 0:100])
                mds2 = np.std(data[0, 0:100, 0:100])
                plib.show_three(dark[PXBEG2:PXEND2 + 1, PXBEG1:PXEND1 + 1], dummy, data[0, :, :],
                                vmin=[md - 3 * mds, md - 3 * mds, md2 - 5 * mds2],
                                vmax=[md + 3 * mds, md + 3 * mds, md2 + 5 * mds2], block=True, pause=0.1,
                                title=['Dark', 'Data', 'Data after dark correction'],
                                xlabel='Pixel', ylabel='Pixel', cmap='gray')
            else:
                md2 = np.mean(data[0, 0, 0:100, 0:100])
                mds2 = np.std(data[0, 0, 0:100, 0:100])
                plib.show_three(dark[PXBEG2:PXEND2 + 1, PXBEG1:PXEND1 + 1], dummy, data[0, 0, :, :],
                                vmin=[md - 3 * mds, md - 3 * mds, md2 - 5 * mds2],
                                vmax=[md + 3 * mds, md + 3 * mds, md2 + 5 * mds2], block=True, pause=0.1,
                                title=['Dark', 'Data', 'Data after dark correction'],
                                xlabel='Pixel', ylabel='Pixel', cmap='gray')

    return data, header


def interpolateImages(image1, image2, dist1I, distI2):
    """
    interpolateImages _summary_

    _extended_summary_

    :param image1: _description_
    :type image1: _type_
    :param image2: _description_
    :type image2: _type_
    :param dist1I: _description_
    :type dist1I: _type_
    :param distI2: _description_
    :type distI2: _type_
    :return: _description_
    :rtype: _type_
    """

    return (image1 * distI2 + image2 * dist1I) / (dist1I + distI2)


def distortion_correction_model(x_u, y_u, pars):
    """
    distortion_correction_model _summary_

    _extended_summary_

    :param x_u: _description_
    :type x_u: _type_
    :param y_u: _description_
    :type y_u: _type_
    :param pars: _description_
    :type pars: _type_
    :return: _description_
    :rtype: _type_
    """
    x_c, y_c, k = pars
    r_u = np.sqrt((x_u - x_c)**2 + (y_u - y_c)**2)
    x_d = x_c + (x_u - x_c) * (1 - k * r_u**2)
    y_d = y_c + (y_u - y_c) * (1 - k * r_u**2)
    return x_d, y_d


def correct_distortion_single(im: np.ndarray, pars: list, get_shifts=False):
    """
    correct_distortion_single _summary_

    _extended_summary_

    :param im: image
    :type im: np.ndarray
    :param pars: distortion parameters (x, y, k); Note: x -> columns, y -> rows!
    :type pars: list
    :param get_shifts: get shift vectors, defaults to False
    :type get_shifts: bool, optional
    :return: _description_
    :rtype: _type_
    """

    nx, ny = im.shape
    x = np.arange(nx)
    y = np.arange(ny)
    X, Y = np.meshgrid(x, y)
    x_d, y_d = distortion_correction_model(X, Y, pars)
    corrected_im = map_coordinates(im, [y_d, x_d], order=1)

    if get_shifts:
        return corrected_im, x_d - X, y_d - Y
    else:
        return corrected_im


def phi_correct_distortion(data, header, pars=None, parallel=False, verbose=False):
    """
    phi_correct_distortion Correct the distortion of a set of images, based on the `distortion_correction_model`.

    _extended_summary_

    :param data: set of images with dimensions (waves, stokes, nx, ny)
    :type data: _type_
    :param header: data header
    :type header: _type_
    :param pars: distortion parameters; if `None`, then the nominal parameters are used, defaults to None
    :type pars: _type_, optional
    :param parallel: _description_, defaults to False
    :type parallel: bool, optional
    :param verbose: verbosity flag, defaults to False
    :type verbose: bool, optional
    :raises TypeError: _description_
    :return: Distortion corrected set of images with the same dimensions as `data`
    :rtype: _type_
    """

    printc('-->>>>>>> Correcting distortion', color=bcolors.OKGREEN)

    t0 = time.time()

    # Nominal parameters
    if pars is None:
        pars = (1064.4, 948.2, 1.9e-08)

    global distortion_map_func  # make distortion_map_func visible to MP

    def distortion_map_func(arg_list):
        image_no, im = arg_list
        return image_no, correct_distortion_single(im, pars)

    # Generate argument list for mapping function
    original_shape = None
    if len(data.shape) == 4:
        original_shape = data.shape
        data = list(np.reshape(data, (data.shape[0] * data.shape[1], data.shape[2], data.shape[3])))
    elif len(data.shape) == 3:
        data = list(data)
    elif len(data.shape) == 2:
        data = [data]
    else:
        raise TypeError('data must have at least 2 dimensions')

    args_list = [(i, data[i]) for i in range(len(data))]

    if parallel:
        n_workers = min(6, os.cpu_count())
        # TODO: replace by values defined in JSON config file
        if verbose:
            print(f'Multi-process with {n_workers} cores')
        results = list(MP.simultaneous(distortion_map_func, args_list, workers=n_workers))
    else:
        results = list(map(distortion_map_func, args_list))

    # sort data according to image no.
    image_no = [r[0] for r in results]
    sorted_indices = np.argsort(image_no)
    data = [r[1] for r in results]
    data = [data[idx] for idx in sorted_indices]

    if len(data) == 1:
        data = data[0]
    else:
        data = np.array(data)

    # Restore original data shape
    if original_shape is not None:
        data = np.reshape(data, original_shape)

    if verbose:
        dt = time.time() - t0
        print(f'Time spent in distortion correction: {dt: .3f}s')

    # Add or update header keyword
    val = '({:.1f}, {:.1f}, {:.1e})'.format(pars[0], pars[1], pars[2])
    if 'CAL_DIS' in header:  # Check for existence
        header['CAL_DIS'] = val
    else:
        header.set('CAL_DIS', val, 'Distortion parameters applied (x, y, k)', after='CAL_DARK')

    return data, header


def phi_correct_prefilter(prefilter_fits, header, data, voltagesData, verbose=False):
    """
    phi_correct_prefilter _summary_

    _extended_summary_

    :param prefilter_fits: _description_
    :type prefilter_fits: _type_
    :param header: _description_
    :type header: _type_
    :param data: _description_
    :type data: _type_
    :param voltagesData: _description_
    :type voltagesData: _type_
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :return: _description_
    :rtype: _type_
    """
    printc('-->>>>>>> Read prefilter and correct for it ')
    printc('          ', prefilter_fits, '   ')
    xd = int(header['NAXIS1'])
    yd = int(header['NAXIS2'])

    prefdata, pref_header = fits_get(prefilter_fits, scale=False)
    prefdata = prefdata.astype(np.float32)

    prefdata = crop_calibration_data(prefdata, pref_header, data, header, ignore_number_of_frames=True)

    # PREFILTER INFO
    # ——————
    # I call the prefilter correction from the python pipeline like this:
    # data_corr = np.reshape(data_corr,(6,4,2048, 2048)) (6,4,y,x)
    prefscale = 8388608. / 2.
    # the scale of the prefilter is irrelevant because it is a relative variation
    # data = data / prefilter. Scale = 2 makes the prefilter to be aroung 1
    # later, everything is normalized wrt the continuum so this is not important.
    scale = 1.
    prefVoltages = [-1300, -1234, -1169, -1103, -1038, -972, -907, -841, -776,
                    -710, -645, -579, -514, -448, -383, -317, -252, -186, -121, -56, 9, 74,
                    140, 205, 271, 336, 402, 467, 533, 598, 664, 729, 795, 860, 926, 991, 1056, 1122, 1187,
                    1253, 1318, 1384, 1449, 1515, 1580, 1646, 1711, 1777, 1842]
    if verbose:
        datap = np.copy(data)

    data = applyPrefilter(data, voltagesData, prefdata, prefscale, prefVoltages, -1, scaledown=scale, verbose=verbose)

    if verbose:
        plt.plot(data[:, 0, yd // 2, xd // 2] / np.mean(data[:, 0, yd // 2, xd // 2]), 'o-', label='center, corrected')
        plt.plot(datap[:, 0, yd // 2, xd // 2] / np.mean(datap[:, 0, yd // 2, xd // 2]), '--', label='center, original')

        plt.plot(data[:, 0, yd // 2, xd // 2 + 400] / np.mean(data[:, 0, yd // 2, xd // 2 + 400]), 'o-', label='center + 400 px, corrected')
        plt.plot(datap[:, 0, yd // 2, xd // 2 + 400] / np.mean(datap[:, 0, yd // 2, xd // 2 + 400]), '--', label='center + 400 px, original')

        plt.plot(data[:, 0, yd // 2, xd // 2 - 400] / np.mean(data[:, 0, yd // 2, xd // 2 - 400]), 'o-', label='center - 400 px, corrected')
        plt.plot(datap[:, 0, yd // 2, xd // 2 - 400] / np.mean(datap[:, 0, yd // 2, xd // 2 - 400]), '--', label='center - 400 px, original')

        plt.legend()
        plt.grid()
        plt.show()
        del datap

    update_header(header, 'CAL_PRE', os.path.basename(prefilter_fits), 'CAL_DARK', 'prefilter file')

    return data, header


def applyPrefilter(data, wvltsData, prefilter, prefScale, wvltsPref, direction, scaledown=8, verbose=False):
    """
    applyPrefilter Modified version from K. Albert.

    _extended_summary_

    :param data: _description_
    :type data: _type_
    :param wvltsData: _description_
    :type wvltsData: _type_
    :param prefilter: _description_
    :type prefilter: _type_
    :param prefScale: _description_
    :type prefScale: _type_
    :param wvltsPref: _description_
    :type wvltsPref: _type_
    :param direction: _description_
    :type direction: _type_
    :param scaledown: _description_, defaults to 8
    :type scaledown: int, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :return: _description_
    :rtype: _type_
    """

    prefToApply = np.zeros((6, prefilter.shape[1], prefilter.shape[2]))
    prefilter = prefilter / prefScale  # dos

    for i in range(6):
        wvlCurr = wvltsData[i]
        valueClosest = min(wvltsPref, key=lambda x: abs(x - wvlCurr))
        if verbose:
            print("iter", i, "wvlCurr", wvlCurr)
            print("iter", i, "valueClosest", valueClosest)
        indexClosest = wvltsPref.index(valueClosest)
        if verbose:
            print("iter", i, "indexClosest", indexClosest)
        if (valueClosest < wvlCurr):
            indexBefore = indexClosest
            indexAfter = indexClosest + 1
        else:
            indexAfter = indexClosest
            indexBefore = indexClosest - 1

        dist1I = abs(wvltsPref[indexBefore] - wvltsData[i])
        distI2 = abs(wvltsPref[indexAfter] - wvltsData[i])
        prefToApply[i, :, :] = interpolateImages(prefilter[indexBefore], prefilter[indexAfter], dist1I, distI2)

        if verbose:
            print("mean prefValue Before:", np.mean(prefilter[indexBefore]) * 256)
            print("mean prefValue After:", np.mean(prefilter[indexAfter]) * 256)
            print("distance1:", dist1I)
            print("distance2:", distI2)
            print("percentage:", distI2 / (dist1I + distI2))

        # Remove scale factor from prefilter
        if verbose:
            print("mean prefilter:", np.mean(prefToApply[i, :, :]) * 256)
        # prefToApply[i,:,:] = prefToApply[i,:,:] / prefScale   #dos
        if verbose:
            print("mean prefilter:", np.mean(prefToApply[i, :, :]))

    if verbose:
        print("Reshaping prefilter:")
        print(prefToApply.shape)
        print(data.shape)
    if (data.shape[2] != prefToApply.shape[1]):
        FOV_Start_y = int(prefToApply.shape[1] / 2 - data.shape[2] / 2)
        FOV_End_y = int(prefToApply.shape[1] / 2 + data.shape[2] / 2)
        prefToApply = prefToApply[:, FOV_Start_y:FOV_End_y, :]
    if verbose:
        print(prefToApply.shape)
    if (data.shape[3] != prefToApply.shape[2]):
        FOV_Start_x = int(prefToApply.shape[2] / 2 - data.shape[3] / 2)
        FOV_End_x = int(prefToApply.shape[2] / 2 + data.shape[3] / 2)
        prefToApply = prefToApply[:, :, FOV_Start_x:FOV_End_x]
    if verbose:
        print(prefToApply.shape)

    dataPrefApplied = np.zeros(data.shape)
    for i in range(4):
        if (direction == 1):
            dataPrefApplied[:, i, :, :] = data[:, i, :, :] * prefToApply
        elif (direction == -1):
            dataPrefApplied[:, i, :, :] = data[:, i, :, :] / prefToApply  # / scaledown  #dos
        else:
            print("Ivnalid direction! Must be 1 (mult) or -1 (div).")

    # This is the maximum range scaled from the division of the prefilter.
    # The smallest number in the prefilter is 0.13977 -> 1/0.13977 = 7.3 ~= 8
    # define RNG_RES_APPL_PREF 8  -> reason for division by 8.

    return dataPrefApplied


def check_pmp_temp(header, telescope: str = 'FDT'):
    """
    check_pmp_temp     "get PMP temperatures"

    _extended_summary_

    :param header: _description_
    :type header: _type_
    :param telescope: _description_, defaults to 'FDT'
    :type telescope: str, optional
    :return: _description_
    :rtype: _type_
    """

    printc(f'Instrument {telescope} PMP temperatures', color=bcolors.YELLOW)
    if telescope == 'FDT':
        printc('-------------------------------------------------------',color=bcolors.OKGREEN)
        printc('            FPMPTSP[1,2] = '+str(header['FPMPTSP1'])+' , '+str(header['FPMPTSP2']),color=bcolors.YELLOW)
        printc('            FPMP[1,2]PT1 = '+str(header['FPMP1PT1'])+' , '+str(header['FPMP2PT1']),color=bcolors.YELLOW)
        printc('            FPMP[1,2]PT2 = '+str(header['FPMP1PT2'])+' , '+str(header['FPMP2PT2']),color=bcolors.YELLOW)
        printc('-------------------------------------------------------',color=bcolors.OKGREEN)
        temperature_set_point = round(float(header['FPMPTSP1']))
        return f'FDT{temperature_set_point}'
    if telescope == 'HRT':
        printc('-------------------------------------------------------',color=bcolors.OKGREEN)
        printc('            HPMPTSP[1,2] = '+str(header['HPMPTSP1'])+' , '+str(header['HPMPTSP2']),color=bcolors.YELLOW)
        printc('            HPMP[1,2]PT1 = '+str(header['HPMP1PT1'])+' , '+str(header['HPMP2PT1']),color=bcolors.YELLOW)
        printc('            HPMP[1,2]PT2 = '+str(header['HPMP1PT2'])+' , '+str(header['HPMP2PT2']),color=bcolors.YELLOW)
        printc('-------------------------------------------------------',color=bcolors.OKGREEN)
        temperature_set_point = round(float(header['HPMPTSP1']))
        return f'HRT{temperature_set_point}'


def phi_apply_demodulation(data, instrument, header=None, demod=False, verbose=False, modulate=False, FDT_MOD_ROTATION_ANGLE: float = -127.6):
    """
    phi_apply_demodulation Use demodulation matrices to demodulate data size ls,ps,ys,xs  (n_wave*S_POL,N,M)

    ATTENTION: FDT40 is fixed to the one Johann is using!!!!

    :param data: _description_
    :type data: _type_
    :param instrument: _description_
    :type instrument: _type_
    :param header: _description_, defaults to None
    :type header: _type_, optional
    :param demod: _description_, defaults to False
    :type demod: bool, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :param modulate: _description_, defaults to False
    :type modulate: bool, optional
    :param FDT_MOD_ROTATION_ANGLE: _description_, defaults to -127.6
    :type FDT_MOD_ROTATION_ANGLE: float, optional
    :raises SystemError: _description_
    :return: _description_
    :rtype: _type_
    """

    if header and instrument == 'auto':
        instrument = check_pmp_temp(header)
        printc('Loading demodulation matrices for: ',instrument,color=bcolors.OKGREEN)

    def rotation_matrix(angle_rot):
        c, s = np.cos(2 * angle_rot * np.pi / 180), np.sin(2 * angle_rot * np.pi / 180)
        return np.array([[1, 0, 0, 0], [0, c, s, 0], [0, -s, c, 0], [0, 0, 0, 1]])

    def rotate_m(angle, matrix):
        rot = rotation_matrix(angle)
        return np.matmul(matrix, rot)

    if instrument == 'FDT40':  # MODEL FIT  INTA April 2022
        mod_matrix = np.array([[0.99913, -0.69504, -0.38074, -0.60761],
                               [1.0051, 0.41991, -0.73905, 0.54086],
                               [0.99495, 0.44499, 0.36828, -0.8086],
                               [1.0008, -0.38781, 0.91443, 0.13808]])
        mod_matrix = rotate_m(FDT_MOD_ROTATION_ANGLE, mod_matrix)
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT40r':  # MODEL FIT  ROTATED 127.6 degree (counterclockwise viewing from detector)
        mod_matrix = np.array(
            [[0.99493622, -0.17760949, 0.761317, -0.60735698],
             [1.00582275, -0.81799487, -0.21224808, 0.54241813],
             [0.99684194, 0.24220817, -0.52514488, -0.8134466],
             [1.0023991, 0.98423895, 0.13729987, 0.13481875]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT40_old':  # Johanns (it is the average in the central area of the one onboard)
        mod_matrix = np.array([[1.0006, -0.7132, 0.4002, -0.5693],
                               [1.0048, 0.4287, -0.7143, 0.5625],
                               [0.9963, 0.4269, -0.3652, -0.8229],
                               [0.9983, -0.4022, 0.9001, 0.1495]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT40_dos':  # NUMERICAL FIT  (0,360,10)-90   *** David Orozco March 2022
        mod_matrix = np.array([[0.99493621, -0.69068949, -0.36619221, -0.60735698],
                               [1.00582274, 0.41415974, -0.73663873, 0.54241813],
                               [0.99684194, 0.4458513, 0.36831856, -0.8134466],
                               [1.00239911, -0.38416442, 0.9165126, 0.13481876]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT40_mod':  # ANALITICA FIT   (0,360,10)-90    *** David Orozco March 2022 - based on PHI / FDT model
        mod_matrix = np.array([[1., -0.69634929, -0.37330967, -0.61297435],
                               [1., 0.41324362, -0.73330607, 0.53989992],
                               [1., 0.44598252, 0.36860311, -0.81561715],
                               [1., -0.3846079, 0.91317683, 0.13485121]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT45':  # MODEL FIT  INTA April 2022
        mod_matrix = np.array([[1.0023, -0.64814, -0.56202, -0.51859],
                               [1.0041, 0.54693, -0.55299, 0.633],
                               [0.99523, 0.46132, 0.54165, -0.69603],
                               [0.99838, -0.61944, 0.66189, 0.42519]])
        mod_matrix = rotate_m(FDT_MOD_ROTATION_ANGLE, mod_matrix)
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT45r':  # MODEL FIT  ROTATED -129
        mod_matrix = np.array([[1.00234094, -0.41906999, 0.74917275, -0.50334909],
                               [1.00217352, -0.64539594, -0.43787681, 0.64452649],
                               [0.9940154, 0.41165963, -0.57186307, -0.71895057],
                               [1.00147014, 0.79262462, 0.45277008, 0.40793693]])
        demodM = np.linalg.inv(mod_matrix)


    elif instrument == 'FDT45_HREW':  # MODEL FIT  INTA April 2022
        mod_matrix = np.array([[1.0022, -0.6543, -0.57471, -0.49363],
                               [1.0038, 0.54924, -0.53817, 0.64209],
                               [0.99438, 0.45867, 0.515, -0.71314],
                               [0.99964, -0.61277, 0.67894, 0.40843]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'FDT45_E2E':  # E2E doc
        mod_matrix = np.array([[1.0035, -0.6598, 0.5817, -0.4773],
                               [1.0032, 0.5647, 0.5275, 0.6403],
                               [0.9966, 0.4390, -0.5384, -0.7150],
                               [0.9968, -0.6169, -0.6443, 0.4425]])
        demodM = np.linalg.inv(mod_matrix)

    elif instrument == 'HRT40_old':  # E2E doc
        demodM = np.array([[0.26450154, 0.2839626, 0.12642948, 0.3216773],
                           [0.59873885, 0.11278069, -0.74991184, 0.03091451],
                           [0.10833212, -0.5317737, -0.1677862, 0.5923593],
                           [-0.46916953, 0.47738808, -0.43824592, 0.42579797]])
    elif instrument == 'HRT40':  # MODEL FIT  INTA April 2022
        mod_matrix = np.array([[0.99816, 0.61485, 0.010613, -0.77563],
                               [0.99192, 0.08382, 0.86254, 0.46818],
                               [1.0042, -0.84437, 0.12872, -0.53972],
                               [1.0057, -0.30576, -0.87969, 0.40134]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'HRT40_dos':  # NUMERICAL FIT  (0,360,10)-90   *** David Orozco March 2022
        mod_matrix = np.array([[0.98889418, 0.63311402, 0.01490015, -0.7816713],
                               [0.99603854, 0.07763719, 0.89156538, 0.46243721],
                               [1.00427941, -0.84080523, 0.12554703, -0.55159669],
                               [1.01078787, -0.29564517, -0.88365522, 0.41235959]])
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'HRT45':  # E2E doc
        mod_matrix = np.array([[1.00159, -0.50032, 0.7093, -0.4931],
                               [1.0040, 0.6615, 0.3925, 0.6494],
                               [0.9954, 0.3356, -0.6126, -0.7143],
                               [0.9989, -0.7474, -0.5179, 0.4126]])  # MIA
        demodM = np.linalg.inv(mod_matrix)
    elif instrument == 'HRT50_E2E':  # E2E doc
        demodM = np.array([[0.28037298, 0.18741922, 0.25307596, 0.28119895],
                           [0.40408596, 0.10412157, -0.7225681, 0.20825675],
                           [-0.19126636, -0.5348939, 0.08181918, 0.64422774],
                           [-0.56897295, 0.58620095, -0.2579202, 0.2414017]])
    elif instrument == 'HRT50':  # MODEL FIT  INTA April 2022
        mod_matrix = np.array([[1.0014, 0.56715, 0.3234, -0.74743],
                               [1.0007, 0.0037942, 0.69968, 0.71423],
                               [1.0002, -0.98937, 0.04716, -0.20392],
                               [0.99769, 0.27904, -0.86715, 0.39908]])
        demodM = np.linalg.inv(mod_matrix)
    else:
        printc('No demod available in demod_phi.py', color=bcolors.FAIL)
        raise SystemError()

    printc('Demodulation matrix for ', instrument, color=bcolors.WARNING)
    printc(demodM, color=bcolors.WARNING)

    ls, ps, ys, xs = data.shape

    if demod:
        return demodM

    if modulate:
        for i in range(ls):
            data[i, :, :, :] = np.reshape(np.matmul(mod_matrix, np.reshape(data[i, :, :, :], (ps, xs * ys))),
                                          (ps, ys, xs))
        return data

    for i in range(ls):
        data[i, :, :, :] = np.reshape(np.matmul(demodM, np.reshape(data[i, :, :, :], (ps, xs * ys))), (ps, ys, xs))

    if header is not None:
        if 'CAL_IPOL' in header:  # Check for existence
            header['CAL_IPOL'] = instrument
        else:
            header.set('CAL_IPOL', instrument, 'Onboard calibrated for instrumental polarization', after='CAL_DARK')
        return data, header
    else:
        return data


def crosstalk_ItoQUV(input_data: np.ndarray,
                     verbose: bool = False,
                     mask: np.ndarray = np.empty([], dtype=float),
                     threshold: float = 0.5,
                     lower_threshold: float = 40.0,
                     norma: float = 1.0,
                     crosstalk_order: int = 1,
                     mode: str = 'standard',
                     divisions: int = 21,
                     cntr_rad: list = [],
                     ind_wave: bool = False,
                     continuum_pos: int = 0):
    """
    crosstalk_ItoQUV calculates the cross-talk from Stokes $I$ to Stokes $Q$, $U$, and $V$.

    The procedure works as follow: (see Sanchez Almeida, J. \& Lites, B.~W.\ 1992, \apj, 398, 359. doi:10.1086/171861)


    :param input_data: input data. Dimensions should be `[Stokes, wavelength, ydim,xdim]`
    :type input_data: np.ndarray
    :param verbose: activate verbosity, defaults to False
    :type verbose: bool, optional
    :param mask: mask for selecting the image area for cross-talk correction dimension = `[ydim,xdim]` , defaults to 0
    :type mask: np.ndarray, optional
    :param threshold: threshold for considering signals in the cross-talk calculation. :math:`p = \sqrt(Q^2 + U^2 = V^2) < threshold`. Given in percent, defaults to 0.5 %
    :type threshold: float, optional
    :param lower_threshold: lower threshold for considering signals in the cross-talk calculation. :math:`I(\\lambda) > lower_threshold`. Given in percent, defaults to 40 %
    :type lower_threshold: float, optional
    :param norma: Data normalization value, defaults to 1.0
    :type norma: float, optional
    :param crosstalk_order: order of the polinomial to fit. Only 1 and 2 are allowed values, defaults to 1
    :type crosstalk_order: int, optional
    :param mode: crosstalk mode, defaults to 'standard'

        there are two different modes for calculating the cross-talk.

        1. ´mode = 'standard'´. It is done using the whole Sun, i.e., one cross-talk coefficient for the full image
        2. ´mode = 'surface'´. In this case, the Sun is divided in different squares (`N = divisions`) along the two dimensions and the crosstalk is calculated for each of the squares.
           Then, a surface is fit to the :math:`N**2` coefficients and applied to the data. In this case, lower_trheshold is ignored and if a mask is not privided, the code generated a mask coinciding with the solar disk.

    :type mode: str, optional
    :param divisions: number of square divisions alon gone axis for `surface` mode. defaults to 6.0
    :type divisions: int, optional
    :param cntr_rad: Center and radius of the solar disk `[cx,cy,rad]`. Needed for `mode='surface'`. defaults to []
    :type cntr_rad: list, optional
    :param ind_wave: Use just the continuum wavelength for the crosstalk or the whole line. defaults to False
    :type ind_wave: bool, optional
    :param continuum_pos: If ind_wave, this keyword is mandatory and contains the position of the continuum. defaults to 0.
    :type continuum_pos: int, optional
    :return: cross-talk parameters
    :rtype: List of np.ndarray
    """

    def __fit_crosstalk(input,masked,full_verbose = False):
        # multiply the data by the mask and flatten everything. Flatenning makes things easier
        xI = (input[:, 0, :, :] * masked).flatten() #Stokes I
        yQ = (input[:, 1, :, :] * masked).flatten() #Stokes Q
        yU = (input[:, 2, :, :] * masked).flatten() #Stokes U
        yV = (input[:, 3, :, :] * masked).flatten() #Stokes V

        # check two conditions:
        # 1) mask should be > 0 and intensity above lower_threshold

        if mask_set:
            idx = (xI != 0)
        else:
            idx = (xI != 0) & (xI > (lower_threshold/100. * norma))

        xI = xI[idx]
        yQ = yQ[idx]
        yU = yU[idx]
        yV = yV[idx]

        # 2) Stokes Q,U, and V has to be below a limit (for not inclusing polarization signals)

        yP = np.sqrt(yQ**2 + yU**2 + yV**2)
        idx = yP < (threshold/100. * norma)

        xI = xI[idx]
        yQ = yQ[idx]
        yU = yU[idx]
        yV = yV[idx]

        # Now we perform a cross-talk fit.

        cQ = np.polyfit(xI, yQ, crosstalk_order)
        cU = np.polyfit(xI, yU, crosstalk_order)
        cV = np.polyfit(xI, yV, crosstalk_order)

        if full_verbose:
            st = 0
            w = 5
            plt.imshow(masked)
            plt.show()
            plt.imshow(input[w,st,:,:])
            plt.show()

            xp = np.linspace(xI.min(), xI.max(), 100)
            ynew = np.polyval(cQ, xp)
            plt.plot(xI,yQ,'.')
            plt.plot(xp,ynew,'o')
            plt.show()
            ynew = np.polyval(cU, xp)
            plt.plot(xI,yU,'.')
            plt.plot(xp,ynew,'o')
            plt.show()
            ynew = np.polyval(cV, xp)
            plt.plot(xI,yV,'.')
            plt.plot(xp,ynew,'o')
            plt.show()

        return cQ, cU, cV

    # Check the dimensions of the input data.
    # Since, this program computes the cross-talk from I to QUV, it needs, at least, 3 dimensions [(I,Q,U,V), Y-dim, X-dim]
    # If input data is four dimensions, it should be [wavelength, (I,Q,U,V), Y-dim, X-dim]
    # since we do not run anything in wavelength, we will just expand the dimensions to 4 if the input_data.shape == 3

    if input_data.ndim == 3:
        # no wavelength has been provided
        input_data = input_data[np.newaxis,:]

    if input_data.ndim != 4:
        printc('Input data shall have 3 or 4 dimensions but it is of ',input_data.ndim,' dimensions',color=bcolors.FAIL)
        ValueError("Check dimensions of input data into crosstalk_ItoQUV")

    wd,sd,yd,xd = input_data.shape

    wave_index = np.arange(input_data.shape[0],dtype=int)
    printc('                     wave_range:',wave_index, color=bcolors.OKBLUE )

    if ind_wave:
        wave_index[:] = continuum_pos
        printc('          Computing the cross-talk using just the continuum....', color=bcolors.OKBLUE)
        printc('                     wave_range has change to:',wave_index, color=bcolors.OKBLUE )

    # First check also if mask has been provided. If np.array in empty with dim (), shape is False.
    if mask.shape:
        printc('mask inside `crosstalk_ItoQUV`, has been provided',color=bcolors.OKGREEN)
        if mask.ndim != 2:
            printc('Input mask shall have 2 dimensions but it is of ',mask.ndim,' dimensions',color=bcolors.FAIL)
            ValueError("Check dimensions of input mask into crosstalk_ItoQUV")
        mask_set = True
    else:
        if mode == 'surface':
            printc('Mask shall be provided when using surface method. Generating a valid one.',color=bcolors.WARNING)
            if cntr_rad:
                mask, coords = generate_circular_mask([yd-1, xd-1], cntr_rad[2] - 20, cntr_rad[2] - 20)
                mask = shift(mask,shift = [int(cntr_rad[0] - xd//2), int(cntr_rad[1]- yd//2)], fill_value=0)
            else:
                printc('Input mode surface requires a valid cntr_rad input',color=bcolors.FAIL)
                ValueError("Provide header in crosstalk_ItoQUV call")
            mask_set = True
        else:
            # mask has NOT been provided. We set it to ONES coinciding with the last to dimensions of the input data
            mask = np.ones((input_data.shape[-2:]))
            #since it was not provided, we will use lower_threshold to avoid noisy profiles
            mask_set = False


    # there are three different modes for calculating the cross-talk
    # 1) mode = 'standard'
    #    It is done using the whole Sun, i.e., one cross-talk coefficient for the full image
    # 2) mode = 'surface'
    #    In this case, the Sun is divided in different squares (`N = divisions`) along the two dimensions and the crosstalk is calculated for each of the squares.
    #    Then, a surface is fit to the N**2 coefficients and applied to the data.
    #    In this case, lower_trheshold is ignored and if a mask is not privided, the code generated a mask coinciding with the solar disk.

    if mode == 'standard':

        cQ, cU, cV = __fit_crosstalk(input_data[wave_index,:,:,:],mask,full_verbose=False)

        print('Cross-talk from I to Q: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cQ[0], cQ[1],width=8,prec=4))
        print('Cross-talk from I to U: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cU[0], cU[1],width=8,prec=4))
        print('Cross-talk from I to V: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cV[0], cV[1],width=8,prec=4))

        # corrects the data:
        corrected_data = np.copy(input_data)
        if crosstalk_order == 1:
            corrected_data[:, 1, :, :] = input_data[:, 1, :, :] - cQ[0] * input_data[:, 0, :, :] - cQ[1]
            corrected_data[:, 2, :, :] = input_data[:, 2, :, :] - cU[0] * input_data[:, 0, :, :] - cU[1]
            corrected_data[:, 3, :, :] = input_data[:, 3, :, :] - cV[0] * input_data[:, 0, :, :] - cV[1]
        if crosstalk_order == 2:
            corrected_data[:, 1, :, :] = input_data[:, 1, :, :] - cQ[0] * input_data[:, 0, :, :]**2 - cQ[1] * input_data[:, 0, :, :] - cQ[2]
            corrected_data[:, 2, :, :] = input_data[:, 2, :, :] - cU[0] * input_data[:, 0, :, :]**2 - cU[1] * input_data[:, 0, :, :] - cU[2]
            corrected_data[:, 3, :, :] = input_data[:, 3, :, :] - cV[0] * input_data[:, 0, :, :]**2 - cV[1] * input_data[:, 0, :, :] - cV[2]

        return cQ, cU, cV, 0 , 0 , 0, corrected_data

    elif mode == 'surface':
        #TODO add more degrees to the fitting surface. A plane might not ne enough

        def __generate_squares(radius,divisions: int = 6):
            square_centers = np.linspace(-radius,radius,divisions+1,endpoint=True)[1:] - radius / divisions
            X,Y = np.meshgrid(square_centers, square_centers)
            return np.vstack([X.ravel(), Y.ravel()])

        def __fit_plane(data, mask=None):

            """
            Fit 2D plane to data. Will be replazed by a global one (comming from had-hoc branch) at some point.
            """
            xd, yd = data.shape
            x = np.arange(xd)
            y = np.arange(yd)
            X, Y = np.meshgrid(x, y)

            if mask is not None:
                X_masked = X[mask]
                Y_masked = Y[mask]
                Z_masked = data[mask]
            else:
                X_masked = X
                Y_masked = Y
                Z_masked = data

            A = np.vstack([X_masked.flatten(), Y_masked.flatten(), np.full(X_masked.size,1)]).T
            a, b, c = np.linalg.lstsq(A, Z_masked.flatten(), rcond=None)[0]
            P = a * X + b * Y + c

            return (P, a, b, c)

        #this mode requires header as input
        if cntr_rad:
            #generate squares to fill the solar disk
            rad = cntr_rad[2] #header['RSUN_ARC'] / header['CDELT1']
            size2 = rad//divisions #half the size of the square
            area = (size2*2)**2
            ndiv = __generate_squares(rad,divisions = divisions)
            cx = cntr_rad[0] #header['CRPIX1']
            cy = cntr_rad[1] #header['CRPIX2']

            if verbose:
                circle = plt.Circle((cx,cy), rad, color='r', fill=False)

                fig, ax = plt.subplots(figsize=(8,8))
                ax.imshow(input_data[0,0,:,:],cmap='gray_r')

                ax.add_patch(circle)
                for i in range(divisions**2):
                    square = plt.Rectangle((ndiv[0,i] + cx - size2,ndiv[1,i] + cy - size2), size2 * 2, size2 * 2 , color='r', fill=False)
                    ax.add_patch(square)
                plt.show()

            # estimated coefficients for the fit
            cQ = np.zeros((2,divisions**2))
            cU = np.zeros((2,divisions**2))
            cV = np.zeros((2,divisions**2))
             #loop over coordinates would be

            for i,loop in enumerate(range(divisions**2)):
                from_x, to_x = np.round(ndiv[0,i] + cx - size2).astype(int) , np.round(ndiv[0,i] + cx + size2).astype(int)
                from_y, to_y = np.round(ndiv[1,i] + cy - size2).astype(int) , np.round(ndiv[1,i] + cy + size2).astype(int)
                if np.sum(mask[from_y:to_y,from_x:to_x])  > area * 0.1:
                    cQ[:,loop], cU[:,loop], cV[:,loop]  = \
                        __fit_crosstalk(input_data[wave_index,:,from_y:to_y,from_x:to_x],mask[from_y:to_y,from_x:to_x],full_verbose=False)

            cQQ = np.reshape(cQ,(2,divisions,divisions))
            cUU = np.reshape(cU,(2,divisions,divisions))
            cVV = np.reshape(cV,(2,divisions,divisions))
            if verbose:
                plt.imshow(cQQ[0,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(cUU[0,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(cVV[0,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(cQQ[1,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(cUU[1,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(cVV[1,:,:]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()

            org_size = input_data.shape[-2:]
            dummy_s = np.zeros(org_size)
            dummy_c = np.zeros(org_size)
            for i,loop in enumerate(range(divisions**2)):
                dummy_s[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cQ[0,loop]
                dummy_c[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cQ[1,loop]

            sfitQ = (__fit_plane(dummy_s, mask = (dummy_s != 0)) , __fit_plane(dummy_c, mask = (dummy_c != 0)) )
            if verbose:
                plt.imshow(sfitQ[0][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(sfitQ[1][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()

            dummy_s = np.zeros(org_size)
            dummy_c = np.zeros(org_size)
            for i,loop in enumerate(range(divisions**2)):
                dummy_s[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cU[0,loop]
                dummy_c[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cU[1,loop]
            sfitU = (__fit_plane(dummy_s, mask = (dummy_s != 0)) , __fit_plane(dummy_c, mask = (dummy_c != 0)) )
            if verbose:
                plt.imshow(sfitU[0][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(sfitU[1][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()

            dummy_s = np.zeros(org_size)
            dummy_c = np.zeros(org_size)
            for i,loop in enumerate(range(divisions**2)):
                dummy_s[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cV[0,loop]
                dummy_c[np.round(ndiv[1,i] + cy).astype(int),np.round(ndiv[0,i] + cx).astype(int)] = cV[1,loop]
            sfitV = (__fit_plane(dummy_s, mask = (dummy_s != 0)) , __fit_plane(dummy_c, mask = (dummy_c != 0)) )
            if verbose:
                plt.imshow(sfitV[0][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()
                plt.imshow(sfitV[1][0]*100,clim=[-0.5,0.5])
                plt.colorbar()
                plt.show()

            # correction
            corrected_data = np.copy(input_data)
            corrected_data[:, 1, :, :] = input_data[:, 1, :, :] - sfitQ[0][0] * input_data[:, 0, :, :] - sfitQ[1][0]
            corrected_data[:, 2, :, :] = input_data[:, 2, :, :] - sfitU[0][0] * input_data[:, 0, :, :] - sfitU[1][0]
            corrected_data[:, 3, :, :] = input_data[:, 3, :, :] - sfitV[0][0] * input_data[:, 0, :, :] - sfitV[1][0]

            return cQ, cU, cV, sfitQ, sfitU, sfitV, corrected_data

        else:
            printc('Input mode surface requires a valid cntr_rad input',color=bcolors.FAIL)
            ValueError("Provide header in crosstalk_ItoQUV call")

    else:
        printc('Input mode not available. Shall be `standard` or `surface` but ',mode,' was provided',color=bcolors.FAIL)
        ValueError("Check input mode in crosstalk_ItoQUV call")


def cross_talk_QUV(input_data, nran=2000, nlevel=0.3, verbose=False, block=True, mask = 0,limit = 0.05):
    """
    cross_talk_QUV _summary_

    _extended_summary_

    :param input_data: _description_
    :type input_data: _type_
    :param nran: _description_, defaults to 2000
    :type nran: int, optional
    :param nlevel: _description_, defaults to 0.3
    :type nlevel: float, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :param block: _description_, defaults to True
    :type block: bool, optional
    :param mask: _description_, defaults to 0
    :type mask: int, optional
    :param limit: _description_, defaults to 0.05
    :type limit: float, optional
    :return: _description_
    :rtype: _type_
    """
    if mask.any() != 0:
        print('mask has been provided')
    else:
        mask = np.ones_like(input_data[0, 0, :, :])

    # para el limite en I
    lx = (input_data[:, 0, :, :] * mask[np.newaxis,:,:]).flatten()
    lv = np.abs(input_data[:, 3, :, :] * mask[np.newaxis,:,:]).flatten()

    ids = ((mask.flatten() > 0) & (lx > (limit * np.mean(x))) & (lv > nlevel / 100.))

    x = (input_data[:, 3, :, :]* mask[np.newaxis,:,:]).flatten()
    x = x[ids]

    A = np.vstack([x, np.ones(len(x))]).T

    # V to Q
    yQ = (input_data[:, 1, :, :]* mask[np.newaxis,:,:]).flatten()
    yQ = yQ[ids]

    m, c = np.linalg.lstsq(A, yQ, rcond=None)[0]
    cVQ = [m, c]
    pQ = np.poly1d(cVQ)

    # V to U
    yU = (input_data[:, 2, :, :]* mask[np.newaxis,:,:]).flatten()
    yU = yU[ids]

    m, c = np.linalg.lstsq(A, yU, rcond=None)[0]
    cVU = [m, c]
    pU = np.poly1d(cVU)

    if verbose:

        PLT_RNG = 3

        N = x.size
        if N > nran:
            nran = N
        idx = sample(range(N), nran)
        mx = x[idx].mean()
        sx = x[idx].std()
        xp = np.linspace(x.min(), x.max(), 100)

        my = [yQ[idx].mean()]
        sy = [yQ[idx].std()]
        my.append(yU[idx].mean())
        sy.append(yU[idx].std())

        plt.figure(figsize=(8, 8))
        plt.scatter(x[idx], yQ[idx], color='red', alpha=0.6, s=10)  # ,c=dS[idx],cmap=cm.Paired)
        plt.plot(xp, pQ(xp), color='red', linestyle='dashed', linewidth=3.0)

        plt.scatter(x[idx], yU[idx], color='blue', alpha=0.6, s=10)
        plt.plot(xp, pU(xp), color='blue', linestyle='dashed', linewidth=3.0)

        plt.xlim([mx - PLT_RNG * sx, mx + PLT_RNG * sx])
        plt.ylim([min(my) - 1.8 * PLT_RNG * statistics.mean(sy), max(my) + PLT_RNG * statistics.mean(sy)])
        plt.xlabel('Stokes V')
        plt.ylabel('Stokes Q/U')

        plt.text(mx - 0.9 * PLT_RNG * sx, min(my) - 1.4 * PLT_RNG * statistics.mean(sy),
                 'Cross-talk from V to Q: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cVQ[0],
                                                                                                               cVQ[1],
                                                                                                               width=8,
                                                                                                               prec=4),
                 style='italic', bbox={'facecolor': 'red', 'alpha': 0.1, 'pad': 1}, fontsize=15)
        plt.text(mx - 0.9 * PLT_RNG * sx, min(my) - 1.55 * PLT_RNG * statistics.mean(sy),
                 'Cross-talk from V to U: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cVU[0],
                                                                                                               cVU[1],
                                                                                                               width=8,
                                                                                                               prec=4),
                 style='italic', bbox={'facecolor': 'blue', 'alpha': 0.1, 'pad': 1}, fontsize=15)
        plt.show(block=block)

    print('Cross-talk from V to Q: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cVQ[0], cVQ[1],
                                                                                                        width=8,
                                                                                                        prec=4))
    print('Cross-talk from V to U: slope = {: {width}.{prec}f} ; off-set = {: {width}.{prec}f} '.format(cVU[0], cVU[1],
                                                                                                        width=8,
                                                                                                        prec=4))

    return cVQ, cVU

def phi_correct_ghost(
        data, header, cpos, params, input_ghost_image = None, verbose: bool = False):
    """
    phi_correct_ghost Apply ghost correction

    _extended_summary_

    :param data: _description_
    :type data: _type_
    :param header: _description_
    :type header: _type_
    :param cpos: _description_
    :type cpos: _type_
    :param flat: _description_
    :type flat: _type_
    :param cpos_f: _description_
    :type cpos_f: _type_
    :param params: _description_
    :type params: _type_
    :param input_ghost_image: _description_, defaults to None
    :type input_ghost_image: _type_, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :return: _description_
    :rtype: _type_
    """

    # TODO Ghost is different for every wavelength. Now only continuum is taken.

    version = 'phi_correct_ghost V2.2 July 8, 2023'
    version = 'phi_correct_ghost V2.2 Sep 13, 2023'
    printc('-->>>>>>> Correcting ghost image ', color=bcolors.OKGREEN)

    # ------------------------------------------------------------------
    # Pre-processing
    # ------------------------------------------------------------------

    # Unpack ghost parameters
    ghost_disp = params[:4]
    ghost_sc = [0] + params[4:7]  # scaling parameters; insert dummy element for Stokes I (no ghost corrrection)
    sigma = params[7]  # sigma of Gaussian blurring kernel

    # Demodulate data
    data, header = phi_apply_demodulation(data, 'auto', header, verbose=False)

    # Solar disc parameters
    sun_cx = header['CRPIXN1']  # x = columns = horizontal
    sun_cy = header['CRPIXN2']  # y = rows = vertical
    sun_radius = header['RSUN_ARC'] / header['CDELT1']

    # Image center
    xd = int(header['NAXIS1'])
    yd = int(header['NAXIS2'])
    im_cx, im_cy = xd // 2, yd // 2

    # Continuum indices
    cont_index = cpos[0]
    printc(f'Continuum index: {cont_index}', color=bcolors.CYAN)
    # flat_cont_index = cpos_f[0]
    # printc(f'Flatfield continuum index: {flat_cont_index}', color=bcolors.CYAN)

    # Nominal ghost displacement and scaling factors
    x, y = sun_cx, sun_cy
    printc(f'Sun center: x = {x},  y = {y}', color=bcolors.OKBLUE)
    x += int(header['PXBEG1']) - 1
    y += int(header['PXBEG2']) - 1
    printc(f'Absolute image center (taking into account image cropping): x: {x:.1f},  y: {y:.1f}', color=bcolors.OKBLUE)
    dx_nom = ghost_disp[0] * x + ghost_disp[1]
    dy_nom = ghost_disp[2] * y + ghost_disp[3]
    printc(f'Nominal ghost displacement: dx = {dx_nom:.1f}, dy = {dy_nom:.1f}', color=bcolors.OKBLUE)
    printc(f'Ghost scaling factors start values (Q, U, V): {ghost_sc[1]:.2e}, {ghost_sc[2]:.2e}, {ghost_sc[3]:.2e}', color=bcolors.OKBLUE)

    radius_mask = sun_radius - 20  # go 20 px inside, to avoid edge effects

    # Mask for mean value, around sun center
    mask_mean, _ = generate_circular_mask([xd - 1, yd - 1], radius_mask, radius_mask)
    mask_mean = shift(mask_mean, (round(sun_cx - im_cx), round(sun_cy - im_cy))) > 0  # shift vector is (x, y), as opposed to (row=y, column=x)!

    # Compute mean intensity values for all wavelengths
    # Will be used for ghost scaling later
    mean = []
    for wave_index in range(data.shape[0]):
        mean.append(get_disk_mean(data[wave_index, 0, :, :], mask=mask_mean))
    printc(f'Mean intensity values: {mean}', color=bcolors.OKBLUE)

    # Normalize all Stokes parameters with mean continuum intensity
    data /= mean[cont_index]

    # Various masks
    radius_width = 100  # width of annulus mask around ghost

    mask_true, _ = generate_circular_mask([xd - 1, yd - 1], radius_mask, radius_mask)
    mask_true = shift(mask_true, (round(sun_cx - im_cx), round(sun_cy - im_cy))) > 0

    mask_sun, _ = generate_circular_mask([xd - 1, yd - 1], sun_radius, sun_radius)
    mask_sun = shift(mask_sun, (round(sun_cx - im_cx), round(sun_cy - im_cy))) > 0

    mask_ghost, _ = generate_circular_mask([xd - 1, yd - 1], sun_radius + radius_width // 2, radius_width)
    mask_ghost = shift(mask_ghost, (round(sun_cx + dx_nom - im_cx), round(sun_cy + dy_nom - im_cy))) > 0

    # Annulus around ghost image, intersected with true solar image
    mask_disp = mask_true & mask_ghost

    # Generate ghost image
    def rotate(im, angle, x, y, external = False):
        """
        Rotate image around solar disc center
        """
        x = round(x)
        y = round(y)
        dx, dy = (x - im_cx, y - im_cy)
        imo = np.copy(im) if external else shift(im, (-dx, -dy))
        im = sp_rotate(imo, angle, reshape=False)
        im = shift(im, (dx, dy))  # move solar disc back to its original position
        return im,imo

    # Supposes the user provides a ghost image which will replace "si".
    # In this case, the image shall be CENTERED in the image, FLTAFIELDED, and ROTATED
    # if ghost_image is not None:
    #     printc('Reading ghost image: ', ghost_image, color=bcolors.OKBLUE)
    #     with pyfits.open(ghost_image) as hdu_list:
    #         si = hdu_list[0].data
    # else:
    #     ff = np.mean(flat[flat_cont_index, :, :, :], axis=0)  # generate modulation-state averaged continuum FF
    #     si = data[cont_index, 0, :, :] / ff  # apply FF
    #     sio = si.copy()  # save original (unrotated) intensity image for active region masking
    #     si = rotate(si, 180, sun_cx, sun_cy)
    #     si[np.invert(mask_sun)] = 0  # set all pixels outside the disc to 0

    ghost_image = data[cont_index, 0, :, :]
    ghost_image_unrotated = ghost_image.copy()  # save original (unrotated) intensity image for active region masking
    ghost_image,ghost_image_at_center_unrotated  = rotate(ghost_image, 180, sun_cx, sun_cy)
    #critical
    if type(input_ghost_image) is np.ndarray:  #subtitute the ghost by the input one
        # the input one is at center and unrotated: ghost_image_at_center_unrotated
        ghost_image,_  = rotate(input_ghost_image, 180, sun_cx, sun_cy, external = True)

    ghost_image[np.invert(mask_sun)] = 0  # set all pixels outside the disc to 0
    # Actually, the ghost image should be the corrected one!!!!

    # -------------------------------------------------------------------------
    # Generate mask for ghost correction
    # Exclude active regions
    # -------------------------------------------------------------------------

    # Mask active regions, based on intensity threshold
    mask_ar = ghost_image_unrotated > np.mean(ghost_image_unrotated[mask_disp]) * 0.80  # use 80% of mean value as threshold
    mask_ar = mask_ar & mask_disp

    # DEBUG: export data for interactive analysis
    # import pickle
    # file = '/home/feller/PHI_FDT/issue_11/dump.dat'
    # printc(f'Write to {file} and exit', color=bcolors.YELLOW)
    # dump_data = data, ghost_image, ghost_image_unrotated
    # dump_masks = mask_true, mask_ghost, mask_disp, mask_ar
    # dump_indices = cont_index
    # dump_coord = sun_cx, sun_cy, dx_nom, dy_nom
    # dump_ghost = ghost_sc, sigma
    # dump_other = sun_radius, radius_mask, radius_width, mean
    # dump = dump_data, dump_masks, dump_indices, dump_coord, dump_ghost, dump_other
    # with open(file, 'wb') as f:
    #     pickle.dump(dump, f)
    # exit()

    # Identify clusters of active regions and expand those clusters into contiguous regions
    x = np.arange(ghost_image_unrotated.shape[0])
    y = np.arange(ghost_image_unrotated.shape[1])
    X, Y = np.meshgrid(x, y)

    r = np.sqrt((X - im_cx)**2 + (Y - im_cy)**2)  # Polar coordinates r, phi
    phi = np.arctan2(X - im_cx, Y - im_cy)
    mask = ~mask_ar & mask_disp  # invert mask to select active regions
    r_masked = r[mask]
    phi_masked = phi[mask]

    # Identify clusters
    def clusters_split(a, gap):
        sorted_indices = np.argsort(a)
        a_sorted = a[sorted_indices]
        da = a_sorted[1:] - a_sorted[0: -1]

        index_cluster = []
        index_clusters = []
        cluster_count = 0
        up = False
        for x, s in zip(da, sorted_indices):
            if x < gap:
                index_cluster.append(s)
                up = True
            else:
                if up:
                    printc(f'    Cluster no. {cluster_count} complete')
                    index_clusters.append(index_cluster)
                    index_cluster = []
                    up = False
                    cluster_count += 1
        if up:
            printc(f'    Cluster no. {cluster_count} complete')
            index_clusters.append(index_cluster)  # append last cluster

        return index_clusters

    printc('Identifying clusters of active regions', color=bcolors.OKBLUE)
    clusters_r = clusters_split(r_masked, 10) # find radial clusters which are separated by at least 10 pixels

    # Next, go through each radial cluster and split it into phi clusters, which are separated by at least 0.1 rad
    clusters = []
    cluster_count = 0
    printc('---')
    for cluster_r in clusters_r:
        printc(f'    Radial cluster no {cluster_count}')
        clusters_phi = clusters_split(phi_masked[cluster_r], 0.1)
        for cluster_phi in clusters_phi:
            clusters.append(np.array(cluster_r)[cluster_phi])
        cluster_count += 1
        printc('---')

    # Expand cluster envelopes by 15 pixels in radial and azimuthal direction
    if clusters != []:
        printc('Expanding cluster envelopes', color=bcolors.OKBLUE)
        pad = 15
        for cluster in clusters:
            cluster_mask = np.full(phi_masked.size, False)
            cluster_mask[cluster] = True
            r_min, r_max, r_mean = r_masked[cluster_mask].min(), r_masked[cluster_mask].max(), r_masked[cluster_mask].mean()
            phi_min, phi_max = phi_masked[cluster_mask].min(), phi_masked[cluster_mask].max()
            # print(f'{r_min:.0f} px < r < {r_max:.0f} px')
            # print(f'{np.rad2deg(phi_min):.1f}° < phi < {np.rad2deg(phi_max):.1f}°')
            pad_r = pad
            pad_phi = pad / r_mean
            mask_disp[(r > r_min - pad_r) & (r < r_max + pad_r) & (phi > phi_min - pad_phi) & (phi < phi_max + pad_phi)] = False

    # DEBUG: export data for interactive analysis
    # import pickle
    # file = '/home/feller/PHI_FDT/issue_11/dump.dat'
    # printc(f'Write to {file} and exit', color=bcolors.YELLOW)
    # dump = mask_disp
    # with open(file, 'wb') as f:
    #     pickle.dump(dump, f)
    # exit()

    # -------------------------------------------------------------------------
    # Apply ghost correction
    # -------------------------------------------------------------------------

    # Ghost correction function
    def correct(sc, dx, dy, im, si, sigma, wave_index=cont_index):
        return im + sc * (mean[wave_index] / mean[cont_index]) * shift_subp(gaussian_filter(si, sigma), shift=(dy, dx), wrap=False)

    # Merit function for ghost displacement optimization
    # Note that we're using continuum Stokes U and V only
    def fun(x, sc, data, si, sigma, mask, verbose=False):
        dx, dy = x  # current ghost displacement (free parameters)
        merit = 0
        for im, s in zip(data[2:4, :, :], sc[2:4]):
            im_corr = correct(s, dx, dy, im, ghost_image, sigma)
            im_corr = gradient(gaussian_filter(im_corr, 5))
            merit += np.std(im_corr[mask])
        merit *= 1e8
        if verbose:
            print(f'{dx:.1f}, {dy:.1f}, {merit:.4e}')
        return merit

    printc('Optimizing ghost displacement ...')
    res = minimize(
        fun, (dx_nom, dy_nom),
        args=(ghost_sc, data[cont_index, :], ghost_image, sigma, mask_disp),
        method='Nelder-Mead', bounds=((dx_nom - 50, dx_nom + 50), (dy_nom - 50, dy_nom + 50)))
    if res.success:
        dx, dy = res.x
    else:
        printc('    WARNING: optimization failed, using nominal values!', color=bcolors.WARNING)
        dx, dy = dx_nom, dy_nom
    printc(f'    New ghost Displacement parameters: dx = {dx:.1f}, dy = {dy:.1f}', color=bcolors.OKBLUE)

    # Merit function for ghost scaling factor optimization
    def fun(sc, dx, dy, im, si, sigma, mask, verbose=False):
        im_corr = correct(sc, dx, dy, im, si, sigma)
        im_corr = gradient(gaussian_filter(im_corr, 5))
        merit = np.std(im_corr[mask]) * 1e8
        if verbose:
            print(f'{float(sc):.3e}, {merit:.4e}')
        return merit

    # DEBUG
    # verbose = True

    plot_index = cont_index  # which wavelength index to use for the verbose plots (default = cont_index)
    stokes = ['I', 'Q', 'U', 'V']
    ghost_sc_new = np.copy(ghost_sc)
    for i in range(1, 4):
        printc(f'Optimizing ghost scaling factor for {stokes[i]} ...')
        sc_nom = ghost_sc[i]
        res = minimize_scalar(
            fun,
            args=(dx, dy, data[cont_index, i, :], ghost_image, sigma, mask_disp),
            bounds=(-3 * abs(sc_nom), 3 * abs(sc_nom)), method='bounded')
        if res.success:
            sc = res.x
        else:
            printc('    WARNING: Optimization failed, using nominal value!', color=bcolors.WARNING)
            sc = sc_nom

        if verbose:
            # Save original continuum image for later plotting
            im_original = data[plot_index, i, :, :].copy()

        if np.abs(sc) > 1e-5:  # If the scaling factor is too small, it is not worth correcting
            printc(f'Correcting {stokes[i]} with scaling factor: {sc:.3e}')
            for j in range(data.shape[0]):
                manual_corr_fac = 1.0
                # DEBUG
                # if j == 2:
                #     manual_corr_fac = 1.2
                data[j, i, :, :] = correct(sc * manual_corr_fac, dx, dy, data[j, i, :, :], ghost_image, sigma, wave_index=j)
        else:
            printc(f'Scaling factor too small ({sc:.3e}), not correcting {stokes[i]}!', color=bcolors.OKGREEN)

        ghost_sc_new[i] = sc

        if verbose:
            did = re.search('.*_([0-9]*)\.fits', header['filename']).group(1)
            plot_file = f'phi_correct_ghost_{did}_{plot_index}_{stokes[i]}.pdf'
            printc(f'VERBOSE: Plotting ghost correction for {stokes[i]} continuum to {plot_file}', color=bcolors.VERBOSE)

            def plot_circle(ax, x, y, radius, color='red'):
                circle = plt.Circle((x, y), radius, fill=False, color=color, ls='--', lw=1.0)
                ax.add_artist(circle)

            def plot_mask(ax, color='blue'):
                ax.contour(mask_disp, levels=0, colors=color, linewidths=1.0, linestyles='--')

            fig, axs = plt.subplots(nrows=2, ncols=3, figsize=(15, 11))
            axs = axs.flatten()

            ax = axs[0]
            im = data[plot_index, 0, :]
            ax.imshow(im, cmap='gray', vmin=0, vmax=1.2)
            plot_circle(ax, sun_cx, sun_cy, sun_radius)
            plot_circle(ax, sun_cx, sun_cy, radius_mask)
            ax.set_title(f'data[{plot_index}, 0, :]')

            ax = axs[1]
            im = ghost_image_unrotated
            ax.imshow(im, cmap='gray', vmin=0, vmax=1.2)
            plot_circle(ax, sun_cx, sun_cy, radius_mask)
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius + radius_width // 2, color='yellow')
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius - radius_width // 2, color='yellow')
            plot_mask(ax)
            ax.set_title('ghost_image_unrotated & mask_disp')

            ax = axs[2]
            im = ghost_image
            ax.imshow(im, cmap='gray', vmin=0, vmax=1.2)
            plot_circle(ax, sun_cx, sun_cy, sun_radius)
            ax.set_title(f'ghost_image (ghost proxy)')

            ax = axs[3]
            im = im_original
            ax.imshow(im, cmap='gray', vmin=-0.005, vmax=0.005)
            ax.set_title(f'Original Stokes {stokes[i]} image')
            plot_circle(ax, sun_cx, sun_cy, radius_mask)
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius, color='green')
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius + radius_width // 2, color='yellow')
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius - radius_width // 2, color='yellow')

            ax = axs[4]
            im = data[plot_index, i, :]
            ax.imshow(im, cmap='gray', vmin=-0.005, vmax=0.005)
            ax.set_title(f'Ghost corrected Stokes {stokes[i]} image')
            plot_circle(ax, sun_cx, sun_cy, radius_mask)
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius + radius_width // 2, color='yellow')
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius - radius_width // 2, color='yellow')
            plot_mask(ax)

            ax = axs[5]
            im = gradient(gaussian_filter(data[plot_index, i, :], 5))
            im[~mask_disp] = 0
            ax.imshow(im, cmap='gray', vmin=0, vmax=3e-9)
            ax.set_title('Merit image')
            plot_circle(ax, sun_cx, sun_cy, radius_mask)
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius + radius_width // 2, color='yellow')
            plot_circle(ax, sun_cx + dx_nom, sun_cy + dy_nom, sun_radius - radius_width // 2, color='yellow')
            plot_mask(ax, color='cyan')

            fig.tight_layout()
            fig.savefig(plot_file)

        printc(f'New ghost scaling factors values (Q, U, V): {ghost_sc_new[1]:.2e}, {ghost_sc_new[2]:.2e}, {ghost_sc_new[3]:.2e}', color=bcolors.OKBLUE)

    # DEBUG
    # verbose = False
    # exit()

    # -------------------------------------------------------------------------
    # Post-processing
    # -------------------------------------------------------------------------

    if verbose:
        plib.show_four_row(data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :], title=['I', 'Q', 'U', 'V'], zoom=2)#, svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004])

    # Undo normalization and demodulation
    data *= mean[cont_index]
    data = phi_apply_demodulation(data, 'auto', header, verbose=False, modulate=True)

    header = update_header(header, 'CAL_GHST', version, 'CAL_DARK', comment='ghost correction version (phifdt_pipe_modules.py)')
    header = update_header(header, 'GHST_PAR', str([dx,dy,ghost_sc_new[1],ghost_sc_new[2],ghost_sc_new[3]]), 'CAL_GHST', comment='ghost correction parameters')

    return data, header, ghost_image_at_center_unrotated


def phi_correct_fringes(data:np.ndarray, header, option: str, fringe_threshold:float = 1.0,
        verbose:bool = False,NFREQ_LEVEL:float = 0.0,ATT_FACTOR: float = 1e-6, continuum_point:int = 0,
        sigma_filter_power:int = 15, max_frequencies:int = 16, rad_min: int = 25, rad_max: int = 40,
        wsize: int = 50, which_stokes=[1,2,3], cropping: bool = False):
    """
    phi_correct_fringes _summary_

    Startup version on Jun 2021
    24 Feb 2022: change int in freq by round since we we were floor rounding the pixel positions
    16 May 2023: updated auto version

    :param data: _description_
    :type data: np.ndarray
    :param header: _description_
    :type header: _type_
    :param option: _description_
    :type option: str
    :param fringe_threshold: _description_, defaults to 1.0
    :type fringe_threshold: float, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :param NFREQ_LEVEL: _description_, defaults to 0.0
    :type NFREQ_LEVEL: float, optional
    :param ATT_FACTOR: _description_, defaults to 1e-6
    :type ATT_FACTOR: float, optional
    :param continuum_point: _description_, defaults to 0
    :type continuum_point: int, optional
    :param sigma_filter_power: _description_, defaults to 15
    :type sigma_filter_power: int, optional
    :param max_frequencies: _description_, defaults to 16
    :type max_frequencies: int, optional
    :param rad_min: _description_, defaults to 25
    :type rad_min: int, optional
    :param rad_max: _description_, defaults to 40
    :type rad_max: int, optional
    :param wsize: _description_, defaults to 50
    :type wsize: int, optional
    :param which_stokes: _description_, defaults to [1,2,3]
    :type which_stokes: list, optional
    :param cropping: _description_, defaults to False
    :type cropping: bool, optional
    """
    version = 'phi_correct_fringes V1.0 Jun 2021'
    version = 'phi_correct_fringes V1.1 Jun 2022'
    version = 'phi_correct_fringes V1.3 Nov 2022'
    version = 'phi_correct_fringes V1.4 May 2023'

    STOKESPAR = ['I', 'Q', 'U', 'V']

    def _some_plots_fringes(data, loop_inwave, loop_stokes, arg3):
        plt.imshow(data[loop_inwave, loop_stokes, :, :],clim=(-0.05,0.05))
        plt.title(f'{arg3}{str(loop_inwave)} {str(loop_stokes)}')
        plt.show()

    wd,pd,yd,xd = data.shape
    zd = wd * pd

#UPDATE FROM HERE ====================================================================================================

    if option == 'interpol':
        print('Correct fringes')
        printc('-->>>>>>> Interpolate for defined frequencies --', color=bcolors.OKGREEN)
        #Characteristics of dataset
        wd,pd,yd,xd = data.shape
        center = (header['CRPIX1'], header['CRPIX2'])
        if 'CPLTSCL' in header:
            radius_pix = header['RSUN_ARC']/header['CPLTSCL']
        elif 'CDELT1' in header:
            radius_pix = header['RSUN_ARC']/header['CDELT1']
        print(center, radius_pix)

        #Check whether data is quadratic
        if xd != yd:
            printc('ERROR, Data is not quadratic!! ', color=bcolors.FAIL)
            raise Exception("Data is not quadratic")

        def _interpol2D(bool_arr, ps , method, fill_value):
            #Interpolate selected (False in bool_arr) pixels of 2D array (ps)
            print('Interpolate with ', method, ' method. ')
            yd, xd = ps.shape
            xx, yy = np.meshgrid(np.arange(xd), np.arange(yd))
            known_x = xx[bool_arr]
            known_y = yy[bool_arr]
            known_v = ps[bool_arr]
            missing_x = xx[~bool_arr]
            missing_y = yy[~bool_arr]

            interp_values = sp.interpolate.griddata((known_x, known_y), known_v, (missing_x, missing_y), method = method, fill_value = fill_value)
            interp_image = ps.copy()
            interp_image[missing_y, missing_x] = interp_values
            # print(interp_values)

            return interp_image, missing_x, missing_y

        def _create_circular_mask(h, w, center=None, radius=None):
            if center is None: # use the middle of the image
                center = (int(w/2), int(h/2))
            if radius is None: # use the smallest distance between the center and image walls
                radius = min(center[0], center[1], w-center[0], h-center[1])

            Y, X = np.ogrid[:h, :w]
            dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

            mask = dist_from_center <= radius
            return mask


        xx, yy = np.meshgrid(np.arange(xd), np.arange(yd))
        mask_orig = _create_circular_mask(yd, xd, center = center, radius = radius_pix)
        masked_orig = data.copy()
        mask_x = xx[~mask_orig]
        mask_y = yy[~mask_orig]
        ondisk_x = xx[mask_orig]
        ondisk_y = yy[mask_orig]

        # Define frequency
        if xd == 768:
            #StokesQ
            px_x_Q = np.array([8, 8, -1])
            px_y_Q = np.array([0, 4, 4])
            px_x_Q1 = np.array([13, 13, 2])
            px_y_Q1 = np.array([4, 9, 7])
            #StokesU
            px_x_U = np.array([8, 8])
            px_y_U = np.array([0, 4])
            px_x_U1 = np.array([13, 13])
            px_y_U1 = np.array([4, 9])
            #StokesV
            px_x_V = np.array([9, 9, 5, 5])
            px_y_V = np.array([0, 4, 6, 9])
            px_x_V1 = np.array([13, 13, 10, 8])
            px_y_V1 = np.array([4, 8, 9, 11])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 896:
            #StokesQ
            px_x_Q = np.array([9, 9, -1])
            px_y_Q = np.array([0, 4, 4])
            px_x_Q1 = np.array([15, 15, 2])
            px_y_Q1 = np.array([4, 10, 8])
            #StokesU
            px_x_U = np.array([9, 9])
            px_y_U = np.array([0, 4])
            px_x_U1 = np.array([15, 15])
            px_y_U1 = np.array([4, 10])
            #StokesV
            px_x_V = np.array([10, 10, 5, 5])
            px_y_V = np.array([0, 4, 7, 10])
            px_x_V1 = np.array([15, 15, 11, 9])
            px_y_V1 = np.array([4, 9, 10, 12])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 1024:
            #StokesQ
            px_x_Q = np.array([10, 10, -1])
            px_y_Q = np.array([0, 5, 5])
            px_x_Q1 = np.array([17, 17, 2])
            px_y_Q1 = np.array([4, 11, 9])
            #StokesU
            px_x_U = np.array([10, 10])
            px_y_U = np.array([0, 5])
            px_x_U1 = np.array([17, 17])
            px_y_U1 = np.array([4, 10])
            #StokesV
            px_x_V = np.array([11, 11, 5, 4])
            px_y_V = np.array([0, 5, 8, 11])
            px_x_V1 = np.array([17, 17, 11, 11])
            px_y_V1 = np.array([5, 12, 11, 14])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 1152:
            #StokesQ
            px_x_Q = np.array([11, 11, -1])
            px_y_Q = np.array([0, 5, 5])
            px_x_Q1 = np.array([19, 19, 2])
            px_y_Q1 = np.array([5, 12, 10])
            #StokesU
            px_x_U = np.array([14, 14])
            px_y_U = np.array([0, 7])
            px_x_U1 = np.array([17, 17])
            px_y_U1 = np.array([4, 10])
            #StokesV
            px_x_V = np.array([14, 14, 10, 8])
            px_y_V = np.array([1, 7, 9, 10])
            px_x_V1 = np.array([17, 17, 14, 12])
            px_y_V1 = np.array([4, 10, 12, 14])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 1280:
            #StokesQ
            px_x_Q = np.array([12, 12, -2])
            px_y_Q = np.array([0, 6, 6])
            px_x_Q1 = np.array([21, 21, 2])
            px_y_Q1 = np.array([5, 13, 11])
            #StokesU
            px_x_U = np.array([12, 12])
            px_y_U = np.array([0, 6])
            px_x_U1 = np.array([21, 21])
            px_y_U1 = np.array([5, 12])
            #StokesV
            px_x_V = np.array([13, 13, 6, 5])
            px_y_V = np.array([0, 6, 9, 13])
            px_x_V1 = np.array([21, 21, 13, 14])
            px_y_V1 = np.array([6, 14, 13, 17])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 1408:
            #StokesQ
            px_x_Q = np.array([13, 13, -3])
            px_y_Q = np.array([0, 7, 6])
            px_x_Q1 = np.array([23, 23, 2])
            px_y_Q1 = np.array([5, 14, 12])
            #StokesU
            px_x_U = np.array([18, 18])
            px_y_U = np.array([0, 8])
            px_x_U1 = np.array([20, 20])
            px_y_U1 = np.array([5, 12])
            #StokesV
            px_x_V = np.array([14, 14, 6, 5])
            px_y_V = np.array([0, 6, 9, 14])
            px_x_V1 = np.array([23, 23, 17, 15])
            px_y_V1 = np.array([7, 15, 14, 18])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 1536:
            #StokesQ
            px_x_Q = np.array([14, 14, -3])
            px_y_Q = np.array([0, 7, 6])
            px_x_Q1 = np.array([25, 25, 2])
            px_y_Q1 = np.array([7, 15, 13])
            #StokesU
            px_x_U = np.array([19, 19])
            px_y_U = np.array([0, 9])
            px_x_U1 = np.array([22, 23])
            px_y_U1 = np.array([5, 15])
            #StokesV
            px_x_V = np.array([15, 15, 6, 5])
            px_y_V = np.array([0, 6, 9, 14])
            px_x_V1 = np.array([25, 25, 19, 16])
            px_y_V1 = np.array([7, 16, 16, 19])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        if xd == 2048:
            #StokesQ
            px_x_Q = np.array([18, 18, -4])
            px_y_Q = np.array([0, 11, 8])
            px_x_Q1 = np.array([33, 33, 3])
            px_y_Q1 = np.array([9, 21, 17])
            #StokesU
            px_x_U = np.array([25, 24])
            px_y_U = np.array([0, 12])
            px_x_U1 = np.array([30, 31])
            px_y_U1 = np.array([7, 19])
            #StokesV
            px_x_V = np.array([20, 20, 8, 6])
            px_y_V = np.array([0, 8, 12, 18])
            px_x_V1 = np.array([33, 33, 25, 21])
            px_y_V1 = np.array([9, 21, 22, 25])
            print('------ Size of frequencies ------')
            print(px_x_Q1 - px_x_Q, px_y_Q1 - px_y_Q)
            print(px_x_U1 - px_x_U, px_y_U1 - px_y_U)
            print(px_x_V1 - px_x_V, px_y_V1 - px_y_V)
            print('------ Size of frequencies ------')
            limit = 0.003

        for pol in np.arange(1, pd):#Loop over polarisation states (no Stokes I)
            for wave in range(0, wd):
                if (pol == 1) or (pol == 3) or (pol ==  2 and wave == continuum_point):
                    print('Wavelength position: ', str(wave), ', polarisation state: ', str(pol), 'Contiuum at ', str(continuum_point))
                    masked_orig[wave, pol, mask_y, mask_x] = np.mean(masked_orig[wave, pol, ondisk_y, ondisk_x])
                    data_in = masked_orig[wave, pol, :, :]
                    if ((pol == 3) and (wave != continuum_point)):
                        print('Special case for wavelength position: ', str(wave), 'and polarisation state: ', str(pol))
                        print(np.min(masked_orig[wave, pol, :, :]), np.max(masked_orig[wave, pol, :, :]))
                        data_red = masked_orig[wave, pol, :, :] - np.mean(masked_orig[wave, pol, ondisk_y, ondisk_x])
                        data_in[np.abs(data_in) > limit] = np.mean(data_red[ondisk_y, ondisk_x])#0.002

                    #Show full frame map
                    # plt.imshow(data_in, clim = (-0.001, 0.001), cmap='gray')
                    # plt.show()
                    #Calculate Fourier transform
                    FT = np.fft.fftshift(np.fft.fft2(data_in))
                    ps = np.log(np.abs(np.real(FT * FT.conj())))
                    # print(np.shape(ps))


                # ----> START: Show plots for checking -------------------------------------------------------------
                    # plt.imshow(ps[yd//2:yd//2+60, xd//2-10:xd//2+40], cmap='gray',clim=(np.min(ps)+np.abs(0.8*np.min(ps)),np.max(ps)))
                    # plt.show()
                # ----> END: Show plots for checking end -------------------------------------------------------------
                    # Set lower left corner and upper right for frequncy box
                    if pol == 1:
                        yl0 = yd//2 + px_y_Q
                        xl0 = xd//2 + px_x_Q
                        yl1 = yd//2 + px_y_Q1
                        xl1 = xd//2 + px_x_Q1
                    if pol == 2:
                        yl0 = yd//2 + px_y_U
                        xl0 = xd//2 + px_x_U
                        yl1 = yd//2 + px_y_U1
                        xl1 = xd//2 + px_x_U1
                    if pol == 3: # and wave != continuum_point:
                        yl0 = yd//2 + px_y_V
                        xl0 = xd//2 + px_x_V
                        yl1 = yd//2 + px_y_V1
                        xl1 = xd//2 + px_x_V1

                    print('Lower left corner of selected region:')
                    print('y-frequencies:', yl0)
                    print('x-frequencies:', xl0)
                    #Set pixels which should be affected to False in bool_array
                    bool_arr = np.ones((yd,xd), dtype=bool) #Here everything is true
                    for f in range (0, len(yl0)):
                        print('Start loop over frequencies')
                        bool_arr[yl0[f]:yl1[f] , xl0[f]:xl1[f]] = False
                        bool_arr[(yd + 1) - yl1[f]:(yd + 1) - yl0[f], (xd + 1) - xl1[f] :(xd + 1) - xl0[f]] = False
                        #Print coordinates
                        print('Bottom', yl0[f], yl1[f], yl1[f] - yl0[f], xl0[f], xl1[f], xl1[f] - xl0[f])
                        print('Top', (yd + 1) - yl1[f], (yd + 1) - yl0[f], (xd + 1) - xl1[f], (xd + 1) - xl0[f])
                        #Mask pixels in PS
                        print('Masked PS')
                        ps[yl0[f]:yl1[f] , xl0[f]:xl1[f]] = np.min(ps)
                        ps[(yd + 1) - yl1[f]:(yd + 1) - yl0[f], (xd + 1) - xl1[f] :(xd + 1) - xl0[f]] = np.min(ps)
                        # plt.imshow(ps[yd//2:yd//2 + 60, xd//2-10:xd//2 + 40],clim=(np.min(ps)+np.abs(0.8*np.min(ps)),np.max(ps)-0.4*np.max(ps)),cmap='gray')


                    #Interpolate full FS data with complex to determine values for defined pixels
                    interp_image, missing_x, missing_y = _interpol2D(bool_arr,FT, method = 'nearest', fill_value = 0)

                    if ((pol == 3) and (wave != continuum_point)):
                        FTorig = np.fft.fftshift(np.fft.fft2(data[wave, pol, :, :]))
                        FT[missing_y, missing_x] = FTorig[missing_y, missing_x] - FT[missing_y, missing_x]
                        interp_image = FT


                    print('Apply to original data with wavelength position: ', str(wave), 'and polarisation state: ', str(pol))
                    #Generate FFT of original full disk data (with surrounding of disk) and correct selected pixels in there
                    FTorig = np.fft.fftshift(np.fft.fft2(data[wave, pol, :, :]))
                    FTorig[missing_y, missing_x] = interp_image[missing_y, missing_x]
                    # plt.imshow(psorig[yd//2:yd//2+60, xd//2-10:xd//2+40],clim=(np.min(ps)+np.abs(0.8*np.min(ps)),np.max(ps)-0.4*np.max(ps)),cmap='gray')
                    # plt.show()

                    # Retransform FFT data
                    corr_arr = np.fft.ifft2(np.fft.ifftshift(FTorig)).real
                    # plt.imshow(corr_arr, clim = (-0.001, 0.001), cmap='gray')
                    # plt.show()
                elif (pol ==  2 and wave != continuum_point):
                    print('Wavelength position: ', str(wave), ', polarisation state: ', str(pol), ' is not corrected. Contiuum at ', str(continuum_point))
                    corr_arr = data[wave, pol, :, :]

                #Save corrected data in original array
                data[wave, pol, :, :] = corr_arr

        return data, header, 0, 0, 0

#UNTIL HERE ==========================================================================================================

    if option == 'auto':
        printc('-->>>>>>> Automatic procedure for removing fringes --', color=bcolors.OKGREEN)

        if cropping:
            crop_size = round(float(header['RSUN_ARC']) / 3.6)
            data_buffer = np.copy(data)
            data = data[:,:,yd//2 - crop_size:yd//2 + crop_size,xd//2 - crop_size:xd//2 + crop_size]
            wd,pd,yd,xd = data.shape

        #--------------------------
        # FRINGE DETECTION PART
        #--------------------------

        # 1) Define parameters to store the frequencies, pixels and amplitudes of the fringes
        #    Maximum allowed points will be 16
        freq_x = np.zeros((wd, 3, max_frequencies),dtype = float)
        freq_y = np.zeros((wd, 3, max_frequencies),dtype = float)
        thresholds = np.zeros((wd, 3),dtype = float)
        rmss = np.zeros((wd, 3),dtype = float)
        means = np.zeros((wd, 3),dtype = float)

        # 2) Set the radius of the ring mask within which we will look for fringes
        #    The radius depends on the size of the image so it is scaled down in such a case
        #    The default values are rad_min: int = 8, rad_max: int = 30, for 2048 image size. They can be configured on call
        rad_min = int(rad_min * xd // 2048)
        rad_max = int(rad_max * xd // 2048)

        # 3) define the size of the window in which we will look for fringes.
        #    The window should be larger than rad_max. If not, an error will occur.
        #    Default value for the window is wsize: int = 50 pixels, configurable on function call.
        wsize = int(wsize * xd // 2048)
        if wsize < rad_max:
            raise ValueError("window size in Fourier domain wsize = {!r} smaller than rad_max = {!r}. Stopping...".format(wsize, rad_max))

        print("fringe_threshold,rad_min,rad_max,wsize,wbin,win_halfw",fringe_threshold,rad_min,rad_max,wsize)

        yd_im, xd_im = wsize*2, wsize*2
        annular_mask, _ = generate_circular_mask([yd_im-1,xd_im-1], rad_max, rad_max - rad_min + 1)

        from scipy import ndimage

        for loop_wave  in range(wd):
            for loop_stokes in which_stokes:

                threshold = np.copy(fringe_threshold)

                # 4a) Calculate the fourier transform of the data
                Fourier_Transform = np.fft.fftshift(np.fft.fft2(data[loop_wave, loop_stokes, :, :]))

                # 4b) Determine the power spectrum  of the first (continuum) wavelength
                power2d = np.log10( (Fourier_Transform * Fourier_Transform.conj()).real )

                # 4c) Remove a gaussian
                # Alternativelly, we could apply a gaussian filter to smooth the power but it does not help?
                # power2d = gaussian_filter(power2d, sigma=(sigma_filter_power,sigma_filter_power))

                # 4d) Create power image as a square around a central area of wsize radious
                im = power2d[yd // 2 - wsize:yd // 2 + wsize, xd // 2 - wsize:xd // 2 + wsize]
                im = np.exp(gaussian_filter(im,sigma=1.4)-gaussian_filter(im,sigma=5))
                # im = gaussian_filter(im, sigma=(1,1))
                # im = gaussian_filter(im, sigma=(1,1)) - gaussian_filter(im, sigma=(sigma_filter_power,sigma_filter_power))

                filtered_im = gaussian_filter(im, sigma=(sigma_filter_power,sigma_filter_power))
                im = im - filtered_im

                # background = np.mean(filtered_im[annular_mask == 1])

                mean = np.mean(im[annular_mask == True])
                rms = np.std(im[annular_mask == True])

                fringe_level = mean + rms * threshold
                # fringe_level = threshold * background

                print("fringe_threshold,mean,rms,fringe_level",fringe_threshold,mean,rms,fringe_level)

                # 4g) Loop to modify the threshold

                while True:

                    # 4h) determine points avobe fringe_threshold * background and correct them

                    idx = np.where(im * annular_mask > fringe_level)
                    if len(idx[0]) > max_frequencies:
                        print('threshold too low....',threshold,' increasing it by 2%:',threshold*1.02,fringe_level)
                        threshold *= 1.02e0
                        # fringe_level = mean + rms * fringe_threshold
                    elif len(idx[0]) == 0:
                        print('threshold too high....',threshold,'lowering it by 2%:  ',threshold*1.98,fringe_level)
                        threshold *= 0.98e0
                        # fringe_level = mean + rms * fringe_threshold
                    else:
                        break
                    fringe_level = mean + rms * threshold
                    # fringe_level = threshold * background

                    thresholds[loop_wave,loop_stokes-1] = threshold
                    rmss[loop_wave,loop_stokes-1] = rms
                    means[loop_wave,loop_stokes-1] = mean

                if verbose:
                    plt.imshow(im*annular_mask,cmap='Greys_r',clim=(mean-2*rms,mean+2*rms),interpolation=None)
                    plt.colorbar()
                    for idx_i in range(len(idx[0])):
                        plt.plot(idx[1][idx_i], idx[0][idx_i], "or", markersize=3)
                    plt.show()

                # save frequencies

                if len(idx[0]) > 0:
                    # loop in points (1st option)
                    for loop, idx_i in enumerate(range(len(idx[0]))):
                        freq_x[loop_wave,loop_stokes - 1, loop] = (idx[1][idx_i]- wsize ) / xd
                        freq_y[loop_wave,loop_stokes - 1, loop] = (idx[0][idx_i]- wsize ) / yd

                        print(
                            "%s: %2d: %s: %.12f %.12f: %s: %2d" % ('Peak detected in Stokes', loop_stokes, ' freq(x,y)', freq_x[loop_wave, loop_stokes - 1, loop], freq_y[loop_wave, loop_stokes - 1, loop], 'loop', loop))

            #--------------------------
            # FRINGE CORRECTION PART
            #--------------------------

        #retake data dimmensions
        if cropping:
            data = data_buffer
            del data_buffer

        wd,pd,yd,xd = data.shape

        window1d = np.abs(np.blackman(5))
        # window1d = np.array([0,0,1,0,0])
        window2d = 1 - np.sqrt(np.outer(window1d,window1d))
        mask_ones = np.ones((wd,pd - 1,yd, xd),dtype=float)

        for loop_wave  in range(wd):
            print('loop_wave',loop_wave)
            for loop_stokes in which_stokes:
                print('loop_stokes',loop_stokes)

                if np.abs(freq_x[loop_wave,loop_stokes - 1,0]) > 0: # meaning there is something to correct

                    #loop in points (1st option)
                    for index in range(max_frequencies):

                        if freq_x[loop_wave,loop_stokes - 1, index] != 0:
                            pixel_x = round(xd * freq_x[loop_wave,loop_stokes - 1, index] + xd//2 )
                            pixel_y = round(yd * freq_y[loop_wave,loop_stokes - 1, index] + yd//2 )

                            print("%s: %2d: %s: %.12f %.12f: %s: %2d" % ('Peak detected in Stokes', loop_stokes,
                                ' pixels(x,y)', pixel_x, pixel_y, 'loop',index))

                            mask_ones[loop_wave,loop_stokes - 1, pixel_y-1:pixel_y+2,pixel_x-1:pixel_x+2] *= window2d[1:-1,1:-1]

                    # 4j) Correct the power spectrum

                # for loop_inwave in range(wd):
                #     print('looop in wave',loop_inwave,wd, ' stokes ',loop_stokes)

                    Fourier_Transform = np.fft.fftshift(np.fft.fft2(data[loop_wave, loop_stokes, :, :]))

                    if verbose:
                        plt.imshow(Fourier_Transform[yd // 2 - wsize:yd // 2 + wsize, xd // 2 - wsize:xd // 2 + wsize].real,clim=(-50,50),interpolation=None)
                        plt.show()
                        plt.imshow(mask_ones[loop_wave,loop_stokes - 1,yd // 2 - wsize:yd // 2 + wsize, xd // 2 - wsize:xd // 2 + wsize] * \
                            Fourier_Transform[yd // 2 - wsize:yd // 2 + wsize, xd // 2 - wsize:xd // 2 + wsize].real,clim=(-50,50),interpolation=None)
                        plt.show()
                        plt.imshow(mask_ones[loop_wave,loop_stokes - 1,yd // 2 - wsize:yd // 2 + wsize, xd // 2 - wsize:xd // 2 + wsize],interpolation=None)
                        plt.show()

                    Fourier_Transform.real = Fourier_Transform.real * mask_ones[loop_wave,loop_stokes - 1,:,:]
                    Fourier_Transform.imag = Fourier_Transform.imag * mask_ones[loop_wave,loop_stokes - 1,:,:]

                    # if verbose:
                    #     _some_plots_fringes(data, loop_inwave, loop_stokes, 'not corrected:')

                    data[loop_wave, loop_stokes, :, :] = np.fft.ifft2(np.fft.ifftshift(Fourier_Transform)).real

                    # if verbose:
                        # _some_plots_fringes(data, loop_inwave, loop_stokes, 'yes corrected:')

        if 'CAL_FRIN' in header:  # Check for existence
            header['CAL_FRIN'] = version
        else:
            header.set('CAL_FRIN', version, 'Fringe correction ( name+version of py module if True )', after='CAL_DARK')

        return data, header ,freq_x,freq_y, (rmss,means,thresholds)


    elif option == 'manual':

        printc('-->>>>>>> Removing fringes with fixed freq. --', color=bcolors.OKGREEN)
        printc('          ', version, '--', color=bcolors.OKGREEN)
        printc('Freq. updated on 3-June-2022 (H. Strecker and D. Orozco Suarez', color=bcolors.WARNING)

        if xd == 2048:
            freq_x_Q = np.array([0.01318359375, 0.01318359375])
            freq_y_Q = np.array([0.00195312500, 0.00732421875])
            freq_x_U = np.array([0.01318359375, 0.01318359375])
            freq_y_U = np.array([0.00195312500, 0.00732421875])
            freq_x_V = np.array([0.01318359375, 0.01318359375, 0.00976562500, 0.0078125000])
            freq_y_V = np.array([0.00195312500, 0.00732421875, 0.00830078125, 0.0107421875])
            boxx = np.array([1, 1, 1, 1])  # missing in previos version
            boxy = np.array([1, 1, 1, 1])  # missing in previos version
            win_halfw = 2
            #
        if xd == 1024:  # added by Hanna 31-may-2022: Frequencies lead to lower left corener of affected region in PS
            freq_x_Q = np.array([0.01269531, 0.01269531])
            freq_y_Q = np.array([0.00097656, 0.00683594])
            freq_x_U = np.array([0.01269531, 0.01269531])
            freq_y_U = np.array([0.00097656, 0.00683594])
            freq_x_V = np.array([0.01269531, 0.01269531, 0.00976563, 0.00683594])
            freq_y_V = np.array([0.00097656, 0.00683594, 0.00781250, 0.01074219])
            boxx = np.array([1, 1, 1, 1])
            boxy = np.array([1, 1, 1, 1])
            #
        if xd == 1280:  # added by Hanna 02-june-2022: Frequencies lead to lower left corener of affected region in PS
            freq_x_Q = np.array([0.01250000, 0.01250000])
            freq_y_Q = np.array([0.00078125, 0.00625000])
            freq_x_U = np.array([0.01250000, 0.01250000])
            freq_y_U = np.array([0.00078125, 0.00625000])
            freq_x_V = np.array([0.01250000, 0.01250000, 0.00937500, 0.00703125])
            freq_y_V = np.array([0.00078125, 0.00625000, 0.00703125, 0.01015625])
            boxx = np.array([1, 1, 1, 1])
            boxy = np.array([2, 2, 2, 1])
            #
        if xd == 1536:
            freq_x_Q = np.array([0.01236979, 0.01302083])
            freq_y_Q = np.array([0.00130208, 0.00651042])
            freq_x_U = np.array([0.01236979, 0.01302083])
            freq_y_U = np.array([0.00130208, 0.00651042])
            # freq_x_U = np.array([0]) #Stokes U should not be corrected, as pixels cannot be located
            # freq_y_U = np.array([0])
            freq_x_V = np.array([0, 0, 0.00911458, 0.00716146])
            freq_y_V = np.array([0, 0, 0.00846354, 0.01041667])
            boxx = np.array([2, 1, 1, 1])
            boxy = np.array([1, 2, 1, 1])

        # freq to pixel in xd, yd (lower left corner)
        px_x_Q = np.round(freq_x_Q * xd).astype(int)
        px_y_Q = np.round(freq_y_Q * yd).astype(int)
        px_x_U = np.round(freq_x_U * xd).astype(int)
        px_y_U = np.round(freq_y_U * yd).astype(int)
        px_x_V = np.round(freq_x_V * xd).astype(int)
        px_y_V = np.round(freq_y_V * yd).astype(int)

        # Generate unique number to identify pixel which should not included in averaging
        # CaseI: Stokes Q and U with 2 frequencies
        p = 0
        totn_pixQU = ((1 + boxx[0]) * (1 + boxy[0])) + ((1 + boxx[1]) * (1 + boxy[1]))
        allpixQU = np.zeros([2, totn_pixQU], dtype='int')
        for i in range(len(px_x_Q)):
            for x in range(boxx[i] + 1):
                for y in range(boxy[i] + 1):
                    allpixQU[0, p] = px_x_Q[i] + x
                    allpixQU[1, p] = px_y_Q[i] + y
                    p = p + 1
                    #
        ignore_QU = ((allpixQU[0, :]) ** 2 + 3 * allpixQU[0, :] + 2 * allpixQU[0, :] * allpixQU[1, :] + allpixQU[1,
                                                                                                        :] + (
                         allpixQU[1, :]) ** 2) / 2.
        # CaseII: Stokes V with 4 frequencies
        p = 0
        totn_pixV = (1 + boxx[0]) * (1 + boxy[0]) + (1 + boxx[1]) * (1 + boxy[1]) + (1 + boxx[2]) * (1 + boxy[2]) + (
                    1 + boxx[3]) * (1 + boxy[3])
        allpixV = np.zeros([2, totn_pixV], dtype='int')
        for i in range(len(px_x_V)):
            for x in range(boxx[i] + 1):
                for y in range(boxy[i] + 1):
                    allpixV[0, p] = px_x_V[i] + x
                    allpixV[1, p] = px_y_V[i] + y
                    p = p + 1
                    #
        ignore_V = ((allpixV[0, :]) ** 2 + 3 * allpixV[0, :] + 2 * allpixV[0, :] * allpixV[1, :] + allpixV[1, :] + (
        allpixV[1, :]) ** 2) / 2.
        #
        # Define lower left corner of region to determine treshold value
        lc_x_Q = px_x_Q - 2
        lc_y_Q = px_y_Q - 2
        lc_x_U = px_x_U - 2
        lc_y_U = px_y_U - 2
        lc_x_V = px_x_V - 2
        lc_y_V = px_y_V - 2

        # loop over wavelegnth (1) and modulation(2)
        for j in np.arange(1, 4):
            if j == 1:
                px_x = px_x_Q
                px_y = px_y_Q
                lc_x = lc_x_Q
                lc_y = lc_y_Q
            if j == 2:
                px_x = px_x_U
                px_y = px_y_U
                lc_x = lc_x_U
                lc_y = lc_y_U
            if j == 3:
                px_x = px_x_V
                px_y = px_y_V
                lc_x = lc_x_V
                lc_y = lc_y_V
                #

            # Define box size for region to average across
            boxtx = boxx + 4
            boxty = boxy + 4

            # consider that first frequency is at the lower box boundary
            if lc_y[0] < 0:
                boxty[0] = boxty[0] + lc_y[0]
                lc_y[0] = 0

            for i in range(zd // 4):

                FT = np.fft.fft2(data[i, j, :, :])
                ps = (FT * np.conj(FT)).astype(np.float)

                # loop across two frequencies for Stokes Q and U and over four for Stokes V
                for l in range(len(px_x)):
                    # Do not start any correction if the frequency is not defined
                    if px_x[l] != 0:  # add if condition for data with 1536 size

                        box_corx = []
                        box_cory = []
                        for x in np.arange(lc_x[l], lc_x[l] + boxtx[l] + 1):
                            for y in np.arange(lc_y[l], lc_y[l] + boxty[l] + 1):
                                # Generate an unique value for the coordinates
                                xy = ((x) ** 2 + 3 * x + 2 * x * y + y + (y) ** 2) / 2.
                                test_in = -1
                                if (j == 1) or (j == 2):
                                    test_in = (xy == ignore_QU)
                                if (j == 3):
                                    test_in = (xy == ignore_V)
                                # print(x,y,xy,test_in,np.any(test_in))
                                if not (np.any(test_in)):
                                    box_corx = np.append(box_corx, x)
                                    box_cory = np.append(box_cory, y)
                                    #
                        box_cory = np.asarray(box_cory).astype(int)
                        box_corx = np.asarray(box_corx).astype(int)
                        print(j, i, l, box_cory, box_corx, px_y[l], px_y[l] + boxy[l], px_x[l], px_x[l] + boxx[l])
                        mean_ps = np.mean(ps[box_cory, box_corx])
                        sdev_ps = np.std(ps[box_cory, box_corx])
                        ft_mean = np.mean(FT[box_cory, box_corx])
                        ft_sdev = np.std(FT[box_cory, box_corx])

                        # Determine pixel where values are larger than the average of surroundings
                        index = -1  ##generate empty arry
                        index = np.where(
                            ps[px_y[l]:px_y[l] + boxy[l] + 1, px_x[l]:px_x[l] + boxx[l] + 1] > mean_ps + 0.5 * sdev_ps)
                        if np.any(index):
                            row = index[0]
                            col = index[1]
                            xl = col + px_x[l]
                            yl = row + px_y[l]
                            # Determine swaped coordinates for Q and U
                            xlswap = yl
                            ylswap = xl
                            # Replace selected pixel by symmetric xy values in FFT domain
                            if (j == 1) or (j == 2):
                                FT[yl, xl] = (FT[ylswap, xlswap]) * ATT_FACTOR
                            if (j == 3):
                                FT[yl, xl] = (ft_mean + 0.5 * ft_sdev) * ATT_FACTOR

                            xu = xd - xl
                            yu = yd - yl
                            xuswap = yu
                            yuswap = xu
                            if (j == 1) or (j == 2):
                                FT[yu, xu] = (FT[yuswap, xuswap]) * ATT_FACTOR
                            if (j == 3):
                                FT[yu, xu] = (ft_mean + 0.5 * ft_sdev) * ATT_FACTOR
                        else:
                            print('No correction necessary')

                        # for values smaller than average of surroundings
                        index = -1
                        index = np.where(
                            ps[px_y[l]:px_y[l] + boxy[l] + 1, px_x[l]:px_x[l] + boxx[l] + 1] < mean_ps - 0.5 * sdev_ps)
                        if np.any(index):
                            row = index[0]
                            col = index[1]
                            xl = col + px_x[l]
                            yl = row + px_y[l]
                            # Determine swaped coordinates for Q and U
                            xlswap = yl
                            ylswap = xl
                            # Replace selected pixel by symmetric xy vvalues in FFT domain
                            if (j == 1) or (j == 2):
                                FT[yl, xl] = (FT[ylswap, xlswap]) * ATT_FACTOR
                            if (j == 3):
                                FT[yl, xl] = (ft_mean + 0.5 * ft_sdev) * ATT_FACTOR

                            xu = xd - xl
                            yu = yd - yl
                            xuswap = yu
                            yuswap = xu
                            if (j == 1) or (j == 2):
                                FT[yu, xu] = (FT[yuswap, xuswap]) * ATT_FACTOR
                            if (j == 3):
                                FT[yu, xu] = (ft_mean + 0.5 * ft_sdev) * ATT_FACTOR
                        else:
                            print('No correction necessary for smaller ')
                    else:  # print if data is 1536 size and frequency needs no correction
                        print('Do not correct this frequency')

                # restransform
                data[i, j, :, :] = np.fft.ifft2(FT)

        if 'CAL_FRIN' in header:  # Check for existence
            header['CAL_FRIN'] = version
        else:
            header.set('CAL_FRIN', version, 'Fringe correction ( name+version of py module if True )', after='CAL_DARK')

        return data, header, 0, 0, 0

    elif option == 'naive':
        printc('-->>>>>>> Removing fringes with fixed freq. --', color=bcolors.OKGREEN)
        printc('          ', version, '--', color=bcolors.OKGREEN)
        printc('Freq. updated on 11-August-2021 (H. Strecker and D. Orozco Suarez', color=bcolors.WARNING)

        # freq_x_Q = np.array([0.01318359375, 0.01318359375])
        freq_x_Q = np.array([0.0131, 0.0134])
        # freq_y_Q = np.array([0.00195312500, 0.00732421875])
        freq_y_Q = np.array([0.00195312500, 0.00732421875])

        # freq_x_U = np.array([0.01318359375, 0.01318359375])
        freq_x_U = np.array([0.0131, 0.0134])
        freq_y_U = np.array([0.00195312500, 0.00732421875])

        freq_x_V = np.array([0.01318359375, 0.01318359375, 0.00976562500, 0.0078125000])
        freq_y_V = np.array([0.00195312500, 0.00732421875, 0.00830078125, 0.0107421875])

        # freq to pixel yd,xd
        px_x_Q = freq_x_Q * xd
        px_y_Q = freq_y_Q * yd
        px_x_U = freq_x_U * xd
        px_y_U = freq_y_U * yd
        px_x_V = freq_x_V * xd
        px_y_V = freq_y_V * yd
        # reflection
        px_x_Q = np.append(px_x_Q, xd - px_x_Q - 1)
        px_y_Q = np.append(px_y_Q, yd - px_y_Q - 1)
        px_x_U = np.append(px_x_U, xd - px_x_U - 1)
        px_y_U = np.append(px_y_U, yd - px_y_U - 1)
        px_x_V = np.append(px_x_V, xd - px_x_V - 1)
        px_y_V = np.append(px_y_V, yd - px_y_V - 1)

        px_x_Q = np.round(px_x_Q).astype(int)
        px_y_Q = np.round(px_y_Q).astype(int)
        px_x_U = np.round(px_x_U).astype(int)
        px_y_U = np.round(px_y_U).astype(int)
        px_x_V = np.round(px_x_V).astype(int)
        px_y_V = np.round(px_y_V).astype(int)

        printc('freq_x_Q [f,px] ', freq_x_Q, px_x_Q, color=bcolors.OKBLUE)
        printc('freq_y_Q [f,px] ', freq_y_Q, px_y_Q, color=bcolors.OKBLUE)
        printc('freq_x_U [f,px] ', freq_x_U, px_x_U, color=bcolors.OKBLUE)
        printc('freq_y_U [f,px] ', freq_y_U, px_y_U, color=bcolors.OKBLUE)
        printc('freq_x_V [f,px] ', freq_x_V, px_x_V, color=bcolors.OKBLUE)
        printc('freq_y_V [f,px] ', freq_y_V, px_y_V, color=bcolors.OKBLUE)

        mask_QUV = np.ones((3, yd, xd))
        apod_size = 5  #ALWAYS IMPAR
        lims = apod_size//2 - 1
        window1d = np.abs(np.blackman(apod_size))
        window2d = NFREQ_LEVEL - np.sqrt(np.outer(window1d,window1d))

        for k in range(len(px_x_Q)):
            # mask_QUV[0,px_y_Q[k]-win_halfw:px_y_Q[k]+win_halfw+1,px_x_Q[k]-win_halfw:px_x_Q[k]+win_halfw+1] = 1e-9
            mask_QUV[0, px_y_Q[k]-lims:px_y_Q[k]+lims+1,px_x_Q[k]-lims:px_x_Q[k]+lims+1] *= window2d[1:-1,1:-1]

        for k in range(len(px_x_U)):
            mask_QUV[1, px_y_U[k]-lims:px_y_U[k]+lims+1,px_x_U[k]-lims:px_x_U[k]+lims+1] *= window2d[1:-1,1:-1]

        for k in range(len(px_x_V)):
            mask_QUV[2, px_y_V[k]-lims:px_y_V[k]+lims+1,px_x_V[k]-lims:px_x_V[k]+lims+1] *= window2d[1:-1,1:-1]

        # if verbose:
        # plib.show_one(mask_QUV[0,0:30,0:30])
        # plib.show_one(mask_QUV[1,0:30,0:30])
        # plib.show_one(mask_QUV[2,0:30,0:30])
        for i in range(zd // 4):
            for j in np.arange(1, 4):
                F = np.fft.fft2(data[i, j, :, :])
                power2d = np.abs( (F*np.conj(F)).astype(np.float) )
                power2d = gaussian_filter(power2d, sigma=(1, 1))
                im = np.log10( power2d[0:30,0:30] )
                mean = np.mean(im[:,10:])
                rms = np.std(im[:,10:])
                plt.imshow(im[:,10:],cmap='Greys',clim=(mean-2*rms,mean+2*rms))
                plt.title('or: '+str([i,j]))
                plt.show()
                F *= mask_QUV[j - 1, :, :]
                power2d = np.abs( (F*np.conj(F)).astype(np.float) )
                power2d = gaussian_filter(power2d, sigma=(1, 1))
                im = np.log10( power2d[0:30,0:30])
                plt.imshow(im[:,10:],cmap='Greys',clim=(mean-2*rms,mean+2*rms))
                plt.title('co: '+str([i,j]))
                plt.show()
                data[i, j, :, :] = np.fft.ifft2(F)

        if 'CAL_FRIN' in header:  # Check for existence
            header['CAL_FRIN'] = version
        else:
            header.set('CAL_FRIN', version, 'Fringe correction ( name+version of py module if True )', after='CAL_DARK')

    else:
        print('No fringe correction')
        return data, header, 0, 0, 0
    #     PLT_RNG = 2
    #     plib.show_four_row(data_d[1,0,:,:],data_d[1,1,:,:],data_d[1,2,:,:],data_d[1,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[1,0,:,:],data[1,1,:,:],data[1,2,:,:],data[1,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])
    #     plib.show_four_row(data_d[3,0,:,:],data_d[3,1,:,:],data_d[3,2,:,:],data_d[3,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[3,0,:,:],data[3,1,:,:],data[3,2,:,:],data[3,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])
    #     plib.show_four_row(data_d[5,0,:,:],data_d[5,1,:,:],data_d[5,2,:,:],data_d[5,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[5,0,:,:],data[5,1,:,:],data[5,2,:,:],data[5,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])

    # np.save('data_f',data)
    # return

    #     PLT_RNG = 2
    #     plib.show_four_row(data_d[1,0,:,:],data_d[1,1,:,:],data_d[1,2,:,:],data_d[1,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[1,0,:,:],data[1,1,:,:],data[1,2,:,:],data[1,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])
    #     plib.show_four_row(data_d[3,0,:,:],data_d[3,1,:,:],data_d[3,2,:,:],data_d[3,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[3,0,:,:],data[3,1,:,:],data[3,2,:,:],data[3,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])
    #     plib.show_four_row(data_d[5,0,:,:],data_d[5,1,:,:],data_d[5,2,:,:],data_d[5,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003],block=False)
    #     plib.show_four_row(data[5,0,:,:],data[5,1,:,:],data[5,2,:,:],data[5,3,:,:],title=['I','Q','U','V'],svmin=[0.1,-0.002,-0.002,-0.003],svmax=[1.1,0.002,0.002,0.003])

    # np.save('data_f',data)
    # return

    return data, header, 0, 0, 0

# GV need to pass output_dir too
def generate_level2(
        data: np.ndarray, header: pyfits.header.Header, wave_axis: np.ndarray, rte_mode: str, temp_dir: str = './',
        ref_wavelength: float = 6173.341000, RTE_code: str = 'pmilos', options: list = [],
        center_line: bool = True, weight: np.ndarray = None, initial_model: np.ndarray = None,
        parallel: bool = False, num_workers: int = 10, mask: np.ndarray = 0, cavity_file: str = str(), correct_cavity: bool = False):
    """
    generate_level2 from a observation

    Needs wavelength axis as input. It can be extracted from the header. To store the data in fits format use ``save_level2()```

    .. note::
         An example of how to use `generate_level2()` can be found `here <genetate_level2.html>`_

    :param data: Stokes data. Dimensions ``[l,p,x,y]``
    :type data: np.ndarray
    :param header: FITS header corresponding to Stokes data
    :type header: pyfits.header.Header
    :param wave_axis: wavelength axis in angstrom.

    .. note::
        Wavelength axis shown be a increasing monotonic function if CE or PSF are activated. If not, wavelength points ordering doesn't matter!

    :type wave_axis: np.ndarray
    :param rte_mode: RTE mode. See ``phi_rte()``
    :type rte_mode: str
    :param temp_dir: Temporary directory for RTE. See ``phi_rte()``, defaults to './'
    :type temp_dir: str
    :param ref_wavelenth: reference wavelength, defaults to 6173.3354
    :type ref_wavelenth: float, optional
    :param milos_executable: pmilos or cmilos executable (path + file), defaults to None
    :type milos_executable: str, optional
    :param options: RTE options, defaults to None. . See ``phi_rte()``
    :type options: list, optional
    :param center_line: If true, center the data to the refeernce wavelength, defaults to True
    :type center_line: bool, optional
    :param weight: Stokes profiles weights, defaults to None. See ``phi_rte()``
    :type weight: np.ndarray, optional
    :param initial_model: ME initial model, defaults to None. See ``phi_rte()``
    :type initial_model: np.ndarray, optional
    :param parallel: run parallel version (python pmilos based only), defaults to ``False``.
    :type parallel: integer, optional
    :param num_workers: number of parallel instances, defaults to ``10``.
    :type num_workers: integer, optional
    :param mask: input bit mask (same dimensions as data input) specifing the pixels to invert. ``0 or 1``, defaults to ``0``, full image.
    :type mask: np.ndarray, optional
    :param cavity_file: FITS file with with cavity map in Angstrom (default: None)
    :type cavity: str, optional
    :param correct_cavity: correct for cavity (default: False)
    :type correct_cavity: bool, optional
    :return: ME model. See ``phi_rte()``
    :rtype: np.ndarray
    """

    if RTE_code == 'cmilos':
        cmd = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'cmilos', 'milos')
        printc('RTE on. Looking for ', cmd, bcolors.OKGREEN)
        if os.path.isfile(cmd):
            printc('CMILOS executable found', color=bcolors.OKBLUE)
        else:
            printc('Cannot find CMILOS executable', bcolors.FAIL)
            raise FileNotFoundError
        # cmd = fix_path(cmd)  # not needed if os.path.join is used
    elif RTE_code == 'pmilos':
        cmd = ''
    else:
        printc("Error in RTE_code", color=bcolors.WARNING)
        raise ValueError

    # ref_wavelenth = 6173.3354
    # ref_wavelenth = 6173.341000  # Changed Sep 27 2022. This one should coincide with the ref wavelength in cmilos [#define CENTRAL_WL 6173.341000]
    # added as input Nov 2022

    # FORCE the wave axis to be centered at line rest position
    # COMMENT AF: why is this needed?
    if center_line:
        print('Centering...')
        if center_line is True:
            # look for line continuum:
            line_center = 3 if np.abs(wave_axis[0] - wave_axis[1]) > np.abs(wave_axis[4] - wave_axis[5]) else 2
            shift_w = wave_axis[line_center] - ref_wavelength
            wave_axis = wave_axis - shift_w

        elif type(center_line) is int:
            shift_w = wave_axis[center_line] - ref_wavelength
            wave_axis = wave_axis - shift_w

    # wave_axis = np.array([-300,-140,-70,0,70,140])
    printc('   It is assumed the wavelength is given by the header info ')
    printc('         wave axis: ', wave_axis, color=bcolors.WARNING)
    printc('         wave axis (step):  ', (wave_axis - ref_wavelength) * 1000., color=bcolors.WARNING)
    printc('         reference wavelength:  ', ref_wavelength, color=bcolors.WARNING)

    if correct_cavity:
        printc('Read cavity map', cavity_file, color=bcolors.OKGREEN)
        cavity, cavity_header = fits_get(cavity_file, scale=False)
        cavity = cavity.astype(np.float32)
        printc('Check cavity map dimensions', color=bcolors.OKGREEN)
        cavity = crop_calibration_data(cavity, cavity_header, data, header)
    else:
        cavity = np.empty([], dtype=float)

    return phi_rte(
        data, wave_axis, rte_mode, temp_dir, cmd = cmd, options=options, weight=weight,
        initial_model=initial_model, parallel=parallel, num_workers=num_workers, mask=mask, cavity = cavity)


def save_level2(
        level2_file: str, rte_invs: np.ndarray, output_directory: str = './',
        rte: str = ' ', mask: np.ndarray = 0, verbose: bool = False, save_png: bool = True,
        outfile_L2: str = ' ', extrakey: str = ''):
    """
    save_level2 _summary_

    _extended_summary_

    :param level2_file: _description_
    :type level2_file: str
    :param rte_invs: _description_
    :type rte_invs: np.ndarray
    :param output_directory: _description_, defaults to './'
    :type output_directory: str, optional
    :param rte: _description_, defaults to ' '
    :type rte: str, optional
    :param mask: _description_, defaults to 0
    :type mask: np.ndarray, optional
    :param verbose: _description_, defaults to False
    :type verbose: bool, optional
    :param save_png: _description_, defaults to True
    :type save_png: bool, optional
    :param outfile_L2: _description_, defaults to ' '
    :type outfile_L2: str, optional
    :param extrakey: _description_, defaults to ''
    :type extrakey: str, optional
    """
    # TODO Separar stokes de inversiones (por si hago las inversiones a parte).

    printc('  ---- >>>>> Saving L2 data.... ', color=bcolors.OKGREEN)
    if outfile_L2 == ' ':
        outfile_L2 = os.path.basename(level2_file)
    if outfile_L2.rfind('stokes') != -1:
        outfile_L2 = set_level(outfile_L2, 'stokes', 'dummy')
    if outfile_L2.rfind('ilam') != -1:
        outfile_L2 = set_level(outfile_L2, 'ilam', 'dummy')

    with pyfits.open(level2_file) as hdu_list:
        header = hdu_list[0].header

        printc('  ---- >>>>> Updating L2 header.... ', color=bcolors.OKGREEN)
        header['history'] = ' RTE CMILOS INVERTER: ' + rte
        header['history'] = ' CMILOS VER: ' + VERS_RTE
        header = update_header(header, 'RTE_ITER', str(15), 'CAL_SCIP', 'Number RTE inversion iterations')

        hdu_list[0].data = rte_invs.astype(np.float32)
        hdu_list[0].header = header

        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'FullModel' + extrakey))
        printc('Writing fits: ', writeto, color=bcolors.OKBLUE)
        hdu_list.writeto(writeto, overwrite=True)

        # rte_invs_stokes_mask = np.copy(rte_invs)
        # umbral = 3.
        # noise_in_V =  np.mean(data[0,3,rry[0]:rry[1],rrx[0]:rrx[1]])
        # low_values_flags = np.max(np.abs(data[:,3,:,:]),axis=0) < noise_in_V*umbral  # Where values are low
        # rte_invs_stokes_mask[2,low_values_flags] = 0
        # rte_invs_stokes_mask[3,low_values_flags] = 0
        # rte_invs_stokes_mask[4,low_values_flags] = 0

    if np.ndim(mask) != 0:
        for i in range(12):
            rte_invs[i, :, :] = rte_invs[i, :, :] * mask

    if verbose:
        plib.show_four_row(
            rte_invs[2, :, :], rte_invs[3, :, :], rte_invs[4, :, :], rte_invs[8, :, :],
            svmin=[0, 0, 0, -6.], svmax=[1200, 180, 180, +6.],
            title=['Field strengh [Gauss]', 'Field inclination [degree]', 'Field azimuth [degree]', 'LoS velocity [km/s]'],
            xlabel='Pixel', ylabel='Pixel')

    # LoS magnetic field
    b_los = rte_invs[2, :, :] * np.cos(rte_invs[3, :, :] * np.pi / 180.)
    if verbose:
        plib.show_one(b_los, vmin=-30, vmax=30, title='LoS magnetic field')

    # Continuum intensity
    if rte != 'CE':
        icont = rte_invs[9, :, :] + rte_invs[10, :, :]
    else:
        icont = rte_invs[0, :, :]  # TODO Check this

    # Save inversion results to fits
    with pyfits.open(level2_file) as hdu_list:
        header['NAXIS3'] = 0

        # BMAG
        hdu_list[0].data = rte_invs[2, :, :].astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Field strength'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'Gauss'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'bmag' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # BINC
        hdu_list[0].data = rte_invs[3, :, :].astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Field inclination'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'degrees'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'binc' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # BAZI
        hdu_list[0].data = rte_invs[4, :, :].astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Field azimuth'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'degrees'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'bazi' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # BLOS
        hdu_list[0].data = b_los.astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'longitudinal magnetic field'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'Gauss'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'blos' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # Vlos
        hdu_list[0].data = rte_invs[8, :, :].astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'LoS velocity'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'km/s'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'vlos' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # Icont
        hdu_list[0].data = icont.astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Intensity [ME]'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'Normalized'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'icnt' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

        # CHI2
        hdu_list[0].data = rte_invs[-1, :, :].astype(np.float32)
        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Merit function'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'adimensioal'
        hdu_list[0].header = header
        writeto = os.path.join(output_directory, set_level(outfile_L2, 'dummy', 'chi2' + extrakey))
        print('Writing fits: ', writeto)
        hdu_list.writeto(writeto, overwrite=True)

    # Save inversion results to png
    if save_png:
        # output_directory = set_level(output_directory, 'level2', 'pngs')
        # printc('  ---- >>>>> Saving plots.... ', color=bcolors.OKGREEN)

        if level2_file.endswith('.fits'):
            filetype = '.fits'
        elif level2_file.endswith('.fits.gz'):
            filetype = '.fits.gz'

        idx = np.where(mask == 0)

        # Plot observables
        fig, maps = plt.subplots(2, 3, sharex='col', sharey='row', figsize=(10, 6))
        fig.subplots_adjust(top=0.92, hspace=0.05)
        fig.suptitle(header['DATE-AVG'], y=0.96, fontsize=14)

        icont[idx] = 0
        im1 = maps[0, 0].imshow(icont, cmap='gist_heat', vmin=0.2, vmax=1.2, interpolation='none')
        plib.colorbar(im1)
        maps[0, 0].title.set_text('Continuum intensity')
        maps[0, 0].title.set_size(12)

        hmimag = plib.cmap_from_rgb_file('HMI', 'hmi_mag.csv')
        b_los[idx] = 0.
        im2 = maps[0, 1].imshow(b_los, cmap=hmimag, vmin=-50, vmax=50, interpolation='none')
        plib.colorbar(im2)
        maps[0, 1].title.set_text('LoS magnetic field [Gauss]')
        maps[0, 1].title.set_size(12)

        im3 = maps[0, 2].imshow(rte_invs[8, :, :], cmap=hmimag, vmin=-1.5, vmax=1.5, interpolation='none')
        plib.colorbar(im3)
        maps[0, 2].title.set_text('LoS velocity [km/s]')
        maps[0, 2].title.set_size(12)

        im4 = maps[1, 0].imshow(rte_invs[3, :, :], cmap='seismic', vmin=40, vmax=140, interpolation='none')
        plib.colorbar(im4)
        maps[1, 0].title.set_text('Field inclination [degree]')
        maps[1, 0].title.set_size(12)

        im5 = maps[1, 1].imshow(rte_invs[2, :, :], cmap='gnuplot_r', vmin=0, vmax=400, interpolation='none')
        plib.colorbar(im5)
        maps[1, 1].title.set_text('Field strength [Gauss]')
        maps[1, 1].title.set_size(12)

        im6 = maps[1, 2].imshow(rte_invs[4, :, :], cmap='hsv', vmin=20, vmax=160, interpolation='none')
        plib.colorbar(im6)
        maps[1, 2].title.set_text('Azimuth [degree]')
        maps[1, 2].title.set_size(12)

        writeto = set_level(outfile_L2, 'dummy', 'plot' + extrakey)
        writeto = set_level(writeto, filetype, '.png')
        writeto = os.path.join(output_directory, writeto)
        print('Writing observables png: ', writeto)
        plt.savefig(writeto, dpi=300)
        plt.close()

        # Plot Chi2
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        fig.subplots_adjust(top=0.92, hspace=0.05)
        fig.suptitle(header['DATE-AVG'], y=0.96, fontsize=14)
        im = ax.imshow(rte_invs[-1, :, :], cmap='seismic', vmin=0, vmax=50, interpolation='none')
        plib.colorbar(im)
        ax.title.set_text('Chi2')
        ax.title.set_size(12)
        writeto = set_level(writeto, 'plot', 'chi2')
        print('Writing Chi2 png: ', writeto)
        plt.savefig(writeto, dpi=300)
        plt.close()


def update_header_stokes(header):
    """
    update_header_stokes _summary_

    _extended_summary_

    :param header: _description_
    :type header: _type_
    :return: _description_
    :rtype: _type_
    """
    header.set('CTYPE3', header["CTYPE1"], 'Spatial axis parallel Solar North', after='CTYPE2')
    header.set('CTYPE4', header["CTYPE2"], 'Spatial axis parallel to Solar + West', after='CTYPE3')
    header.set('CTYPE1', 'WAVE', 'Wavelength dimension', after='CAL_DARK')
    header.set('CTYPE2', 'STOKES', 'Stokes dimension', after='CTYPE1')
    header.set('CRPIX3', header["CRPIX1"], 'Description of ref pixel along axis 3', after='CRPIX2')
    header.set('CRPIX4', header["CRPIX2"], 'Description of ref pixel along axis 4', after='CRPIX3')
    header.set('CRPIX1', '0')
    header.set('CRPIX2', '0')
    header.set('CUNIT3', header["CUNIT1"], 'Units along axis 3', after='CUNIT2')
    header.set('CUNIT4', header["CUNIT2"], 'Units along axis 4', after='CUNIT3')
    header.set('CUNIT1', 'wavekength')
    header.set('CUNIT2', 'Stokes')

    header.set('PC3_3', header["PC1_1"], 'Coordinate transformation to LIF', after='PC2_2')
    header.set('PC3_4', header["PC1_2"], 'Coordinate transformation to LIF', after='PC3_3')
    header.set('PC4_3', header["PC2_1"], 'Coordinate transformation to LIF', after='PC3_4')
    header.set('PC4_4', header["PC2_2"], 'Coordinate transformation to LIF', after='PC4_3')

    header.set('PC3_3', 1.0)
    header.set('PC3_4', 1.0)
    header.set('PC4_3', 1.0)
    header.set('PC4_4', 1.0)

    header.set('CDELT3', header["CDELT1"], 'Pixel scale along axis 3 in unit CUNIT3', after='CDELT2')
    header.set('CDELT4', header["CDELT2"], 'Pixel scale along axis 4 in unit CUNIT4', after='CDELT3')
    header.set('CDELT1', 80)
    header.set('CDELT2', 1.0)

    header.set('CRVAL3', header["CRVAL1"], '[arcsec] Coordinates of ref pixel along axis 3', after='CRVAL2')
    header.set('CRVAL4', header["CRVAL2"], '[arcsec] Coordinates of ref pixel along axis 4', after='CRVAL3')
    header.set('CDELT1', 1.0)
    header.set('CDELT2', 1.0)

    return header


def crop_calibration_data(
        cal_data: np.ndarray, cal_header: pyfits.header.Header,
        data: np.ndarray, header: pyfits.header.Header, ignore_number_of_frames: bool = False):
    """
    Check calibration data dimensions and crop to match the science data if necessary

    :param cal_data: calibration data
    :type rte_mode: numpy.ndarray
    :param cal_header: calibration header
    :type cal_header: astropy.io.fits.header.Header
    :param data: science data
    :type data: numpy.ndarray
    :param header: science header
    :type header: astropy.io.fits.header.Header
    """

    pxbeg1 = int(header['PXBEG1']) - 1
    pxend1 = int(header['PXEND1']) - 1
    pxbeg2 = int(header['PXBEG2']) - 1
    pxend2 = int(header['PXEND2']) - 1

    if data.ndim == 4:
        wd, pd, yd, xd = data.shape
        zd = wd * pd
    elif data.ndim == 3:
        zd, yd, xd = data.shape

    if cal_data.ndim == 3:
        cal_zd, cal_yd, cal_xd = cal_data.shape
        if not ignore_number_of_frames and cal_zd != zd:
            raise ValueError('Calibration data has different number of wavelengths or polarization states')
    elif cal_data.ndim == 2:
        cal_yd, cal_xd = cal_data.shape
    else:
        raise ValueError('Calibration data has wrong dimensionality')

    if cal_xd < xd or cal_yd < yd:
        raise ValueError('Calibration data is smaller than science data in x or y dimensions')

    if 'PXBEG1' in cal_header:  # we assume that if PXBEG1 is in the header, all other keywords are there as well
        cal_pxbeg1 = int(cal_header['PXBEG1']) - 1
        cal_pxend1 = int(cal_header['PXEND1']) - 1
        cal_pxbeg2 = int(cal_header['PXBEG2']) - 1
        cal_pxend2 = int(cal_header['PXEND2']) - 1
    elif cal_xd < 2048 or cal_yd < 2048:
        raise ValueError('Calibration data have been cropped, but no cropping keywords are present in the header')
    else:
        cal_pxbeg1 = 0
        cal_pxend1 = 2047
        cal_pxbeg2 = 0
        cal_pxend2 = 2047

    if cal_pxbeg1 > pxbeg1 or cal_pxend1 < pxend1 or cal_pxbeg2 > pxbeg2 or cal_pxend2 < pxend2:
        raise ValueError('Cropping region of calibration data not compatible with science data')

    if cal_pxbeg1 != pxbeg1 or cal_pxend1 != pxend1 or cal_pxbeg2 != pxbeg2 or cal_pxend2 != pxend2:
        printc('Cropping calibration data to match science data', color=bcolors.OKBLUE)
        if cal_data.ndim == 3:
            cal_data = cal_data[
                :,
                pxbeg2 - cal_pxbeg2: pxend2 - cal_pxbeg2 + 1,
                pxbeg1 - cal_pxbeg1: pxend1 - cal_pxbeg1 + 1]
        else:
            cal_data = cal_data[
                pxbeg2 - cal_pxbeg2: pxend2 - cal_pxbeg2 + 1,
                pxbeg1 - cal_pxbeg1: pxend1 - cal_pxbeg1 + 1]

    return cal_data
