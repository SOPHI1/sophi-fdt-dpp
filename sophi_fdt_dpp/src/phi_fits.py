"""
``phi_fits.py`` Contains useful functions for reading PHI `fits` or `fits.gz` files
as well as for extracting infortant header information and other useful routines.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez**
   :Contributors:

.. list-table::
   :widths: 25 50
   :header-rows: 1

   * - program
     - Short summary
   * - :py:meth:`fits_get`
     - Function to load PHI files
   * - :py:meth:`scale_data`
     - Data scaling function
   * - :py:meth:`fits_get_fpatimes`
     - Get FPA times from header
   * - :py:meth:`list_fits`
     - List available fits files in a folder
   * - :py:meth:`fits_get_sampling`
     - Get wavelength and voltage sampling from header
   * - :py:meth:`fits_get_part`
     - Get single image from PHI Stokes file from given polarization state and wavelength
   * - :py:meth:`read_shifts`
     - Read stored shifts from file (for KLL)
   * - :py:meth:`write_shifts`
     - Write shifts into file (for KLL)
   * - :py:meth:`set_level`
     - For setting file level
   * - :py:meth:`expand_id`
     - For setting file level
   * - :py:meth:`find_string`
     - Find string/s in a string

"""

import contextlib
from astropy.io import fits as pyfits
import numpy as np
from os import path, walk
from typing import Tuple
from .tools import bcolors, printc

def fits_get(file: str, info: bool = False, head: int = 0, get_scaling: bool = False, scale: bool = True, section: int = 0) -> any:
    # sourcery skip: low-code-quality
    """
    Helper function to load FITS data set

    - if only file data is given, return data (16bits) + header: ``d,h = fits_get(file)``

    - if ``info = True`` prints fits info (headers structure)

    - if ``head`` is provided, returns data and selected header

    - if ``scaling = True``, return the file scaling

    The scaling will be a list of two tuple elements:

    -- scaling["Present"][0] = True ->  IMGFMT_16_0_S scaling

    -- scaling["Present"][0] = False ->  IMGFMT_24_8 scaling

    Scaling is stored in *scaling["scaling"]* tuple

    .. warning:: The ``section`` option is ment for special purposes. Use it at your own risk. It's slow as hell.

    :param file: fits file to load
    :type file: str
    :param info: True if fits information is returned
    :type info: bool
    :param head: header extension to extract
    :type head: integer
    :param get_scaling: If True returns the scaling information
    :type get_scaling: integer
    :param scale: list of two tuple elements
    :type scale: tuple
    :param section: integer number defining the size (2xsection + 1) around [CRPIX1,CRPIX2] to get from the fits file
    :type scale: integer
    :return: data and header or scaling of ``get_scaling = True``
    :rtype: (np.ndarray,dict) or float

    .. code-block:: python

        import sophi_fdt_dpp as fdt
        file = 'Stokes_file.fits'
        data, header = fdt.fits_get(file)
        # for getting header info
        fdt.fits_get(file,info = True)
        # for getting different header
        data, header = fdt.fits_get(file, head = 4)
        # for getting the scaling
        scaling = fdt.fits_get(file, scaling = True)

    """

    if info:
        try:
            return pyfits.info(file)
        except Exception:
            print("Unable to open fits file: {}", file)
            raise
    if get_scaling:
        index = 1
        scaling = {"Present": [False, True], "scaling": [0, 0]}
        while True:
            try:
                dummy_head = pyfits.getheader(file, index)
            except Exception:
                print("index in get_scaling is out bounds: {}", index)
                break
            if dummy_head['EXTNAME'] == 'PHI_FITS_imageSummary':
                with pyfits.open(file) as hdu_list:
                    header_data = hdu_list[index].data
                    # case 1 if that there is only ONE scaling (untouched data)
                    if len(header_data) == 1:
                        scaling["Present"][0] = False
                        scaling["Present"][1] = True
                        scaling["scaling"][0] = 0.
                        scaling["scaling"][1] = float(header_data[0][12])
                        break
                    # case 2 if that there is more than TWO scaling data
                    if len(header_data) > 2:
                        # check the first one from below and if it is IMGFMT_16_0_S store it and continue
                        if header_data[-1][3] == 'IMGFMT_16_0_S':
                            scaling["Present"][0] = True
                            scaling["Present"][1] = True
                            scaling["scaling"][0] = float(header_data[-1][12])
                            scaling["scaling"][1] = float(header_data[-3][12])
                            break
                        if header_data[-1][3] == 'IMGFMT_24_8':
                            scaling["Present"][0] = False
                            scaling["Present"][1] = True
                            scaling["scaling"][0] = 0.
                            scaling["scaling"][1] = float(header_data[-1][12])
                            break
                    # case 2 if that there is more than TWO scaling data
                    if len(header_data) == 2:
                        # check the first one from below and if it is IMGFMT_16_0_S store it and continue
                        if header_data[-1][3] == 'IMGFMT_16_0_S':
                            scaling["Present"][0] = True
                            scaling["Present"][1] = True
                            scaling["scaling"][0] = float(header_data[-1][12])
                            scaling["scaling"][1] = float(header_data[-2][12])
                            break
                        if header_data[-1][3] == 'IMGFMT_24_8':
                            scaling["Present"][0] = False
                            scaling["Present"][1] = True
                            scaling["scaling"][0] = 0.
                            scaling["scaling"][1] = float(header_data[-1][12])
                            break
            index += 1
        return scaling

    if section:
        head = 0
        with pyfits.open(file,use_fsspec=True, memmap=True) as hdu_list:
            header = hdu_list[head].header
            cx = header['CRPIX1']
            cy = header['CRPIX2']
            from_x = int(cx-section)
            to_x = int(cx+section)
            from_y = int(cy-section)
            to_y = int(cy+section)
            data = hdu_list[head].section[:6,from_y:to_y,from_x:to_x].astype(np.dtype('float32'))
            if scale:
                print('-->>>>>>> Scaling data... ')
                try:
                    head = 9
                    data_ext9 = hdu_list[head].data
                    header_ext9 = hdu_list[head].header
                    IMGformat = data_ext9['PHI_IMG_format'][-1]
                except Exception:
                    print("PHI_IMG_format not found: Most likely file does not have 9th Image extension")
                    IMGformat = 'IMGFMT_16'
    else:
        with pyfits.open(file) as hdu_list:
            if head != 0:
                data = hdu_list[head].data
            else:
                data = hdu_list[head].data.astype(np.dtype('float32'))
            header = hdu_list[head].header
            if scale:
                print('-->>>>>>> Scaling data... ')
                try:
                    head = 9
                    data_ext9 = hdu_list[head].data
                    header_ext9 = hdu_list[head].header
                    IMGformat = data_ext9['PHI_IMG_format'][-1]
                except Exception:
                    print("PHI_IMG_format not found: Most likely file does not have 9th Image extension")
                    IMGformat = 'IMGFMT_16'

    if scale:
        if IMGformat != 'IMGFMT_24_8':
            print('          INPUT IS DIVIDED by 256.   ')
            data /= 256.
        else:
            print("Dataset downloaded as raw: no bit convert scaling needed")

        try:
            maxRange = data_ext9['PHI_IMG_maxRange']
            data *= maxRange[0] / maxRange[-1]
            print("max range ", maxRange[0], maxRange[-1], maxRange[0] / maxRange[-1])
        except Exception:
            print("PHI_IMG_maxRange not found: Most likely file does not have 9th Image extension")
            data *= 81920 / 128
            print("max range ", 81920, 128, 81920 / 128)

    return data, header


def scale_data(data: np.ndarray, header: dict) -> np.ndarray:
    """
    scale_data perform data scaling according to `PHI_IMG_maxRange` header value.

    :param data: imput data
    :type data: np.ndarray
    :param header: header
    :type header: dict
    :return: data
    :rtype: np.ndarray
    """

    try:
        IMGformat = header['PHI_IMG_format'][-1]
    except Exception:
        print("PHI_IMG_format not found: Most likely file does not have 9th Image extension")
        IMGformat = 'IMGFMT_16'
    if IMGformat != 'IMGFMT_24_8':
        print('          INPUT IS DIVIDED by 256.   ')
        data /= 256.
    else:
        print("Dataset downloaded as raw: no bit convert scaling needed")

    try:
        maxRange = header['PHI_IMG_maxRange']
        data *= maxRange[0] / maxRange[-1]
        print("max range ", maxRange[0], maxRange[-1], maxRange[0] / maxRange[-1])
    except Exception:
        print("PHI_IMG_maxRange not found: Most likely file does not have 9th Image extension")
        data *= 81920 / 128
        print("max range ", 81920, 128, 81920 / 128)

    return data


def fits_get_fpatimes(file: str, offset: str = str()) -> Tuple[np.ndarray, str]:
    """
    fits_get_fpatimes get FPA times from header

    This program just returns the time corresponding to the FPA exposures (for wavelength and polarization)
    The time is provided relative to the first frame

    :param file: fits file containing Stokes data
    :type file: str
    :param offset: time offset to add to FPA times, defaults to None
    :type offset: float, optional
    :return: FPA adquisition times and initial time of observation
    :rtype: (np.ndarray,float)
    """

    from datetime import datetime
    fpa_head = 4

    dummy_head = pyfits.getheader(file, 0)
    NAXIS3 = dummy_head['NAXIS3']  # 6 or 24
    ACCCOLIT = dummy_head['ACCCOLIT']  # Cycles

    dummy_head = pyfits.getheader(file, fpa_head)
    if dummy_head['EXTNAME'] != 'PHI_FITS_FPA_settings':
        printc("ERROR .... fpa_head is not number 4 ", bcolors.FAIL)
        raise KeyError("PHI_FITS_FPA_settings not found in header")

    adq_times = np.zeros((NAXIS3), dtype=float)

    with pyfits.open(file) as hdu_list:
        dat_fpa = hdu_list[fpa_head].data

    init = datetime.strptime(dat_fpa[0][1], '%Y-%m-%dT%H:%M:%S.%f')
    for i in range(0, NAXIS3 * ACCCOLIT, ACCCOLIT):
        the_time = datetime.strptime(dat_fpa[i][1], '%Y-%m-%dT%H:%M:%S.%f')
        adq_times[i // ACCCOLIT] = float((the_time - init).total_seconds())
        # print(i,i/ACCCOLIT)

    if offset:
        diff = init - offset
        print('diff: ', adq_times)
        print('diff: ', float(diff.total_seconds()))
        adq_times = adq_times + float(diff.total_seconds())

    return adq_times, init


def list_fits(inpath:str = './', contain: str=None, remove_dir:bool=False,endswith=['.fits','.fits.gz']) -> list:
    """
    list_fits Find all fits or fits.gz files in a given directory.

    :param inpath: directory path, defaults to './'
    :type inpath: str, optional
    :param contain: text string to be searched in the filenames, defaults to None
    :type contain: str, optional
    :param remove_dir: if True remove trailing path, defaults to False
    :type remove_dir: bool, optional
    :return: list of files fulfilling `contains` conditions in `ìnpath` directory
    :rtype: list

    .. warning:: Note that it is directory recursive. If the program is run in a folder with
     multiple directories, it will return all files fulfilling ``contain``.

    Example:

    .. code-block:: python

        import sophi_fdt_dpp as fdt
        files = list_fits(contain = '_fdt_', remove_dir = True)

    """
    assert path.isdir(inpath)

    list_of_files = []
    for (dirpath, dirnames, filenames) in walk(inpath):
        for filename in filenames:
            check_swith = filename.endswith(tuple(endswith))
            if (
                check_swith and filename[:2] != '._'
            ):
                if contain is None:
                    list_of_files.append(dirpath + filename)
                else:
                    if np.ndim(contain) == 0:
                        _, exist = find_string(filename, contain)
                    else:
                        exist = 1
                        for clave in contain:
                            _, check = find_string(filename, clave)
                            if check == -1:
                                exist = -1
                    if exist != -1:
                        if remove_dir:
                            list_of_files.append(filename)
                        else:
                            list_of_files.append(f'{dirpath}/{filename}')
    return list_of_files


def fits_get_sampling(file: str, verbose: bool=False) -> Tuple[np.ndarray,np.ndarray,float,int,float]:
    """
    fits_get_sampling gets sampling from Stokes datafile header

    This program extract the information about the wavelength sampling and voltage of the HPVS from a given header.

    In particular, it outputs:

    * wavelength axis in Anstrong
    * wavelength axis in Volts
    * PHI_FG_setWavelength
    * Tunning constant of the HVPS
    * Position of the continuum in the dataset
    * reference wavelength

    No S/C velocity correction is applied.
    if `cpos = 0` continuum is at first wavelength and if `cpos = 5` the continuum point is at the end.

    Example:

    .. code-block:: python

        import sophi_fdt_dpp as fdt
        wave_axis,voltagesData,PHI_FG_setWavelength,tunning_constant,cpos = fits_get_sampling(file)

    :param file: input Stokes file
    :type file: str
    :param verbose: verbosity, defaults to False
    :type verbose: bool, optional
    :return: tuple with:
        - wavelength axis in Anstrong
        - wavelength axis in Volts
        - Tunning constant of the HVPS
        - Position of the continuum in the dataset: [order, cpos]
        - reference wavelength
    :rtype: (np.ndarray,np.ndarray,float,Tuple,float)
    """

    def __define_continuum_position(volts):
        diff = volts - np.roll(volts,1)
        value = np.argmax(np.abs(diff))
        roll = 0
        if value == 5:
            print("continuum was taken in the BLUE and stored in the RED")
            # CASE A) IF continuum was taken in the BLUE but stored in the RED np.roll([0,1,2,3,4,5], 1) -> [5,0,1,2,3,4]
            roll = 1
            continuum_pos = 0 # NOTE: Indicate the TRUE continuum position but not where it was stored
        if value == 0:
            if np.abs((volts[0]-volts[1])) > np.abs((volts[-1]-volts[-2])):
                print("continuum was taken in the BLUE and stored in the BLUE")
                continuum_pos = 0
                roll = 0
                # CASE B) IF continuum was taken in the BLUE and stored in the BLUE
            if np.abs((volts[0]-volts[1])) < np.abs((volts[-1]-volts[-2])):
                print("continuum was taken in the RED and stored in the RED")
                # CASE C) IF continuum was taken in the RED and stored in the RED
                continuum_pos = 5
                roll = 0
        if value == 1:
            print("continuum was taken in the RED and stored in the BLUE")
            # CASE D) IF continuum was taken in the RED but stored in the BLUE  np.roll([0,1,2,3,4,5], -1) -> [1,2,3,4,5,0]
            roll = -1
            continuum_pos = 5  # NOTE: Indicate the TRUE continuum position but not where it was stored
        return continuum_pos, roll

    def __get_volts(list_of_voltages: list, jump_size: float = 60.):
        list_of_voltages = np.array(list_of_voltages)
        jumps = np.roll(list_of_voltages - np.roll(list_of_voltages,1),-1)
        loop = 0
        voltages = []
        for index,jump_i in enumerate(jumps):
            if np.abs(jump_i) > jump_size:
                voltages.append( np.median(list_of_voltages[index-loop:index+1] ) )
                loop = 0
            else:
                loop += 1
        # assert len(voltages) == 6
        return np.array(voltages)

    print(f'-- Obtaining sampling information from......{file}')
    print('-- Only works for nominal six wavelength scan data. Line scan and PD are not supported yet.')
    fg_head = 3

    with pyfits.open(file) as hdu_list:
        header = hdu_list[fg_head].data

    tunning_constant = float(header[0][4]) / 1e9
    ref_wavelength = float(header[0][5]) / 1e3
    voltagesData = __get_volts([v[2] for v in header])
    PHI_FG_setWavelength = __get_volts([v[3] for v in header])

    if verbose:
        print('     Voltages: ', voltagesData)

    cpos = 0 if np.abs(voltagesData[0] - voltagesData[1]) > np.abs(voltagesData[4] - voltagesData[5]) else 5  #overwritten by the next line
    cpos, roll = __define_continuum_position(voltagesData)

    if verbose:
        print('Continuum position at ',('blue' if cpos else 'red'),'. cpos = ', cpos)
    wave_axis = voltagesData * tunning_constant + ref_wavelength  # 6173.3356
    if verbose:
        print('     Data wave axis [mA]: ', wave_axis)

    return wave_axis, voltagesData, PHI_FG_setWavelength, tunning_constant, (cpos, roll), ref_wavelength


def fits_get_part(file: str, wave: int, npol: int, info=False) -> Tuple[np.ndarray,dict]:
    """
    fits_get_part Helper function to load part of FITS data set

    It calls `fits_get()` and get only a single wavelength and polarization state

    :param file: fits file to load
    :type file: str
    :param wave: wavelength position to extract
    :type wave: int
    :param npol: modulation position to extract
    :type npol: int
    :param info: True if fits information is returned
    :type info: bool
    :return: data and header
    :rtype: (np.ndarray,dict)
    """
    data, header = fits_get(file, head=0, info=info)
    if len(data.shape) == 4:
        return data[wave, npol, :, :], header
    if len(data.shape) == 3:
        return data[wave * 4 + npol, :, :], header

    # try:
    #     with pyfits.open(file, mode='denywrite') as hdu_list:
    #         if info:
    #             hdu_list.info()
    #         data = hdu_list[0].data.astype(np.dtype('float32'))
    #         header = hdu_list[0].header
    #         image = data[wave*4+npol,:,:]
    #     return image,header
    # except Exception:
    #     print("Unable to open fits file: {}",file)

    #     return


def read_shifts(file:str) -> np.ndarray:
    """
    read_shifts read center positions from file

    :param file: file
    :type file: str
    :return: center positions
    :rtype: np.ndarray
    """
    print('Reading ', file)
    return np.loadtxt(file, dtype=np.int_)


def write_shifts(file:str, content: np.ndarray):
    """
    write_shifts write center positions to file

    :param file: file
    :type file: str
    :param file: content
    :type file: np.ndarray
    :return: None
    """
    import os
    dirname = os.path.dirname(file)
    with contextlib.suppress(Exception):
        os.makedirs(dirname)
        print('directory created ', file)

    print('writing ', file)
    np.savetxt(file, content, fmt='%5.0d')

def set_level(file:str, what:str, towhat:str)->str:
    """
    set_level simple string replacement routine

    Replaces something in a text by other string

    :param file: input string
    :type file: str
    :param what: string to be replaced
    :type what: str
    :param towhat: new string
    :type towhat: str
    :return: modified string
    :rtype: str
    """
    _, exist = find_string(file, what)
    if exist != -1:
        return file.replace(what, towhat)


def append_id(file:str, filetype:str, vers:str, DID:str)->str:
    """
    append_id Add ID to PHI fits

    :param file: filename
    :type file: str
    :param filetype: filename extension
    :type filetype: str
    :param vers: version to add
    :type vers: str
    :param DID: file DID
    :type DID: str
    :return: modified filename
    :rtype: str
    """
    ande = file.index(filetype)
    return file.split('_V')[0] + '_V' + vers + '_' + DID + file[ande:]

def find_string(where:str, what:str) -> Tuple[list,int]:
    """
    find_string find a string in an string

    :param where: input string
    :type where: str
    :param what: search string
    :type what: str
    :return: tuple with two strings: position of string and 0 if nothing found
    :rtype: (list,int)
    """
    index = 0
    add = len(what)
    found = []
    nothing = -1
    while index < len(where):
        index = where.find(what, index)
        if index == -1:
            break
        nothing = 1
        found.append(index)
        # print('ll found at', index)
        index += add  # +2 because len('_') == 1
    return found, nothing
