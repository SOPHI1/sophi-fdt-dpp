"""
Generate standard plots for pipeline results
"""

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import glob
import os
import re
from argparse import ArgumentParser
import numpy as np

import sys
sys.path.append('..')
import src as spg
from tools import printc, bcolors

# Parse command line arguments
arg_parser = ArgumentParser(description='Plot pipeline results')
arg_parser.add_argument('path', type=str, metavar='PATH', help='Path to inversion results')
arg_parser.add_argument('--out_path', type=str, metavar='OUTPATH', default=None, help='Output path for PDF files (default is PATH)')
arg_parser.add_argument('--no_mask', action='store_true', help='Do not show mask')
arg_parser.add_argument('--no_offset', action='store_true', help='Do not subtract offsets')
arg_parser.add_argument('--no_inverted', action='store_true', help='Do not process inverted data (Stokes images only)')
args = arg_parser.parse_args()

hmimag = spg.plot_lib.cmap_from_rgb_file('HMI', 'hmi_mag.csv')  # color map

# Determine number of datasets in the path
prefix = 'solo_L2_phi-fdt-'
files = glob.glob(os.path.join(args.path, f'{prefix}icnt_*.fits*'))
n_datasets = len(files)
printc(f'Found {n_datasets} datasets', color=bcolors.OKBLUE)

if args.out_path is None:
    args.out_path = args.path

for file in files:
    pattern = re.findall(prefix + r'icnt_(.*)\.fits.*$', os.path.basename(file))[0]
    save_file = os.path.join(args.out_path, f'{prefix}{pattern}.pdf')
    printc(f'    Prepare {save_file}', color=bcolors.OKBLUE)
    p = PdfPages(save_file)

    # -----------------------------------------------------------------------------
    # Plot inversion results
    # -----------------------------------------------------------------------------

    dat = {}
    keys = ['icnt', 'vlos', 'blos', 'binc', 'bmag', 'bazi', 'chi2']
    for key in keys:
        datfile = glob.glob(os.path.join(args.path, f'{prefix}{key}_{pattern}.fits.*'))[0]
        printc(f'        Read {os.path.basename(datfile)}', color=bcolors.OKBLUE)
        dat[key], header = spg.fits_get(datfile, scale=False)

    # Get disc center and radius from header
    cx = header['CRPIX1']
    cy = header['CRPIX2']
    radius = header['RSUN_ARC'] / header['CDELT1']

    # Create mask for disc
    mask = np.linalg.norm(np.indices(dat['icnt'].shape) - np.full(list(dat['icnt'].shape) + [2], (int(cy), int(cx))).transpose(), axis=0) < radius

    def plot_mask_border(ax, cx, cy, radius):
        mask_border = plt.Circle((cx, cy), radius, fill=False, color='white', ls='--', lw=1.0)
        ax.add_artist(mask_border)

    # Plot parameters
    panel_sz = 4
    dpi = 80
    rows = 2
    columns = 3

    fig, axs = plt.subplots(
        rows, columns,
        sharey=True,
        subplot_kw={'aspect': 1},
        figsize=(columns * panel_sz + 2, rows * panel_sz), dpi=dpi,
        layout='constrained')

    # Continuum intensity
    ax = axs[0, 0]
    im = ax.imshow(dat['icnt'], cmap='gist_heat', interpolation='none', vmin=0.2, vmax=1.2)
    spg.colorbar(im)
    ax.set_title('Continuum intensity')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # vLOS
    if not args.no_offset:
        mean_val = np.mean(dat['vlos'][mask])
        dat['vlos'][mask] -= mean_val
    ax = axs[0, 1]
    im = ax.imshow(dat['vlos'], cmap=hmimag, interpolation='none', vmin=-1.5, vmax=1.5)
    spg.colorbar(im, label='km/s')
    ax.set_title('LoS velocity')
    if not args.no_offset:
        ax.text(0.05, 0.94, f'Offset of {mean_val:.3f} km/s subtracted', transform=ax.transAxes, color='black')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # BLOS
    if not args.no_offset:
        mean_val = np.mean(dat['blos'][mask])
        dat['blos'][mask] -= mean_val
    ax = axs[0, 2]
    im = ax.imshow(dat['blos'], cmap=hmimag, interpolation='none', vmin=-50, vmax=50)
    spg.colorbar(im, label='G')
    ax.set_title('LoS magnetic field')
    if not args.no_offset:
        ax.text(0.05, 0.94, f'Offset of {mean_val:.3f} G subtracted', transform=ax.transAxes, color='black')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # B inclination
    ax = axs[1, 0]
    im = ax.imshow(dat['binc'], cmap='seismic', interpolation='none', vmin=80, vmax=100)
    spg.colorbar(im, label='°')
    ax.set_title('Magn. field inclination')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # B
    grayscale = (0, 500)
    if not args.no_offset:
        mean_val = np.mean(dat['bmag'][mask])
        dat['bmag'][mask] -= mean_val
        grayscale = (-50, 50)
    ax = axs[1, 1]
    im = ax.imshow(dat['bmag'], cmap='gnuplot_r', interpolation='none', vmin=grayscale[0], vmax=grayscale[1])
    spg.colorbar(im, label='G')
    ax.set_title('Magn. field strength')
    if not args.no_offset:
        ax.text(0.05, 0.94, f'Offset of {mean_val:.3f} G subtracted', transform=ax.transAxes, color='black')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # B azimuth
    ax = axs[1, 2]
    im = ax.imshow(dat['bazi'], cmap='hsv', interpolation='none', vmin=0, vmax=180)
    spg.colorbar(im, label='°')
    ax.set_title('Magn. field azimuth')
    if not args.no_mask:
        plot_mask_border(ax, cx, cy, radius)

    # Figure title
    fig.suptitle(f'{prefix}{pattern}', fontsize=14)

    fig.savefig(p, format='pdf')

    # Chisq
    fig, ax = plt.subplots(
        subplot_kw={'aspect': 1},
        figsize=(panel_sz + 2, panel_sz), dpi=dpi,
        layout='constrained')
    im = ax.imshow(dat['chi2'], cmap='turbo', interpolation='none', vmin=0, vmax=50)
    spg.colorbar(im)
    ax.set_title('$\chi^2$')

    fig.savefig(p, format='pdf')

    # -----------------------------------------------------------------------------
    # Plot Stokes images
    # -----------------------------------------------------------------------------

    datfile = glob.glob(os.path.join(args.path, f'{prefix}stokes_{pattern}.fits.*'))[0]
    printc(f'        Read {os.path.basename(datfile)}', color=bcolors.OKBLUE)
    dat, _ = spg.fits_get(datfile)
    wavelengths = spg.fits_get_sampling(datfile)[0]

    dat = np.transpose(dat, (1, 0, 2, 3))  # re-arrange Stokes and wavelength axes

    grayscales = [1] + [0.01] * 3  # I, Q, U, V
    row_labels = ['I', 'Q', 'U', 'V']
    column_labels = ['{:.3f} nm'.format(wave) for wave in wavelengths]
    title = os.path.basename(datfile)

    fig = spg.show_image_array(
        dat, grayscales, row_labels=row_labels,
        column_labels=column_labels, fig_title=title)

    fig.savefig(p, format='pdf')

    p.close()

