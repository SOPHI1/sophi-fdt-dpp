"""
Functions for working with solar disk images: registration, masking, shifting, mean across disk ...
Adopted from fdt_tools/fdt_disk.py by Alex Feller
"""

import numpy as np
import matplotlib.pyplot as plt
from itertools import combinations
from scipy.signal import savgol_filter


def simple_shift(xs, shift=0, fill_value=0):
    """define shift operator"""
    e = np.empty_like(xs)
    if shift > 0:
        e[:shift] = fill_value
        e[shift:] = xs[:-shift]
    elif shift < 0:
        e[shift:] = fill_value
        e[:shift] = xs[-shift:]
    else:
        e = xs
    return e


def find_center(im, sjump=10, njumps=50, threshold=0.9):
    ys, xs = im.shape
    jumps = np.linspace(-sjump * njumps // 2, sjump * njumps // 2, njumps - 1)
    rf = np.array([], dtype=float)
    xc = np.array([], dtype=float)
    yc = np.array([], dtype=float)
    for [i, j] in combinations(jumps, 2):  # overall 36 combinations
        xi = xs // 2 - int(i)  # 570#1024
        yi = ys // 2 - int(j)  # 610#1024
        xcut = im[:, yi]
        ycut = im[xi, :]
        xcut = savgol_filter(xcut, 5, 3)  # window size 51, polynomial order 3
        ycut = savgol_filter(ycut, 5, 3)  # window size 51, polynomial order 3

        # calculate derivative
        xcut_d = xcut - simple_shift(xcut, shift=10)
        ycut_d = ycut - simple_shift(ycut, shift=10)
        xcut_d[:5] = 0
        ycut_d[:5] = 0
        xcut_d[-5:] = 0
        ycut_d[-5:] = 0

        # meter condicion de aumentar threshold si hay muchos o pocos/ninguno
        indices_x_max = np.asarray(
            np.where(xcut_d > xcut_d.max() * threshold)
        ).flatten()
        indices_x_min = np.asarray(
            np.where(xcut_d < xcut_d.min() * threshold)
        ).flatten()
        indices_y_max = np.asarray(
            np.where(ycut_d > ycut_d.max() * threshold)
        ).flatten()
        indices_y_min = np.asarray(
            np.where(ycut_d < ycut_d.min() * threshold)
        ).flatten()

        x1 = (
            np.mean(indices_x_max * xcut_d[indices_x_max])
            / np.mean(xcut_d[indices_x_max])
            - 5
        )
        x2 = (
            np.mean(indices_x_min * xcut_d[indices_x_min])
            / np.mean(xcut_d[indices_x_min])
            - 5
        )
        y1 = (
            np.mean(indices_y_max * ycut_d[indices_y_max])
            / np.mean(ycut_d[indices_y_max])
            - 5
        )
        y2 = (
            np.mean(indices_y_min * ycut_d[indices_y_min])
            / np.mean(ycut_d[indices_y_min])
            - 5
        )

        x0 = (x1 + x2) / 2
        y0 = (y1 + y2) / 2
        r1 = np.sqrt((y0 - yi) * (y0 - yi) + (x0 - x1) * (x0 - x1))
        r2 = np.sqrt((y0 - y1) * (y0 - y1) + (x0 - xi) * (x0 - xi))
        r = (r1 + r2) / 2

        rf = np.append(rf, r)
        xc = np.append(xc, x0)
        yc = np.append(yc, y0)

    rf_m = np.mean(
        rf[np.where((rf < (np.median(rf) + 1)) & (rf > (np.median(rf) - 1)))]
    )
    xc_m = np.mean(
        xc[np.where((xc < (np.median(xc) + 1)) & (xc > (np.median(xc) - 1)))]
    )
    yc_m = np.mean(
        yc[np.where((yc < (np.median(yc) + 1)) & (yc > (np.median(yc) - 1)))]
    )

    print(xc_m, yc_m, rf_m)

    return xc_m, yc_m, rf_m


def get_disk_mask(im, show=False):
    mask_rad, ctr, rad = get_mask_rad(im)
    mask = mask_rad < rad

    if show:
        plot_mask(mask, ctr, rad)

    return mask


def get_ring_mask(im, ring_size, show=False):
    mask_rad, ctr, rad = get_mask_rad(im)
    mask = (mask_rad > rad - ring_size / 2) & (mask_rad < rad + ring_size / 2)
    if show:
        plot_mask(mask, ctr, rad, ring_size)
    return mask


def get_mask_rad(im):
    out = find_center(im)
    ctr = out[0:2]
    rad = out[2]
    mask_rad = np.linalg.norm(np.indices(im.shape) - np.full(list(im.shape)+[2], ctr).transpose(), axis=0)
    return mask_rad, ctr, rad


def plot_mask(mask, ctr, rad, ring_size=0):
    im_mask = mask * 0
    im_mask[mask] = 1
    fig, ax = plt.subplots()
    ax.imshow(im_mask, cmap='gray')
    circle_inner = plt.Circle((ctr[1], ctr[0]), rad - ring_size/2, fill=False, color='red', ls='--', lw=1.0)
    circle_outer = plt.Circle((ctr[1], ctr[0]), rad + ring_size/2, fill=False, color='red', ls='--', lw=1.0)
    ax.add_artist(circle_inner)
    ax.add_artist(circle_outer)


def get_disk_mean(im, mask=None):
    if mask is None:
        mask = get_disk_mask(im)
    return im[mask>0].mean()
