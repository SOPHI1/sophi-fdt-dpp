"""
``phifdt_flat.py`` contains the pipeline implementation for calculating the FDT flat field.
It includes three different algorithms for flat calculations and to algorithms for
finding the disc center coordinates: the circular Hough Transform in the frequency domain and
a specially tailored simple circle fit program.

Main program is called ``fdt_flat``

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez**
   :Contributors: Nestor Albelo (albelo@mps.mpg.de) and Alex Feller (feller@mps.mpg.de)

.. list-table::
   :widths: 25 50
   :header-rows: 1

   * - program
     - Short summary
   * - :py:meth:`Neckel`
     - dot product between two vectors
   * - :py:meth:`clv`
     - angle between two vectors
   * - :py:meth:`azimuthal_mask`
     - angle between two vectors using tan()
   * - :py:meth:`fit_clv`
     - cross product
   * - :py:meth:`centers_flat`
     - spherical coordinates to cartesian
   * - :py:meth:`do_hough`
     - cartesian to spherical coordinates
   * - :py:meth:`fdt_flat_gen`
     - cartesian to polar coordinates
   * - :py:meth:`fdt_flat`
     - polar coordinates to cartesian
   * - :py:meth:`fdt_flat_preprocessing`
     - azimuthal average of an image

"""

import itertools
from typing import Tuple
from scipy.ndimage import gaussian_filter

import numpy as np
from matplotlib import pyplot as plt
from .phi_fits import *
from .phi_reg import *
from .phi_utils import *
from .phifdt_pipe_modules import (
    phi_correct_dark,
    phi_correct_ghost,
    phi_correct_prefilter,
    phi_correct_distortion,
    phi_correct_fringes,
    check_pmp_temp,
    phi_apply_demodulation,
)
from .phifdt_disk import find_center


def Neckel(r: float) -> np.ndarray:
    """
    This functions provides de Neckel center to limb intensity variation *CLV* at 625 nm

    Provided r (solar radius) the program estimates the heliocentric angle :math:`{\\mu}`

    .. math:: \mu = \sqrt{ 1 - r^2/r_\odot^2 } = \cos{\\theta}
        :label: eqn9
        :name: eq:9

    The *CLV* is given by:

    .. math:: I = I_0  ( 1 - \sum_{k=1}^{n} a_k \\mu^k)
        :label: eqn10
        :name: eq:10

        where

        \\mu = \cos{\\theta}

    :param r: the radius of the solar disk
    :type r: float
    :return: center to limb variation
    :rtype: np.ndarray

    """

    r_vector = np.arange(0, np.max(r), 1)
    mu = np.sqrt((1 - r_vector**2 / r**2))

    f1 = 0.31414418403067174
    f2 = 1.3540877312885482
    f3 = -1.827014432405401
    f4 = 2.355950448408068
    f5 = -1.6848471461910317
    f6 = 0.48767921486914473

    return f1 + f2 * mu + f3 * mu**2 + f4 * mu**3 + f5 * mu**4 + f6 * mu**5


def clv(r: float, r_val: float = 0, coefficients: list = []) -> np.ndarray:
    """
    This functions provides the center to limb intensity variation *CLV* model for PHI FDT data

    Provided r (solar radius) the program estimates the heliocentric angle :math:`{\\mu}`

    If the user does not provide coefficients, it takes default parameters for PHI FDT data
    coefficients = [1., 0.44984082, 0.11827295, 0.05089393]

    .. math:: \mu = \sqrt{ 1 - r^2/r_\odot^2 } = \cos{\\theta}
        :label: eqn11
        :name: eq:11

    The *CLV* is given by:

    .. math:: I = I_0  ( 1 - \sum_{k=1}^{n} a_k  \\mu^k)
        :label: eqn12
        :name: eq:12

        where

        \\mu = \cos{\\theta}

    :param r: the radius of the solar disk
    :type r: float
    :param r_val: if different from zero, returns the value of the *CLV* at the given solar radius, defaults to zero
    :type r_val: float [in pixel units
    :param coefficients: the coefficients of the CLV function
    :type coefficients: float np.ndarray
    :return: center to limb variation
    :rtype: np.ndarray or float
    """
    # TODO homogenize this function with limb_darkening and newton.
    with contextlib.suppress(Exception):
        if not coefficients:
            coefficients = [
                1.0,
                0.44984082,
                0.11827295,
                0.05089393
                ]  # PHI values with n = 4 and solar radius of 789

    r_vector = np.arange(0, np.max(r), 1) if r_val == 0 else r_val

    mu = 1 - np.sqrt((1 - r_vector**2 / r**2))

    f = sum(coefficients[i + 1] * mu ** (i + 1) for i in range(len(coefficients) - 1))

    return np.array(coefficients[0] * (1 - f))


def azimuthal_mask(
    sx: int, sy: int, center: Tuple[int, int], r: float, coefficients: list = []
    ) -> np.ndarray:
    """
    This functions provides a CLV mask given the dimensions and the center and radius

    :param sx: x image dimensions
    :type sx: int
    :param sy: y image dimensions
    :type sy: int
    :param center: center (xc,yc) is a tuple with the center of the disk
    :type center: tuple(xc,yx)
    :param r: the radius of the solar disk
    :type r: int
    :param coefficients: the coefficients of the CLV function [optional]
    :type coefficients: float np.ndarray
    :return: a 2D image constructed from CLV parameters
    :rtype: np.ndarray

    *Example*

    .. code-block:: python

        mask = azimuthal_mask(2048,2048,[996,1245],786)

    """

    x_axis = np.arange(0, sx, 1)
    y_axis = np.arange(0, sy, 1)
    x_axis = x_axis - center[0] + 0.5  # if dimensions are odd add 0.5 for center
    y_axis = y_axis - center[1] + 0.5
    mask = np.zeros((sy, sx))
    for i, j in itertools.product(range(sx), range(sy)):
        pxy = np.sqrt(x_axis[i] ** 2 + y_axis[j] ** 2)
        if pxy < r:
            mask[int(j), int(i)] = clv(r, r_val=pxy, coefficients=coefficients)
    return mask


def fit_clv(data: np.ndarray, centers: list, verbose: bool = False) -> np.ndarray:
    """
    Fits the center to limb o a 2D solar image using the Newton's method

    :param data: input image. Shall be a 2D image with the full Sun
    :type data: np.ndarray
    :param centers: center of image [x,y]
    :type centers: tuple(integer)
    :param verbose: verbose
    :type verbose: bool
    :return: an array with the coefficients of the CLV fit to use with ``clv.py``
    :rtype: np.ndarray
    """
    intensity, radius = azimutal_average(data, [centers[0], centers[1]])
    yd, xd = data.shape

    ints = np.zeros(int(np.sqrt(xd**2 + yd**2)))  # vector of sqrt(xd^^+yd^^)
    ints_rad = np.zeros(int(np.sqrt(xd**2 + yd**2)))

    ints[: len(intensity)] = intensity
    ints_rad[: len(intensity)] = radius

    # STEP --->>> FIT LIMB DATA
    der = np.roll(intensity, 1) - intensity  # simple derivative
    idx_max = np.where(der == np.max(der))  # look for peak position
    clv = intensity[: int(idx_max[0]) + 1]
    clv_rho = radius[: int(idx_max[0]) + 1]
    mu = np.sqrt((1 - clv_rho**2 / clv_rho[-1] ** 2))

    first_coefficient = 0.5
    i0 = 100
    locations = np.where(mu > 0.1)
    pars = [i0, first_coefficient, 0.2, 0.2, 0.2]
    coefficients = newton(clv[locations], mu[locations], pars, limb_darkening)

    if verbose:
        # TODO: clean ints_fit, etc... many unused parameters
        plt.plot(clv_rho, clv)
        plt.xlabel("Solar radius [pixel]")
        plt.ylabel("Intensity [DN]")
        plt.show()

        ints_fit = np.zeros(int(np.sqrt(xd**2 + yd**2)))
        ints_syn = np.zeros(int(np.sqrt(xd**2 + yd**2)))
        ints_fit_pars = np.zeros(len(pars))

        fit, _ = limb_darkening(mu, coefficients.tolist())
        ints_fit[: len(fit)] = fit
        ints_fit_pars[:] = coefficients

        ints_syn = np.copy(ints)
        ints_syn[: len(fit)] = fit

        # STEP --->>> NORMALIZE
        ints_syn = ints_syn / ints_fit_pars[0]
        ints_fit = ints_fit / ints_fit_pars[0]
        ints = ints / ints_fit_pars[0]

        plt.plot(ints_fit, label="fitted clv")
        plt.plot(ints, ".", label="real clv")
        plt.plot(ints_syn, "--", label="synt clv")
        plt.xlabel("Heliocentric angle [" + r"$\theta$]")
        plt.ylabel("Intensity [DN]")
        plt.legend()
        plt.show()

    print("CLV coefficients:", coefficients)

    return coefficients


def centers_flat(
    n_images: int,
    inner_radius: int,
    outer_radius: int,
    steps: int,
    r_width: int,
    binmask: list,
    imsize: tuple,
    verbose=None):
    """
    Main routine of Hough transform function

    :param verbose:
    :param n_images: number of images
    :type n_images: int
    :param inner_radius: smallest radius for votes
    :type inner_radius: int
    :param outer_radius: largest radius for votes
    :type outer_radius: int
    :param steps: number of subdivisions [+1]  between inner and outer radius
    :type steps: int
    :param r_width: width of the ring for cross-correlating with circle
    :type r_width: int
    :param binmask: input image
    :type binmask: list of images
    :param imsize: image size
    :type imsize: tuple(int)
    :return: the center and radius of the image calculated using Hough
    :rtype: tuple(float)
    """

    centers = []
    radius = []
    radii = np.linspace(inner_radius, outer_radius, steps + 1)
    printc("Analyzing ", n_images, " images", color=bcolors.OKGREEN)

    for i in range(n_images):
        # acc_conv = find_Circles(
        #    binmask[i], radii_coarse, r_width_coarse, verbose=verbose, full=True)
        acc_conv = find_Circles_ida(binmask[i], radii, r_width)
        center, rad, c, d = votes(acc_conv, radii)

        centers.append(center)
        radius.append(rad)
        printc(
            "Found center: ",
            centers[i],
            " and radius: ",
            radius[i],
            color=bcolors.WARNING)
        if verbose:
            plt.figure(frameon=False)
            plt.imshow(binmask[i], cmap=plt.cm.gray, alpha=0.5)
            circle_fit = bin_annulus(imsize, radius[i], 1, full=False).astype(float)
            dd = np.array(centers[i])
            dx = dd[0] - imsize[0] // 2
            dy = dd[1] - imsize[1] // 2
            circle_fit = shift(circle_fit, shift=[dx, dy])
            plt.imshow(circle_fit, cmap=plt.cm.gray, alpha=0.5)
            plt.show()

    return centers, radius


def do_hough(
    image: np.ndarray,
    inner_radius: int,
    outer_radius: int,
    steps: int,
    org_centers: np.ndarray = None,
    method: str = "prewitt",
    save: bool = False,
    loop_threshold: int = 10,
    normalize: bool = False,
    verbose: bool = False,
    otsu: bool = False,
    threshold: float = 0.15):
    """
    Calculates the position and radius of the solar disk in a set of input images using the Hough transform.
    The method fits a circle to the solar limb using the Hough transform.
    Internally, the Hough tranform is based of FFT for the *voting* part as described in [Hollitt2013]_.

    To propoerly use this program the user needs to set up the ``step`` parameter, which, in principle,
    corresponds to the number of steps to look for the solar radius. In particular:

        #. ``step`` represents the number of coarse find **jumps**: ``np.linspace(inner_radius, outer_radius, steps)``
        #. ``step`` is also the width of the ring for cross-correlating the solar disk: ``(outer_radius - inner_radius)//steps * 2``

    If ``step`` is a negative number then uses the SO/PHI Flight Model limb finding strategy:


        * Total of 4 iterations

            #. inner_radius = 152;  outer_radius = 1048; steps = 64; 15 iterations
            #. inner_radius = Prev.Radius-32;  outer_radius = Prev.Radius+32; steps = 16; 5 iterations
            #. inner_radius = Prev.Radius-8;  outer_radius = Prev.Radius+8; steps = 4; 5 iterations
            #. inner_radius = Prev.Radius-2;  outer_radius = Prev.Radius+2; steps = 1; 5 iterations

    :param image: imput data. np.array of dimension [n, dy, dx] where n is the number of images of dyxdx size.
    :type image: np.ndarray
    :param inner_radius: Minimum search radious
    :type inner_radius: int
    :param outer_radius: Maximum search radious
    :type outer_radius: int
    :param steps: Number of steps to look for solar radius.
    :type steps: int
    :param org_centers: numpy array [n,2] centers for comparison (deprecatted)
    :type org_centers: np.ndarray, optional
    :param method: method for finding the solar limb boundary. Defaults to "prewitt".
        More information can be found in :py:meth:`FindEdges`.
    :type method: str, optional
    :param save: save the centers as 'hough_centers.txt' -> ASCII (centers_fine,radii_fine), defaults to False
    :type save: bool, optional
    :param loop_threshold: Iterations for finetuning the threshold level in :py:meth:`FindEdges`, defaults to 10
    :type loop_threshold: float, optional
    :param normalize: Normalize images (using a box of 100x100 px in the central area), defaults to False
    :type normalize: bool, optional
    :param verbose: verbosity level, defaults to False
    :type verbose: bool, optional
    :param otsu: Activate Otsu method in :py:meth:`FindEdges` module, defaults to False
    :type otsu: bool, optional
    :param threshold: Threshold factor for :py:meth:`FindEdges`, defaults to 0.15
    :type threshold: float, optional
    :return: Tuple with (centers, radius)
        centers : numpy int array of [n,2] elements where [i,0] = x-centers and [i,1] = y-centers
        radious : numpy int array of [n] elements containing the radious of the n-images in pixels
    :rtype: Tuple

    .. [Hollitt2013]
        Hollitt, C. A convolution approach to the circle Hough transform for arbitrary radius.
        Machine Vision and Applications 24, 683-694 (2013). `https://doi.org/10.1007/s00138-012-0420-x <https://doi.org/10.1007/s00138-012-0420-x>`_

    """

    imsize = image[0].shape
    n_images = len(image)
    if org_centers is None:
        org_centers = np.tile(np.array([0.0, 0.0], dtype=np.int16), (n_images, 1))

    ############################
    # Normalize images (using a box 100x100 in the central image)
    ############################

    if normalize:
        norma = np.mean(
            image[0][
                imsize[0] // 2 - 100 : imsize[0] // 2 + 100,
                imsize[0] // 2 - 100 : imsize[0] // 2 + 100]
        )
        if verbose:
            print(
                "Normalization constant: ",
                norma,
                "[calculated with first image assumed to be central one]")

        for i in range(n_images):
            image[i] = image[i] / norma

    image_dummy, threshold = FindEdges(
        image[0],
        threshold,
        method=method,
        loop_threshold=loop_threshold,
        verbose=verbose,
        Otsu=otsu)
    binmask = [image_dummy]
    for i in range(1, n_images):
        image_dummy = FindEdges(
            image[i], threshold, method=method, verbose=verbose, Otsu=otsu
        )
        binmask.append(image_dummy)

    # Mask to prevent edges
    circle_mask, _ = generate_circular_mask([imsize[0] - 1, imsize[1] - 1], 1020, 1020)
    for i in range(n_images):
        binmask[i] = binmask[i] * circle_mask

    if steps > 0:
        #############################
        # FIND CENTERS - COARSE SEARCH
        #############################
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))

        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            printc(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i],
                ),
                color=bcolors.FAIL)
        ###########################
        # FIND CENTERS - FINE SEARCH
        ###########################
        mean_r = np.int(np.mean(radius))
        inner_radius = mean_r - 32
        outer_radius = mean_r + 32
        steps = 16
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))

        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            printc(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i],
                ),
                color=bcolors.FAIL)
        ################################
        # FIND CENTERS - VERY FINE SEARCH
        ################################
        mean_r = np.round(np.mean(radius),0)
        inner_radius = mean_r - 4
        outer_radius = mean_r + 4
        steps = 8
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))

        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            printc(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i],
                ),
                color=bcolors.FAIL)
    elif steps < 0:
        ##################################
        # FIND CENTERS - FM SEARCH STRATEGY
        ##################################
        inner_radius = 128
        outer_radius = 1024
        steps = 32
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))
        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            print(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i])
            )
        mean_r = np.round(np.mean(radius),0)
        inner_radius = mean_r - 32
        outer_radius = mean_r + 32
        steps = 16
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))
        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            print(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i])
            )
        mean_r = np.round(np.mean(radius),0)
        inner_radius = mean_r - 8
        outer_radius = mean_r + 8
        steps = 8
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))
        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            print(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i])
            )
        mean_r = np.round(np.mean(radius),0)
        inner_radius = mean_r - 2
        outer_radius = mean_r + 2
        steps = 4
        r_width = (outer_radius - inner_radius) // steps * 2
        print(np.linspace(inner_radius, outer_radius, steps + 1))
        printc(
            "from: ",
            inner_radius,
            " to: ",
            outer_radius,
            " steps: ",
            steps,
            " width: ",
            r_width,
            color=bcolors.OKGREEN)
        centers, radius = centers_flat(
            n_images,
            inner_radius,
            outer_radius,
            steps,
            r_width,
            binmask,
            imsize,
            verbose=verbose)
        print("Image |   Original  |  Inferred   |   Radius")
        for i in range(n_images):
            print(
                "  %2.0f  | (%4.0f,%4.0f) | (%4.0f,%4.0f) |  %6.2f"
                % (
                    i,
                    org_centers[i, 0],
                    org_centers[i, 1],
                    centers[i][1],
                    centers[i][0],
                    radius[i])
            )
    else:
        print("NO HOUGH **** WRONG")

    if save:
        write_shifts("hough_centers.txt", (centers, radius))

    return centers, radius


def fdt_flat_gen(
    image: list,
    rel_centers: list,
    method: str,
    radious: int = 0,
    mask_threshold: float = 0.05,
    iter: int = 15,
    bit_trun: int = 0,
    verbose: int = 0,
    expand: int = 0,
    c_term: np.ndarray = np.zeros(shape=(), dtype=float),
    clv: float = 0,
    normalize: list = [0],
    SIGMA_NORMA: float = 5.0,
    flat_images: bool = False):
    """
    Main routine for generating a flatfield from n input displaced images. It can use three different methods:

    - The Khun-Lin-Lorantz algorithm [KLL]_. Default method.
    - The Chae algorithm [Chae]_. Works as KLL but the masking of ARs in the solar data is still to be implemented.
    - The Caron et al. algorithm [Caron]_ . This one is still not working propoerly.

    :param image: list containing the n displaced images [y,x].
    :type image: list
    :param rel_centers: list containing the n centers [n_images,2] where [:,0]=dx and [:,1]=dy. Displacements are given with respect to image origin (0,0).
    :type rel_centers: list
    :param method: string containing the method ``kll`` , ``chae``, or ``Caron``, defaults to ``kll``.
    :type method: str
    :param radious: radious of circular mask. In ``radius = 0``, the code uses the ``mask_threshold`` to create the masks. Defaults to 0.
    :type radious: int
    :param mask_threshold:  If ``radius = 0`` (or not given) the mask is determined taking all values of the data above ``mask_threshold``.
      In this case, ``expand``  and ``normalization`` are ignored. defaults to 0.
    :type mask_threshold: float, optional
    :param iter: [``kll``,``chae``] iterations, defaults to 4
    :type iter: int, optional
    :param verbose: vervosity, defaults to False
    :type verbose: int, optional
    :param expand: :mask expand parameter. Reduce the radious of the circular mask by ``expand`` pixels, defaults to 1
    :type expand: int, optional
    :param c_term: initial ``c_term`` values for the ``chae`` method. ``c_term`` stands for the normalization factor of all the n images. Defaults to 0
    :type c_term: np.ndarray, optional
    :param normalize: Normalization option for the n images prior to flat calculation. should be a list (combined normalizations, e.g.,[1], [1,3]). Defaults to [0]
    :type normalize: list, optional

        * 0: Do nothing.
        * 1: Normalize images
        * 2: Normalize log images
        * 3: Normalize the image differences within KLL which have no counterparts.
    :param clv: Threshold for masking active regions in the data, defaults to False

        * ``clv = 0`` : Do nothing.
        * ``clv > 0`` : flatten the solar images using Allen tabulated center to limb variation and mask the ARs using ``clv`` value (assumes images are normalized to 1).
        * ``clv < 0`` : Same as before but fits the center to limb variation using the Newton method.
    :type clv: bool, optional
    :param flat_images: if ``clv`` is set and ``True`` , the images are flattened, defaults to False
    :type flat_images: bool, optional
    :param SIGMA_NORMA: sigma to remove outliers when calculating the different normalizations, defaults to 5.
    :type SIGMA_NORMA: float, optional
    :return: gain and normalization of each image
    :rtype: (np.ndarray,np.ndarray)

    .. [KLL]
        J. R. Kuhn, H. Lin, and D. Loranz,  PASP 103, 1097-1108, 1991. `doi:10.1086/132932 <https://iopscience.iop.org/article/10.1086/132932>`_

    .. [Chae]
        J. Chae, Solar Physics 221: 1-14, 2004. `doi:10.1023/B:SOLA.0000033357.72303.89 <https://doi.org/10.1023/B:SOLA.0000033357.72303.89>`_

    .. [Caron]
        James N. Caron, Marcos J. Montes, and Jerome L. Obermark, Review of Scientific Instruments 87, 063710 (2016); `doi: 10.1063/1.4954730 <https://aip.scitation.org/doi/10.1063/1.4954730>`_

    """

    imsize = image[0].shape
    n_images = len(image)

    # NOTE: We assume it is list
    if not (isinstance(normalize, list)):
        normalize = [normalize]

    def _do_normalization(
        array: np.ndarray,
        mask: np.ndarray,
        type: str = "zero",
        SIGMA_NORMA: float = 5.0,
        method: str = "mean") -> tuple:
        """
        Normalize data to be around zero or one taking data within mask and using the mean or median.

        :param array: input data
        :type array: np.ndarray
        :param mask: binary mask with valid points.
        :type mask: np.ndarray.
        :param type: *zero* or *one* , defaults to *zero*
        :type type: str, optional
        :param SIGMA_NORMA: sigma to remove outliers, defaults to 5.
        :type SIGMA_NORMA: float, optional.
        :param method: *mean* or *median* , defaults to *mean*
        :type method: str, optional
        :return: normalized array
        :rtype: np.ndarray
        """

        idx = np.where(mask > 0)
        s = array[idx]
        sm = 0
        sm2 = 0
        #  calculate average of gain table for normalization
        if method == "mean":
            sm = np.mean(s)
            sm2 = np.mean(s**2)
            five_sigma = SIGMA_NORMA * np.sqrt(sm2 - sm * sm)
            idx2 = np.where(np.abs(s - sm) < five_sigma)
            sm = np.mean(s[idx2])
        if method == "median":
            sm = np.median(s)
            sm2 = np.median(s**2)
            five_sigma = SIGMA_NORMA * np.sqrt(sm2 - sm * sm)
            idx2 = np.where(np.abs(s - sm) < five_sigma)
            sm = np.median(s[idx2])
        if type == "zero":
            array = array - sm2
        if type == "one":
            array = array / sm

        return array, sm
    ############################
    # set displacements of observed images (A) with respect Object image (centered)
    ############################
    xyshifts = np.empty([n_images, 2], dtype=int)
    xyshifts[:, 0], xyshifts[:, 1] = (
        rel_centers[:n_images, 0] - imsize[0] // 2,
        rel_centers[:n_images, 1] - imsize[1] // 2)

    ############################
    # calculate masks
    ############################
    mask = np.zeros([n_images, imsize[0], imsize[1]], dtype=np.int8)

    if radious != 0:  # In case radius of solar disk is provided....
        maskn, dummy = generate_circular_mask(
            [imsize[0] - 1, imsize[1] - 1], radious - expand, radious - expand)
        printc("Using circular mask", color=bcolors.OKGREEN)
        for i in range(n_images):
            mask[i] = shift(maskn, shift=[xyshifts[i, 0], xyshifts[i, 1]], fill_value=0)
    else:
        # find pixel coordinates with solar information (> mask_threshold given by user, default = 0.05)
        # This step assumes input data has a mean value of one.
        printc(
            "Setting mask depending n threshold level",
            mask_threshold,
            color=bcolors.OKGREEN)
        for i in range(n_images):
            x, y = np.where(image[i] > mask_threshold)
            mask[i][x, y] = 1

    circle_mask, _ = generate_circular_mask([imsize[0] - 1, imsize[1] - 1], 1020, 1020)
    for i in range(n_images):
        mask[i] = mask[i] * circle_mask

    ############################
    # NORMALIZATION Option 1
    ############################
    norma = np.zeros(n_images)
    if (1 in normalize) and radious != 0:
        mask_norma, dummy = generate_circular_mask(
            [imsize[0] - 1, imsize[1] - 1], round(radious * 0.3), round(radious * 0.3))
        printc("Using circular mask", color=bcolors.OKGREEN)
        for i in range(n_images):
            mask_norma_shifted = shift(
                mask_norma, shift=[xyshifts[i, 0], xyshifts[i, 1]], fill_value=0)
            mask_norma_shifted *= circle_mask
            image[i], norma[i] = _do_normalization(
                image[i], mask_norma_shifted, type="one", SIGMA_NORMA=SIGMA_NORMA)
            printc("Normalization image ", i, ": ", norma[i], color=bcolors.YELLOW)
            # image[i] = image[i] - gaussian_filter(image[i], sigma=(64, 64))  #BBT TODO TODO TODO FIX
            # debug("i",msg="Image in normalization")

    ############################
    # NORMALIZATION Option 4 - blurr imput images
    ############################
    if 4 in normalize:
        sigma_n3 = 32
        for image_loop, index in zip(image, range(len(image))):
            mask_norma3, _ = generate_circular_mask(
                [imsize[0] - 1, imsize[1] - 1], radious, radious)
            mask_norma3 = shift(
                mask_norma3,
                shift=[xyshifts[index, 0], xyshifts[index, 1]],
                fill_value=0)
            mask_norma3 *= circle_mask
            filtered = gaussian_filter(image_loop, sigma=(sigma_n3, sigma_n3))
            weights = gaussian_filter(
                mask_norma3.astype(float), sigma=(sigma_n3, sigma_n3))
            image[index] = image_loop / filtered * weights

    ############################
    # calculate a mask with the center to limb variation of the sun to flat the images
    ############################
    if clv != 0:
        printc("Correcting CLV for masking spots", color=bcolors.OKGREEN)

        if clv < 0:
            printc("Reevaluating limb darkening", color=bcolors.OKBLUE)
            pars = fit_clv(image[0], rel_centers[0, :], verbose=verbose)
        else:
            pars = False
        clv = np.abs(clv)

        amask = azimuthal_mask(imsize[0],imsize[1],(imsize[0] // 2, imsize[1] // 2),radious,coefficients=pars)

        # find px outside threshols
        printc("... clv threshold ", clv, color=bcolors.OKBLUE)

        for i in range(n_images):
            mask_dummy = shift(
                amask, shift=[xyshifts[i, 0], xyshifts[i, 1]], fill_value=0)
            image_dummy = image[i] / (mask_dummy * circle_mask)  # if any scale
            image_dummy[np.bitwise_not(np.isfinite(image_dummy))] = 0
            if not(1 in normalize):
                image_dummy, norma_ = _do_normalization(
                    image_dummy, mask[i], type="one", SIGMA_NORMA=SIGMA_NORMA)
            idx = np.where(image_dummy > (1 + clv))
            mask[i][idx] = 0
            idx = np.where(image_dummy < (1 - clv))
            mask[i][idx] = 0
            mask[i] = expand_mask(mask[i], pixels=1, step=2)
            if flat_images:
                image[i] = image_dummy

    ############################
    # DO LOG
    ############################

    D = np.log10(image)
    # replace NaNs and Infs by 0
    D[np.bitwise_not(np.isfinite(D))] = 0

    ############################
    # NORMALIZATION Option 2
    ############################
    if 2 in normalize:
        tn = 0
        for i in range(n_images):
            norma = np.mean(D[i][np.where(mask[i] != 0)])
            print("Normalization to around zero in the log images: ", norma)
            D[i] = D[i] - norma
            tn += norma
        tn /= n_images

    ############################
    # FLAT DETERMINATION PART
    ############################

    #compute final mask
    final_mask = np.zeros_like(mask[0])
    for itera in mask:
        final_mask += itera
    final_mask[final_mask > 1] = 1

    if method == "kll":

        n = np.zeros([imsize[0], imsize[1]], dtype=np.int8)  # *** int to float
        sum_image = np.zeros([imsize[0], imsize[1]], dtype=float)
        print("Rel centers: ", rel_centers)
        #  for [iq, ir] in itertools.combinations(range(n_images), 2): # overall 36 combinations
        for iq in range(1, n_images):  # loop in iq
            for ir in range(iq):  # loop in ir
                # shift of iq with respect ir
                dx = rel_centers[iq, 0] - rel_centers[ir, 0]
                dy = rel_centers[iq, 1] - rel_centers[ir, 1]
                t_mask_1 = mask[ir] & shift(mask[iq], [-dx, -dy])
                t_mask_2 = mask[iq] & shift(mask[ir], [dx, dy])
                t_mask = t_mask_1 + t_mask_2  # DOS  | vs + and /2.  Sep 10-2022
                t_image_1 = shift(D[iq], [-dx, -dy])
                t_image_2 = shift(D[ir], [dx, dy])
                aa = (D[iq] - t_image_2) * t_mask_2  # add _2
                bb = (D[ir] - t_image_1) * t_mask_1  # add _1

                if 3 in normalize:
                    t_mask_xor = t_mask_1 ^ t_mask_2  # DOS  | vs + and /2.  Sep 10-2022
                    idx_aa = t_mask_xor * t_mask_2 == 1
                    idx_bb = t_mask_xor * t_mask_1 == 1
                    aaa, norma_aa = _do_normalization(
                        aa,
                        t_mask_xor * t_mask_2,
                        type="zero",
                        SIGMA_NORMA=SIGMA_NORMA,
                        method="median")
                    bbb, norma_bb = _do_normalization(
                        bb,
                        t_mask_xor * t_mask_1,
                        type="zero",
                        SIGMA_NORMA=SIGMA_NORMA,
                        method="median")
                    ccc, norma_cc = _do_normalization(
                        aa + bb,
                        t_mask_1 | t_mask_2,
                        type="zero",
                        SIGMA_NORMA=SIGMA_NORMA,
                        method="median")
                    aa[idx_aa] = aa[idx_aa] - norma_aa + norma_cc
                    bb[idx_bb] = bb[idx_bb] - norma_bb + norma_cc
                    debug("norma_aa", "norma_bb", "norma_cc")

                image_pair = aa + bb
                sum_image += image_pair
                n += t_mask  # DOS
                if verbose == 2:
                    debug("iq", "ir", "dx", "dy")
                    plt.plot(aa[1000, :], ".")
                    plt.plot(bb[1000, :], "+")
                    plt.show()
                    plt.plot(image_pair[1000, :], "-")
                    plt.show()
                    plt.plot(sum_image[1000, :], ".")
                    plt.show()
                    plt.imshow(image_pair, vmin=-0.05, vmax=0.05)
                    plt.show()

        K = sum_image / n.astype(float)
        # replace NaNs and Infs by 0
        K[np.bitwise_not(np.isfinite(K))] = 0
        if verbose >= 1:
            plt.imshow(K, cmap="gray")
            plt.clim(vmax=0.02, vmin=-0.02)
            plt.colorbar()
            plt.show()

        G = np.copy(K)
        if bit_trun == 1:
            K = np.int32(K * 256) / 256  # bit truncation
        k = np.power(10, K)
        if bit_trun == 1:
            k = np.int32(k * 256) / 256  # bit truncation

        for itera in range(iter):
            r_res = np.zeros(imsize, dtype=float)
            for iq in range(1, n_images):  # loop in iq
                for ir in range(iq):  # loop in ir
                    # shift of iq with respect ir
                    dx = rel_centers[iq, 0] - rel_centers[ir, 0]
                    dy = rel_centers[iq, 1] - rel_centers[ir, 1]
                    if verbose == 2:
                        print("dx,dy,sqrt(dx**2+dy**2)",dx,dy,np.sqrt(dx**2 + dy**2),iq,ir)
                    t_mask_1 = mask[ir] & shift(mask[iq], [-dx, -dy])
                    t_mask_2 = mask[iq] & shift(mask[ir], [dx, dy])

                    t_image_1 = shift(G, [-dx, -dy]) * t_mask_1
                    t_image_2 = shift(G, [dx, dy]) * t_mask_2
                    correction = t_image_1 + t_image_2
                    r_res += correction

            corr = r_res / n.astype(float)
            G = K + corr
            # replace NaNs and Infs by 0
            G[np.bitwise_not(np.isfinite(G))] = 0

            idx = np.where(n > 0)
            s = G[idx]
            #  calculate average of gain table for normalization
            sm = np.mean(s)
            sm2 = np.mean(s**2)
            five_sigma = SIGMA_NORMA * np.sqrt(sm2 - sm * sm)
            idx2 = np.where(np.abs(s - sm) < five_sigma)
            sm = np.mean(s[idx2])
            # G[idx] = G[idx] - sm
            G = G - sm
            # G,sm = donorma(G,n,dntype='zero')
            change = np.std(K - G)

            printc(
                "Iteration: ",
                itera,
                " of ",
                iter,
                "Gain mean:",
                sm,
                "using rms level",
                SIGMA_NORMA,
                " rms variation:",
                change,
                color=bcolors.OKBLUE)
            if verbose >= 1:
                plt.imshow(G, cmap="gray")
                plt.clim(vmax=0.02, vmin=-0.02)
                plt.colorbar()
                plt.show()

                plt.imshow(K - G, cmap="gray")
                plt.clim(vmax=0.02, vmin=-0.02)
                plt.colorbar()
                plt.show()

        g = np.power(10, G)
        g[np.bitwise_not(np.isfinite(g))] = 0

        return g, norma, final_mask


    elif method == "chae_exp":

        tmask = np.sum(mask, axis=0)
        # mask_Ob,dummy = generate_circular_mask([imsize[0]-1, imsize[1]-1],radious,radious)
        # mask_Ob,dummy = generate_circular_mask([imsize[0]-1, imsize[1]-1],radious - expand,radious - expand)
        mask_Ob = shift(mask[0], shift=[-xyshifts[0, 0], -xyshifts[0, 1]], fill_value=0)

        # Constant term
        if len(c_term) == 0:
            fit_c = 0
            c_term = np.log10(np.ones((n_images)))
        else:
            c_term = np.log10(np.ones((n_images)))

        flat = np.log10(np.ones_like(D[0]))
        Ob = np.zeros_like(D[0])

        for i in range(n_images):
            # shift input image to the center of the frame.
            Ob += shift(D[i], shift=-xyshifts[i, :])
        Ob = Ob / float(n_images)
        Ob[np.bitwise_not(np.isfinite(Ob))] = 0
        idx = tmask >= 1

        for k in range(iter):

            numerator = np.zeros((imsize))
            for i in range(n_images):
                numerator += (
                    c_term[i] + Ob - shift(D[i] - flat, shift=-xyshifts[i, :])
                ) * mask_Ob  # (i+xk,j+yk) Eq 8

            # Ob -= (numerator/mask_Ob/n_images)
            Ob -= numerator / mask_Ob / tmask
            Ob[np.bitwise_not(np.isfinite(Ob))] = 0

            if verbose == 1:
                tshow = np.power(10, Ob)
                plt.imshow(
                    tshow,
                    cmap="gray",
                    vmin=np.median(tshow[idx]) * 0.9,
                    vmax=np.median(tshow[idx]) * 1.1)
                plt.show()

            numerator = np.zeros((imsize))
            for i in range(n_images):
                dummy = (
                    c_term[i] + shift(Ob, shift=+xyshifts[i, :]) + flat - D[i]
                ) * mask[i]
                dummy[np.bitwise_not(np.isfinite(dummy))] = 0
                numerator += dummy
                c_term[i] -= np.sum(dummy[idx]) / np.sum(mask[i])

            dummy = numerator / tmask
            flat -= dummy
            flat[np.bitwise_not(np.isfinite(flat))] = 0

            if verbose == 1:
                plt.imshow(flat, cmap="gray", vmin=-0.02, vmax=0.02)
                plt.show()
            if verbose >= 1:
                print("Iter: ", k, " STD: ", np.max(np.abs(dummy[idx])), np.exp(c_term))

            s = flat[idx]
            sm = np.mean(s)
            sm2 = np.mean(s**2)
            five_sigma = 5 * np.sqrt(sm2 - sm * sm)
            print(
                "Iteration: ",
                k,
                five_sigma,
                "5*rms",
                sm,
                " of ",
                k,
                " STD: ",
                np.max(np.abs(dummy[idx])))

        flat, mf = _do_normalization(flat, tmask, SIGMA_NORMA=SIGMA_NORMA)
        Ob = Ob + mf + np.mean(c_term)
        # flat = flat - np.mean(flat)
        # Ob = Ob + np.mean(flat) + np.mean(c_term)
        c_term = c_term - np.mean(c_term)

        flat = np.power(10, flat)
        flat[np.bitwise_not(np.isfinite(flat))] = 0

        if verbose >= 1:
            plt.imshow(
                flat, cmap="gray", vmin=0.95, vmax=1.05
            )  # vmin=)np.min(,vmax=0.05)
            plt.show()

        return flat, norma, final_mask


    elif method == "chae":

        tmask = np.sum(mask, axis=0)
        mask_Ob, dummy = generate_circular_mask(
            [imsize[0] - 1, imsize[1] - 1], radious, radious)

        # Constant term
        if c_term == 0:
            fit_c = 0
            c_term = np.log10(np.ones(n_images))
        else:
            c_term = np.log10(np.ones(n_images))

        flat = np.log10(np.ones_like(D[0]))
        Ob = np.zeros_like(D[0])

        for i in range(n_images):
            # shift input image to the center of the frame.
            Ob += shift(D[i], shift=-xyshifts[i, :])
        Ob = Ob / float(n_images)
        Ob[np.bitwise_not(np.isfinite(Ob))] = 0
        idx = tmask >= 1

        for k in range(iter):

            numerator = np.zeros(imsize)
            for i in range(n_images):
                numerator += (
                    c_term[i] + Ob - shift(D[i] - flat, shift=-xyshifts[i, :])
                ) * mask_Ob  # (i+xk,j+yk) Eq 8

            Ob -= numerator / mask_Ob / n_images
            Ob[np.bitwise_not(np.isfinite(Ob))] = 0

            if verbose == 1:
                tshow = np.power(10, Ob)
                plt.imshow(
                    tshow,
                    cmap="gray",
                    vmin=np.median(tshow[idx]) * 0.9,
                    vmax=np.median(tshow[idx]) * 1.1)
                plt.show()

            numerator = np.zeros(imsize)
            for i in range(n_images):
                dummy = (
                    c_term[i] + shift(Ob, shift=+xyshifts[i, :]) + flat - D[i]
                ) * mask[i]
                dummy[np.bitwise_not(np.isfinite(dummy))] = 0
                numerator += dummy
                c_term[i] -= np.sum(dummy) / np.sum(mask[i])

            dummy = numerator / tmask
            flat -= dummy
            flat[np.bitwise_not(np.isfinite(flat))] = 0

            if verbose == 1:
                plt.imshow(flat, cmap="gray", vmin=-0.02, vmax=0.02)
                plt.show()
            if verbose >= 1:
                print("Iter: ", k, " STD: ", np.max(np.abs(dummy[idx])), np.exp(c_term))

            s = flat[idx]
            sm = np.mean(s)
            sm2 = np.mean(s**2)
            five_sigma = 5 * np.sqrt(sm2 - sm * sm)
            print(
                "Iteration: ",
                k,
                five_sigma,
                "5*rms",
                sm,
                " of ",
                k,
                " STD: ",
                np.max(np.abs(dummy[idx])))

        # flat = flat - np.mean(flat)
        # Ob = Ob + np.mean(flat) + np.mean(c_term)
        # c_term = c_term - np.mean(c_term)
        flat, mf = _do_normalization(flat, tmask, SIGMA_NORMA=SIGMA_NORMA)
        Ob = Ob + mf + np.mean(c_term)
        c_term = c_term - np.mean(c_term)

        flat = np.power(10, flat)
        flat[np.bitwise_not(np.isfinite(flat))] = 0

        if verbose >= 1:
            plt.imshow(
                flat, cmap="gray", vmin=0.95, vmax=1.05
            )  # vmin=)np.min(,vmax=0.05)
            plt.show()

        return flat, norma, final_mask


    elif method == "Caron":

        # Extracting flat-field images from scene-based image sequences using phase
        # correlation
        # James N. Caron, Marcos J. Montes, and Jerome L. Obermark
        # Citation: Review of Scientific Instruments 87, 063710 (2016); doi: 10.1063/1.4954730
        # View online: https://doi.org/10.1063/1.4954730
        # View Table of Contents: http://aip.scitation.org/toc/rsi/87/6
        # Published by the American Institute of Physics

        Gf = np.zeros([imsize[0], imsize[1]], dtype=np.float64)
        n = np.zeros([imsize[0], imsize[1]], dtype=np.float64)
        sum_image = np.zeros([imsize[0], imsize[1]], dtype=np.float64)
        Gr = np.zeros([n_images, imsize[0], imsize[1]], dtype=np.float64)

        for itera in range(iter):
            ir = 0
            for iq in range(n_images):
                dx = (
                    rel_centers[iq, 0] - rel_centers[ir, 0]
                )  # shift of iq with respect ir
                dy = rel_centers[iq, 1] - rel_centers[ir, 1]
                t_mask = mask[ir] * shift(mask[iq], [dx, dy])
                n += t_mask
                t_image = shift(D[iq] - Gf, [dx, dy])
                sum_image += t_mask * t_image

            K = sum_image / n
            K[np.isneginf(K)] = 0
            K[np.isnan(K)] = 0
            idx = np.where(K > 5)
            mask[0][idx] = 0
            idx2 = np.where(n == 0)
            K[idx2] = 0

            iq = 0
            for ir in range(n_images):
                dx = (
                    rel_centers[iq, 0] - rel_centers[ir, 0]
                )  # shift of iq with respect ir
                dy = rel_centers[iq, 1] - rel_centers[ir, 1]
                Kn = shift(K * mask[0], [dx, dy])

                G = (D[ir] - Kn) * mask[ir]
                G[np.isneginf(G)] = 0
                G[np.isnan(G)] = 0
                Gr[ir, :, :] = G

            m = np.sum(mask, axis=0)
            Gf = np.sum(Gr, axis=0) / m
            Gf[np.isneginf(K)] = 0
            Gf[np.isnan(K)] = 0
            print("Iteration: ", itera)

        g = np.power(10, Gf)
        g[np.bitwise_not(np.isfinite(g))] = 0

        return g, norma, final_mask


def fdt_flat(
    files: list,
    wavelength: int,
    npol: int,
    method: str = "kll",
    flat_init: np.ndarray = np.zeros(shape=(),dtype=float),
    r_shifts: int = 0,
    shifts: list = [],
    shifts_file: str = str(),
    disp_method: str = "Hough",
    verbose: bool = False,
    expand: int = 1,
    mask_threshold: float = 0.0,
    iter: int = 4,
    normalize: list = [0],
    c_term: int = 0,
    inner_radius: int = 400,
    outer_radius: int = 800,
    steps: int = 20,
    imasize: list = [],
    single: bool = False,
    clv: float = 0.0,
    SIGMA_NORMA: float = 5,
    threshold: float = 0.05,
    loop_threshold: int = 2,
    flat_images: bool = False,
    dark: np.ndarray = np.zeros(shape=(),dtype=float)):
    """
    Main module for computing the flatfield from n displaced images.
    This routine reads or calculate the center of the displaced images and then run the main flatfielding algorithm.

    :param files: filanames containing the data. Hast to be an ordered list.
    :type files: list
    :param wavelength: wavelength index for flat calculation. Assumes the data input is in the format :math:`{[n_{wave},n_{npol},d_x,d_y]}`
    :type wavelength: int
    :param npol: modulation index for flat calculation. Assumes the data input is in the format :math:`{[n_{wave},n_{npol},d_x,d_y]}`
    :type npol: int
    :param method: Method for flatfield calculation: [``kll``,``chae``], defaults to ``kll``.
    :type method: str, optional
    :param flat_init: image containing a preliminary flat guess (data is divided by this flat), defaults to ''. Dimensions should be same as data.
    :type flat_init: np.ndarray, optional
    :param r_shifts: Method for finding the image centers and Sun radious, defaults to 0

        * 0: find the disc center position and Sun radious using either the Hough transform of a circle fit method. The method is given by in ``disp_method`` option.
        * 1: read the shifts from files with a specific format and whose initial leadtext is given by ``shifts_file``
        * 2: shifts are provided by the user in [center] and [radious] keywords
        * 3: shifts are from the header information [CRPIX1,CRPIX2,RADIOUS]. In the flat preproccesing the user should take care of updating these keywords.

    :type r_shifts: int, optional
    :param shifts: center and radius of input images. List in which ``centers = shifts[0]`` and ``radius = shifts[1]``. Mandatory if ``r_shifts=2``. defaults to None
    :type shifts: list, optional
    :param shifts_file: out filename for displacements, defaults to str().

        * ``shifts_file + '_cnt_w' + str(wavelength) + '_n' + str(npol) + '.txt'``, for centers.
        * ``shifts_file + '_rad_w' + str(wavelength) + '_n' + str(npol) + '.txt'``, for radius.
    :type shifts_file: str, optional
    :param disp_method: method for finding the image centers [``Hough`` , ``FFT`` , ``circle``], defaults to ``Hough``
    :type disp_method: str, optional
    :param verbose: vervosity, defaults to False
    :type verbose: int, optional
    :param expand: :py:meth:`fdt_flat_gen` expand parameter. Reduce the radious of the circular mask by ``expand`` pixels, defaults to 1
    :type expand: int, optional
    :param mask_threshold:  If ``radius = 0`` (or not given) the mask is determined taking all values of the data above ``mask_threshold``.
      In this case, ``expand`` and ``normalization`` are ignored. defaults to 0.
    :type mask_threshold: float, optional
    :param iter: [``kll``,``chae``] iterations, defaults to 4
    :type iter: int, optional
    :param normalize: Normalization option for the n images prior to flat calculation. See :py:meth:`fdt_flat_gen` for details. Defaults to False
    :type normalize: list or int, optional
    :param c_term: initial ``c_term`` values for the ``chae`` method. See :py:meth:`fdt_flat_gen` for details. Defaults to 0
    :type c_term: int, optional
    :param inner_radius: ``Hough`` transform minimum solar radius. Used when ``method = hough``. defaults to 400
    :type inner_radius: int, optional
    :param outer_radius: ``Hough`` transform maximum solar radius. Used when ``method = hough``. , defaults to 800
    :type outer_radius: int, optional
    :param steps: ``Hough`` transform steps. Used when ``method = hough``. , defaults to 20
    :type steps: int, optional
    :param threshold: threshold above which pixels are valid (assuming image is normalized to one). Defaults to 0.05. See :py:meth:`do_hough`, defaults to 0.05
    :type threshold: float, optional
    :param loop_threshold: Iterations for finetuning the threshold level in :py:meth:`FindEdges`, defaults to 10
    :type loop_threshold: int, optional
    :param imasize: size of inpit frames. It will be [2048,2048] if defaults to None
    :type imasize: two elements list, optional
    :param single: This software is written explicitly for SOPHI since it uses SOPHI specially tailored read routines.
      However, one can use it with simulated data. In such a case, the keywords ``pol_state`` and ``wavelengt``
      loose their meaning and one should set ``single = True``. Defaults to False.
    :type single: bool, optional
    :param clv: Threshold for masking active regions in the data. See :py:meth:`fdt_flat_gen` for details. , defaults to False
    :type clv: bool, optional
    :param flat_images: if ``clv`` is set and ``True``, the images are flattened, defaults to False
    :type flat_images: bool, optional
    :param SIGMA_NORMA: sigma to remove outliers in :py:meth:`fdt_flat_gen`, defaults to 5.
    :type SIGMA_NORMA: float, optional
    :param dark: image containing the dark, defaults to 0. This option will be deprecated since the flatfield data is preprocessed in advance.
      Dimensions should be same as input images.
    :type dark: np.ndarray, optional
    :param correct_ghost: Apply ghost correction, defaults to False. This option will be deprecated since the flatfield data is preprocessed in advance.
    :type correct_ghost: bool, optional
    :return: gain and normalization of each image
    :rtype: (np.ndarray,np.ndarray)
    """

    ############################
    # open 9 fits files and get one images corresponding to one polarization [pol_state] and one wavelength [wavelength].
    # This can be done all at once but I have this like that because I am lazy.
    ############################

    if not(imasize):
        imasize = [2048, 2048]
    if single:
        image = []
        header = []
        for i in files:
            img, hd = fits_get(i)
            image.append(img)
            header.append(hd)
    else:
        image = []
        header = []
        for i in files:
            im, hd = fits_get_part(i, wavelength, npol)
            image.append(im)
            header.append(hd)

    n_images = len(image)
    ys, xs = image[0].shape

    ############################
    # Correct Dark if not done in advance
    # dark current is provided in dark keyword
    ############################

    with contextlib.suppress(Exception):
        printc("...Correcting dark current...", color=bcolors.OKGREEN)
        for i in range(n_images):
            image[i] = np.abs(image[i] - dark)
            # REVIEW the dark correction is usually not O.K. in a 0.2-0.3%. I can remove the residual dark but
            # this will affect the INTENSITY LEVEL. So, it is mandatory to correct the darks BEFOREHAND!!!
            # TODO PUT THIS AS AN ADDITIONAL INPUT OPTION
            # columns = np.mean(image[i][0:40,:],axis=0)
            # image[i] = image[i] - columns[np.newaxis,:]
            # image[i] = np.abs(image[i])
            # plt.imshow(image[i],vmax=100)
            # plt.show()

    ############################
    # Correct initial flat
    ############################
    if flat_init.shape != ( ):
        printc("...Correcting init_flat...", color=bcolors.OKGREEN)
        for i in range(n_images):
            image[i] = image[i] / flat_init[0:2048, 0:2048]
            image[i][np.bitwise_not(np.isfinite(image[i]))] = 0

    ############################
    # Determine the image shifts
    # there are three different options: r_shifts = [0,1,2]
    # 0 find the disc center position and Sun radious using either the Hough transform of a circle fit method
    # 1 read the shifts from files with a specific format and whose initial leadtext is given by [shifts_file]
    # 2 shifts are provided by the uses in centers and radious keywords
    ############################

    if r_shifts == 1:
        try:
            printc("... read user input shifts_file ...", color=bcolors.OKGREEN)
            centers = read_shifts(
                shifts_file + "_cnt_w" + str(wavelength) + "_n" + str(npol) + ".txt")
            radius = read_shifts(
                shifts_file + "_rad_w" + str(wavelength) + "_n" + str(npol) + ".txt")
            printc(
                "File: ",shifts_file + "_cnt_w" + str(wavelength) + "_n" + str(npol) + ".txt",
                color=bcolors.OKBLUE)
            for i in range(n_images):
                printc(
                    "Image",i, "c: ",centers[i, 0],",",centers[i, 1]," rad: ",radius[i],
                    color=bcolors.YELLOW)
        except Exception:
            printc(
                "Unable to open file: {}",
                shifts_file + "_cnt_w" + str(wavelength) + "_n" + str(npol) + ".txt",
                color=bcolors.FAIL)

    elif r_shifts == 2:
        printc("... shifts provided by user ...", color=bcolors.OKGREEN)
        centers = shifts[0]
        printc(centers, "... read ", color=bcolors.OKBLUE)
        radius = shifts[1]
        printc(radius, "... read ", color=bcolors.OKBLUE)

    elif r_shifts == 0:

        if disp_method == "Hough":
            printc("... calculating shifts using Hough ...", color=bcolors.OKGREEN)

            centers, radius = do_hough(
                image,
                inner_radius,
                outer_radius,
                steps,
                verbose=verbose,
                threshold=threshold,
                loop_threshold=loop_threshold)

        elif disp_method == "FFT":
            printc(
                "... calculating shifts using FFT _NOT TESTED_ ...",
                color=bcolors.WARNING,
            )
            printc(
                'Input parameter "expand" should be negative number representing solar disk',
                color=bcolors.WARNING,
            )
            image_dummy = np.zeros((n_images, ys, xs))
            for i in range(n_images):
                image_dummy[i, :, :] = image[i]
            s_y, s_x, _ = PHI_shifts_FFT(
                image_dummy, prec=5, verbose=False, norma=False, coarse_prec=150)
            centers = np.zeros((n_images, 2))
            radius = np.zeros(n_images)
            radius[:] = -expand
            expand = 5
            centers[:, 0] = -s_x + xs // 2
            centers[:, 1] = -s_y + ys // 2
            for i in range(n_images):
                printc(
                    "Image",i,
                    "c: ",centers[i, 0],",",centers[i, 1],
                    " rad: ", radius[i],"*",
                    color=bcolors.YELLOW)

        elif disp_method == "circle":
            printc(
                "... calculating shifts using circle fit method ...",
                color=bcolors.OKGREEN)
            centers = np.zeros((n_images, 2))
            radius = np.zeros(n_images)
            for i in range(n_images):
                centers[i, 1], centers[i, 0], radius[i] = find_center(
                    image[i], sjump=4, njumps=100, threshold=0.8)
                printc(
                    "Image",i,
                    "c: ",centers[i, 0],",",centers[i, 1],
                    " rad: ", radius[i],"*",
                    color=bcolors.YELLOW)

        else:
            printc(
                "Error in disp_method option. Given",
                disp_method,
                "while options are [Hough,FFT,circle]",
                color=bcolors.FAIL)
            raise Exception

        if shifts_file:
            write_shifts(
                shifts_file + "_cnt_w" + str(wavelength) + "_n" + str(npol) + ".txt",
                centers)
            write_shifts(
                shifts_file + "_rad_w" + str(wavelength) + "_n" + str(npol) + ".txt",
                radius)


        # TODO
        # Check if this needs to be updated to the CRPIXN1, CRPIXN2 keywords
        # Discard RADIUS keyword and determine the radius as header['RSUN_ARC'] / header['CDELT1']
        loop = 0
        for i in range(n_images):

            if "CRPIX1" in header[loop]:  # Check for existence
                header[loop]["CRPIX1"] = round(centers[loop, 0], 2)
            else:
                header[loop].append("CRPIX1")
                header[loop]["CRPIX1"] = round(centers[loop, 0], 2)

            if "CRPIX2" in header[loop]:  # Check for existence
                header[loop]["CRPIX2"] = round(centers[loop, 1], 2)
            else:
                header[loop].append("CRPIX2")
                header[loop]["CRPIX2"] = round(centers[loop, 1], 2)
            loop += 1

    elif r_shifts == 3:
        printc(
            "... getting centers from header information ...",
            color=bcolors.OKGREEN)


        centers = np.zeros((n_images,2))
        radius = np.zeros((n_images))
        try:
            loop = 0
            for i in range(n_images):
                centers[i,0] = header[loop]['CRPIXN1']
                centers[i,1] = header[loop]['CRPIXN2']
                radius[i] = header[loop]['RADIUS']
                loop += 1
        except:
            loop = 0
            for i in range(n_images):
                centers[i,0] = header[loop]['CRPIX1']
                centers[i,1] = header[loop]['CRPIX2']
                radius[i] = header[loop]['RADIUS']
                loop += 1

    else:
        printc(
            "Error in r_shift option. Given",
            r_shifts,
            "while options are 0,1, or 2",
            color=bcolors.FAIL)
        raise Exception

    # make sure we have integer numpy numbers in the centers
    centers = np.array(centers).astype(int)
    mean_radii = np.mean(radius)

    ############################
    # Call flat generation routine
    ############################

    if mask_threshold != 0:
        gain, norma, final_mask = fdt_flat_gen(
            image,
            centers,
            method,
            iter=iter,
            verbose=verbose,
            c_term=c_term,
            normalize=normalize,
            flat_images=flat_images,
            mask_threshold=mask_threshold,
            clv=clv,
            SIGMA_NORMA=SIGMA_NORMA)
    else:
        gain, norma, final_mask = fdt_flat_gen(
            image,
            centers,
            method,
            iter=iter,
            verbose=verbose,
            c_term=c_term,
            normalize=normalize,
            flat_images=flat_images,
            radious=mean_radii,
            expand=expand,
            clv=clv,
            SIGMA_NORMA=SIGMA_NORMA)

    ############################
    # Add back the initial flat
    ############################
    if flat_init.shape != ( ):
        printc("...putting back init_flat...", color=bcolors.OKGREEN)
        gain = gain * flat_init[0:2048, 0:2048] * final_mask

        # if putmediantozero == '2D':
        #     #TODO Include masking of ARs

        #     mask_blurred = gaussian_filter(mask_at_edge.astype(float), sigma=(SIGMA_MEDIAN_FILTER, SIGMA_MEDIAN_FILTER))#, truncate=3.5)
        #     mask_blurred *= mask_at_edge
        #     # mask_blurred = mask_blurred.astype(float)
        #     # mask_blurred /= mask_blurred[yd//2,xd//2]
        #     if verbose:
        #         plt.imshow(mask_at_edge)
        #         plt.colorbar()
        #         plt.show()
        #         plt.imshow(mask_blurred)
        #         plt.colorbar()
        #         plt.show()

        #     #mask_blurred *= mask
        #     for p in range(3):
        #         # md = np.mean(data[:,p+1,:,:],axis=0)
        #         md = data[cpos[0],p+1,:,:]
        #         # apply Gaussian blur, creating a new image
        #         md_blurred = gaussian_filter(md*mask_at_edge, sigma=(SIGMA_MEDIAN_FILTER, SIGMA_MEDIAN_FILTER))#, truncate=3.5)
        #         if verbose:
        #             plt.imshow(md_blurred,clim=(-0.002,0.002))
        #             plt.colorbar()
        #             plt.show()
        #         md_blurred /= mask_blurred
        #         md_blurred[np.bitwise_not(np.isfinite(md_blurred))] = 0
        #         if verbose:
        #             plt.imshow(md_blurred,clim=(-0.002,0.002))
        #             plt.colorbar()
        #             plt.show()
        #             plt.imshow(data[cpos[0],p+1,:,:],clim=(-0.002,0.002))
        #             plt.colorbar()
        #             plt.show()
        #         data[:,p+1,:,:] -= md_blurred[np.newaxis,:,:]
        #         if verbose:
        #             plt.imshow(data[cpos[0],p+1,:,:],clim=(-0.002,0.002))
        #             plt.colorbar()
        #             plt.show()
        #     data[np.bitwise_not(np.isfinite(data))] = 0

        # printc('-->>>>>>> Putting median to zero ',color=bcolors.OKGREEN)
        # # printc('          Median evaluated in x = [',rrx_m[0],':',rrx_m[1],'] y = [',rry_m[0],':',rry_m[1],']',' using ',factor*100,"% of the disk",color=bcolors.OKBLUE)
        # printc('          Median evaluated in ',PERCENT_OF_DISK_FOR_NORMALIZATION,' % of the disk',color=bcolors.OKBLUE)
        # for i in range(zd//4):
        #     # PQ = np.median( data[i,1, maski > 0])#,axis=(1,2))
        #     # PU = np.median( data[i,2, maski > 0])#,axis=(1,2))
        #     # PV = np.median( data[i,3, maski > 0])#,axis=(1,2))
        #     PQ = zero_level(data[i,1,:,:],maski,FIVE_SIGMA=FIVE_SIGMA)
        #     PU = zero_level(data[i,2,:,:],maski,FIVE_SIGMA=FIVE_SIGMA)
        #     PV = zero_level(data[i,3,:,:],maski,FIVE_SIGMA=FIVE_SIGMA)
        # # PQ = np.median(data[:,1,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
        # # PU = np.median(data[:,2,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
        # # PV = np.median(data[:,3,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
        #     data[i,1,:,:] = data[i,1,:,:] - PQ#[:,np.newaxis,np.newaxis]
        #     data[i,2,:,:] = data[i,2,:,:] - PU#[:,np.newaxis,np.newaxis]
        #     data[i,3,:,:] = data[i,3,:,:] - PV#[:,np.newaxis,np.newaxis]
        #     printc(PQ,PU,PV)

    return gain, norma


def fdt_flat_preprocessing(
    file: str,
    dark_f: str,
    verbose: bool = True,
    correct_ghost: bool = False,
    flat_ghost: np.ndarray = None,
    correct_distortion: bool = False,
    correct_prefilter: bool = False,
    prefilter_fits: str = "0000990710_noMeta.fits",
    version: str = "01",
    parallel: bool = False,
    ghost_params: list = [-1.98796, 1936.70, -1.98944, 1947.15, 0.0008, 0.0024, -0.0032, 7],
    correct_fringes: str = str(), fringe_threshold: float = 1.55,
    shifts_method:str = 'Hough',
    calculate_shifts: bool = True,
    hough_params: list = [250, 800, 100],
    loop_threshold: int = 2,
    save_stokes: bool = True,
    save_ghost_image: str = str(),
    ghost_image: str = str(),
    interactive: bool = False,
    ):

    """
    This module preprocesses FDT off-points data to remove dark, prefilter, ghost, distortion or fringes from the data
    and saves the results into ``level 1.5`` format.
    In principle, it could be done with the main data reduction pipeline :py:meth:`phifdt_pipe` as well.
    Preprocessing the off-point data is necessary to speed up the calculations. Resulting are stored with the ID ``ilam_offpoints``.

    Example::

        if i = list of files
        for i in files:
            fdt.fdt_flat_preprocessing(i, dark_file, verbose = True,correct_ghost = True,correct_prefilter = True,
                prefilter_fits = prefilter_file, version = '01')

    :param file: input data filemane
    :type file: str
    :param dark_f: input dark filename
    :type dark_f: str
    :param verbose: verbosity, defaults to True
    :type verbose: bool, optional
    :param correct_ghost: correct ghost or not, defaults to False
    :type correct_ghost: bool, optional
    :param flat_ghost: flat to be applied for properly correcting the ghost. 2kx2kx24. defaults to empty
    :type flat_ghost: bp.ndarray, optional
    :param correct_distortion: correct FDT distortion or not, defaults to False
    :type correct_distortion: bool, optional
    :param correct_prefilter: correct prefilter or not, defaults to False
    :type correct_prefilter: bool, optional
    :param prefilter_fits: prefilter fits file including location, defaults to '0000990710_noMeta.fits'
    :type prefilter_fits: str, optional
    :param version: version to add to the resulting files ``_V???_``, defaults to '01'
    :type version: str, optional
    :param parallel: use code parallelism or not, defaults to False
    :type parallel: bool, optional
    :param num_workers: number of workiers if ``parallel = True``, defaults to 10
    :type num_workers: int, optional
    :param ghost_params: parameters for ghost correction
    :type extra_offset: list, optional
    :param correct_fringes: Activate fringe correction, defaults to False

        * 'manual': first FM version. Freq, mask applied to all images with fixed frequencies
        * 'auto' : calculate fringes freq. automatically (in development).

    :type correct_fringes: str
    :param fringe_threshold: threshold above which to detect fringes in the power of the data, defaults to 1.55
    :type correct_fringes: float
    :params hough_params: Hough parameters [600, 900, 20]; inner_radius = 250, outer_radius = 600, steps = 20, initial values for finding sun center
    :type hough_params: list
    :param threshold: threshold above which pixels are valid (assuming image is normalized to one). Defaults to 0.05. See :py:meth:`do_hough`, defaults to 0.05
    :type threshold: float, optional
    :param loop_threshold: Iterations for finetuning the threshold level in :py:meth:`FindEdges`, defaults to 10
    :type loop_threshold: int, optional
    :param save: save results, defaults to True
    :type save: bool, optional
    :param save_ghost_image: save ghost image, defaults to str()
    :type save_ghost_image: str, optional
    :param input_ghost: input ghost image, defaults to str()
    :type input_ghost: str, optional

    .. attention:: This program will be replaced by :py:meth:`phifdt_pipe` at some point since it does the same thing.


    """

    printc("------------------------------", color=bcolors.FAIL)
    printc("DEPRECATTED. USE MAIN PIPELINE", color=bcolors.FAIL)
    printc("------------------------------", color=bcolors.FAIL)

    if os.path.isfile(file):
        printc("Data file exist", bcolors.OKGREEN)
    else:
        printc("Data file do not exist in current location ", file, bcolors.FAIL)
        raise FileNotFoundError("input files error")

    if os.path.isfile(dark_f):
        printc("Dark file exist", bcolors.OKGREEN)
    else:
        printc("Dark file do not exist in current location ", dark_f, bcolors.FAIL)
        raise FileNotFoundError("input files error")

    if correct_prefilter:
        if os.path.isfile(prefilter_fits):
            printc("Prefilter file exist", bcolors.OKGREEN)
        else:
            printc(
                "Prefilter file do not exist in current location ",
                prefilter_fits,
                bcolors.FAIL,
            )
            raise FileNotFoundError("input files error")

    # CHECK IF input is FITS OR FITS.GZ
    if file.endswith(".fits"):
        filetype = ".fits"
    elif file.endswith(".fits.gz"):
        filetype = ".fits.gz"
    else:
        raise ValueError("input data file type nor .fits neither .fits.gz")

    # -----------------
    # READ DATA
    # -----------------

    try:
        printc("-->>>>>>> Reading Data file: " + file, color=bcolors.OKGREEN)
        data, header = fits_get(file)
    except:
        printc("ERROR, Unable to open fits file: {}", file, color=bcolors.FAIL)
        raise Exception("input files error")

    did = header["PHIDATID"]
    acc = header["ACCACCUM"]

    printc("-->>>>>>> data DID " + did, color=bcolors.OKGREEN)

    printc(
        "-->>>>>>> Reshaping data to [wave,Stokes,y-dim,x-dim] ", color=bcolors.OKGREEN
    )
    zd, yd, xd = data.shape
    data = np.reshape(data, (zd // 4, 4, yd, xd))
    data = np.ascontiguousarray(data)

    # -----------------
    # TAKE DATA DIMENSIONS
    # -----------------
    PXBEG1 = int(header["PXBEG1"]) - 1
    PXEND1 = int(header["PXEND1"]) - 1
    PXBEG2 = int(header["PXBEG2"]) - 1
    PXEND2 = int(header["PXEND2"]) - 1
    printc("Dimensions: ", PXBEG1, PXEND1, PXBEG2, PXEND2, color=bcolors.OKGREEN)

    if xd != (PXEND1 - PXBEG1 + 1) or yd != (PXEND2 - PXBEG2 + 1):
        printc(
            "ERROR, Keyword dimensions and data array dimensions dont match ",
            color=bcolors.FAIL)
        return 0
    if xd < 2047:
        printc(
            "         data cropped to: [",
            PXBEG1,",",
            PXEND1,"],[",
            PXBEG2,",",
            PXEND2,"]",color=bcolors.WARNING)

    data_scale = fits_get(file, get_scaling=True)

    # -----------------
    # READ AND CORRECT DARK FIELD
    # -----------------
    data, header = phi_correct_dark(dark_f, data, header, data_scale, verbose=verbose)

    ############################
    # Determine the image shifts. Fundamental for the case of flats. Here we just CALCULATE THEM and STORE THEM IN THE HEADER
    # calculate_shifts = True or False
    # shifts_method = 'circle' or 'Hough'
    ############################

    if calculate_shifts:
        if shifts_method == "Hough":
            printc("... calculating shifts using Hough ...", color=bcolors.OKGREEN)
            inner_radius, outer_radius, steps = hough_params
            centers, radius, threshold = find_circle_hough(data[0, 0, :, :], inner_radius, outer_radius, steps,
                threshold=0.01, loop_threshold=loop_threshold,normalize=False, verbose=verbose)
            cx = centers[0]
            cy = centers[1]

        elif shifts_method == "circle":
            printc("... calculating shifts using circle fit method ...",color=bcolors.OKGREEN)
            centers = np.array([0,0])
            centers[1],centers[0], radius = find_center(
                    data[0,0,:,:], sjump=4, njumps=100, threshold=0.8)

        else:
            printc(
                "Error in disp_method option. Given ",shifts_method," while options are [Hough,FFT,circle]",color=bcolors.FAIL)
            raise Exception

        # printc("center: ",centers[0],centers[1]," rad: ", radius,"*", color=bcolors.YELLOW)
        # printc("center (OLD): ",str(header['CRPIX1']),str(header['CRPIX2']), color=bcolors.YELLOW)
        # header['history'] = ' CRPIX 1 and CRPIX2 updated from ' + str(header['CRPIX1']) + ' and ' + str(header['CRPIX2'])
        # header['CRPIX1'] = (round(centers[0], 2))
        # header['CRPIX2'] = (round(centers[1], 2))
        # if 'RADIUS' in header:  # Check for existence
        #     header['RADIUS'] = (np.round(radius, 2))
        # else:
        #     header.set('RADIUS', (np.round(radius, 2)), 'Disk radius', after='CRPIX2')

        # Add new centers to the header
        printc('          Uptade header with new center:', color=bcolors.OKBLUE)
        printc('          Center from converter:CRPIX1[x]=', header['CRPIX1'], ' CRPIX2[y]=', header['CRPIX2'], ' radius=', radius, color=bcolors.OKBLUE)
        update_header(header, 'CRPIXN1', (round(cx, 3)), 'CRPIX1', comment='Description of ref pixel along axis 1, recomputed in phifdt_flat')
        update_header(header, 'CRPIXN2', (round(cy, 3)), 'CRPIX2', comment='Description of ref pixel along axis 2, recomputed in phifdt_flat')
        printc('          Center from converter:CRPIXN1[x]=', header['CRPIXN1'], ' CRPIXN2[y]=', header['CRPIXN2'], ' radius=', radius, color=bcolors.OKBLUE)


    # -----------------
    # GET INFO ABOUT VOLTAGES/WAVELENGTHS
    # -----------------
    printc("-->>>>>>> Obtaining voltages from data ", color=bcolors.OKGREEN)
    wave_axis, voltagesData, _, tunning_constant, cpos, ref_wavelength = fits_get_sampling(
        file
    )
    printc("          Data FG voltages: ", voltagesData, color=bcolors.OKBLUE)
    printc("          Continuum position at wave: ", cpos[0], color=bcolors.OKBLUE)
    printc("          Data ref_wavelength [mA]: ", ref_wavelength, color=bcolors.OKBLUE)
    printc("          Data wave axis [mA]: ", wave_axis, color=bcolors.OKBLUE)
    printc(
        "          Data wave axis - axis[0] [mA]: ",
        wave_axis - wave_axis[0],
        color=bcolors.OKBLUE)
    dummy_1 = (voltagesData - np.roll(voltagesData, -1)) * (tunning_constant * 1000)
    dummy_2 = np.sort(np.abs(dummy_1))
    sampling = np.mean(dummy_2[0:-2])
    printc('          Data average sampling [mA]: ', sampling, ' using tunning constant: ', (tunning_constant * 1000), color=bcolors.OKBLUE)

    # -----------------
    # FRINGE CORRECTION
    # -----------------

    if correct_fringes == 'auto' or correct_fringes == 'manual':
        #demodulate data:
        instrument = 'auto'
        data, header = phi_apply_demodulation(data, instrument, header=header, FDT_MOD_ROTATION_ANGLE = -127.6)

        if verbose:
            ic = np.mean(data[0,0,yd//2-50:yd//2+50,xd//2-50:xd//2+50])

            plib.show_four_row(
                data[cpos[0], 0, :, :]/ic, data[2, 1, :, :]/ic, data[2, 2, :, :]/ic, data[2, 3, :, :]/ic,
                title=['I - before fringe', 'Q', 'U', 'V'],
                zoom=3,
                svmin=[0, -0.005, -0.005, -0.005], svmax=[1.2, 0.005, 0.005, 0.005])

        NFREQ_LEVEL = 0. #TODO add input option
        ATT_FACTOR = 1e-6  #TODO add input option
        which_stokes = [1,2,3] #TODO add input option

        data, header, _, _, _ = phi_correct_fringes(
            data, header, option=correct_fringes, verbose=verbose, fringe_threshold=fringe_threshold,
            NFREQ_LEVEL=NFREQ_LEVEL, ATT_FACTOR=ATT_FACTOR, continuum_point=cpos[0], max_frequencies=30, rad_min=8, rad_max=30,
            wsize=50, which_stokes=which_stokes)

        if verbose:
            plib.show_four_row(
                data[cpos[0], 0, :, :]/ic, data[2, 1, :, :]/ic, data[2, 2, :, :]/ic, data[2, 3, :, :]/ic,
                title=['I - after fringe', 'Q', 'U', 'V'],
                zoom=3,
                svmin=[0, -0.005, -0.005, -0.005], svmax=[1.2, 0.005, 0.005, 0.005])
        #modulate back
        data = phi_apply_demodulation(data, instrument, header=header,modulate=True, FDT_MOD_ROTATION_ANGLE = -127.6)

    elif correct_fringes is not None:
        printc('Error in option finge correction. Options are "manual", "auto", or None. Given: ', correct_fringes, color=bcolors.WARNING)
    else:
        printc("-->>>>>>> No fringes correction", color=bcolors.WARNING)

    # -----------------
    # CORRECT DISTORTION
    # -----------------

    if correct_distortion:
        data, header = phi_correct_distortion(
            data, header, parallel=parallel, verbose=verbose,
        )
    else:
        printc("-->>>>>>> No distortion correction", color=bcolors.WARNING)

    # -----------------
    # CORRECT PREFILTER
    # -----------------

    if correct_prefilter:
        data, header = phi_correct_prefilter(
            prefilter_fits, header, data, voltagesData, verbose=verbose
        )
    else:
        printc("-->>>>>>> No prefilter correction", color=bcolors.WARNING)

    # -----------------
    # GHOST CORRECTION
    # -----------------

    if correct_ghost:
        # all this commented 19 April. Not used.
        # # here I am recalculating the correct center of the image. If I use the header it does not work
        # cy, cx, radius = find_center(
        #     data[0, 0, :, :], sjump=4, njumps=100, threshold=0.8
        # )  # OJO Cy... Cx
        # c = np.array([int(cx), int(cy)])
        # # El vector es [0,1,2,...] == [x,y,z,...] == [cx,cy,cz,...] Pero esto ultimo esta al reves
        # radius = int(radius)

        # TODO: not 100% sure about the cont_index = data_index[cpos[0]]

        data, header = phi_correct_ghost(data, header, cpos[0], ghost_params, flat=flat_ghost, verbose=verbose, ghost_image=ghost_image)

    else:
        printc("-->>>>>>> No ghost correction", color=bcolors.WARNING)

    printc(
        "---------------------------------------------------------",
        color=bcolors.OKGREEN)

    # -----------------
    # SAVE DATA
    # -----------------
    # basically replace L1 by L1.5
    # outfile = set_level(file, "L1", "L1.5")
    # outfile = set_level(outfile, "ilam", "ilam_offpoints")
    outfile = append_id(file, filetype, version, did)

    printc(" Saving data to:", outfile)

    # data = data.astype(np.float32)
    data = data.astype('float32')
    nwl, npol, ny, nx = data.shape
    nframes = nwl * npol
    data = np.reshape(data, (nframes, ny, nx))

    if save_stokes:
        with pyfits.open(file) as hdu_list:
            hdu_list[0].data = data
            hdu_list[0].header = header
            hdu_list.writeto(outfile, overwrite=True)

    return data,header
