import sys
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

# sys.path.append('../../')
# import sophi_fdt_dpp as fdt

def rte_inter():

    from .phi_fits import fits_get
    from .phi_rte import VERS_RTE, phi_rte

    def format_axes(fig):
        for ax in fig.axes:
            # ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
            ax.tick_params(labelbottom=True, labelleft=True)

    class CursorApp(object):
        def __init__(self, ax):
            self.ax  = ax                     # the plot
            self.lx  = ax.axhline(color='red',linewidth = 0.5)  # the horiz line
            self.ly  = ax.axvline(color='red',linewidth = 0.5)  # the vert line

            # Text location in data coords
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.4)
            self.txt = self.ax.text(0,0, '', fontsize = 8, bbox=props)

        def mouse_move(self, event):
            """
            Track the movement of the mouse and update the position of the pixels
            """

            #shall be within the plot axis!!!!
            if not event.inaxes:
                return
            # # shall also be in the right pannel (the image on the left)

            # for i, ax in enumerate([ax1, ax2, ax3, ax4, ax5]):
            #     # For infomation, print which axes the click was in
            #     if ax == event.inaxes:
            #         print("Click is in axes ax",i+1)

            if event.inaxes in [ax1]:
                self._mouse_move_event(event)

        def _mouse_move_event(self, event):

            # xx, yy = event.x, event.y       # x,y data
            x = np.clip(round(event.xdata),a_min = 0, a_max = sx - 1)
            y = np.clip(round(event.ydata),a_min = 0, a_max = sy - 1)           # x,y coordinates of mouse
            # print('x=%1.2f, y=%1.2f' % (x, y))

            self.txt.set_position((x,y))
            self.txt.set_text('v = %1.2f' % (s[x,y]))

            # Find closest pt on data curve to (x,y) of cross-air intersection
            # indx = min(np.searchsorted(self.x, [x])[0], len(self.x) - 1)
            # x = self.x[indx]
            # y = self.y[indx]
            # Update the line positions
            self.lx.set_ydata(y)
            self.ly.set_xdata(x)
            # place a text box in upper left in axes coords
            #self.ax.text(x, y, 'test', transform=ax.transAxes, fontsize=8,
            #        verticalalignment='top', bbox=props)
            self.ax.figure.canvas.draw_idle()

        def key_press(self,event):
            print('press', event.key)
            sys.stdout.flush()

            if event.key == 'q':
                sys.exit
            elif event.key == 'b':
                me_map = 0
            elif event.key == 'g':
                me_map = 1
            elif event.key == 'a':
                me_map = 2
            elif event.key == 'v':
                me_map = 3
            elif event.key == 'l':
                me_map = 4
            elif event.key == 'c':
                me_map = 5

        # Make a plot
    t = np.arange(0.0, 1.0, 0.01)
    s = np.sin(2 * 2 * np.pi * t)
    s = np.outer(s,s)

    # Make a plot
    w = np.arange(0.0, 5.0, 1)
    p = np.sin(2 * 2 * np.pi * w)

    # create figure
    fig = plt.figure(figsize=(14,5), constrained_layout=False)

    gs = GridSpec(2, 4, figure=fig,wspace=0.3,hspace=0.3)
    ax1 = fig.add_subplot(gs[:, :2])
    ax2 = fig.add_subplot(gs[0, 2])
    ax3 = fig.add_subplot(gs[0, 3])
    ax4 = fig.add_subplot(gs[1, 2])
    ax5 = fig.add_subplot(gs[1, 3])

    format_axes(fig)
    ###

    # fig, maps = plt.subplots(1,2,figsize=(14,8))
    # plt.subplots_adjust(hspace=0.3, wspace=0.3)

    fig.canvas.set_window_title('Data inversion viewer')

    #plot data

    # ax1.plot (t, s, 'o')
    ax1.imshow(s)
    sy,sx = s.shape
    # ax1.axis([0, 1, -1, 1])
    ax1.grid(axis='both')

    ax2.plot(w,p,'.-')
    ax2.grid(axis='both')
    ax3.plot(w,p,'.-')
    ax3.grid(axis='both')
    ax4.plot(w,p,'.-')
    ax4.grid(axis='both')
    ax5.plot(w,p,'.-')
    ax5.grid(axis='both')

    # cursor = Cursor(ax)
    cursor = CursorApp(ax1)
    # plt.connect('motion_notify_event', cursor.mouse_move)
    fig.canvas.mpl_connect('motion_notify_event', cursor.mouse_move)
    fig.canvas.mpl_connect('key_press_event', cursor.key_press)

    plt.show()

def test_it():
    import sys
    sys.path.append('../../')
    import sophi_fdt_dpp as fdt
    fdt.rte_inter()

if __name__=='__main__':
    for i in sys.argv:
        print(i)
    test_it()
