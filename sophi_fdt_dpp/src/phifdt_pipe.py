"""phifdt_pipe

``phifdt_pipe.py`` Main routine for onground data procesing of PHI/FDT raw data.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez**
   :Contributors: Nestor Albelo (albelo@mps.mpg.de) and Alex Feller (feller@mps.mpg.de)

Check main documentation for explanations.

"""

import datetime
import json
import os.path
import subprocess

import numpy as np
from scipy.ndimage import gaussian_filter
from copy import deepcopy
from matplotlib import pyplot as plt
from astropy.io import fits as pyfits

from . import plot_lib as plib
from .phi_fits import fits_get, fits_get_sampling, set_level, append_id
from .phi_gen import find_circle_hough, generate_circular_mask, shift
from .phi_reg import PHI_shifts_FFT, shift_subp
from .phi_utils import debug
from .phifdt_pipe_modules import (
    check_pmp_temp,
    cross_talk_QUV,
    crosstalk_ItoQUV,
    generate_level2,
    phi_apply_demodulation,
    phi_correct_dark,
    phi_correct_fringes,
    phi_correct_ghost,
    phi_correct_prefilter,
    save_level2,
    zero_level,
    phi_correct_distortion,
    update_header_stokes,
    crop_calibration_data)
from .tools import bcolors, printc, update_header
from .phifdt_disk import find_center


def phifdt_pipe(
        json_input: str = None,
        data_f: str = None,
        dark_f: str = None,
        flat_f: str = None,
        input_data_dir: str = './',
        output_dir: str = './',
        instrument: str = 'auto',
        flat_c: bool = True,
        dark_c: bool = True,
        ItoQUV: bool = False,
        VtoQU: bool = False,
        ind_wave: bool = False,
        hough_params: list = [250, 800, 100],
        normalize_flat: bool = False,
        flat_scaling: float = 1.,
        flat_index: list = [], #TODO: we should remove this option becouse of the new USE_CONT_FLAT option
        USE_CONT_FLAT: bool = False,
        smooth_flats_polarization: bool = False,
        use_six_flats: bool = False,
        prefilter: bool = True,
        prefilter_fits: str = '0000990710_noMeta.fits',
        realign: bool = False,
        verbose: bool = True,
        shrink_mask: int = -10,
        correct_fringes: str = False,
        which_stokes: list = [1,2,3],
        fringe_threshold: float = 1.00,
        correct_ghost: bool = False,
        ghost_params: list = [-1.98796, 1936.70, -1.98944, 1947.15, 0.0008, 0.0024, -0.0032, 7],
        correct_distortion: bool = False,
        putmediantozero: bool = False,
        extra_debug: bool = False,
        nlevel: float = 0.3,
        center_method: str = 'circlefit',
        vers: str = '01',
        rte: str = False,
        cavity_file: str = None,
        correct_cavity: bool = False,
        weight: list = [1.00,4.20,3.66,3.82],
        RTE_code: str = 'cmilos',
        pol_c_file: str = None,
        outfile: str = None,
        scattering_veil: float = 0,
        FIGUREOUT: str = '.png',
        SIX_FLATS: bool = False,
        SIGMA_MEDIAN_FILTER: float = 30.,
        FLAT_MEDIAN_FILTER: float = 9.,
        PLT_RNG: float = 5,
        PERCENT_OF_DISK_FOR_MASKI: float = 0.9,  # 90% of the disk
        PERCENT_OF_DISK_FOR_NORMALIZATION: float = 0.2,  # 20% of the disk
        PERCENT_OF_DISK_FOR_CROSST: float = 0.96,  # 96% of the disk
        PERCENT_OF_DISK_FOR_FLAT_NORM: float = 0.3,  # 30% of the disk
        FDT_MOD_ROTATION_ANGLE: float = -127.6,
        FIVE_SIGMA: float = 5,
        ATT_FACTOR: float = 1e-6,
        NFREQ_LEVEL: float = 0.,
        crosstalk_order: float = 1.,
        #these are needed for the flat data processing
        flat_preprocess: bool = False,
        calculate_shifts: bool = True,
        threshold: float = 0.05,
        loop_threshold: int = 2,
        save_stokes: bool = True,
        save_ghost_image: bool = False,
        ghost_image: str = str(),
        #until here
        parallel: bool = False,
        num_workers: int = 10,
        RTE_options = None) -> int:

    '''
    Main program for processing FDT data. Currently in development, it processes FDT raw (level 1)
    data. It has many input parameters that have to be tailored depending on the observations (PMP temperatures, orbit position, etc.).
    The pipeline can be run directly from a Python terminal and seting the different keywords or using a JSON file.

    The main steps done within the pipeline are:

    (TO BE REVISED)
    1. Read data and check dimensions and other keywords in the header
    2. Read dark field and correct the data
    3. Read input flats and do the flat field correction
    4. Find center of the Sun in the data for masking and ghost correction
    5. Correct prefilter - needs prefilter data file
    6. Demodulate data using appropriate demodulation matrix
    7. Correct ghost
    8. Correct fringe pattern
    9. Correct cross-talk from I to QUV
    10. Normalize data and set the polarize continuum to zero
    11. Get wavelength sampling from header
    12. Save level 2 stokes data
    13. Prepare data for inversion
    14. Invert data
    15. Save level 2 inversion results

    A full explanation of the different options are provided in the main documentation.
    Here we just provide the basic functionality of the different keywords.

    :param json_input: input JSON file. Overrides all parameters. ``data, header = phifdt_pipe(json_input='input.json')``, defaults to None
    :type json_input: str
    :param data_f: data filename. For path use input_data_dir keyword.
    :type data_f: str
    :param input_data_dir: data filename full path, defaults to local directory
    :type input_data_dir: str
    :param dark_f: fits file of a valid dark file (processed dark), including the full path, defaults to None
    :type dark_f: str
    :param dark_c: run dark correction, defaults to False
    :type dark_c: bool
    :param flat_f: fits file of a Valid FDT flatfield, including the full path, defaults to None
    :type flat_f: str
    :param flat_c: run flat correction, defaults to False
    :type flat_c: bool
    :param output_dir: full path of output directory, defaults to './'
    :type output_dir: str

    .. note::
        dark_f, flat_f, and prefilter file must be provided with the FULL PATH.

    Flat related correction parameters.

    :param normalize_flat: normalize flats internally to be around one, defaults to False
    :type normalize_flat: bool
    :param PERCENT_OF_DISK_FOR_FLAT_NORM: Percentage of sun (wrt the input data solar radious) to be used for determining the normalization factor, defaults to 0.3 (30% of the solar disk)
    :type PERCENT_OF_DISK_FOR_FLAT_NORM: float
    :param flat_scaling: additional flat scaling ``flat = flat / flat_scaling``, defaults to one
    :type flat_scaling: float
    :param flat_index: Useful for selecting which flat to applay to which wavelength. Example: ``flat_index = [5,1,2,3,4,0]`` exchange the first and last wave flats, defaults to None
    :type flat_index: None or list
    :param use_six_flats: Average the flat in the polarization dimension and apply the resulting six flats to the data, defaults to False
    :type use_six_flats: bool
    :param smooth_flats_polarization: Smooth out the flats (the polarization only) using a Gaussiang filter, defaults to False
    :type smooth_flats_polarization: bool
    :param FLAT_MEDIAN_FILTER: Amount of blurring in the Gaussian filter applied in ``smooth_flats_polarization``, defaults to 9
    :type FLAT_MEDIAN_FILTER: float
    :param SIX_FLATS: Use intensity flats only for the four modulations, defaults to False
    :type SIX_FLATS: bool
    :param USE_CONT_FLAT: Use continuum flat only. In this case, it is advisory to add a cavity file, defaults to False
    :type USE_CONT_FLAT: bool

    Rest of parameters:

    :param prefilter: apply perfilter correction, defaults to False
    :type prefilter: bool
    :param prefilter_fits: fits file of a valid prefilter file, including the full path, defaults to None
    :type prefilter_fits: str
    :param correct_ghost: apply ghost correction, defaults to False
    :type correct_ghost: bool
    :param ghost_params: ghost parameters [ax, bx, ay, by, sc_q, sc_u, sc_v, sigma], defaults to [-1.98796, 1936.70, -1.98944, 1947.15, 0.0008, 0.0024, -0.0032, 7]
    :type ghost_params: list
    :param correct_distortion: apply FDT distortion correction, defaults to False
    :type correct_distortion: bool
    :param correct_fringes: apply FDT fringes correction, ``['auto','manual','interpol']``, defaults to False
    :type correct_fringes: bool
    :param fringe_threshold: fringe detection level for `àuto`` mode, defaults to 1.0
    :type fringe_threshold: float
    :param ATT_FACTOR: attenuation factor for ``manual`` mode, defaults to 1e-6
    :type ATT_FACTOR: float
    :param NFREQ_LEVEL: attenuation factor for ``auto`` mode, defaults to 0
    :type NFREQ_LEVEL: float
    :param ItoQUV: apply crostalk correction from Stokes I to Stokes Q, U, and V, , ``['standard','surface']``, defaults to None
    :type ItoQUV: str
    :param VtoQU: apply crostalk correction from Stokes V to Stokes Q, U, defaults to False. NOT WORKING
    :type VtoQU: bool
    :param ind_wave: apply crostalk correction from Stokes I to Stokes Q, U, V in individual wavelengths, defaults to False
    :type ind_wave: bool
    :param crosstalk_order: apply crostalk correction from Stokes I to Stokes Q, U, V using a polinomial fir or given order, defaults to 1
    :type crosstalk_order: int
    :param which_stokes: select Stokes parameters to apply crostalk correction, defaults to [1,2,3] (i.e., Stokes Q, U and V are corrected)
    :type which_stokes: list
    :param nlevel: Noise level wrt Ic above which to evaluate cross_talk_VQU (under development), defaults to 0.3
    :type nlevel: float
    :param PERCENT_OF_DISK_FOR_CROSST:  Percentage of sun (wrt the input data solar radious) to be used for determining the crosstalk parameters, defaults to 0.96, (96% of the disk)
    :type PERCENT_OF_DISK_FOR_CROSST: float
    :param instrument: demodulation matrix for FDT: ``['FDT40',FDT45','auto']``, defaults to ``auto``
    :type instrument: str
    :param FDT_MOD_ROTATION_ANGLE: Modulation matrix rotation, defaults to -127.6
    :type FDT_MOD_ROTATION_ANGLE: float
    :param PERCENT_OF_DISK_FOR_NORMALIZATION: Percentage of sun (wrt the input data solar radious) to be used for normalizing the data to the continuum intensity, defaults to 0.2 (20% of the solar disk)
    :type PERCENT_OF_DISK_FOR_NORMALIZATION: float
    :param putmediantozero: Last step prior RTE, puts the median value of the polarization continuum (Q,U, and V) to ve of zero median value. Can be ``[True,False`'2D']``, defaults to True
    :type putmediantozero: bool or str
    :param PERCENT_OF_DISK_FOR_MASKI: Percentage of sun (wrt the input data solar radious) to be used for calculating the median value, defaults to 0.9 (90% of the solar disk)
    :type PERCENT_OF_DISK_FOR_MASKI: float
    :param pol_c_file: fits file of a valid image file to be suppresed to the polarization images (6 waves in Stokes Q, U, and V), including the full path, defaults to None
    :type pol_c_file: str
    :param SIGMA_MEDIAN_FILTER: When ``putmediantozero='2D'``, this parameters sets the Gaussian blurring to be applied to the polarization continuum before subtraction, defaults to 30 (pixels)
    :type SIGMA_MEDIAN_FILTER: float

    RTE options:

    :param shrink_mask: Number of pixels to extend the sun mask for RTE input, defaults to -10 (extends outwards)
    :type shrink_mask: int
    :param RTE_code: uses CMILOS ASCII version or pmilos ``RET_code = ['cmilos','pmilos']``, defaults to ``'pmilos'``
    :type RTE_code: str
    :param rte: False or string specifying the RTE mode, defaults to False

        * ``rte = 'RTE'`` : ME only
        * ``rte = 'CE+RTE'`` : ME + with classical estimates
        * ``rte = 'CE'`` : Only classical estimates
        * ``rte = 'CE+RTE+PSF'`` : RTE with classical estimates and spectral PSF (106 mA)

    :type rte: str
    :param weight: uses CMILOS with given weight for Stokes I,Q,U, and V. Default is None, which takes CMILOS internal weights ``weight = [1,10,10,4]``

    .. warning::
        ``weight`` keyword only takes effect when using ``RTE_code = pmilos`` . In ``cmilos`` the weights are hardcoded in line 454 of ``milos.c``

    :type weight: np.ndarray
    :param correct_cavity: correct cavity map before inversion, defaults to False
    :type correct_cavity: bool
    :param cavity_file: fits file of a valid cavity file (in mA), including the full path, defaults to None, defaults to None
    :type cavity_file: str
    :param RTE_options: list of options for the RTE. See phi_ret.py , defaults to None
    :type RTE_options: list
    :param scattering_veil: percentage of ``veil`` in the data (under developement), defaults to 0
    :type scattering_veil: float

    Other options:

    :param center_method: method for finding the center of the Sun. It is used for generating all masks ``['circlefit','hough',None]``. If None, it takes the center information from the header (CRPIX). defaults to ``'circlefit'``

    .. note::
        Keywords CRPIX1 and CRPIX2 are updated following the new center calculation within the pipeline. Old values are stored in the history.
        Keywords CRVAL1 and CRVAL2 are NOT updated.

    :type center_method: float
    :param hough_params: initial values for finding sun center when ``center_method = 'hough'``, defaults to [250, 800, 100], inner_radius = 250, outer_radius = 600, steps = 100
    :type hough_params: tuple
    :param realign: Realign all images before demodulating using FFT (under development), defaults to False
    :type realign: bool
    :param extra_debug: For debugging purposes, defaults to False
    :type extra_debug: bool
    :param PLT_RNG: image plots range = meax +/-  PLT_RNG, defaults to 5
    :type PLT_RNG: float
    :param FIVE_SIGMA: sigma to avoid outliers in the zero mean calculations, defaults to 5
    :type FIVE_SIGMA: float
    :param parallel: run RTE in parallel, defaults to False
    :type parallel: bool
    :param num_workers: number of processor to use in parallel processing, defaults to 10
    :type num_workers: int
    :param FIGUREOUT: output figure format, defaults to ``'.png'``
    :type FIGUREOUT: str
    :param verbose: increase the verbosity (many plots here) - default False
    :type verbose: bool
    :param outfile: output filename. Overides default, defaults to None
    :type outfile: str
    :param vers: output filename version to be added to the fits. defaults to '01'
    :type vers: str

    Flat preprocessing options:
    :param flat_preprocess: if true, the saved stokes vector is modulated back for flatfield inputs, defaults to False
    :type flat_preprocess: bool
    :param calculate_shifts: recalculate shifts. Options are 'Hough0 or 'circlefit', defaults to False
    :type calculate_shifts: bool
    :param threshold: circlefit option, defaults to 0.05
    :type threshold: float
    :param loop_threshold: circlefit option, defaults to 2`
    :type loop_threshold: int
    :param save_stokes: save Stokes vector or not - default to True
    :type save_stokes: str
    :param save_ghost_image: To save the ghost image in fits format, in output directory. Defaults to Fase.
    :type save_ghost_image: bool
    :param ghost_image: input ghost image. If provided, this image (fits) is used as ghost image. Need full path.
    :type ghost_image: str


    -----
    This program is not optimized for speed. It assumes that input data is 6 wavelength.
    C-MILOS must be compiled in each specific machine (C)
    The software update some of the information in the fits keyword:

    TODO:
    # data_f -> input data (single file for FDT - ADD MULTIPLE FILES!!!! )
    keyword to provide fixed cross-talk coefficients
    keyword to provide fixed data normalization (based on a first obs)
    # pending to add class stile (for module development)

    Keywords in the header (modified or added) within this program:

    VERS_SW =  / software version
    CAL_DARK =  / Onboard calibrated for dark field ! Dark correction ( DID )
    CAL_FLAT =  / Onboard calibrated for gain table ! Flat correction ( DID )
    CAL_DIS  =  / 'Distortion parameters applied (x, y, k)'
    CAL_GHST =  / ghost correction version (phifdt_pipe_modules.py)
    CAL_PRE  =             Prefilter / Prefilter correction ( prefilter file)
    CAL_IPOL  =  instrument /'Onboard calibrated for instrumental polarization'
    CAL_NORM  =  normalization constant
    * CAL_REAL=              Prefilter / Prealigment of images before demodulation ( name+version of py module if True )
    CAL_CRT0=               float / cross-talk from I to Q (slope value, wrt normalized data in python)
    CAL_CRT1=               float / cross-talk from I to Q (off-set value, wrt normalized data in python)
    CAL_CRT2=               float / cross-talk from I to U (slope value, wrt normalized data in python)
    CAL_CRT3=               float / cross-talk from I to U (off-set value, wrt normalized data in python)
    CAL_CRT4=               float / cross-talk from I to V (slope value, wrt normalized data in python)
    CAL_CRT5=               float / cross-talk from I to V (off-set value, wrt normalized data in python)
    * CAL_CRT6=               float / cross-talk from V to Q (slope value, wrt normalized data in python)
    * CAL_CRT7=               float / cross-talk from V to Q (off-set value, wrt normalized data in python)
    * CAL_CRT8=               float / cross-talk from V to U (slope value, wrt normalized data in python)
    * CAL_CRT9=               float / cross-talk from V to U (off-set value, wrt normalized data in python)
    CAL_RTE=                990510 / ok
    RTE_ITER=           4294967295 / Number RTE inversion iterations
    CRPIXN1 = Recomputed solar disk center
    CRPIXN2 = Recomputed solar disk center
    RADIUS = Recomputed solar disk radius

    (*) are not touched in this software.

    This main program shall also be used for preprocess FDT off-points data to remove dark, prefilter, ghost, distortion or fringes from the data
    and save the results into a ``level 1.5`` format prior flatfield determination.
    Since the ghost correction needs a flat, the flat calculation is done as follows:

    1- Do a correction with NO flat and NO ghost (we could correct the rest of things)
    2- Obtain a preliminary flat with those preprocessed data
    3- Use the obtained flat to activate the ghost correction

       For correcting the off-points ghost one "needs" the ghost from the first (no offpoint) position

       3.1 correct first flat dataset with save_ghost_image = True
       3.2 correct rest of flat datasets with ghost_image = 'name.fits'

    4- Optain a final flat.

    There is a special keyword that shall be used for saving the Stokes data in the proper format.

    Example::

        if i = list of files
        for i in files:
            fdt.fdt_flat_preprocessing(i, dark_file, verbose = True,correct_ghost = True,correct_prefilter = True,
                prefilter_fits = prefilter_file, version = '01')


    '''

    if json_input:

        # =========================================================================== #
        # READING input_parameters FILE AND PRINTING
        printc('--------------------------------------------------------------', bcolors.OKGREEN)
        printc(' Reading input_parameters json file ' + json_input, bcolors.OKGREEN)
        with open(json_input) as j:
            input_parameters = json.load(j)

        # Get mandatory parameters
        data_f = input_parameters['data_f']

        # Extra params are pre-defined and overwritten if present in json file
        flat_f = input_parameters.get('flat_f', flat_f)
        dark_f = input_parameters.get('dark_f', dark_f)
        fringe_threshold = input_parameters.get('fringe_threshold', fringe_threshold)
        pol_c_file = input_parameters.get('pol_c_file', pol_c_file)
        FIGUREOUT = input_parameters.get('FIGUREOUT', FIGUREOUT)
        SIX_FLATS = input_parameters.get('SIX_FLATS', SIX_FLATS)
        SIGMA_MEDIAN_FILTER = input_parameters.get('SIGMA_MEDIAN_FILTER', SIGMA_MEDIAN_FILTER)
        FLAT_MEDIAN_FILTER = input_parameters.get('FLAT_MEDIAN_FILTER', FLAT_MEDIAN_FILTER)
        PLT_RNG = input_parameters.get('PLT_RNG', PLT_RNG)
        PERCENT_OF_DISK_FOR_MASKI = input_parameters.get('PERCENT_OF_DISK_FOR_MASKI', PERCENT_OF_DISK_FOR_MASKI)
        PERCENT_OF_DISK_FOR_NORMALIZATION = input_parameters.get('PERCENT_OF_DISK_FOR_NORMALIZATION', PERCENT_OF_DISK_FOR_NORMALIZATION)
        PERCENT_OF_DISK_FOR_FLAT_NORM = input_parameters.get('PERCENT_OF_DISK_FOR_FLAT_NORM', PERCENT_OF_DISK_FOR_FLAT_NORM)
        PERCENT_OF_DISK_FOR_CROSST = input_parameters.get('PERCENT_OF_DISK_FOR_CROSST', PERCENT_OF_DISK_FOR_CROSST)
        FDT_MOD_ROTATION_ANGLE = input_parameters.get('FDT_MOD_ROTATION_ANGLE', FDT_MOD_ROTATION_ANGLE)
        FIVE_SIGMA = input_parameters.get('FIVE_SIGMA', FIVE_SIGMA)
        ATT_FACTOR = input_parameters.get('ATT_FACTOR', ATT_FACTOR)
        NFREQ_LEVEL = input_parameters.get('NFREQ_LEVEL', NFREQ_LEVEL)
        crosstalk_order = input_parameters.get('crosstalk_order', crosstalk_order)
        cavity_file = input_parameters.get('cavity_file', cavity_file)
        correct_cavity = input_parameters.get('correct_cavity', correct_cavity)
        weight = input_parameters.get('weight', weight)
        correct_ghost = input_parameters.get('correct_ghost', correct_ghost)
        ghost_params = input_parameters.get('ghost_params', ghost_params)
        normalize_flat = input_parameters.get('normalize_flat', normalize_flat)
        smooth_flats_polarization = input_parameters.get('smooth_flats_polarization', smooth_flats_polarization)
        use_six_flats = input_parameters.get('use_six_flats', use_six_flats)
        verbose = input_parameters.get('verbose', verbose)
        input_data_dir = input_parameters.get('input_data_dir', input_data_dir)
        shrink_mask = input_parameters.get('shrink_mask', shrink_mask)
        center_method = input_parameters.get('center_method', center_method)
        hough_params = input_parameters.get('hough_params', hough_params)
        instrument = input_parameters.get('instrument', instrument)
        dark_c = input_parameters.get('dark_c', dark_c)
        flat_c = input_parameters.get('flat_c', flat_c)
        flat_index = input_parameters.get('flat_index', flat_index)
        USE_CONT_FLAT = input_parameters.get('USE_CONT_FLAT', USE_CONT_FLAT)
        flat_scaling = input_parameters.get('flat_scaling', flat_scaling)
        prefilter_fits = input_parameters.get('prefilter_fits', prefilter_fits)
        prefilter = input_parameters.get('prefilter', prefilter)
        output_dir = input_parameters.get('output_dir', output_dir)
        rte = input_parameters.get('rte', rte)
        correct_distortion = input_parameters.get('correct_distortion', correct_distortion)
        correct_fringes = input_parameters.get('correct_fringes', correct_fringes)
        which_stokes = input_parameters.get('which_stokes', which_stokes)
        putmediantozero = input_parameters.get('putmediantozero', putmediantozero)
        extra_debug = input_parameters.get('extra_debug', extra_debug)
        vers = input_parameters.get('vers', vers)
        RTE_code = input_parameters.get('RTE_code', RTE_code)
        ItoQUV = input_parameters.get('ItoQUV', ItoQUV)
        VtoQU = input_parameters.get('VtoQU', VtoQU)
        realign = input_parameters.get('realign', realign)
        ind_wave = input_parameters.get('ind_wave', ind_wave)
        nlevel = input_parameters.get('nlevel', nlevel)
        parallel = input_parameters.get('parallel', parallel)
        num_workers = input_parameters.get('num_workers', num_workers)
        flat_preprocess = input_parameters.get('flat_preprocess', flat_preprocess)
        calculate_shifts = input_parameters.get('calculate_shifts', calculate_shifts)
        threshold = input_parameters.get('threshold', threshold)
        loop_threshold = input_parameters.get('loop_threshold', loop_threshold)
        save_stokes = input_parameters.get('save_stokes', save_stokes)
        save_ghost_image = input_parameters.get('save_ghost_image', save_ghost_image)
        ghost_image = input_parameters.get('ghost_image', ghost_image)
        #until here

    else:
        printc(' Using sequencial mode ', bcolors.OKGREEN)
        input_parameters = deepcopy(locals())

    def __parinfo(params):
        printc('_______________________________________________',color=bcolors.OKGREEN)
        printc('Input parameter:                   [value]     ',color=bcolors.OKGREEN)
        printc('_______________________________________________',color=bcolors.OKGREEN)
        for item in params.items():
            extension = len(item[0])
            spaces = 28 - extension
            string_val = " " * spaces
            printc('-->',f"{item[0]}: {string_val} {item[1]}",color=bcolors.OKBLUE)
        printc('_______________________________________________',color=bcolors.OKGREEN)

    __parinfo(input_parameters)

    # check all input files exist

    # version = 'V1.0 July 2021'
    # version = 'V1.0 13th September 2021'
    # version = 'V1.0 3th November 2021'
    # # added json configuration and modify all keyword names to be consistent with HRT pipe
    # version = 'V1.1 13th April 2022'
    # # modifies normalization within RTE
    # version = 'V1.1 8th June 2022'
    # VERS_SW = 'V1.1 June 8th 2022'
    # VERS_SW = 'V1.2 Sep 30th 2022'
    # added documentation (partly) and many modifications
    # 1-
    # VERS_SW = 'V1.3 Apr 3rd 2023'
    # Solved cropping issues with flatfield and cavity map (AF)
    # VERS_SW = 'V1.4 Aug 30rd 2023'
    # added functionalyty for fdt_flat_preprocessing (AF)
    # heavy cleaning
    VERS_SW = 'V1.5 Sept. 25rd 2023' #will be deprecated by gitlab releases
    version_cmilos = 'CMILOS v0.91 (July - 2021)' #will be deprecated by gitlab releases

    def __get_git_branch(path=None):
        from .phi_utils import cwd_
        with cwd_(os.path.dirname(os.path.realpath(__file__))):
            try:
                if path is None:
                    path = os.path.curdir
                command = 'git rev-parse --abbrev-ref HEAD'.split()
                branch = subprocess.Popen(command, stdout=subprocess.PIPE, cwd=path).stdout.read()
                return branch.strip().decode('utf-8')
            except:
                printc('Cannot get git branch name.',color=bcolors.WARNING)
                return ''

    printc('--------------------------------------------------------------', bcolors.OKGREEN)
    printc('             PHI FDT data reduction software  ', bcolors.OKGREEN)
    printc('             VERS_SW: ' + VERS_SW, bcolors.OKGREEN)
    printc('             GIT BRANCH: ' + __get_git_branch(), bcolors.OKGREEN)
    printc('--------------------------------------------------------------', bcolors.OKGREEN)

    # -----------------
    # CHECK INPUTS
    # -----------------

    data_filename = os.path.join(input_data_dir, data_f)

    if data_f is None:
        printc("Data file not provided ", data_filename, bcolors.FAIL)
        raise Exception("input error")

    if os.path.isfile(data_filename):
        printc("Data file exist", bcolors.OKGREEN)
    else:
        printc("Data file do not exist in current location ", data_filename, bcolors.FAIL)
        raise Exception("input files error")

    if flat_c:
        if flat_f is None:
            printc("Flat correction is ON but no flat file was provided", flat_f, bcolors.FAIL)
            raise Exception("flat file not provided")
        if os.path.isfile(flat_f):
            printc("Flat file exist", bcolors.OKGREEN)
        else:
            printc("Flat file do not exist in current location ", flat_f, bcolors.FAIL)
            raise Exception("input files error")

    if dark_c:
        if dark_f is None:
            printc("Dark correction is ON but no dark file was provided", flat_f, bcolors.FAIL)
            raise Exception("dark file not provided")

        if os.path.isfile(dark_f):
            printc("Dark file exist", bcolors.OKGREEN)
        else:
            printc("Dark file do not exist in current location ", dark_f, bcolors.FAIL)
            raise Exception("input files error")

    if prefilter:
        if os.path.isfile(prefilter_fits):
            printc("Prefilter file exist", bcolors.OKGREEN)
        else:
            printc("Prefilter file do not exist in current location ", prefilter_fits, bcolors.FAIL)
            raise Exception("input files error")

    if correct_cavity:
        if os.path.isfile(cavity_file):
            printc("Cavity file exists", bcolors.OKGREEN)
        else:
            printc("Cavity file does not exist in current location ", cavity_file, bcolors.FAIL)
            raise Exception("input files error")

    if correct_ghost and len(ghost_params) != 8:
        printc("Ghost params should be 8. Given ",len(ghost_params), bcolors.OKGREEN)
        printc("Error in number of ghost params ", ghost_params, bcolors.FAIL)
        raise Exception("input files error")

    # CHECK IF input is FITS OR FITS.GZ
    if data_f.endswith('.fits'):
        filetype = '.fits'
    elif data_f.endswith('.fits.gz'):
        filetype = '.fits.gz'
    else:
        raise ValueError("input data type neither .fits nor .fits.gz")

    # check if output_dir exist and if not, create it

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
        print("created folder : ", output_dir)
    else:
        print(output_dir, "output folder already exists.")

    # -----------------
    # READ DATA
    # -----------------

    try:
        printc('-->>>>>>> Reading Data file: ' + data_filename, color=bcolors.OKGREEN)
        data, header = fits_get(data_filename)
    except Exception:
        printc("ERROR, Unable to open fits file: {}", data_filename, color=bcolors.FAIL)
        raise

    DID = header['PHIDATID']
    ACC = header['ACCACCUM']

    printc('-->>>>>>> data DID ' + DID, color=bcolors.OKGREEN)

    # -----------------
    # SAVE JSON with DID
    # -----------------

    if json_input is None:
        FDT_json = output_dir + 'fdt_V' + vers + '_' + DID + '.json'
        with open(FDT_json, 'w', encoding="utf-8", newline='\r\n') as outfile_json:
            json.dump(input_parameters, outfile_json, indent=4, ensure_ascii=False)

    #TODO GET THE RIGHT VERSION HERE
    header = update_header(header, 'VERS_SW', VERS_SW, 'PHIDATID')

    printc('-->>>>>>> Reshaping data to [wave,Stokes,y-dim,x-dim] ', color=bcolors.OKGREEN)
    xd = int(header['NAXIS1'])
    yd = int(header['NAXIS2'])
    zd = int(header['NAXIS3'])
    zd_check, yd_check, xd_check = data.shape
    if (xd != xd_check) or (yd != yd_check) or (zd != zd_check):
        printc("ERROR, header and data size do not match", color=bcolors.FAIL)
        raise Exception("input files error")

    data = np.reshape(data, (zd // 4, 4, yd, xd))
    data = np.ascontiguousarray(data)

    # PHI_FITS_FPA_settings
    # FPIMGCMD= 8 / FPA image command
    # FPA_SROW= 0 / FPA start row setting FPA_EROW= 1022 / FPA end row setting
    # FPA_NIMG= 20 / FPA number of images set FPEXPSTC= 1592452786 / [s] FPA exposure start time coarse
    # FPEXPSTF= 699245 / [us] FPA exposure start time fine
    # INTTIME = 0.01 / [s] Exposure time of single readout
    # TELAPSE = 58.1974400877953 / [s]
    # Elapsed time between start and end of obser
    # NSUMEXP = 480 / Number of detector readouts
    # XPOSURE = 4.8 / [s] Total effective exposure time
    # ACCLENGT= 4194304 / ACCU number of pixel set
    # ACCNROWS= 6 / ACCU number of rows set
    # ACCROWIT= 1 / ACCU number of row iterations set
    # ACCNCOLS= 4 / ACCU number of columns set
    # ACCCOLIT= 1 / ACCU number of column iterations set
    # ACCACCUM= 20 / ACCU number of accumulations set
    # ACCADDR = 0 / ACCU readout address (start)

    header['history'] = ' Data processed with phifdt_pipe.py ' + VERS_SW
    header['history'] = '      and time ' + str(datetime.datetime.now())
    # header['history'] = ' Parameters normalize_flat: ' + str(normalize_flat)
    # header['history'] = ' Parameters flat_scaling: ' + str(flat_scaling)
    # header['history'] = ' Parameters shrink_mask: ' + str(shrink_mask)
    # header['history'] = ' Parameters center_method: ' + str(center_method)
    # header['history'] = ' Parameters Hough: ' + str(hough_params)

    if verbose:
        plib.show_one(data[0, 0, :, :], vmin=0, xlabel='pixel', ylabel='pixel', title='Data first image raw (1 of 24)', cbarlabel='DN', save=None, cmap='gray')

    #    * CAL_RTE=                990510 / ok
    #    * CAL_SCIP= 'None'               / Onboard scientific data analysis
    #    * RTE_ITER=           4294967295 / Number RTE inversion iterations
    #    * PHIDATID= '142010402'          / PHI dataset Id

    # -----------------
    # TAKE DATA DIMENSIONS AND SCALING
    # -----------------

    PXBEG1 = int(header['PXBEG1']) - 1
    PXEND1 = int(header['PXEND1']) - 1
    PXBEG2 = int(header['PXBEG2']) - 1
    PXEND2 = int(header['PXEND2']) - 1
    printc('Data dimensions: ', PXBEG1, PXEND1, PXBEG2, PXEND2, color=bcolors.OKGREEN)

    if xd != (PXEND1 - PXBEG1 + 1) or yd != (PXEND2 - PXBEG2 + 1):
        printc('ERROR, Keyword dimensions and data array dimensions dont match ', color=bcolors.FAIL)
        raise Exception("input files error")
    if xd < 2047:
        printc('         data cropped to: [', PXBEG1, ',', PXEND1, '],[', PXBEG2, ',', PXEND2, ']', color=bcolors.WARNING)

    data_scale = fits_get(data_filename, get_scaling=True)

    # -----------------
    # START ALL CORRECTIONS FROM HERE
    # -----------------

    # -----------------
    # READ AND CORRECT DARK FIELD
    # -----------------

    if dark_c:
        data, header = phi_correct_dark(dark_f, data, header, data_scale, verbose=verbose)
    else:
        printc('-->>>>>>> No darks mode                    ', color=bcolors.WARNING)

    # -----------------
    # CHECK PMP Temperature
    # -----------------
    if instrument == 'auto':
        instrument = check_pmp_temp(header)

    # -----------------
    # GET INFO ABOUT VOLTAGES/WAVELENGTHS
    # -----------------

    info = ["Continuum stored in right position","BLUE contunuum and stored in the RED","RED contunuum and stored in the BLUE"]

    printc('-->>>>>>> Obtaining voltages from data ', color=bcolors.OKGREEN)
    wave_axis, voltagesData, PHI_FG_setWavelength, tunning_constant, cpos, ref_wavelength = fits_get_sampling(data_filename)
    #correct wave_axis if FG temperature is 56 or 66 degree:
    if header["FGOV1PT1"] <= 57:
        wave_axis -= (36.46*5/1000.)
    if header["FGOV1PT1"] >= 65:
        wave_axis += (36.46*5/1000.)

    data_index = np.roll([0, 1, 2, 3, 4, 5], cpos[1])   # We need to undo the roll in the data index
    printc('          Data FG voltages: ', voltagesData, color=bcolors.OKBLUE)
    printc('          Data continuum position at wave: ', cpos[0], color=bcolors.OKBLUE)
    printc('          Data roll position of continuum: ', info[cpos[1]], color=bcolors.OKBLUE)
    printc('          Data index elements: ', data_index, color=bcolors.OKBLUE)
    printc('          Data ref_wavelength [mA]: ', ref_wavelength, color=bcolors.OKBLUE)
    printc('          Data wave axis [mA]: ', wave_axis, color=bcolors.OKBLUE)
    printc('          Data wave axis - axis[0] [mA]: ', wave_axis - wave_axis[0], color=bcolors.OKBLUE)
    dummy_1 = (voltagesData - np.roll(voltagesData, -1)) * (tunning_constant * 1000)
    dummy_2 = np.sort(np.abs(dummy_1))
    sampling = np.mean(dummy_2[0:-2])
    printc('          Data average sampling [mA]: ', sampling, ' using tunning constant: ', (tunning_constant * 1000), color=bcolors.OKBLUE)


    # -----------------
    # FIND DATA CENTER #TODO: should we do it after dark correction? FIXED
    # -----------------

    printc("... getting center from header ...",color=bcolors.OKGREEN)
    # get from header
    cx = header['CRPIX1']
    cy = header['CRPIX2']
    c = np.array([int(cx), int(cy)])  # El vector es [0,1,2,...] == [x,y,z,...] == [cx,cy,cz,...] Pero esto ultimo esta al reves
    radius = header['RSUN_ARC'] / header['CDELT1']
    update_header(header, 'RADIUS', (np.round(radius, 2)), 'CRPIX2', comment='Solar disk radius in pixel units')

    if calculate_shifts:
        printc('-->>>>>>> finding the center of the solar disk (needed for masking) ', color=bcolors.OKGREEN)
        if center_method.lower() == 'hough':
            printc("... calculating shifts using Hough ...", color=bcolors.OKGREEN)
            inner_radius, outer_radius, steps = hough_params
            centers, radius, threshold = find_circle_hough(data[0, 0, :, :], inner_radius, outer_radius, steps, threshold=0.01, loop_threshold=loop_threshold, normalize=False, verbose=verbose)
            cx = centers[0]
            cy = centers[1]
        elif center_method.lower() == 'circlefit':
            printc("... calculating shifts using circle fit method ...",color=bcolors.OKGREEN)
            cy, cx, radius = find_center(data[0, 0, :, :], sjump=4, njumps=100, threshold=0.8)  # OJO Cy... Cx
            c = np.array([int(cx), int(cy)])  # El vector es [0,1,2,...] == [x,y,z,...] == [cx,cy,cz,...] Pero esto ultimo esta al reves
        else:
            printc("... method not valid ... using keyword values",color=bcolors.FAIL)

        # Add new centers to the header
        printc('          Uptade header with new center:', color=bcolors.OKBLUE)
        printc('          Center from converter:CRPIX1[x]=', header['CRPIX1'], ' CRPIX2[y]=', header['CRPIX2'], ' radius=', radius, color=bcolors.OKBLUE)
        update_header(header, 'CRPIXN1', (round(cx, 3)), 'CRPIX1', comment='Description of ref pixel along axis 1, recomputed in phifdt_pipe')
        update_header(header, 'CRPIXN2', (round(cy, 3)), 'CRPIX2', comment='Description of ref pixel along axis 2, recomputed in phifdt_pipe')
        update_header(header, 'RADIUS', (np.round(radius, 2)), 'CRPIXN2', comment='Solar disk radius in pixel units')
        printc('          New center: CRPIXN1[x]=', header['CRPIXN1'], ' CRPIXN2[y]=', header['CRPIXN2'], ' radius=', header['RADIUS'], color=bcolors.OKBLUE)

    # WARNING
    # find_circle_hough devuelve c = c[0] = x and c[1] = y !!!!!!!!!!!!!!
    # Esto viene porque en el KLL esta definido así (al reves) en la rutina votes()

    # -----------------
    # DEFINE ALL THE MASKS THAT WILL BE USED IN THE PROCESSING
    # -----------------


    printc('-->>>>>>> Creating a mask for RTE with ', shrink_mask, ' px margin: mask')
    size_of_mask = radius - shrink_mask
    rx = [int(c[0] - size_of_mask), int(c[0] + size_of_mask)]
    ry = [int(c[1] - size_of_mask), int(c[1] + size_of_mask)]
    printc('   RX = ', rx, 'RY = ', ry, ' Only for display purposes', color=bcolors.WARNING)
    mask, coords = generate_circular_mask([xd - 1, yd - 1], size_of_mask, size_of_mask)
    mask = shift(mask, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0)

    printc('-->>>>>>> Creating a tight mask: mask_at_edge')
    mask_at_edge, coords = generate_circular_mask([xd - 1, yd - 1], np.round(radius) + 1, np.round(radius) + 1)
    mask_at_edge = shift(mask_at_edge, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0)

    printc('-->>>>>>> Creating a mask for flat normalization (optional): mask_for_flat')
    mask_for_flat, coords = generate_circular_mask([xd - 1, yd - 1], radius * PERCENT_OF_DISK_FOR_FLAT_NORM, radius * PERCENT_OF_DISK_FOR_FLAT_NORM)
    mask_for_flat = shift(mask_at_edge, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0)

    printc('-->>>>>>> Creating a mask for Stokes profiles (polarization) zero-level (optional): maski')
    maski, coords = generate_circular_mask([xd - 1, yd - 1], radius * PERCENT_OF_DISK_FOR_MASKI, radius * PERCENT_OF_DISK_FOR_MASKI)
    maski = shift(maski, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0).astype(int)

    printc('-->>>>>>> Creating a mask for cross-talk calculation: maskt')
    maskt, coords = generate_circular_mask([xd - 1, yd - 1], radius * PERCENT_OF_DISK_FOR_CROSST, radius * PERCENT_OF_DISK_FOR_CROSST)
    maskt = shift(maskt, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0).astype(int)

    printc('-->>>>>>> Creating a mask for data normalization: maskii')
    maskii, coords = generate_circular_mask([xd - 1, yd - 1], radius * PERCENT_OF_DISK_FOR_NORMALIZATION, radius * PERCENT_OF_DISK_FOR_NORMALIZATION)
    maskii = shift(maskii, shift=(c[0] - xd // 2, c[1] - yd // 2), fill_value=0).astype(int)

    # -------------------------------------------------------------------------
    # READ AND PREPARE FLAT FIELDS
    # Crop to data dimensions
    # Ghost correction needs a flatfield, even if flatfield correction
    # is not enabled!
    # -------------------------------------------------------------------------

    if flat_c:

        printc('-->>>>>>> Reading flat file' + flat_f, color=bcolors.OKGREEN)
        printc('          Assumes that flatfields are already normalized to ONE ', color=bcolors.OKGREEN)
        printc('          input should be [wave X Stokes,y-dim,x-dim].', color=bcolors.OKGREEN)

        try:
            flat, flat_header = fits_get(flat_f, scale=False)
        except Exception:
            printc("ERROR, something happened while reading the file: {}", flat_f, color=bcolors.FAIL)
            return 0

        printc('-->>>>>>> Checking flat dimensions', color=bcolors.OKGREEN)
        flat = crop_calibration_data(flat, flat_header, data, header)

        printc('-->>>>>>> Reshaping Flat to [wave,Stokes,y-dim,x-dim] ', color=bcolors.OKGREEN)
        fz, fy, fx = flat.shape
        flat = np.reshape(flat, (fz // 4, 4, fy, fx))

        printc('-->>>>>>> Scaling ', color=bcolors.OKGREEN)
        flat /= float(flat_scaling)

        new_flat_index = []
        if (len(flat_index)) == 6:
            new_flat_index = flat_index

        printc('-->>>>>>> Obtaining voltages from flats ', color=bcolors.OKGREEN)
        wave_axis_f, voltagesFlat,PHI_FG_setWavelength,  tunning_constant_f, cpos_f, ref_wavelength_f = fits_get_sampling(flat_f)
        flat_index = np.roll([0, 1, 2, 3, 4, 5], cpos_f[1])   # We need to undo the roll in the data index
        printc('          FLAT FG voltages: ', voltagesFlat, color=bcolors.OKBLUE)
        printc('          FLAT Continuum position at wave: ', cpos_f[0], color=bcolors.OKBLUE)
        printc('          FLAT roll position of continuum: ', info[cpos_f[1]], color=bcolors.OKBLUE)
        printc('          FLAT index elements: ', flat_index, color=bcolors.OKBLUE)
        printc('          FLAT ref_wavelength [mA]: ', ref_wavelength_f, color=bcolors.OKBLUE)
        printc('          FLAT wave axis [mA]: ', wave_axis_f, color=bcolors.OKBLUE)
        printc('          FLAT wave axis - ref_wavelength [mA]: ', wave_axis_f - ref_wavelength_f, color=bcolors.OKBLUE)
        dummy_1 = (voltagesFlat - np.roll(voltagesFlat, -1)) * (tunning_constant * 1000)
        dummy_2 = np.sort(np.abs(dummy_1))
        sampling_f = np.mean(dummy_2[0:-2])
        printc('          FLAT average sampling [mA]: ', sampling_f, color=bcolors.OKBLUE)

        if new_flat_index:
            flat_index = new_flat_index
            printc('          FLAT index elements CHANGED BY USER INPUT TO: ', flat_index, color=bcolors.WARNING)

        if USE_CONT_FLAT:
            flat_index[:] = flat_index[cpos_f[0]]
            printc('          USING CONTINUUM FLAT ONLY. The continuum corresponds to position', flat_index[cpos_f[0]], ' according to the header info', color=bcolors.WARNING)
            printc('          NEW FLAT index elements: ', flat_index, color=bcolors.OKBLUE)
            printc('          NOTE: the flat will not remove the cavity so the user should provide a cavity file.', color=bcolors.OKBLUE)

        if normalize_flat:
            printc('          Normalizing flats using ', PERCENT_OF_DISK_FOR_FLAT_NORM, ' % of the disk', color=bcolors.OKBLUE)
            norma_factor = np.mean(flat[:,:,mask_for_flat > 0],axis=(2))
            flat /= norma_factor[:,:,np.newaxis,np.newaxis]

        if SIX_FLATS:
            for i in range(6):
                mm = np.mean(flat[i, :, :, :], axis=0)
                flat[i, :, :, :] = mm[np.newaxis, :, :]

        if smooth_flats_polarization:
            demodulated_flat, header = phi_apply_demodulation(flat, 'auto', header=header, FDT_MOD_ROTATION_ANGLE=FDT_MOD_ROTATION_ANGLE)
            for p in np.arange(1, 4):
                debug("p")
                for wave in range(int(zd // 4)):
                    demodulated_flat[wave, p, :, :] = gaussian_filter(demodulated_flat[wave, p, :, :], sigma=(FLAT_MEDIAN_FILTER, FLAT_MEDIAN_FILTER))  # , truncate=3.5)
            flat = phi_apply_demodulation(demodulated_flat, 'auto', header=header,modulate=True, FDT_MOD_ROTATION_ANGLE=FDT_MOD_ROTATION_ANGLE)
            del demodulated_flat

        DID_flat = flat_header['PHIDATID']
        header = update_header(header, 'CAL_FLAT', DID_flat, 'CAL_DARK', comment='Onboard calibrated for gain table')

        if verbose:
            plib.show_one(flat[0, 0, :, :], xlabel='pixel', ylabel='pixel', title='Flat first image raw (1 of 24)', cbarlabel='Any (as input)', save=None, cmap='gray')

    else:
        printc('-->>>>>>> No flats mode                    ', color=bcolors.WARNING)


    # -----------------
    # CORRECT FRINGES
    # -----------------

    # COMMENT AF: Fringe correction should go here, I think

    # -----------------
    # CORRECT DISTORTION
    # -----------------

    if correct_distortion:
        data, header = phi_correct_distortion(data, header, parallel=parallel, verbose=verbose)
    else:
        printc('-->>>>>>> No distortion correction', color=bcolors.WARNING)

    # -----------------
    # TODO: INTERPOLATE THE FLAT TO MATCH THE WAVELENG (CAVITY)
    # -----------------

    # from scipy.interpolate import RegularGridInterpolator
    # x = np.linspace(0,2047,2048).astype(int)
    # y = np.linspace(0,2047,2048).astype(int)
    # z = np.array([-300.,-140.,-70.,0.,70.,140.,300.]) #ojo con el -300
    # zn = np.array([-175.,-140.,-105.,-70.,-35.,0.,35.,70.,105.,140.,175.,300.])

    # flat_rsw1 = np.concatenate(((flat_rsw1[5,:,:,:])[np.newaxis,:,:,:],flat_rsw1))
    # fn = RegularGridInterpolator((z,y,x), flat_rsw1[:,0,:,:])
    # pts = np.array([-40,10,10])
    # print(fn(pts))

    #     pts = np.meshgrid(-40.,y,x)
    # pts = np.array([m.flatten() for m in pts])
    # flat_n = fn(pts.T)
    # result = flat_n.reshape((2048,2048))
    # plt.imshow(result,vmin=0.9,vmax=1.1)

    # flat_n = np.zeros((12,4,2048,2048))

    # for i in range(4):
    #   fn = RegularGridInterpolator((z,y,x), flat_rsw1[:,i,:,:],bounds_error=False)
    #   for j in range(12):
    #     print(i,zn[j])
    #     pts_list = np.meshgrid(zn[j],y,x)
    #     pts = np.array([m.flatten() for m in pts_list])
    #     flat_n[j,i,:,:] = fn(pts.T).reshape((2048,2048))

    # -----------------
    # APPLY FLAT CORRECTION
    # TODO: TAKE THE REAL FLAT
    # -----------------

    if flat_c:
        printc('-->>>>>>> Correcting Flatfield', color=bcolors.OKGREEN)

        # check if continuum position in data is in different position as in the flat:
        if cpos[0] != cpos_f[0]:
            #continuum are in different places. Depending on RED or BLUE:
            if cpos[0] == 0:
                assert(cpos_f[0] == 5)
                flat_index = np.roll(flat_index,1)
            if cpos[0] == 5:
                assert(cpos_f[0] == 0)
                flat_index = np.roll(flat_index,-1)

        pol_index = [0,1,2,3]
        if use_six_flats: #meaning you use the first flat (I1) in all four polarizations
            pol_index = [0,0,0,0]

        for p in range(len(pol_index)):
            for wave in range(int(zd // 4)):
                print('  ... pol: ', p, ' wave: ', wave, ' index: ', '[data / flat] index: ',data_index[wave],' , ',flat_index[wave])

                #correct the data
                data[data_index[wave], p, :, :] = data[data_index[wave], p, :, :] / flat[flat_index[wave], pol_index[p], :, :]

        if verbose:
            plib.show_one(data[cpos[0], 0, :, :], vmax=None, vmin=0, xlabel='pixel', ylabel='pixel', title='Data / flat at continuum', cbarlabel='DN', save=None, cmap='gray')

        data[np.bitwise_not(np.isfinite(data))] = 0

    # -----------------------
    # APPLY GHOST CORRECTION
    # NOTE: DOS moved the ghost correction AFTER the flat.
    # We should not do the flat correction inside the ghost module.
    # The way to proceed would be, for the ghost correction in the data:
    # 1- Do flat correction
    # 2- Do ghost correction using flatfielded data
    # for the flatfield data processing this is different becouse there is no flat yet.
    # Hence, the way to do it would be:
    # 1- Do a correction with NO flat and NO ghost (we could correct the rest of things)
    # 2- Obtain a preliminary flat with those preprocessed data
    # 3- Use the obtained flat to activate the ghost correction
    #    For correcting the off-points ghost one "needs" the ghost from the first (no offpoint) position
    #    3.1 correct first flat dataset with save_ghost_image = 'fits'
    #    3.2 correct rest of flat datasets with ghost_image = 'fits'
    # 4- Optain a final flat.
    # -----------------------

    if correct_ghost:
        # Note that we need flatfield corrected data!!!
        if not(flat_preprocess) and not(flat_c):
            printc("-->>>>>>> WARNING: ghost correction was activated but NO FLAT correction was applied", color=bcolors.WARNING)
        if flat_preprocess and not(flat_c):
            printc("-->>>>>>> WARNING: ghost correction was activated in FLAT_PROPROCESING MODE but FLAT correction IN NOT ACTIVATED", color=bcolors.WARNING)
            printc("-->>>>>>> Ghost correction need a pre-flat", color=bcolors.WARNING)
            printc("-->>>>>>> Standard way for flatdata preprocessing is the following", color=bcolors.WARNING)
            printc("-->>>>>>>      1- Do a correction with NO flat and NO ghost ", color=bcolors.WARNING)
            printc("-->>>>>>>      2- Obtain a preliminary flat with those preprocessed data", color=bcolors.WARNING)
            printc("-->>>>>>>      3- Use the obtained flat to activate the ghost correction", color=bcolors.WARNING)
            printc("-->>>>>>>         For correcting the off-points ghost one 'needs' the ghost from the first (no offpoint) position", color=bcolors.WARNING)
            printc("-->>>>>>>         3.1 correct first flat dataset with save_ghost_image = 'fits'", color=bcolors.WARNING)
            printc("-->>>>>>>         3.2 correct rest of flat datasets with ghost_image = 'fits'", color=bcolors.WARNING)
            printc("-->>>>>>>      4- Obtain a final flat.", color=bcolors.WARNING)

        if ghost_image:
            printc('Reading ghost image: ', ghost_image, color=bcolors.OKBLUE)
            with pyfits.open(ghost_image) as hdu_list:
                input_ghost_image = hdu_list[0].data
        else:
            input_ghost_image = None

        data, header, ghost = phi_correct_ghost(data, header, cpos, ghost_params, verbose=verbose,
            input_ghost_image = input_ghost_image)

        if save_ghost_image:

            ghost_image_file = set_level(data_f, 'L1', 'L1.5')
            ghost_image_file = set_level(ghost_image_file, 'ilam', 'ghost')
            ghost_image_file = append_id(ghost_image_file, filetype, vers, DID)
            ghost_image_file = os.path.join(output_dir, ghost_image_file)
            printc(' Saving ghost to: ', ghost_image_file)
            hdu = pyfits.PrimaryHDU(ghost)
            hdu.writeto(ghost_image_file, overwrite=True)

    else:
        printc("-->>>>>>> No ghost correction", color=bcolors.WARNING)

    # -----------------
    # CORRECT PREFILTER
    # -----------------

    if prefilter:
        data, header = phi_correct_prefilter(prefilter_fits, header, data, voltagesData, verbose=verbose)

    # -----------------
    # REALIGN DATA BEFORE DEMODULATION
    # -----------------

    if realign:
        printc('-->>>>>>> Realigning data...           ', color=bcolors.OKGREEN)
        for i in range(zd // 4):
            s_x, s_y, _ = PHI_shifts_FFT(data[i, :, :, :], prec=500, verbose=verbose, norma=False)
            for j in range(4):
                data[i, j, :, :] = shift_subp(data[i, j, :, :], shift=[s_x[j], s_y[j]])  # estra y,z asi que esta al reves FFT
        header = update_header(header, 'CAL_REAL', 'FFT', 'CAL_DARK', comment='Realigment of data (phifdt_pipe_modules.py)')

    # -----------------
    # APPLY DEMODULATION
    # -----------------

    printc('-->>>>>>> Demodulating data...         ', color=bcolors.OKGREEN)
    if extra_debug:
        datan = np.copy(data)
        ds = np.copy(data)
        demodM = np.array(
            [[0.168258, 0.357277, 0.202212, 0.273266],
             [-0.660351, 0.314981, 0.650029, -0.299685],
             [0.421242, 0.336994, -0.183068, -0.576202],
             [-0.351933, 0.459820, -0.582167, 0.455458]])
        for i in range(zd // 4):
            for wave in range(xd):
                for m in range(yd):
                    datan[i, :, m, wave] = np.matmul(demodM, ds[i, :, m, wave])
        plib.show_four_row(datan[3, 0, :, :], datan[3, 1, :, :], datan[3, 2, :, :], datan[3, 3, :, :], svmin=[0, -0.2, -0.2, -1.], svmax=[100, 0.1, 0.1, 0.1])

    data[np.bitwise_not(np.isfinite(data))] = 0

    data, header = phi_apply_demodulation(data, instrument, header=header, FDT_MOD_ROTATION_ANGLE=FDT_MOD_ROTATION_ANGLE)

    if verbose:
        mac = np.max(data[3, 0, :, :])
        plib.show_four_row(
            data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :], title=['I', 'Q', 'U', 'V'],
            zoom=2,
            svmin=[0, -mac * 0.01, -mac * 0.01, -mac * 0.01],
            svmax=[mac, mac * 0.01, mac * 0.01, mac * 0.01])

    # -----------------
    # VEIL: UNDER DEVELOPMENT
    # -----------------

    if scattering_veil and not(flat_preprocess):
        veil = data[0, 0, :, :]
        data[:, 0, :, :] = (data[:, 0, :, :] - scattering_veil * veil[np.newaxis, :, :]) / (1 - scattering_veil)
    # Borrero paper

    # -----------------
    # APPLY NORMALIZATION
    # -----------------

    if not(flat_preprocess):

        printc('-->>>>>>> Applying normalization --', color=bcolors.OKGREEN)
        norma = zero_level(data[cpos[0], 0, :, :], maskii)
        print('          Norma is: ', norma, ' evaluated in ', PERCENT_OF_DISK_FOR_NORMALIZATION * 100, ' % of the disk')
        data = data / norma
        header = update_header(header, 'CAL_NORM', np.round(norma, 6), 'CAL_DARK', comment='Normalization constant PROC_Ic')

        if extra_debug:
            datan = datan / norma
            plib.show_four_row(data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :])
            plib.show_four_row(datan[3, 0, :, :], datan[3, 1, :, :], datan[3, 2, :, :], datan[3, 3, :, :])

        if verbose:
            plib.show_four_row(data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :], title=['I', 'Q', 'U', 'V'], zoom=2, svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004])

    # -----------------
    # CROSS-TALK CALCULATION FROM I TO QUV
    # -----------------

    if ItoQUV and not(flat_preprocess):
        # ItoQUV may be True of string
        mode = 'standard'
        if ItoQUV == 'surface':
            mode = 'surface'

        printc('-->>>>>>> Cross-talk correction from Stokes I to Stokes Q,U,V --', color=bcolors.OKGREEN)
        printc('          Using ', PERCENT_OF_DISK_FOR_CROSST * 100, '% of the disk                     ', color=bcolors.OKGREEN)
        printc('          and mode ', mode, color=bcolors.OKGREEN)

        cQ, cU, cV, sfitQ, sfitU, sfitV, data = crosstalk_ItoQUV(data, mode = mode, verbose=verbose, mask=maskt, crosstalk_order=crosstalk_order,cntr_rad=[cx,cy,radius],ind_wave=ind_wave,continuum_pos=cpos[0])

        if verbose:
            plib.show_hist(data[0, 1, maskt > 0].flatten(), bins='auto', title=' ', leave='open', color='green')
            plib.show_hist(data[0, 2, maskt > 0].flatten(), bins='auto', title=' ', leave='open', color='red')
            plib.show_hist(data[0, 3, maskt > 0].flatten(), bins='auto', title='Stokes Q/U/V - no zero', color='blue')
            plib.show_four_row(
                data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :],
                title=['I - corr', 'Q - corr', 'U - corr', 'V - corr'],
                zoom=3,
                svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004])

        if mode == 'standard' and crosstalk_order == 1:
            header = update_header(header, 'CAL_CRT0', np.round(cQ[0] * 100, 3), 'CAL_DARK', comment='cross-talk I to Q (slope in %), wrt CAL_NORM')
            header = update_header(header, 'CAL_CRT1', np.round(cQ[1] * 100, 3), 'CAL_CRT0', comment='cross-talk I to Q (off-set in %), wrt CAL_NORM')
            header = update_header(header, 'CAL_CRT2', np.round(cU[0] * 100, 3), 'CAL_CRT1', comment='cross-talk I to U (slope in %), wrt CAL_NORM')
            header = update_header(header, 'CAL_CRT3', np.round(cU[1] * 100, 3), 'CAL_CRT2', comment='cross-talk I to U (off-set in %), wrt CAL_NORM')
            header = update_header(header, 'CAL_CRT4', np.round(cV[0] * 100, 3), 'CAL_CRT3', comment='cross-talk I to V (slope in %), wrt CAL_NORM')
            header = update_header(header, 'CAL_CRT5', np.round(cV[1] * 100, 3), 'CAL_CRT4', comment='cross-talk I to V (off-set in %), wrt CAL_NORM')

    # -----------------
    # CROSS-TALK CALCULATION FROM V TO QU (Interactive)
    # -----------------

    if VtoQU and not(flat_preprocess):
        printc('-->>>>>>> Cross-talk correction from Stokes V to Stokes Q,U ', color=bcolors.OKGREEN)

        # factor = 0.3
        factor = PERCENT_OF_DISK_FOR_CROSST
        rrx = [int(c[0] - radius * factor), int(c[0] + radius * factor)]
        rry = [int(c[1] - radius * factor), int(c[1] + radius * factor)]
        printc('          Using ', factor * 100, '% of the disk                     ', color=bcolors.OKGREEN)
        nram = 2000
        if np.sum(maskt) <= nram:
            nran = np.sum(maskt) - 1

        cVQ, cVU = cross_talk_QUV(data[:, :, rry[0]:rry[1], rrx[0]:rrx[1]], nran=2000, nlevel=nlevel, block=False, mask=maskt)

        option = input('Do you want to apply the correction (y/n) [n]: ')
        if option == 'y':
            datao = np.copy(data)
            print('Applying V to QU cross-talk correction...')
            datao[:, 2, :, :] = data[:, 2, :, :] - cVQ[0] * data[:, 3, :, :] - cVQ[1]
            datao[:, 3, :, :] = data[:, 3, :, :] - cVU[0] * data[:, 3, :, :] - cVU[1]
            plib.show_two(data[3, 1, ry[0]:ry[1], rx[0]:rx[1]], datao[3, 1, ry[0]:ry[1], rx[0]:rx[1]], block=False, title=['Stokes Q', 'Stokes Q corrected'], zoom=3)
            plib.show_two(data[3, 2, ry[0]:ry[1], rx[0]:rx[1]], datao[3, 2, ry[0]:ry[1], rx[0]:rx[1]], block=False, title=['Stokes U', 'Stokes U corrected'], zoom=3)
            option2 = input('Do you want to continue (y/n) [n]: ')
            if option2 == 'y':
                data = np.copy(datao)
                del datao
            plt.close()

        header = update_header(header, 'CAL_CRT6', np.round(cVQ[0] * 100, 3), 'CAL_CRT5', comment='cross-talk V to Q  (slope in %), wrt CAL_NORM')
        header = update_header(header, 'CAL_CRT7', np.round(cVQ[1] * 100, 3), 'CAL_CRT6', comment='cross-talk V to Q (off-set in %), wrt CAL_NORM')
        header = update_header(header, 'CAL_CRT8', np.round(cVU[0] * 100, 3), 'CAL_CRT7', comment='cross-talk V to U (slope in %), wrt CAL_NORM')
        header = update_header(header, 'CAL_CRT9', np.round(cVU[1] * 100, 3), 'CAL_CRT8', comment='cross-talk V to U (off-set in %), wrt CAL_NORM')

    # -----------------
    # FRINGE CORRECTION
    # COMMENT AF: should be done after dark correction, but before any other correction, see comment above
    # -----------------

    if correct_fringes == 'auto' or correct_fringes == 'manual' or correct_fringes == 'interpol':
        if verbose:
            plib.show_four_row(data[2,0,:,:],data[2,1,:,:],data[2,2,:,:],data[2,3,:,:],title=['I - before fringe','Q','U','V'],zoom=3,svmin=[0,-0.004,-0.004,-0.004],svmax=[1.2,0.004,0.004,0.004])

        data, header, _, _, _ = phi_correct_fringes(data,header,option=correct_fringes,verbose=verbose,fringe_threshold=fringe_threshold,
            NFREQ_LEVEL=NFREQ_LEVEL,ATT_FACTOR=ATT_FACTOR,continuum_point=cpos[0],max_frequencies= 16, rad_min = 25, rad_max = 40,
            wsize = 50,which_stokes=which_stokes)
        if verbose:
            plib.show_four_row(
                data[2, 0, :, :], data[2, 1, :, :], data[2, 2, :, :], data[2, 3, :, :],
                title=['I - after fringe', 'Q', 'U', 'V'],
                zoom=3,
                svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004])
    elif correct_fringes is False:
        pass
    else:
        printc('Error in option finge correction. Options are "manual", "auto", "old-manual", "manual-forced", or false. Given: ', correct_fringes, color=bcolors.WARNING)

    # -----------------
    # SET MEDIAN TO ZERO
    # -----------------

    if putmediantozero and not(flat_preprocess):
        # TODO Include masking of ARs

        mask_blurred = gaussian_filter(mask_at_edge.astype(float), sigma=(SIGMA_MEDIAN_FILTER, SIGMA_MEDIAN_FILTER))  # , truncate=3.5)
        mask_blurred *= mask_at_edge
        # mask_blurred = mask_blurred.astype(float)
        # mask_blurred /= mask_blurred[yd//2,xd//2]
        if verbose:
            plt.imshow(mask_at_edge)
            plt.colorbar()
            plt.show()
            plt.imshow(mask_blurred)
            plt.colorbar()
            plt.show()

        # mask_blurred *= mask
        for p in range(3):
            # md = np.mean(data[:,p+1,:,:],axis=0)
            md = data[cpos[0], p + 1, :, :]  # QUV continuum
            # apply Gaussian blur, creating a new image
            md_blurred = gaussian_filter(md * mask_at_edge, sigma=(SIGMA_MEDIAN_FILTER, SIGMA_MEDIAN_FILTER))  # , truncate=3.5)
            if verbose:
                plt.imshow(md_blurred, clim=(-0.002, 0.002))
                plt.colorbar()
                plt.show()
            md_blurred /= mask_blurred
            md_blurred[np.bitwise_not(np.isfinite(md_blurred))] = 0
            if verbose:
                plt.imshow(md_blurred, clim=(-0.002, 0.002))
                plt.colorbar()
                plt.show()
                plt.imshow(data[cpos[0], p + 1, :, :], clim=(-0.002, 0.002))
                plt.colorbar()
                plt.show()
            data[:, p + 1, :, :] -= md_blurred[np.newaxis, :, :]
            if verbose:
                plt.imshow(data[cpos[0], p + 1, :, :], clim=(-0.002, 0.002))
                plt.colorbar()
                plt.show()
        data[np.bitwise_not(np.isfinite(data))] = 0

        printc('-->>>>>>> Putting median to zero ', color=bcolors.OKGREEN)
        printc('          Median evaluated in ', PERCENT_OF_DISK_FOR_NORMALIZATION * 100, ' % of the disk', color=bcolors.OKBLUE)
        for i in range(zd // 4):
            # PQ = np.median( data[i,1, maski > 0])#,axis=(1,2))
            # PU = np.median( data[i,2, maski > 0])#,axis=(1,2))
            # PV = np.median( data[i,3, maski > 0])#,axis=(1,2))
            PQ = zero_level(data[i, 1, :, :], maski, FIVE_SIGMA=FIVE_SIGMA)
            PU = zero_level(data[i, 2, :, :], maski, FIVE_SIGMA=FIVE_SIGMA)
            PV = zero_level(data[i, 3, :, :], maski, FIVE_SIGMA=FIVE_SIGMA)
        # PQ = np.median(data[:,1,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
        # PU = np.median(data[:,2,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
        # PV = np.median(data[:,3,rry[0]:rry[1],rrx[0]:rrx[1]],axis=(1,2))
            data[i, 1, :, :] = data[i, 1, :, :] - PQ  # [:,np.newaxis,np.newaxis]
            data[i, 2, :, :] = data[i, 2, :, :] - PU  # [:,np.newaxis,np.newaxis]
            data[i, 3, :, :] = data[i, 3, :, :] - PV  # [:,np.newaxis,np.newaxis]
            printc(PQ, PU, PV)

        header['history'] = ' Parameters putmediantozero [%]: ' + str(np.round(PQ * 100, 6)) + ' ' + str(np.round(PU * 100, 6)) + ' ' + str(np.round(PV * 100, 6))

    # ctnd = data[0,1:3,:,:]
    # for i in range(zd//4):
    #     data[i,1:3,:,:] = data[i,1:3,:,:] - ctnd
    # plib.show_four_row(data[0,0,:,:],data[0,1,:,:],data[0,2,:,:],data[0,3,:,:],title=['I','Q','U','V'],zoom=3,svmin=[0,-0.004,-0.004,-0.004],svmax=[1.2,0.004,0.004,0.004])

    if verbose:
        plib.show_hist(data[0, 1, maski > 0].flatten(), bins='auto', title=' ', leave='open', color='green')
        plib.show_hist(data[0, 2, maski > 0].flatten(), bins='auto', title=' ', leave='open', color='red')
        plib.show_hist(data[0, 3, maski > 0].flatten(), bins='auto', title='Stokes Q/U/V - zero', color='blue')
        plib.show_four_row(
            data[3, 0, :, :], data[3, 1, :, :], data[3, 2, :, :], data[3, 3, :, :],
            title=['I', 'Q', 'U', 'V'],
            zoom=3,
            svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004])


    # -----------------
    # CHECK FOR INFs
    # -----------------

    data[np.bitwise_not(np.isfinite(data))] = 0

    # -----------------
    # SAVE DATA
    # TODO: CMILOS FORMAT AND FITS
    # -----------------

    # check if npz,pngs and level2 exist
    # dirs = ['pngs', 'level2']

    # for checkit in dirs:
    #     checkit_full = os.path.join(output_dir, checkit)
    #     check_dir = os.path.isdir(checkit_full)
    #     if not check_dir:
    #         os.makedirs(checkit_full)
    #         print("created folder : ", checkit_full)
    #     else:
    #         print(checkit_full, "folder already exists.")

    # for i in range(zd // 4):
    #     plib.show_four_row(
    #         data[i, 0, :, :], data[i, 1, :, :], data[i, 2, :, :], data[i, 3, :, :], title=['I', 'Q', 'U', 'V'],
    #         zoom=3, svmin=[0, -0.004, -0.004, -0.004], svmax=[1.2, 0.004, 0.004, 0.004], save=os.path.join(output_dir, 'pngs', f's{i}.png'))

    printc('---------------------------------------------------------', color=bcolors.OKGREEN)
    if outfile is None:
        # basically replace L1 by L1.5
        try:
            outfile_L2 = set_level(data_f, 'L1', 'L2')
            outfile_L2 = set_level(outfile_L2, 'ilam', 'stokes')
            outfile_L2 = append_id(outfile_L2, filetype, vers, DID)
            # outfile_L2 = outfile_L2.split('V')[0] + 'V' + vers+ '_' + DID  + filetype
        except Exception:
            outfile_L2 = set_level(data_f, 'L0', 'L2')
            outfile_L2 = set_level(outfile_L2, 'ilam', 'stokes')
            outfile_L2 = append_id(outfile_L2, filetype, vers, DID)
            # outfile_L2 = outfile_L2.split('V')[0] + 'V' + vers+ '_' + DID  + filetype
    else:
        outfile_L2 = outfile

    if flat_preprocess:
        #modulate back
        data = phi_apply_demodulation(data, instrument, header=header,modulate=True, FDT_MOD_ROTATION_ANGLE = FDT_MOD_ROTATION_ANGLE)
        data = data.astype('float32')
        nwl, npol, ny, nx = data.shape
        nframes = nwl * npol
        data = np.reshape(data, (nframes, ny, nx))

    if save_stokes:

        outfile_L2_full = os.path.join(output_dir, outfile_L2)
        printc(' Saving data to: ', outfile_L2_full)

        # hdu = pyfits.PrimaryHDU(data)
        # hdul = pyfits.HDUList([hdu])
        # hdul.writeto(outfile, overwrite=True)

        # UPDATE HEADER INFORMATION
        header_updated = update_header_stokes(header)

        if 'BTYPE' in header:  # Check for existence
            header['BTYPE'] = 'Stokes'
        if 'BUNIT' in header:  # Check for existence
            header['BUNIT'] = 'Normalized units'

        with pyfits.open(data_filename) as hdu_list:
            hdu_list[0].data = data
        #        header = hdu_list[0].header
            hdu_list[0].header = header_updated

            # Add a new key to the header
            # header.insert(20, ('NEWKEY', 'OMIT', 'test'))
            # header.set('NEWKEY','50.5')
            hdu_list.writeto(outfile_L2_full, overwrite=True)
        #        hdu_list.writeto(directory+outfile+'_L1.fits', clobber=True)

    # with pyfits.open(data_f) as hdu_list:
    #     hdu_list[0].data = mask
    #     hdu_list.writeto(directory+outfile+'_red-mask.fits', clobber=True)

    # special option for further correction of data
    if pol_c_file is not None:
        with pyfits.open(pol_c_file) as hdu_list:
            md = hdu_list[0].data

        for j in range(1, 4):
            for k in range(6):
                data[k, j, :, :] = data[k, j, :, :] - md[j - 1, :, :]

    if flat_preprocess:

        return data, header

    # -----------------
    # INVERSION OF DATA WITH CMILOS
    # -----------------

    rte_modes = ['RTE','RTE+PSF','CE','CE+RTE','CE+RTE+PSF']
    if rte in rte_modes:

        printc('---------------------RUNNING CMILOS --------------------------', color=bcolors.OKGREEN)

        # IN PRINCIPLE, the main program just calls generate_level2. But some adjustments need to be done in case we call PSF.
        # If this is the case, we just move the continuum point to the RED if it is in the BLUE
        # TODO: REMOVE THIS CODE. IT IS NOT NECESARY. Take into account that pipeline behabiour with 2020 data has to be updated as well (**)
        if rte == 'CE+RTE+PSF' or rte == 'RTE+PSF':
            print("Warning: when using the PSF the continuum shall be ALWAYS in the RED for 2020 DATA - CHECK THIS OUT")
            # check values of cpos
            if cpos[0] == 0:
                print('The continuum is in the BLUE. Check PSF in CMILOS (Assumes RED continuum) TBC')
                if cpos[1] == 1:
                    print('The continuum is stored in the RED (move it to the right place - not active)')
                    # wave_axis = np.roll(wave_axis, 1)
                    # data = np.roll(data, 1, axis = 0)
                if cpos[1] == 0:
                    print('The continuum is stored in the BLUE (do nothing)')

            if cpos[0] == 5:
                print('The continuum is in the RED')
                if cpos[1] == -1:
                    print('The continuum is stored in the BLUE (move it to the RED - not active)')
                    # wave_axis = np.roll(wave_axis, -1)
                    # data = np.roll(data, -1, axis = 0)


        rte_invs = generate_level2(
            data, header, wave_axis, rte, num_workers=num_workers, temp_dir=output_dir,
            RTE_code=RTE_code, mask=mask, cavity_file=cavity_file, correct_cavity=correct_cavity,
            weight = np.array(weight),options=RTE_options)

        save_level2(
            data_filename, rte_invs, output_directory=output_dir,
            rte=rte, mask=mask, verbose=verbose, outfile_L2=outfile_L2)

        printc('--------------------- END  ----------------------------', color=bcolors.OKGREEN)

    return data, header