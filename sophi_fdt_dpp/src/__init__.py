#Esto hace que los nombres aparezcan en el nivel superior al importar.
#no es necesario excepto para aquellas funciones que sean mas usadas
#puedo llamar a una funcion sophi_fdt_dpp.phi_gets() en vez de sophi_fdt_dpp.phi_fits.phi_gets()

from .phi_fits import *
from .phi_gen import *
from .phi_reg import *
from .phi_utils import *
from .phifdt_flat import *
from .phifdt_pipe import *
from .phi_rte import *
from .plot_lib import *
from .tools import *
from .phifdt_pipe_modules import *
from .phi_json_gen import json_generator
from .cog import *
from .phifdt_disk import *
from .phifdt_rte_interactive import *