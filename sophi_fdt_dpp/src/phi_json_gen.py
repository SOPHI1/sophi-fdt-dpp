
import json
def json_generator():

    json_file = './FDT.json'
    fits_version = '01'
    dict_FDT = {

        "__comment1__":"data and calibration data goes here",
        "data_f" : "solo_L1_phi-hrt-ilam_20201117T170209_V202108301639C_0051170001.fits",
        "flat_f" : "/data/slam/home/sinjan/fits_files/april_avgd_2020_flat.fits",
        "dark_f" : "../fits_files/solo_L0_phi-fdt-ilam_20200228T155100_V202002281636_0022210004_000.fits",
        'input_data_dir' : './',
        'verbose' : False,
        "__comment2__":"Data reduction options go here",
        'dark_c' : True,
        'flat_c' : True,
        'instrument' : 'FDT40',
        'hough_params' : None,
        'center_method' : 'circlefit',
        'shrink_mask' : 2,
        'norm_f' : False,
        'flat_scaling' : 1,
        'flat_index' : False,
        'prefilter': False,
        'prefilter_fits' : '0000990710_noMeta.fits',
        'rte' : False,
        'correct_fringes' : 'manual',
        'correct_ghost' : False,
        'putmediantozero' : True,
        'ItoQUV' : False,
        'VtoQU' : False,
        'realign' : False,
        'ind_wave' : False,
        'nlevel' : 0.3,
        'debug' : False,
        'vers': fits_version,        #desired version number, only 2 characters 01 -> 99, if not specified, '01' default
        'RTE_code': 'cmilos',        #specify which code will be used in the inversion. Default is cmilos (uses .txt ASCII input files)
        "__comment3__":"Output options go here",
        'output_dir' : './',

    }

    with open(json_file, 'w', encoding="utf-8", newline='\r\n') as outfile:
        json.dump(dict_FDT, outfile, indent=4, ensure_ascii=False)
