"""phifdt

``phifdt.py`` Main routine (class) for onground data procesing of PHI/FDT raw data.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2023-09-19
   :Authors: **David Orozco Suárez**
   :Contributors: Nestor Albelo (albelo@mps.mpg.de) and Alex Feller (feller@mps.mpg.de)

Check main documentation for explanations.

"""
import datetime
import os.path
import subprocess

import numpy as np

from math import nan
from scipy.ndimage import gaussian_filter
from copy import deepcopy
from matplotlib import pyplot as plt
from astropy.io import fits as pyfits

from . import plot_lib as plib
from .phi_fits import fits_get, fits_get_sampling, set_level, append_id
from .phi_gen import find_circle_hough, generate_circular_mask, shift
from .phi_reg import PHI_shifts_FFT, shift_subp
from .phi_utils import debug,cwd_
from .phifdt_pipe_modules import (
    check_pmp_temp,
    cross_talk_QUV,
    crosstalk_ItoQUV,
    generate_level2,
    phi_apply_demodulation,
    phi_correct_dark,
    phi_correct_fringes,
    phi_correct_ghost,
    phi_correct_prefilter,
    save_level2,
    zero_level,
    phi_correct_distortion,
    update_header_stokes,
    crop_calibration_data)
from .tools import bcolors, printc, update_header
from .phifdt_disk import find_center

import json
import numpy as np

from dataclasses import dataclass

@dataclass
class phidata():

    _name: str="phidata"

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, v: str) -> None:
        self._name = v

    import reprlib
    r = reprlib.Repr()
    r.maxlist = 4        # max elements displayed for lists
    r.maxstring = 100    # max characters displayed for strings

    @staticmethod
    def _mprint(*args, **kw):
        print('-->',*args, **kw)

    def pinfo(self,*args, **kw):
        if self.params.verbose:
            self._mprint(*args, **kw)

    def does_exist(self,thing):
        if not os.path.isfile(thing):
            self._mprint("Data file do not exist in current location ", thing)
        else:
            return True
    class special():
        bit_depth: np.float32
        dark_offset: float = 1
        data_filename: str = str()
        filetype: str = str()

    class data():
        image  = None
        header = None
        imageSummary = None
        imageSummary_head = None
        scaling = {"Present": [False,True], "scaling": [0,0], 'bit-depth': None}
        DID = None

    class params():
        data_file: str = str()
        flat_file = False
        dark_file = False
        input_data_dir = './'
        output_dir = './'
        instrument = 'auto'
        flat_c =  True
        dark_c =  True
        ItoQUV =  False
        VtoQU =  False
        ind_wave = False
        hough_params = [250, 800, 100]
        normalize_flat = False
        flat_scaling = 1.
        flat_index = False
        USE_CONT_FLAT = False
        smooth_flats_polarization = False
        use_six_flats = False
        prefilter = True
        prefilter_fits = '0000990710_noMeta.fits'
        realign = False
        verbose = True
        shrink_mask = -10
        correct_fringes = False
        which_stokes = [1,2,3]
        fringe_threshold = 1.00
        correct_ghost = False
        ghost_params = [-1.98796, 1936.70, -1.98944, 1947.15, 0.0008, 0.0024, -0.0032, 7]
        correct_distortion = False
        putmediantozero = False
        extra_debug = False
        nlevel = 0.3
        center_method = 'circlefit'
        vers = '01'
        rte = False
        cavity_file = False
        correct_cavity = False
        weight = [1.00,4.20,3.66,3.82]
        RTE_code = 'pmilos'
        pol_c_file = False
        outfile = False
        scattering_veil = False
        FIGUREOUT = '.png'
        SIX_FLATS = False
        SIGMA_MEDIAN_FILTER = 30.
        FLAT_MEDIAN_FILTER = 9.
        PLT_RNG = 5
        PERCENT_OF_DISK_FOR_MASKI = 0.9  # 90% of the disk
        PERCENT_OF_DISK_FOR_NORMALIZATION = 0.2  # 20% of the disk
        PERCENT_OF_DISK_FOR_CROSST = 0.96  # 96% of the disk
        PERCENT_OF_DISK_FOR_FLAT_NORM = 0.3  # 30% of the disk
        FDT_MOD_ROTATION_ANGLE = -127.6
        FIVE_SIGMA = 5
        ATT_FACTOR = 1e-6
        NFREQ_LEVEL = 0.
        crosstalk_order = 1.
        flat_preprocess = False
        calculate_shifts = True
        threshold = 0.05
        loop_threshold = 2
        save_stokes = True
        save_ghost_image = False
        ghost_image = False
        parallel = False
        num_workers = 10
        RTE_options = None

    @property
    def data_file(self):
        return self.params.data_file
    @data_file.setter
    def data_file(self,value):
        self.params.data_file = value
        self._mprint("Data file: ",self.params.data_file)
        self.special.data_filename = os.path.join(self.params.input_data_dir, self.params.data_file)
        self._mprint("Data file (with full path): ",self.special.data_filename)
        if not self.does_exist(self.special.data_filename):
            self._mprint("return 0 in data_file",self.special.data_filename)
            return

        # CHECK IF input is FITS OR FITS.GZ
        if self.special.data_filename.endswith('.fits'):
            self.special.filetype = '.fits'
        elif self.special.data_filename.endswith('.fits.gz'):
            self.special.filetype = '.fits.gz'
        else:
            raise ValueError("input data type neither .fits nor .fits.gz")


    def __init__(self, **karg):

        #define data holder
        data = self.data()
        params = self.params()
        special = self.special()

        #check if json dictionary has been provided
        if karg:
            if 'json_input' in karg:
                self.pinfo('--------------------------------------------------------------')
                self.pinfo(f' Reading config json file {karg["json_input"]}')
                with open(karg['json_input']) as json_file_opened:
                    dict_param = json.load(json_file_opened)

                for key, value in dict_param.items():
                    if key in vars(self.params):
                        setattr(self.params, key, value)
                    else:
                        self._mprint('json',key,'not recognized')
                # CHECK JSON INPUTS
                # IF process keyword (meaning do process) check input
                # data file...

            else:
        # check which arguments have been provided
                for key,item in karg.items():
                    if key in vars(self.params):
                        setattr(self.params, key, item)
                    else:
                        self._mprint('option',key,'not recognized')
        else:
            self._mprint('no arguments')

    def dscale(self):
        if self.data.image.dtype != self.special.bit_depth:
            self.data.image = self.data.image.astype(self.special.bit_depth)
        self.pinfo('image bit depth ',self.data.image.dtype)

    def parinfo(self):
        info = vars(self.params)
        self._mprint('_______________________________________________')
        self._mprint('Available params:              [value]')
        self._mprint('...............................................')
        for item in info.items():
            if item[0][0] != '_':
                extension = len(item[0])
                spaces = 28 - extension
                string_val = " " * spaces
                self._mprint(f"{item[0]}: {string_val} {item[1]}")
        self._mprint('_______________________________________________')

    def datainfo(self):
        info = vars(self.data)
        self._mprint('_______________________________________________')
        self._mprint('Image information:              [value]')
        self._mprint('...............................................')
        for item in info.items():
            if item[0][0] != '_':
                extension = len(item[0])
                spaces = 28 - extension
                string_val = " " * spaces
                self._mprint(f"{item[0]}: {string_val} {item[1]}")
        self._mprint('_______________________________________________')

    def load(self,verbose: bool = False):
        print('load')

    def apply_dark(self,dark,verbose: bool = False):
        print('dark')


def phifdt_test():
    '''
    Just for local test run in a folder at same level of SPGlib
    '''

    #create a class
    dark = phidata()
    dark.file = ''
    dark.filepath = ''

    phidata.dark_offset = 0.98
    for i in observations:
       i.load()
       i.apply_dark(dark,verbose=True)
