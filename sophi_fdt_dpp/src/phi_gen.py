"""
``phi_gen.py`` contains generic routines used in the pipeline and flat field generation.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez**
   :Contributors:

.. list-table::
   :widths: 25 50
   :header-rows: 1


   * - program
     - Short summary
   * - :py:meth:`shift`
     - Shift operator
   * - :py:meth:`generate_circular_mask`
     - Create a circle or annulus mask of size = [dy,dx]
   * - :py:meth:`gradient`
     - calculate gradient of real image.
   * - :py:meth:`threshold_otsu`
     - Return threshold value based on Otsu's method.
   * - :py:meth:`histogram`
     - Return histogram of image.
   * - :py:meth:`FindEdges`
     - calculate the image gradient and find the edges
   * - :py:meth:`make_circles`
     - Create a circle mask
   * - :py:meth:`find_Circles_ida`
     - Part of the Hough transform algorithm
   * - :py:meth:`votes`
     - Part of the Hough transform algorithm
   * - :py:meth:`bin_annulus`
     - TBD
   * - :py:meth:`circle_grid`
     - TBD
   * - :py:meth:`find_circle_hough`
     - TBD
   * - :py:meth:`simple_shift`
     - TBD
   * - :py:meth:`find_center`
     - TBD
   * - :py:meth:`FFTs`
     - TBD
   * - :py:meth:`Laplacian`
     - TBD
   * - :py:meth:`rebin`
     - TBD
   * - :py:meth:`apod`
     - TBD
   * - :py:meth:`expand_mask`
     - TBD
   * - :py:meth:`sigma_mask`
     - TBD
   * - :py:meth:`n_coord`
     - TBD
   * - :py:meth:`dmodel`
     - TBD
   * - :py:meth:`cmesh`
     - TBD
"""

# =============================================================================
# Project: SoPHI
# File:    phi_gen.py
# Author:  David Orozco Suárez (orozco@iaa.es)
# Contributors:
# -----------------------------------------------------------------------------
# Description:
# -----------------------------------------------------------------------------

import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage import gaussian_filter
from scipy.signal import fftconvolve
from scipy.signal.windows import tukey

from . import plot_lib as plib

# __all__ = ['bar', 'baz']


def shift(matrix: np.ndarray, shift: list = None, fill_value: int = 0) -> np.ndarray:
    """
    Shift operator

    Shift an image in 2D space naively as in SO/PHI FM instrument.

    Faster and more efficient methods can be used in normal CPU.

    fill_value = float.
    This method does not have any boundary condition.

    :param matrix: Image to shift. Two dimensions.
    :type matrix: np.ndarray
    :param shift: Input is a vector ``shift=[x,y]`` of x and y displacement in pixel units. +x -> positive; +y -> positive
    :type shift: list
    :param fill_value: Since there is no boundary conditions, fill_value represents the pixel value to the empty data, defaults to 0
    :type fill_value: float
    :return: shifted image
    :rtype: np.ndarray

    .. code-block:: python

        import sophi_fdt_dpp as fdt

        data = fdt.shift(data,shift=[23,43])

    """
    try:
        dimy, dimx = matrix.shape
    except:
        raise ValueError("Input is not 2D matrix")

    try:
        nx = shift[1]
        ny = shift[0]
    except:
        raise ValueError(
            "Provided shift not in rigth format 'shift=[0, 0]' of not present"
        )

    e = np.empty_like(matrix)
    if nx > 0:
        e[:nx, :] = fill_value
        e[nx:, :] = matrix[:-nx, :]
    elif nx < 0:
        e[nx:, :] = fill_value
        e[:nx, :] = matrix[-nx:, :]
    else:
        e = matrix

    s = np.empty_like(matrix)
    if ny > 0:
        s[:, :ny] = fill_value
        s[:, ny:] = e[:, :-ny]
    elif ny < 0:
        s[:, ny:] = fill_value
        s[:, :ny] = e[:, -ny:]
    else:
        s = e

    return s


def generate_circular_mask(size: list, radius: int, r_width: int) -> np.ndarray:
    """
    Create a circle mask of size = [dy,dx] with radious ``radius`` or a annular mask with  radious ``radius`` and inner radious ``radious - r_width``

    :param size: list with the size of the mask ``[dx,dy]``
    :type size: list
    :param radius: Radious of the mask
    :type radius: integer
    :param r_width: width of the annular mask.
    :type r_width: integer
    :return: circular mask
    :rtype: np.ndarray
    """

    grids = np.mgrid[-size[0] // 2 : size[0] // 2 + 1, -size[1] // 2 : size[1] // 2 + 1]

    # [j][i] = r^2
    r2 = grids[0] ** 2 + grids[1] ** 2
    theta = np.arctan2(grids[1], grids[0])

    # get boolean value for inclusion in the circle
    outer_circle = r2 <= radius**2
    inner_circle = r2 < (radius - r_width) ** 2

    # back to integers
    outer_circle.dtype = inner_circle.dtype = np.int8
    annulus = outer_circle - inner_circle

    coords = np.where(annulus == 1)

    return annulus, coords


def gradient(image: np.ndarray, method: str = "simply", size: int = 3) -> np.ndarray:
    """
    calculate gradient of real image.

    The gradient can be caoculated using:

    * Finite differences method: ``method = "simply"`` (default)
    * Prewitt method: ``method = "Prewitt"`` (optional)
    * Sobel method: ``method = "prewittsmooth"`` (optional)

    :param image: Input image
    :type image: np.ndarray
    :param method: gradient method
    :type method: string
    :return: image gradient
    :rtype: np.ndarray
    """

    method = method.lower()
    if method == "simply":
        # print("Using simply for edges")
        sx = image - shift(image, shift=[1, 0], fill_value=0)
        sy = image - shift(image, shift=[0, 1], fill_value=0)
    elif method == "prewitt":
        # print("Using prewitt for edges")
        sx = (shift(image, shift=[-1, 0]) - shift(image, shift=[+1, 0])) / 2
        sy = (shift(image, shift=[0, -1]) - shift(image, shift=[0, +1])) / 2
    elif method == "prewittsmooth":
        # print("Using prewittsmooth for edges")
        sx = np.zeros_like(image)
        sy = np.zeros_like(image)

        start = size // 2
        for i in range(size):
            sx = sx + (shift(image, shift=[-1, -start + i]) - shift(image, shift=[+1, -start + i]))
            sy = sy + (shift(image, shift=[-start + i, -1]) - shift(image, shift=[-start + i, +1]))
        sx = sx / (2 * size)
        sy = sy / (2 * size)
    else:
        print("wrong method")
        return 0
    return sx**2 + sy**2


def threshold_otsu(image: np.ndarray, nbins: int=256) -> float:
    """
    Return threshold value based on Otsu's method.

    :param matrix: Grayscale input image.
    :type matrix: (N, M) ndarray
    :param nbins: Number of bins used to calculate histogram. This value is ignored for integer arrays.
    :type nbins: integer, optional.
    :param fill_value: Since there is no boundary conditions, fill_value represents the pixel value to the empty data, defaults to 0
    :type fill_value: float
    :return: Upper threshold value. All pixels with an intensity higher than this value are assumed to be foreground.
    :rtype: float
    :raises ValueError:  If `image` only contains a single grayscale value.

    .. [Otsu]
        Wikipedia, http://en.wikipedia.org/wiki/Otsu's_Method

    .. note:: The input image must be grayscale.

    """

    if len(image.shape) > 2 and image.shape[-1] in (3, 4):
        raise ValueError(
            "threshold_otsu is expected to work correctly only for "
            "grayscale images; image shape {0} looks like an RGB image"
        )

    # Check if the image is multi-colored or not
    if image.min() == image.max():
        raise ValueError(
            "threshold_otsu is expected to work with images "
            "having more than one color. The input image seems "
            "to have just one color {0}.".format(image.min())
        )

    hist, bin_centers = histogram(image.ravel(), nbins)
    hist = hist.astype(float)

    # class probabilities for all possible thresholds
    weight1 = np.cumsum(hist)
    weight2 = np.cumsum(hist[::-1])[::-1]
    # class means for all possible thresholds
    mean1 = np.cumsum(hist * bin_centers) / weight1
    mean2 = (np.cumsum((hist * bin_centers)[::-1]) / weight2[::-1])[::-1]

    # Clip ends to align class 1 and class 2 variables:
    # The last value of `weight1`/`mean1` should pair with zero values in
    # `weight2`/`mean2`, which do not exist.
    variance12 = weight1[:-1] * weight2[1:] * (mean1[:-1] - mean2[1:]) ** 2

    idx = np.argmax(variance12)
    return bin_centers[:-1][idx]


def histogram(image: np.ndarray, nbins: int = 256) -> np.ndarray:
    """
    Return histogram of image.

    Unlike `numpy.histogram`, this function returns the centers of bins and
    does not rebin integer arrays. For integer arrays, each integer value has
    its own bin, which improves speed and intensity-resolution.

    The histogram is computed on the flattened image: for color images, the
    function should be used separately on each channel to obtain a histogram
    for each color channel.

    :param image: Grayscale input image.
    :type image: (N, M) ndarray
    :param nbins: Number of bins used to calculate histogram. This value is ignored for integer arrays.
    :type nbins: integer, optional.
    :return: A tupple with the values of the histogram and the values at the center of the bins.
    :rtype: np.ndarray, np.ndarray

    """
    sh = image.shape
    # For integer types, histogramming with bincount is more efficient.
    if np.issubdtype(image.dtype, np.integer):
        offset = 0
        image_min = np.min(image)
        if image_min < 0:
            offset = image_min
            image_range = np.max(image).astype(np.int64) - image_min
            # get smallest dtype that can hold both minimum and offset maximum
            offset_dtype = np.promote_types(
                np.min_scalar_type(image_range), np.min_scalar_type(image_min)
            )
            if image.dtype != offset_dtype:
                # prevent overflow errors when offsetting
                image = image.astype(offset_dtype)
            image = image - offset
        hist = np.bincount(image.ravel())
        bin_centers = np.arange(len(hist)) + offset

        # clip histogram to start with a non-zero bin
        idx = np.nonzero(hist)[0][0]
        return hist[idx:], bin_centers[idx:]
    else:
        hist, bin_edges = np.histogram(image.flat, bins=nbins)
        bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.0
        return hist, bin_centers


def FindEdges(
    input_image: np.ndarray, threshold: float, method: str = "simply", loop_threshold: int = 1, Otsu: bool = False, verbose: bool = False):

    """
    Use a method ``[simply, prewitt, prewittsmooth]``
    for calculating the image gradient and find the edges of the image above a given threshold.
    The threshold definition is such that ``image_edges_values > image_gradient.max() * threshold``

    If ``loop_threshold != 1`` then the program automatically finds a thresholds automatically such
    that the number op points in the image gradient ``15% > npx > 1%``.

    If ``Otsu == True``, the the threshold finding method is overrided by the Otsu method.
    Override current method. Using Otsu histogram

    :param input_image: Grayscale input image.
    :type input_image: (N, M) ndarray
    :param threshold: Threshold above which to detect edges in the image gradient (in percent wrt image gradient max value).
    :type threshold: float.
    :param method: Gradient method ``[simply, prewitt, prewittsmooth]``
    :type method: string
    :param loop_threshold: Number of iterations for finding a threshold automatically. Defaults to 1 = No auto.
    :type loop_threshold: int
    :param Otsu: Activate Otsu method for finding the edges
    :type Otsu: bool
    :param verbose: Activate verbosity
    :type verbose: bool
    :return: binary mask containing the edges. If ``loop_threshold`` is =/= 1, then it resutns a tupple containing the binary mask and the threshold
    :rtype: (N, M) ndarray

    """
    # prewitt filter gradient
    im_grad = gradient(input_image.real, method=method)
    im_grad -= im_grad.min()

    # make binary image
    for _ in range(loop_threshold):
        image = np.copy(im_grad)
        image = image > image.max() * threshold
        image.dtype = np.int8
        nonzero = np.array(image.nonzero())
        density = float(nonzero[0].size) / image.size
        print("Signal density:", density * 100.0)
        if density > 0.15:
            if verbose == True:
                print("Too many points")
            threshold = threshold * 2.0
        elif density > 0.01 and density < 0.15:
            if verbose == True:
                print("Threshold OK - exit (between 0.15 and 0.01")
            break
        elif density < 0.01:
            if verbose == True:
                print("Too less points")
            threshold = threshold / 2.0

    if Otsu:
        print("Override current method. Using Otsu histogram")
        thresh = threshold_otsu(im_grad)
        image = np.copy(im_grad)
        image = image > image.max() * thresh
        image.dtype = np.int8
    # show_one(image2)
    # np.save('test.npy', image)  # npzfile = np.load(outfile)
    # a = np.load('test.npy')
    # print(a)
    # raise SystemExit()
    # imgbin = image >= 1
    # plt.imshow(image)
    # plt.show()
    # show_one(image)
    # show_one(imgbin)
    if verbose == True:
        print("Stop in FindEdges. Close plot window to continue.")
        plib.show_one(image, title="FindEdges thresholded image")
    return image if loop_threshold == 1 else (image, threshold)


def make_circles(radius: int, r_width:int) ->np.ndarray:
    """
    Create a circle mask of size radious ``radius`` or a annular mask with  radious ``radius`` and inner radious ``radious - r_width``

    :param radius: Radious of the mask
    :type radius: integer
    :param r_width: width of the annular mask.
    :type r_width: integer
    :return: circular mask
    :rtype: np.ndarray
    """
    grids = np.mgrid[-radius : radius + 1, -radius : radius + 1]

    # [j][i] = r^2
    kernel_template = grids[0] ** 2 + grids[1] ** 2

    # get boolean value for inclusion in the circle
    outer_circle = kernel_template <= radius**2
    inner_circle = kernel_template < (radius - r_width) ** 2

    # back to integers
    outer_circle.dtype = inner_circle.dtype = np.int8
    return outer_circle - inner_circle


def find_Circles_ida(image: np.ndarray, radii: np.ndarray, r_width: int, verbose: bool = False):
    """
    Part of the Hough transform algorithm

    Perfrom a FFT Convolution over all the radii with the given annulus width.
    Smaller annulus width = more precise

    :param image: input image
    :type image: np.ndarray
    :param radii: radii
    :type radii: np.ndarray
    :param r_width: widths
    :type r_width: np.adarray
    :param verbose: Activate verbosity
    :type verbose: bool
    :return:
    :rtype:
    """
    acc = np.zeros((radii.size, image.shape[0], image.shape[1]))

    for i, r in enumerate(radii):
        C = make_circles(r, r_width)
        acc[i, :, :] = fftconvolve(image, C, "same")
        print("Running fft convolution ", i, " from a total of ", len(radii), end="\r")
        if verbose:
            print(image.shape, radii[i], r_width)
            plib.show_one(C, title="circle*")
            plib.show_one(acc[i], title="convolution")
    return acc

def votes(acc: np.ndarray, radii: np.ndarray) -> tuple:

    """
    Part of the Hough transform algorithm.

    Returns: (circle_y, circle_x), radius, maxima, max_positions

    - c[0] = x
    - c[1] = y   (The other way around of the definition!!!! which would be c[1] = x and c[0] = y)

    """
    maxima = []
    max_positions = []
    max_signal = 0
    circle_x = 0
    circle_y = 0
    radius = 0
    print("calc: radius |  maxima  | max_position (x,y) | signal")

    # FIXME Hay que mejorarlo. Coger una caja de 5x5 y buscar el máximo en la caja
    for i, r in enumerate(radii):
        max_positions.append(np.unravel_index(acc[i].argmax(), acc[i].shape))
        maxima.append(acc[i].max())
        # use the radius to normalize
        signal = maxima[i] / np.sqrt(float(r))
        #        if signal > max_signal:
        #            max_signal = signal
        if maxima[i] > max_signal:
            max_signal = np.copy(maxima[i])
            (circle_y, circle_x) = max_positions[i]
            radius = np.copy(r)
        print(
            "calc: %8.2f | %8.2f | %s | %8.2f"
            % (r, maxima[i], (max_positions[i]), signal)
        )

    print(f"Last: {radius:8.2f} | {np.max(maxima):8.2f} | {(circle_x, circle_y)}")

    # Identify maximum. Note: the values come back as index, row, column
    #    max_index, circle_y, circle_x = np.unravel_index(acc.argmax(), acc.shape)

    return (circle_x, circle_y), radius, maxima, max_positions  #

def bin_annulus(shape, radius, width, full=False):
    """
    This function creates a anulus mask of radius and width
    radius - width//2 < radius < radius + width//2 + 1
    """

    rho = circle_grid(shape)  # FOR GENERATING CENTERS GLOVAL VARIABLE

    mask1 = rho <= radius + width // 2 + 1
    mask2 = rho >= radius - width // 2
    mask1.astype(np.int8)
    mask2.astype(np.int8)
    return mask1 if full == True else mask1 == mask2


def circle_grid(shape):
    """
    This function creates a grid of points with NxN dimensions.
    Output:

    X,Y: X and Y meshgrid of the detector
    """
    N = shape[0]
    if N % 2 != 0:
        print("Number of pixels must be an even integer!", N, N % 2)
        raise ValueError("error in circle_grid")
    x = np.linspace(-N / 2, N / 2, N)
    y = np.copy(x)
    X, Y = np.meshgrid(x, y)
    # globals()['rho'] = rho

    return np.sqrt(X**2 + Y**2)


def find_circle_hough(
    image,
    inner_radius,
    outer_radius,
    steps,
    method="prewitt",
    loop_threshold=10,
    normalize=False,
    verbose=False,
    Otsu=None,
    threshold=0.15):
    """
    Do Hough Transform
    """
    imsize = image.shape

    ############################
    # Normalize images (using a box 100x100 in the central image)
    ############################

    if normalize == True:
        norma = np.mean(
            image[
                imsize[0] // 2 - 100 : imsize[0] // 2 + 100,
                imsize[0] // 2 - 100 : imsize[0] // 2 + 100,
            ]
        )
        image = image / norma

    ############################
    # CALCULATE THE MASK GRADIENT FOR EACH IMAGE
    ############################

    binmask = []
    # threshold = 0.15
    # central image to determine threshold
    binmask, threshold = FindEdges(
        image,
        threshold,
        method=method,
        loop_threshold=loop_threshold,
        verbose=verbose,
        Otsu=Otsu,
    )

    # show_one(binmask)
    print(threshold)

    ############################
    # FIND CENTERS
    ############################

    print("Analizing image........")
    r_width = (outer_radius - inner_radius) // steps * 2
    radii = np.linspace(inner_radius, outer_radius, steps)
    print("r_width", r_width, "radii", radii)
    acc_conv = find_Circles_ida(binmask, radii, r_width)
    # acc_conv = find_Circles(binmask, radii, r_width, verbose=verbose, full=True)
    center, radius, c, d = votes(acc_conv, radii)
    print("Found center [y,x]: ", center, " and radius: ", radius)

    if verbose == True:
        fig = plt.figure(frameon=False)
        im1 = plt.imshow(binmask, cmap=plt.cm.gray, alpha=0.5)
        circle_fit = bin_annulus(imsize, radius, 1, full=False).astype(float)
        dd = np.array(center)
        dx = dd[0] - imsize[0] // 2
        dy = dd[1] - imsize[1] // 2
        circle_fit = shift(circle_fit, shift=[dx, dy])
        im2 = plt.imshow(circle_fit, cmap=plt.cm.gray, alpha=0.5)
        plt.show()

    return center, radius, threshold


def FFTs(f, dir):
    """fft with shifting"""
    what = np.ascontiguousarray(f)

    if dir == 1:
        return np.fftshift(np.fftn(what))
    elif dir == -1:
        return np.fftshift(np.ifftn(np.ifftshift(what)))
    else:
        print("Select direction: 1 -> Direct FFT; -1 -> Inverse FFT")
        quit()
        return 0


def Laplacian(xs):
    "calculate gradient of real image using Laplacian filter"
    lxx = (shift(xs, shift=[1, 0]) - 2 * xs + shift(xs, shift=[-1, 0])) / 2
    lyy = (shift(xs, shift=[0, 1]) - 2 * xs + shift(xs, shift=[0, -1])) / 2
    return lxx**2 + lyy**2


def rebin(arr, new_shape):
    """Rebin 2D array arr to shape new_shape by averaging."""
    shape = (
        new_shape[0],
        arr.shape[0] // new_shape[0],
        new_shape[1],
        arr.shape[1] // new_shape[1],
    )
    return arr.reshape(shape).mean(-1).mean(1)


def apod(nx, alpha):
    window = tukey(int(nx), alpha=alpha)
    return 1 - np.outer(window, window)


def expand_mask(mask, pixels=1, step=1):

    msk = (
        shift(mask, shift=[-step, -step], fill_value=0)
        & shift(mask, shift=[0, step], fill_value=0)
        & shift(mask, shift=[-step, 0], fill_value=0)
        & shift(mask, shift=[0, -step], fill_value=0)
        & shift(mask, shift=[step, 0], fill_value=0)
        & shift(mask, shift=[1, step], fill_value=0)
        & mask
    )
    if pixels > 1:
        for _ in range(pixels):
            msk = (
                shift(msk, shift=[-step, -step], fill_value=0)
                & shift(msk, shift=[0, step], fill_value=0)
                & shift(msk, shift=[-step, 0], fill_value=0)
                & shift(msk, shift=[0, -step], fill_value=0)
                & shift(msk, shift=[step, 0], fill_value=0)
                & shift(msk, shift=[1, step], fill_value=0)
                & msk
            )

    return msk


def sigma_mask(input, sigma=5, verbose=False, level=0.02):
    """Input HAS to be a n,ny,nx array becouse we average in n"""
    shape = input.shape
    if len(shape) != 3:
        print("Input should be [n,ny,nx]")
        raise IndexError
    md = np.mean(input, axis=0)
    md_std = np.std(input, axis=0)
    md_std = gaussian_filter(md_std, sigma=sigma)
    if verbose:
        plt.imshow(md_std)
        plt.colorbar()
        plt.show()
    ms = np.ones_like(md)
    ms[md_std > level] = 0
    ms = ms.astype(int)
    if verbose:
        plt.imshow(ms.astype(int), interpolation=None, cmap="Greys")
        plt.colorbar()
        plt.show()
    return ms


def n_coord(x, y, d=1):
    w, h = x.shape
    assert w, h == y.shape
    if d == 1:
        print("Normalizing")
        return (2 * x - w) / w, (2 * y - h) / h
    if d == -1:
        print("De-Normalizing")
        return (x + 1) * w / 2, (y + 1) * h / 2
    print("wrong direction")
    return 0, 0


def dmodel(r, k1, k2):
    """r is a map of the grid"""
    return 1 + k1 * r + k2 * r**2


def cmesh(w, h, **kwargs):
    X, Y = np.meshgrid(np.arange(w), np.arange(h))
    x, y = n_coord(X, Y, **kwargs)
    r = np.sqrt(x**2 + y**2)
    return x, y, r


def test_distortion():
    # adjust k_1 and k_2 to achieve the required distortion
    k1 = 0.02
    k2 = 0.02
    margin = np.max([k1, k2]) * 4 + 1
    w, h = 2048, 2048
    wn, hn = int(np.round(w * margin)), int(np.round(h * margin))
    print(w, h)
    x, y, r = cmesh(wn, hn)
    model = dmodel(r, k1, k2)
    # the model is applied like
    x_new = x * model
    y_new = y * model
    x_new2, y_new2 = n_coord(x_new, y_new, d=-1)

    # TODO incomplete program
    # grid = ndimage.map_coordinates(m, [y_new2.ravel(),x_new2.ravel()])
