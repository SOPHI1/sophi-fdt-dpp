"""
``phi_utils.py`` Contains useful functions for operating with vectors and others.
It also contains fitting methods, functions to determine the center to limb behavior and orbit information.

   :Project: Solar Orbiter Polarimetric and Helioseismic Imager (SoPHI - FDT)
   :Date: 2022-10-24
   :Authors: **David Orozco Suárez**
   :Contributors:

.. list-table::
   :widths: 25 50
   :header-rows: 1

   * - program
     - Short summary
   * - :py:meth:`dot`
     - dot product between two vectors
   * - :py:meth:`angle`
     - angle between two vectors
   * - :py:meth:`angle_3D`
     - angle between two vectors using tan()
   * - :py:meth:`crossp`
     - cross product
   * - :py:meth:`sphere2cart`
     - spherical coordinates to cartesian
   * - :py:meth:`cart2sphere`
     - cartesian to spherical coordinates
   * - :py:meth:`cart2polar`
     - cartesian to polar coordinates
   * - :py:meth:`polar2cart`
     - polar coordinates to cartesian
   * - :py:meth:`azimutal_average`
     - azimuthal average of an image
   * - :py:meth:`limb_darkening`
     - Limb darkening function and its derivative
   * - :py:meth:`newton`
     - Newton’s method optimization algorithm
   * - :py:meth:`allen_clv`
     - Center to limb variation provided by Allen's Astrophysical Quantities
   * - :py:meth:`get_time`
     - Print time
   * - :py:meth:`phi_orbit`
     - Get basic orbitar parameters using Spice kernels.
   * - :py:meth:`genera_2d`
     - Created a 2D images from a radial shape
   * - :py:meth:`running_mean`
     - Perform a running mean
   * - :py:meth:`rotate_grid`
     - Rotate a grid or a point
   * - :py:meth:`create_dirs`
     - create directories from list
   * - :py:meth:`find_file_in_runtime_directory`
     - Find file

"""


import contextlib
from datetime import timedelta, datetime
from math import sin, cos
from os import path
from inspect import getframeinfo, stack
from sys import _getframe

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import spiceypy
import warnings
from astropy import units as u
from scipy.interpolate import interp1d

from .tools import *

warnings.filterwarnings('ignore')


def dot(K: np.ndarray, L: np.ndarray) -> np.ndarray:
    """
    Return the dot product between two vectors.

    :param K: input matrix :math:`\mathbf{K}`
    :type K: np.ndarray
    :param L: input matrix :math:`\mathbf{L}`
    :type L: np.ndarray
    :return: :math:`\mathbf{K}\cdot\mathbf{L}`
    :rtype: array
    """
    return sum(i * j for i, j in zip(K, L)) if len(K) == len(L) else 0


def angle(K: np.ndarray, L: np.ndarray) -> float:
    r"""
    Calculate the angle between two vectors :math:`\vec{K}` and :math:`\vec{L}`

    .. math:: \cos(\alpha) = \frac{\vec{K}\cdot\vec{L}}{\|\vec{K}\|\|\vec{L}\|}
        :label: eqn1
        :name: eq:1

    :param K: input vector
    :type K: np.ndarray
    :param L: input vector
    :type L: np.ndarray
    :return: :math:`\alpha` in degrees
    :rtype: float
    """
    dot_prod = dot(K, L)
    n_K = np.linalg.norm(K, axis=0)
    n_L = np.linalg.norm(L, axis=0)
    c = dot_prod / n_K / n_L
    angle = np.arccos(np.clip(c, -1, 1))
    return np.degrees(angle)


def angle_3D(K: np.ndarray, L: np.ndarray, n: np.ndarray) -> float:
    r"""
    Angle between two 3D-vectors

    Same as :ref:`in dot product <eq:1>` but using the cross-product

    .. math::
        :label: eqn2
        :name: eq:2

        \vec{K}\cdot\vec{L}  =  {\|\vec{K}\|\|\vec{L}\|} \cos(\alpha)

        \vec{K}\times\vec{L} =  {\|\vec{K}\|\|\vec{L}\|} \sin(\alpha) \vec{n}

        \alpha  =  \arctan(\|\vec{K}\times\vec{L}\|,\vec{K}\cdot\vec{L})

    :param K: input vector
    :type K: np.ndarray
    :param L: input vector
    :type L: np.ndarray
    :param n: input vector
    :type n: np.ndarray
    :return: :math:`\alpha` in degrees
    :rtype: float
    """
    cross_prod = crossp(K, L)
    dot_prod = dot(K, L)
    n_c = np.linalg.norm(cross_prod, axis=0)
    vect_c = np.sign(dot(cross_prod, np.transpose([n] * K.shape[1]))) * n_c
    ang = np.arctan2(vect_c, dot_prod)
    return np.degrees(ang)


def crossp(K: np.ndarray, L: np.ndarray) -> np.ndarray:
    """
    cross product :math:`\\vec{K}\\times\\vec{L}`

    Accepts 2D matrices.

    :param K: input vector
    :type K: np.ndarray
    :param L: input vector
    :type L: np.ndarray
    :return: cross-product
    :rtype: float
    """
    r = np.zeros((K.shape))
    for n in range(K.shape[1]):
        r[:, n] = np.cross(K[:, n], L[:, n])
    return r


def sphere2cart(r: float, theta: float, phi: float) -> tuple:
    """
    Spherical to Cartesian coordinates defined as (radial, polar, azimuthal), e.g., form a physicist point of view.

    Within this convention `r` is the radial distance, `theta` is the
    inclination (polar) angle, measured from the positive z-axis with :math:`0\le\\theta<2\pi,
    and `phi` is the azimuth angle, measured in the xy-plane from the x-axis with  :math:`0\le\\phi<2\pi`

    .. math::
        :label: eqn3
        :name: eq:3

        x = r \sin\\theta \cos\phi

        y = r \sin\\theta \sin\phi

        z = r \cos\\theta


    .. figure:: _static/ref_system.svg
       :height: 200px
       :width: 200px
       :scale: 100 %
       :alt: alternate text
       :align: left
       :figwidth: 100 %

    Reference system

    :param r: radial distance
    :type r: float
    :param phi: polar angle
    :type phi: float
    :param theta: azimuthal angle
    :type theta: float
    :return: cartesian coordinates
    :rtype: tuple(x,y,z)
    """
    x = r * np.cos(theta) * np.sin(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return (x, y, z)


def cart2sphere(x: float, y: float, z: float) -> tuple:
    """
    Cartesian `x`, `y`, and `z` to spherical :math:`(r,\\theta,\phi)`.
    In this case the (polar) angle corresponds to :math:`\\theta = \delta` where :math:`\delta` is the latitude measured from the xy-plane.

    .. math::
        :label: eqn4
        :name: eq:4

        r = \sqrt(x^2+y^2+z^2)

        \\theta = \\arctan(z,\sqrt(x^2+y^2))

        \phi = \\arctan(y,x)

    .. note::
        Different convention as in :ref:`sphere2cart() <eq:3>`.

    :param x: `x` coordinate
    :type x: float
    :param y: `y` coordinate
    :type y: float
    :param z: `z` coordinate
    :type z: float
    :return: Spherical coordinates (radial, polar, azimuthal)
    :rtype: Tuple
    """
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)  # r
    theta = np.arctan2(z, np.sqrt(x ** 2 + y ** 2))  # theta
    phi = np.arctan2(y, x)  # phi
    return (r, theta, phi)


def cart2polar(x: float, y: float)-> tuple:
    """
    Transform cartesian to polar coordinates

    :param x: x coordinate
    :type x: float
    :param y: y coordinate
    :type y: float
    :return: polar coordinates (r,theta)
    :rtype: tuple
    """
    r = np.sqrt(x ** 2 + y ** 2)
    theta = np.arctan2(y, x)
    return r, theta


def polar2cart(r:float, theta:float)-> tuple:
    """
    Transform polar coordinates to cartesian

    :param x: modulos r
    :type x: float
    :param y: angle
    :type y: float
    :return: cartesian coordinates (x,y)
    :rtype: tuple
    """
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return x, y


def azimutal_average(img: np.ndarray, centers: list, size_of_bin: int = 2) -> tuple:
    """
    Calculatiom of an azimuthal average of an image

    The calculatiom of an azimuthal average is done making circular rings around a given center.
    The integration is done over an area defined by :math:`(r-S)\le R \le(r+S)` where :math:`R = [0,\sqrt(x^2+y^2]`
    with :math:`x` and :math:`y` the maximum number of pixels of a grid with center :math:`c = (x_c,y_c)`, :math:`S`
    is the size of the bin and r the integrand.

    :param img: input image
    :type img: np.ndarray
    :param centers: list providing the center of the image
    :type centers: np.ndarray
    :param size_of_bin: size of bin (2xsize) in the radial direction, defaults to 2
    :type size_of_bin: int, optional
    :return: tuple with (variation of the intensity along radial distance, radial distance)
    :rtype: tuple
    """
    sy, sx = img.shape
    [X, Y] = np.meshgrid(np.arange(sx) - centers[0], np.arange(sy) - centers[1])
    R = np.sqrt(np.square(X) + np.square(Y))
    rad = np.arange(1, np.max(R), 1)
    intensity = np.zeros(len(rad))
    size_of_bin = 2
    for index, r in enumerate(rad):
        mask = np.greater(R, r - size_of_bin) & np.less(R, r + size_of_bin)
        values = img[mask]
        intensity[index] = np.mean(values)
    return intensity, rad


def limb_darkening(x: float, a_k: list) -> tuple:
    """
    Limb darkening function and its derivative

    This function returns the limb darkening function and its derivative from :math:`\mu = 1 - \cos\\theta` with
    :math:`\\theta` the heliocentric angle and :math:`a_k` the parameters of the center to limb function (see
    `D. Hestroffer and C. Magnan, Astron. Astrophys. 333, 338–342 (1998) <https://ui.adsabs.harvard.edu/abs/1998A%26A...333..338H/abstract>`_ ):

    .. math::
        :label: eqn5
        :name: eq:5

        I = I_0 ( 1 - \sum_{k=1}^{n} a_k  \mu^k)

    In this expresion, :math:`n = len(a_k)`.

    :param x: :math:`\mu`
    :type x: float or np.ndarray
    :param pars: :math:`a_k`
    :type pars: list
    :return: limb darkening at :math:`\mu` and its derivative
    :rtype: tuple
    """

    # Order 1 case (simplest)
    # df_dI0 = -(1 - u * (1 - mu))
    # df_du  = -( I0 * (mu - 1))

    order = len(a_k)
    I0 = a_k[0]
    mu = 1 - x
    exponent = np.arange(order - 1, dtype=float) + 1.0

    fn = np.zeros((len(mu)))
    fn[:] = I0
    for i in exponent:
        fn[:] -= I0 * a_k[int(i)] * mu ** i

    dr = np.zeros((len(mu), len(a_k)))
    dr[:, 0] = 1
    for i in exponent:
        dr[:, 0] -= a_k[int(i)] * mu ** i
    for i in exponent:
        dr[:, int(i)] = - I0 * mu ** i

    return fn, dr


def newton(y: np.ndarray, x: np.ndarray, pars: list, func: callable, iterations: int = 3,  **args) -> np.ndarray:
    """
    Newton’s method optimization algorithm for smooth functions when derivatives are available.

    Newton’s method is based on fitting the function to a quadratic form given by the Taylor expansion:

    .. math::
        :label: eqn6
        :name: eq:6

        f(x) \\approx f(x_0)+\\nabla f(x_0)(x-x_0)+\\frac{1}{2}(x-x_0)^\\top H(x_0)(x-x_0)

    where :math:`H(x_0)` is the Hessian matrix and :math:`\\nabla f(x_0)` the gradient.
    If the derivatives exist and are positive and given an initial estimate :math:`x_0`,
    Newton’s method creates a sequence of updates as:

    .. math::
        :label: eqn7
        :name: eq:7

        x_{k+1} - x_k = \Delta x = - H(x_k)^{-1}\\nabla f(x_k)

    This is called the Newton-Conjugate gradient iterative method.

    :param y: measurements, 1-D array with shape (n,)
    :type y: np.ndarray
    :param x: 1-D array with shape (n,)
    :type x: np.ndarray
    :param pars: function parameters as initial guess
    :type pars: list or np.ndarray
    :param func: The objective function to be minimized. `func(x, pars, **args)`
        where x is a 1-D array with shape (n,),
        pars are the fit parameters and args are any other external parameters to the function;
        func must return  a tuple (f, df/dx) with the function value and its derivative
    :type func: callable
    :param iterations: total number of iterations, defaults to 3
    :type iterations: int, optional
    :return: function parameters
    :rtype: np.ndarray
    """

    for i in range(iterations):
        fn, dr = func(x, pars, **args)

        chi2 = np.sum((y - fn) ** 2)  # NOT USED
        chi = (y - fn)
        J = np.matmul(chi, - dr)
        H = np.matmul(np.transpose(- dr), - dr)
        HI = np.linalg.inv(H)
        Change = np.matmul(HI, np.transpose(J))
        pars_new = pars - Change
        print('Newton... Old params: ', pars, ' New params: ', pars_new, ' Iteration: ', i)
        pars = pars_new

    return pars


def allen_clv(wave: float, theta: float, check: int = 0) -> float:
    """
    Center to limb variation provided by Allen's Astrophysical Quantities, Springer, 2000

    .. math::
        :label: eqn8
        :name: eq:8

        I =  1 - u - v + u \cos\\theta + v\cos\\theta

    :param wave: wavelength in Angstroms.
    :type wave: float or np.ndarray
    :param theta: angle between the Sun's radius vector and the line of sight. In radians.
    :type theta: float or np.ndarray
    :param check: there are three options:

        * check = 0, default. Returns a tuple with (Allen CLV, u, v, u + v)
        * check = 1. Same as 0 but plots the CLV
        * check = 2. Returns a tuple with (u + v)
    :type check: int, optional
    :return: tuple

    :rtype: float
    """

    def coefs_(wave):
        u = (- 8.9829751 + 0.0069093916 * wave - 1.8144591e-6 * wave ** 2 + 2.2540875e-10 * wave ** 3 -
             1.3389747e-14 * wave ** 4 + 3.0453572e-19 * wave ** 5)
        v = (+ 9.2891180 - 0.0062212632 * wave + 1.5788029e-6 * wave ** 2 - 1.9359644e-10 * wave ** 3 +
             1.1444469e-14 * wave ** 4 - 2.5994940e-19 * wave ** 5)
        return u, v

    u, v = coefs_(wave)
    with contextlib.suppress(Exception):
        if check == 1:
            gamma = np.arange(0, 90, 1) * np.pi / 180.
            I = 1 - (u + v) + (u + v) * np.cos(gamma)
            clv = 1 - u - v + u * np.cos(gamma) + v * np.cos(gamma)
            plt.plot(gamma, clv)
            plt.plot(gamma, I, '--')

    if check == 2:
        return u + v
    return 1 - u - v + u * np.cos(theta) + v * np.cos(theta), u, v, u + v


def get_time(h):
    time = datetime.strptime(h[0][1], '%Y-%m-%d %H:%M:%S.%f')
    print('TIME: ', time)
    return time


def phi_orbit(init_date, object, end_date=None, frame='SOLO_HEEQ', resolution: float = 1, kernel=None, kernel_dir=None):
    """
    Get basic orbitar parameters using Spice kernels.

    In particular, a Tuple of:

    [sc_time, sc_r, sc_lon, sc_lat, sc_x, sc_y, sc_z, sc_vx, sc_vy, sc_vz, sc_sp, sc_vr, elevation, angle, s_size]

    Need to have spicepy installed ( https://pypi.org/project/spicepy/ ).
    The program goes to the kernel dir for loading the mk (meta-kernel) file that takes into account all stuff.

    The orbits kernels should be installed in the following directory:

    # .. figure:: _static/tree-1.svg
    #    :height: 200px
    #    :width: 200px
    #    :scale: 100 %
    #    :alt: alternate text
    #    :align: left
    #    :figwidth: 100 %

    | src
    | ├── cmilos
    | ├── cvs
    | ├── *orbits-data*
    |     └── kernels
    |         └── ck
    |         └── fk
    |         └── ...
    |     └── misc
    | ├── polcal_py

    In the folder structure above:

    - ``orbits-data`` is the folder where the kernel is located. These can be downloaded from https://www.cosmos.esa.int/web/spice/solar_orbiter

    ``kernel`` and ``kernel_dir`` can be provided in case one wants to put the kernels elsewhere.

    Default frame is SOLO_HEEQ.

    .. note::
         An example of how to use `phi_orbits` can be found `here <phi_orbits.html>`_

    ..warning::
        The kernel directory is obtained via ``os.environ['SPICE']`` so the user should add the system variable "SPICE" to the system

    Kernels provided by SOLO (see readme in the kernels)

    .. list-table::
        :widths: 25 50
        :header-rows: 1

        * - SPICE Frame Name
          - Common names and other designators
        * - SUN_ARIES_ECL
          - HAE, Solar Ecliptic (SE)
        * - SUN_EARTH_CEQU
          - HEEQ, Stonyhurst Heliographic
        * - SUN_INERTIAL
          - HCI, Heliographic Inertial (HGI)
        * - EARTH_SUN_ECL
          - GSE of Date, Hapgood
        * - SUN_EARTH_ECL
          - HEE of Date
        * - EARTH_MECL_MEQX
          - Mean Ecliptic of Date (ECLIPDATE)

    The Heliocentric Earth Ecliptic frame (HEE) is defined as follows:

        * X-Y plane is defined by the Earth Mean Ecliptic plane of date, therefore, the +Z axis is the primary vector,and it defined as the normal vector to the Ecliptic plane that points toward the north pole of date.
        * +X axis is the component of the Sun-Earth vector that is orthogonal to the +Z axis.
        * +Y axis completes the right-handed system.
        * the origin of this frame is the Sun's center of mass.

    The Heliocentric Inertial Frame (HCI) is defined as follows:

        * X-Y plane is defined by the Sun's equator of epoch J2000: the +Z axis, primary vector, is parallel to the Sun's rotation axis of epoch J2000, pointing toward the Sun's north pole.
        * +X axis is defined by the ascending node of the Sun's equatorial plane on the ecliptic plane of J2000.
        * +Y completes the right-handed frame.
        * the origin of this frame is the Sun's center of mass.

    The Heliocentric Earth Equatorial (HEEQ) frame is defined as follows:

        * X-Y plane is the solar equator of date, therefore, the +Z axis is the primary vector and it is aligned to the Sun's north pole of date.
        * +X axis is defined by the intersection between the Sun equatorial plane and the solar central meridian of date as seen from the Earth. The solar central meridian of date is defined as the meridian of the Sun that is turned toward the Earth. Therefore, +X axis is the component of the Sun-Earth vector that is orthogonal to the +Z axis.
        * +Y axis completes the right-handed system.
        * the origin of this frame is the Sun's center of mass.

    SOLO mission specific generic frames are (see solo\_ANC\_soc-sci-fk\_???.tf):

    .. list-table::
        :widths: 25 50
        :header-rows: 1

        * - SPICE Frame Name
          - description
        * - SOLO_SUN_RTN
          - Sun Solar Orbiter Radial-Tangential-Normal
        * - SOLO_SOLAR_MHP
          - S/C-centred mirror helioprojective
        * - SOLO_IAU_SUN_2009
          - Sun Body-Fixed based on IAU 2009 report
        * - SOLO_IAU_SUN_2003
          - Sun Body-Fixed based on IAU 2003 report
        * - SOLO_GAE
          - Geocentric Aries Ecliptic at J2000 (GAE)
        * - SOLO_GSE
          - Geocentric Solar Ecliptic at J2000 (GSE)
        * - SOLO_HEE
          - Heliocentric Earth Ecliptic at J2000 (HEE)
        * - SOLO_VSO
          - Venus-centric Solar Orbital (VSO)

    .. list-table::
        :widths: 25 50
        :header-rows: 1

        * - Heliospheric Coordinate Frames
          - for the NASA STEREO mission
        * - SOLO_ECLIPDATE
          - Mean Ecliptic of Date Frame
        * - SOLO_GEORTN
          - Heliocentric Inertial Frame
        * - SOLO_HEEQ
          - Heliocentric Earth Ecliptic Frame
        * - SOLO_HEE_NASA
          - Heliocentric Earth Equatorial Frame
        * - SOLO_HCI
          - Geocentric Radial Tangential Normal Frame

    .. list-table::
        :widths: 25 50
        :header-rows: 1

        * - Heliocentric Generic Frames
          - (*)
        * - SUN_ARIES_ECL
          - Heliocentric Aries Ecliptic   (HAE)
        * - SUN_EARTH_CEQU
          - Heliocentric Earth Equatorial (HEEQ)
        * - SUN_EARTH_ECL
          - Heliocentric Earth Ecliptic   (HEE)
        * - SUN_INERTIAL
          - Heliocentric Inertial         (HCI)

    :param init_date: datetime
    :type init_date: datetime object
    :param object: Body, e.g., "Solar Orbiter"
    :type object: str
    :param end_date: datetime, defaults to None
    :type end_date: datetime object, optional
    :param frame: kernel frame, defaults to 'SOLO_HEEQ'
    :type frame: str, optional
    :param resolution: resulution is days, defaults to 1
    :type resolution: float, optional
    :param kernel: spice kernel file, defaults to None
    :type kernel: str, optional
    :param kernel_dir: spice kernel location, defaults to None
    :type kernel_dir: str, optional
    """

    REQUIRED_KERNELS = ['solo_ANC_soc-flown-mk.tm']

    # KERNELS_FULL_PATH_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
    KERNELS_FULL_PATH_DIRECTORY = os.environ['SPICE']
    KERNELS_FULL_PATH_DIRECTORY = f'{KERNELS_FULL_PATH_DIRECTORY}/kernels/mk/'
    if os.path.isfile(KERNELS_FULL_PATH_DIRECTORY+REQUIRED_KERNELS[0]):
        printc("Kernel",REQUIRED_KERNELS[0],"found at:", KERNELS_FULL_PATH_DIRECTORY, color=bcolors.OKGREEN)
    else:
        raise ValueError('Cannot find kernel (246):', KERNELS_FULL_PATH_DIRECTORY + REQUIRED_KERNELS[0])

    with contextlib.suppress(Exception):
        if kernel_dir != None:
            KERNELS_FULL_PATH_DIRECTORY = kernel_dir
    with contextlib.suppress(Exception):
        if kernel != None:
            REQUIRED_KERNELS = kernel

    # print(f'before context manager: {os.getcwd()}')
    with cwd_(KERNELS_FULL_PATH_DIRECTORY):
        # print(f'inside context manager: {os.getcwd()}')
        try:
            # Load the kernals
            spiceypy.furnsh(REQUIRED_KERNELS[0])
        except:
            raise IOError("Error reading kernel files",REQUIRED_KERNELS[0])
    # print(f'after context manager: {os.getcwd()}')

    # Calculate the timerange
    # times -->> TDB seconds past the J2000 epoch.

    def roll_angle(object,time,frame):
        spacecraft= '-144'
        with cwd_(KERNELS_FULL_PATH_DIRECTORY):
            sclkdp = spiceypy.spiceypy.sce2c(int(spacecraft), time)
        tolerance=100
        sc_frame = -144000
        with cwd_(KERNELS_FULL_PATH_DIRECTORY):
            try:
                (cmat,clkout)=spiceypy.spiceypy.ckgp(sc_frame, sclkdp, tolerance, frame)
            except Exception:
                warnings.warn("ckgp error. roll, pitch, yaw = 0")
                return 0.,0.,0.

        roll, pitch,yaw = spiceypy.spiceypy.m2eul(cmat, 1, 2, 3)
        if abs(roll) > np.pi: roll = roll - np.sign(2.*np.pi, roll)
        if abs(pitch) > np.pi/2.:
            pitch = np.sign(np.pi,pitch) - pitch
            yaw = yaw - np.sign(np.pi, yaw)
            roll = roll - np.sign(np.pi, roll)

        cnv = 180./np.pi
        return roll*cnv, pitch*cnv, yaw*cnv

    if end_date is None:
        sc_time = init_date
    else:
        sc_time = []
        while init_date < end_date:
            sc_time.append(init_date)
            init_date += timedelta(days=resolution)

    # Convert UTC to ephemeris time
    if end_date != None or isinstance(sc_time, list):
        sc_time_spice = [spiceypy.str2et(t.strftime('%Y-%m-%d %H:%M')) for t in sc_time]
        roll = []
        pitch = []
        yaw = []
        for t in sc_time_spice:
            r, p, y = roll_angle(object,t,frame)
            roll.append(r)
            pitch.append(p)
            yaw.append(y)
        # solo, lT = spiceypy.spkezr(object, sc_time_spice, frame , 'NONE', 'Sun')
        # solo, lT = spiceypy.spkezr(object, sc_time_spice, frame , 'NONE', 'Sun')
        with cwd_(KERNELS_FULL_PATH_DIRECTORY):
            solo, lT = spiceypy.spkezr(object, sc_time_spice, frame, 'NONE', 'SUN')
            earth, lT = spiceypy.spkezr("EARTH", sc_time_spice, frame, 'NONE', 'SUN')

        pos_solo = np.array(solo)[:, :3] * u.km
        vel_solo = np.array(solo)[:, 3:] * u.km / u.s

        sc_x = pos_solo[:, 0].to(u.au)
        sc_y = pos_solo[:, 1].to(u.au)
        sc_z = pos_solo[:, 2].to(u.au)
        sc_vx = vel_solo[:, 0]
        sc_vy = vel_solo[:, 1]
        sc_vz = vel_solo[:, 2]

        # +++ Calculate position for Earth +++
        pos_earth = np.array(earth)[:, :3] * u.km
        earth_x = pos_earth[:, 0].to(u.au)
        earth_y = pos_earth[:, 1].to(u.au)
        earth_z = pos_earth[:, 2].to(u.au)

    else:
        sc_time_spice = spiceypy.str2et(sc_time.strftime('%Y-%m-%d %H:%M'))
        with cwd_(KERNELS_FULL_PATH_DIRECTORY):
            solo, lT = spiceypy.spkezr(object, sc_time_spice, frame, 'NONE', 'Sun')
            earth, lT = spiceypy.spkezr("EARTH", sc_time_spice, frame , 'NONE', 'SUN')
        roll, pitch, yaw = [roll_angle(object,t,frame) for t in sc_time_spice]

        pos_solo = np.array(solo)[:3] * u.km
        vel_solo = np.array(solo)[3:] * u.km / u.s

        sc_x = pos_solo[0].to(u.au)
        sc_y = pos_solo[1].to(u.au)
        sc_z = pos_solo[2].to(u.au)
        sc_vx = vel_solo[0]
        sc_vy = vel_solo[1]
        sc_vz = vel_solo[2]

        # +++ Calculate position for Earth +++
        pos_earth = np.array(earth)[:3] * u.km
        earth_x = pos_earth[0].to(u.au)
        earth_y = pos_earth[1].to(u.au)
        earth_z = pos_earth[2].to(u.au)

    sc_sp = np.sqrt(sc_vx ** 2 + sc_vy ** 2 + sc_vz ** 2)

    sc_r = np.sqrt(sc_x ** 2 + sc_y ** 2 + sc_z ** 2)
    elevation = np.rad2deg(np.arcsin(sc_z / sc_r))
    angle = np.rad2deg(
        np.arcsin((sc_x.to_value() ** 2 + sc_y.to_value() ** 2 + sc_z.to_value() ** 2) / sc_r.to_value()))
    sc_r, sc_lat, sc_lon = cart2sphere(sc_x, sc_y, sc_z)

    # +++ Earth +++
    earth_r = np.sqrt(earth_x**2 + earth_y**2 + earth_z**2)
    earth_elevation = np.rad2deg(np.arcsin(earth_z / earth_r))
    earth_angle = np.rad2deg(np.arcsin((earth_x.to_value()**2 + earth_y.to_value()**2 + earth_z.to_value()**2 ) / earth_r.to_value()))
    earth_r, earth_lat, earth_lon = cart2sphere(earth_x,earth_y,earth_z)

    #_, sc_lon, sc_lat = spiceypy.reclat(solo[:3])
    # print(sc_r, sc_lat, sc_lon)

    # ECL2EQU_MAT = spiceypy.pxform(fromstr='SOLO_HEEQ', \
    #                             tostr='SUN_EARTH_CEQU', \
    #                             et=sc_time_spice)

    rad_sun = 1391016  # solar radius in km
    dit_sun = 149597870  # Sun-Earth distance 1 AU
    asiz = np.arctan(rad_sun / dit_sun)  # Sun angular size
    sun_arc = asiz * 180 / np.pi * 60 * 60  # Sun angular size in arcsec

    s_size = sun_arc / sc_r  # ,'solar size in arcsec from solo'

    sc_vr = (sc_x * sc_vx + sc_y * sc_vy + sc_z * sc_vz) / np.sqrt(sc_x ** 2 + sc_y ** 2 + sc_z ** 2)
    sc_vt = (sc_x * sc_vx + sc_y * sc_vy + sc_z * sc_vz) / np.sqrt(sc_x ** 2 + sc_y ** 2 + sc_z ** 2)

    # screc = np.rec.array([mdates.date2num(sc_time), sc_r, sc_lon, sc_lat, sc_x, sc_y, sc_z, sc_vx, sc_vy, sc_vz,
    #     sc_sp, sc_vr, elevation, angle, s_size],
    #     dtype=[('time', 'f8'), ('r', 'f8'), ('lon', 'f8'), ('lat', 'f8'), ('x', 'f8'),
    #     ('y', 'f8'), ('z', 'f8'), ('vx', 'f8'), ('vy', 'f8'), ('vz', 'f8'), ('sp', 'f8'),
    #     ('vr', 'f8'), ('elevation', 'f8'), ('angle', 'f8'), ('s_size', 'f8')])

    #ROLL ANGLE IMPLEMENTATION


    # +++ Includes corrdinates and parameters for Earth with keyword 'earth_...' +++
    screc=np.rec.array([mdates.date2num(sc_time), sc_r, sc_lon, sc_lat, sc_x, sc_y, sc_z, sc_vx, sc_vy, sc_vz,\
        sc_sp, sc_vr, elevation, angle, s_size,\
        earth_r, earth_lon, earth_lat, earth_x, earth_y, earth_z, earth_elevation, earth_angle,
        roll,pitch,yaw],\
        dtype=[('time','f8'), ('r','f8'), ('lon','f8'), ('lat','f8'), ('x','f8'), ('y','f8'),\
            ('z','f8'), ('vx','f8'), ('vy','f8'), ('vz','f8'),\
            ('sp','f8'), ('vr','f8'), ('elevation','f8'), ('angle','f8'), ('s_size','f8'),\
            ('earth_r','f8'),('earth_lon','f8'),('earth_lat','f8'),('earth_x','f8'),('earth_y','f8'),\
            ('earth_z','f8'),('earth_elevation','f8'),('earth_angle','f8'),\
            ('roll','f8'),('pitch','f8'),('yaw','f8')])
    # #
    # # Compute the apparent state of the Sun as seen from solar orbiter in the SOLO_HEEQ frame.
    # #
    # target = 'SUN'
    # frame  = 'SOLO_HEEQ'
    # corrtn = 'LT+S'
    # observ = 'Solar Orbiter'
    # sundir, ltime = spiceypy.spkpos(target, sc_time_spice, frame,corrtn, observ)
    # sundir = spiceypy.vhat(sundir[0])

    # # Transformation from the inertial SOLO_HEEQ to the non-inertial body-fixed IAU_PHOEBE
    # # frame.  Since we want the apparent position, we
    # # need to subtract ltime from et.
    # #
    # sform = spiceypy.sxform( 'J2000', 'IAU_PHOEBE', et-ltime )

    # print('SUNDIR(X) ={:20.6f}'.format(sundir[0]))
    # print('SUNDIR(Y) ={:20.6f}'.format(sundir[1]))
    # print('SUNDIR(Z) ={:20.6f}'.format(sundir[2]))

    spiceypy.unload(REQUIRED_KERNELS)
    return screc

@contextlib.contextmanager
def cwd_(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)

def genera_2d(what: np.ndarray)-> np.ndarray:
    """
    Created a 2D images from a radial shape

    :param what: 1D radial variation. Even dimensions.
    :type what: np.ndarray
    :return: 2D image (sauqre) generated with the radial profile.
    :rtype: np.ndarray
    """
    n = len(what)
    x, y = np.meshgrid(range(2 * n + 1), range(2 * n + 1))  # generate ODD grid
    d = np.sqrt((x - n) ** 2 + (y - n) ** 2)
    lafuncion = np.concatenate((what, np.zeros((n + 1))))
    f = interp1d(np.arange(2 * n + 1), lafuncion)
    return f(d.flat).reshape(d.shape)


def running_mean(x: np.ndarray, N: int) -> np.ndarray:
    """
    Perform a running mean over an array (smoothing function) using convolution

    :param x: 1-D array
    :type x: np.ndarray
    :param N: Number of points to perform the running mean
    :type N: int
    :return: running mean
    :rtype: np.ndarray
    """

    return np.convolve(x, np.ones((N,)) / N, mode='same')


def rotate_grid(xy, radians):
    """
    Rotate a grid or a point.

    Example:

    .. code-block:: python

        wave_axis,voltagesData,tunning_constant,cpos = fits_get_sampling(file)

        import sophi_fdt_dpp as fdt

        # single point

        theta = math.radians(33)
        point = np.array(((5, -11),(5, -11)))
        fdt.rotate_grid(point, theta)

        # grid

        x_dim = 100
        y_dim = 100
        cx = x_dim/2.
        cy = y_dim/2.
        Y, X = np.mgrid[0:y_dim, 0:x_dim]
        X = X - cx
        Y = Y - cy

        rotated_grid = fdt.rotate_grid((X.reshape(x_dim*y_dim),Y.reshape(x_dim*y_dim)), theta)
        rotated_grid = fdt.rotated_grid[0,:].reshape(x_dim,y_dim).astype(int)

    """
    x, y = xy
    c, s = cos(radians), sin(radians)
    j = np.matrix([[c, s], [-s, c]])
    return np.dot(j, [x, y])


def create_dirs(dirs: str, leading_dir: str = './') -> None:
    """
    create directories from list

    :param dirs: list of directories
    :type dirs: list
    :param leading_dir: leading directory, defaults to './'
    :type leading_dir: str, optional
    """
    if isinstance(dirs, list):
        for checkit in dirs:
            if os.path.isdir(leading_dir + checkit):
                print(leading_dir+checkit, "folder already exists.")
            else:
                os.makedirs(leading_dir+checkit)
                print("created folder : ", leading_dir+checkit)
    else:
        print('input a list....')


def find_file_in_runtime_directory(file: str):
    """
    Returns a tuple with the filename including the full path or False, if it does not exist.

    :param file: filaname
    :type file: str
    :return: filename location
    :rtype: str
    """
    runtime_directory = path.realpath(__file__)
    runtime_directory = runtime_directory[:runtime_directory.rfind('/') - len(runtime_directory) + 1]
    if path.isfile(runtime_directory + file):
        printc(f"{file} located at:", runtime_directory + file, color=bcolors.WARNING)
        return runtime_directory + file
    else:
        return False


def debug(*expression, msg=''):
    caller = getframeinfo(stack()[1][0])
    frame = _getframe(1)
    filename = caller.filename
    filename = filename[filename.rfind('/') + 1:]
    for i in expression:
        print("%s:%s%d - %s" % (filename, ' Line= ', caller.lineno, msg), i, '=', repr(eval(i, frame.f_globals, frame.f_locals)), end=";\n")
