<div style="width:800px">

<img src="./SPGLOGO-LR.png" align="right" width=100px />

## `sophi_fdt_dpp`
--------------------------

This repository contains libraries and functions developed by the [Solar Physics Group](http://spg.iaa.es/) of the [Instituto de Astrofísica de Andalucía](https://www.iaa.es), for the analysis and pre-procesing of data coming from the Polarimetric and Helioseismic Imager of the Solar Orbiter ESA mission.


Additional tools can be found in the main web of the [SPG group](http://spg.iaa.es/) and in the following repositories:
- <https://github.com/IAA-InvCodes/P-MILOS>
- <https://github.com/vivivum/MilosIDL>

</div>

Installation
------------

sophi_fdt_dpp is based in Python 3+ and can be installed in your distribution using pip:


```shell
pip install -r requirements.txt git+https://gitlab.com/SOPHI1/sophi-fdt-dpp.git@ [BRANCH]
```

If you download the source, you can choose to install it in a specific environment:

```shell
pip install -e .   OR   pip install -r requirements.txt -e .

```

As you wish.

If you want to update to last version:

```shell
pip install sophi_fdt_dpp --upgrade
```

Getting started
===============

Package is loaded in the stardard way provided it is in your path.

```python
import sophi_fdt_dpp as fdt
```
----
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)