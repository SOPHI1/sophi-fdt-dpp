
The PHI/FDT on-ground pipeline
==============================

Installation
------------

PIP

The PHI Full Disk Telescope (FDT)
---------------------------------

SO/PHI Full Disk Telescope takes images of the Sun with a 2 degree field of view.
In the instrument, the light beam :math:`\mathbf{S} = (I,Q,U,V)` first passes
through the Heat Rejection Entrance Window (HREW), whose fundamental task is to
block a large part of the solar radiation and protect the instrument. Then, it reaches
the polarization modulation package (PMP) where four sets of observations are obtained
as a result of different linear combinations of polarization states. Along the way, it encounters
a focusing lens. It then goes through the etalon, where the spectral analysis is performed on
the Fe I 617.3 nm iron line. The line is observed at only five wavelengths plus one point in
the near continuum. Finally, the beam reaches the detector. The HREW modifies the incident light
by adding a small amount of blur and negligible birefringence :math:`\left<R(x,y)\right>` which
is field dependent (:math:`x` and :math:`y` represent pixel coordinates) since
the FDT entrance pupil does not coincide with the HREW location. Between the PMP, the HREW,
and the etalon, interference phenomena occur that generate residual fringes as well as a ghost,
probably associated with a reflection of the etalon. Both contributions are represented
by :math:`F(x,y;\lambda)`, and may have a wavelength :math:`\lambda` dependence. This term may
also represent any optical distortion and the sorting prefilter.
The more important contributors to the measured intensities :math:`I_m^{\mathrm{obs}}` are
the etalon :math:`\Psi\left(x, y ; \lambda\right)`, the dark current :math:`d(x,y)`, and
the gain factor :math:`g(x,y)`. Then, the measured :math:`I_m^{\mathrm{obs}}`, where :math:`m` represnet each of the four measured
modulations states, could be then written as:

.. math:: I_m^{\mathrm{obs}} = g(x,y) \int_{0}^{\infty}  O_m\left(x, y ; \lambda-\lambda_{0}\right) \Psi\left(x, y ; \lambda-\lambda_{0}\right)  \mathrm{d} \lambda + d(x,y)

where

.. math:: O_m\left(x, y ; \lambda-\lambda_{0}\right) = \sum_{p=1}^{4}M_{mp}[F(x,y;\lambda)\left<R(x,y)\right>S_{p}(x,y;\lambda)]

.. figure:: _static/optical_layout.svg
   :height: 300px
   :width: 900px
   :scale: 100 %
   :alt: alternate text
   :align: center
   :figwidth: 100 %

In the above equations, ...



PHI/FDT expected products
-------------------------------

Esta es la documentación para mi programa.

Lo que hay que ver

Pipeline description
----------------------------

Two fundamental blocks:
- Data preprocessing
- Instrument calibration

.. figure:: _static/modes.png
   :height: 300px
   :width: 900px
   :scale: 100 %
   :alt: alternate text
   :align: center
   :figwidth: 100 %

The data preprocessing ... (whole process and corrections)

The instrument calibration deals with ... (flat and dark)


Esta es la documentación para mi programa.

subseccion
***********

parts

