FDT flatfield generation:
=========================

Introduction
------------


The on-board calibration pipeline includes the (accurate) Gain table correction of the science images (so-called flat fielding).
In the case of the Full Disk Telescope, the Gain table is calculated by using the algorithm of Khun J.R., Lin H., Loranz D. [KLL, 1991, Astrn.Soc.Pacific, 103, 1097]. 
The KLL algorithm determines the flat field from a set of n full disk images, each of them taken by displacing the solar disk along the field-of-view of the camera, i.e., moving the Spacecraft (S/C). 
The displacements are usually a few percent of the solar radius. 
The n images have to fulfill several (math rules) constraints. The most important one being:
The method demands constant light source (e.g. negligible evolution of the solar structures) and image stability.



Flat field determination algorithm
----------------------------------

* Hacer algo
* Hacer otra cosa
* ...

subseccion
***********

Esta es la documentación para mi programa.


