phi_fits
^^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phi_fits
   :members:

****

tools
^^^^^

.. automodule:: sophi_fdt_dpp.src.tools
   :members:

****

phi_utils
^^^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phi_utils
   :members:

****

phi_gen
^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phi_gen
   :members:

****

phi_rte
^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phi_rte
   :members:

****

phifdt_flat
^^^^^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phifdt_flat
   :members:

****

phifdt_pipe
^^^^^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phifdt_pipe
   :members:

****

phifdt_pipe_modules
^^^^^^^^^^^^^^^^^^^

.. automodule:: sophi_fdt_dpp.src.phifdt_pipe_modules
   :members:


.. tools
.. ^^^^^

.. .. automodule:: sophi_fdt_dpp.lib.tools
..    :members:
