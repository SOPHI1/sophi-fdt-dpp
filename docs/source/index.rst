.. image:: _static/SPGLOGO-LR.png
    :width: 40px

.. image:: _static/phi_logo2.png
    :width: 40px

.. * `#` with overline, for parts
.. * `*` with overline, for chapters
.. * `=`, for sections
.. * `-`, for subsections
.. * `^`, for subsubsections
.. * `"`, for paragraphs

****

.. raw:: html

  <style> .red {color:#aa0060; font-weight:bold; font-size:16px} </style>

.. role:: red

SOPHI FDT data processing pipeline documentation
================================================

A Python package for the on-ground processing of :red:`SOPHI/FDT` data

****

.. topic:: Overview

   This webpage describes the data reduction pipeline for the SOPHI FDT telescope.

   More information about SOPHI can be found `here <https://www.aanda.org/articles/aa/abs/2020/10/aa35325-19/aa35325-19.html>`_, and in the webpages of the `P.I. <https://www.mps.mpg.de/solar-physics/solar-orbiter-phi>`_ and `Co P.I. <http://spg.iaa.es/>`_ institutions.

   :Date: |today|
   :Author: **David Orozco Suárez** (orozco@iaa.es)_
   :Contributors: Nestor Albelo (albelo@mps.mpg.de) and Alex Feller (feller@mps.mpg.de)

.. toctree::
   :caption: sophi_fdt_dpp
   :maxdepth: 3

   Overview
   flats_preprocessing
   data_preprocessing

****

.. toctree::
   :caption: Examples
   :maxdepth: 2

   flat_preprocessing
   generate_level2
   phi_orbits

****

.. toctree::
   :caption: Reference
   :maxdepth: 2

   reference

****

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
