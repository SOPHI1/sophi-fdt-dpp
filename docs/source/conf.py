# -- Path setup --------------------------------------------------------------

import os,sys
sys.path.insert(0, os.path.abspath('../../'))

# -- Project information -----------------------------------------------------

project = 'sophi_fdt_dpp'
copyright = '2022, David Orozco Suárez'
author = 'David Orozco Suárez'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
        "IPython.sphinxext.ipython_console_highlighting",
        'nbsphinx',
        "sphinx_rtd_theme",
        'sphinx.ext.autodoc',
        "sphinx_automodapi.automodapi",
        'sphinx.ext.intersphinx',
        'sphinx.ext.todo',
        'sphinx.ext.mathjax',
        'sphinx.ext.autosummary', # solamente si se la quiere usar
        'sphinx.ext.viewcode',
        "sphinx.ext.doctest",
        "nbsphinx_link",
        "myst_parser"
]

# 'sphinx.ext.mathjax'

intersphinx_mapping = {
    "astropy": ("http://docs.astropy.org/en/latest/", None),
    "matplotlib": ("https://matplotlib.org/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "py": ("https://docs.python.org/3/", None),
}

numpydoc_show_class_members = False

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '**.ipynb_checkpoints']

nbsphinx_execute = 'never'

autosummary_generate = True
# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_sidebars = { '**': ['globaltoc.html', 'relations.html',
        'sourcelink.html', 'searchbox.html'], }

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
def setup(app):
    app.add_css_file("sophi_fdt_dpp.css")
    app.add_js_file("copybutton.js")

