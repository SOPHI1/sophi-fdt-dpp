��r0      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�The PHI/FDT on-ground pipeline�h]�h	�Text����The PHI/FDT on-ground pipeline�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�K/Users/orozco/Dropbox (IdAdA)/Python/sophi-fdt-dpp/docs/source/Overview.rst�hKubh)��}�(hhh]�(h)��}�(h�Installation�h]�h�Installation�����}�(hh0hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh-hhhh,hKubh	�	paragraph���)��}�(h�PIP�h]�h�PIP�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubeh}�(h!]��installation�ah#]�h%]��installation�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�!The PHI Full Disk Telescope (FDT)�h]�h�!The PHI Full Disk Telescope (FDT)�����}�(hhYhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhVhhhh,hKubh?)��}�(hX�  SO/PHI Full Disk Telescope takes images of the Sun with a 2 degree field of view.
In the instrument, the light beam :math:`\mathbf{S} = (I,Q,U,V)` first passes
through the Heat Rejection Entrance Window (HREW), whose fundamental task is to
block a large part of the solar radiation and protect the instrument. Then, it reaches
the polarization modulation package (PMP) where four sets of observations are obtained
as a result of different linear combinations of polarization states. Along the way, it encounters
a focusing lens. It then goes through the etalon, where the spectral analysis is performed on
the Fe I 617.3 nm iron line. The line is observed at only five wavelengths plus one point in
the near continuum. Finally, the beam reaches the detector. The HREW modifies the incident light
by adding a small amount of blur and negligible birefringence :math:`\left<R(x,y)\right>` which
is field dependent (:math:`x` and :math:`y` represent pixel coordinates) since
the FDT entrance pupil does not coincide with the HREW location. Between the PMP, the HREW,
and the etalon, interference phenomena occur that generate residual fringes as well as a ghost,
probably associated with a reflection of the etalon. Both contributions are represented
by :math:`F(x,y;\lambda)`, and may have a wavelength :math:`\lambda` dependence. This term may
also represent any optical distortion and the sorting prefilter.
The more important contributors to the measured intensities :math:`I_m^{\mathrm{obs}}` are
the etalon :math:`\Psi\left(x, y ; \lambda\right)`, the dark current :math:`d(x,y)`, and
the gain factor :math:`g(x,y)`. Then, the measured :math:`I_m^{\mathrm{obs}}`, where :math:`m` represnet each of the four measured
modulations states, could be then written as:�h]�(h�tSO/PHI Full Disk Telescope takes images of the Sun with a 2 degree field of view.
In the instrument, the light beam �����}�(hhghhhNhNubh	�math���)��}�(h�:math:`\mathbf{S} = (I,Q,U,V)`�h]�h�\mathbf{S} = (I,Q,U,V)�����}�(hhqhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubhX�   first passes
through the Heat Rejection Entrance Window (HREW), whose fundamental task is to
block a large part of the solar radiation and protect the instrument. Then, it reaches
the polarization modulation package (PMP) where four sets of observations are obtained
as a result of different linear combinations of polarization states. Along the way, it encounters
a focusing lens. It then goes through the etalon, where the spectral analysis is performed on
the Fe I 617.3 nm iron line. The line is observed at only five wavelengths plus one point in
the near continuum. Finally, the beam reaches the detector. The HREW modifies the incident light
by adding a small amount of blur and negligible birefringence �����}�(hhghhhNhNubhp)��}�(h�:math:`\left<R(x,y)\right>`�h]�h�\left<R(x,y)\right>�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh� which
is field dependent (�����}�(hhghhhNhNubhp)��}�(h�	:math:`x`�h]�h�x�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh� and �����}�(hhghhhNhNubhp)��}�(h�	:math:`y`�h]�h�y�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubhX;   represent pixel coordinates) since
the FDT entrance pupil does not coincide with the HREW location. Between the PMP, the HREW,
and the etalon, interference phenomena occur that generate residual fringes as well as a ghost,
probably associated with a reflection of the etalon. Both contributions are represented
by �����}�(hhghhhNhNubhp)��}�(h�:math:`F(x,y;\lambda)`�h]�h�F(x,y;\lambda)�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�, and may have a wavelength �����}�(hhghhhNhNubhp)��}�(h�:math:`\lambda`�h]�h�\lambda�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�� dependence. This term may
also represent any optical distortion and the sorting prefilter.
The more important contributors to the measured intensities �����}�(hhghhhNhNubhp)��}�(h�:math:`I_m^{\mathrm{obs}}`�h]�h�I_m^{\mathrm{obs}}�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh� are
the etalon �����}�(hhghhhNhNubhp)��}�(h�':math:`\Psi\left(x, y ; \lambda\right)`�h]�h�\Psi\left(x, y ; \lambda\right)�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�, the dark current �����}�(hhghhhNhNubhp)��}�(h�:math:`d(x,y)`�h]�h�d(x,y)�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�, and
the gain factor �����}�(hhghhhNhNubhp)��}�(h�:math:`g(x,y)`�h]�h�g(x,y)�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�. Then, the measured �����}�(hhghhhNhNubhp)��}�(h�:math:`I_m^{\mathrm{obs}}`�h]�h�I_m^{\mathrm{obs}}�����}�(hj%  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�, where �����}�(hhghhhNhNubhp)��}�(h�	:math:`m`�h]�h�m�����}�(hj7  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hohhgubh�R represnet each of the four measured
modulations states, could be then written as:�����}�(hhghhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhhVhhubh	�
math_block���)��}�(h��I_m^{\mathrm{obs}} = g(x,y) \int_{0}^{\infty}  O_m\left(x, y ; \lambda-\lambda_{0}\right) \Psi\left(x, y ; \lambda-\lambda_{0}\right)  \mathrm{d} \lambda + d(x,y)

�h]�h��I_m^{\mathrm{obs}} = g(x,y) \int_{0}^{\infty}  O_m\left(x, y ; \lambda-\lambda_{0}\right) \Psi\left(x, y ; \lambda-\lambda_{0}\right)  \mathrm{d} \lambda + d(x,y)

�����}�hjQ  sbah}�(h!]�h#]�h%]�h']�h)]��docname��Overview��number�N�label�N�nowrap���	xml:space��preserve�uh+jO  hh,hK"hhVhhubh?)��}�(h�where�h]�h�where�����}�(hjf  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK$hhVhhubjP  )��}�(h�xO_m\left(x, y ; \lambda-\lambda_{0}\right) = \sum_{p=1}^{4}M_{mp}[F(x,y;\lambda)\left<R(x,y)\right>S_{p}(x,y;\lambda)]

�h]�h�xO_m\left(x, y ; \lambda-\lambda_{0}\right) = \sum_{p=1}^{4}M_{mp}[F(x,y;\lambda)\left<R(x,y)\right>S_{p}(x,y;\lambda)]

�����}�hjt  sbah}�(h!]�h#]�h%]�h']�h)]��docname�j`  �number�N�label�N�nowrap��jd  je  uh+jO  hh,hK&hhVhhubh	�figure���)��}�(hhh]�h	�image���)��}�(h��.. figure:: _static/optical_layout.svg
   :height: 300px
   :width: 900px
   :scale: 100 %
   :alt: alternate text
   :align: center
   :figwidth: 100 %
�h]�h}�(h!]�h#]�h%]�h']�h)]��height��300px��width��900px��scale�Kd�alt��alternate text��uri��_static/optical_layout.svg��
candidates�}��*�j�  suh+j�  hj�  hh,hNubah}�(h!]�h#]�h%]�h']�h)]��width��100%��align��center�uh+j�  hhVhhhh,hNubh?)��}�(h�In the above equations, ...�h]�h�In the above equations, …�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK0hhVhhubeh}�(h!]��the-phi-full-disk-telescope-fdt�ah#]�h%]��!the phi full disk telescope (fdt)�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�PHI/FDT expected products�h]�h�PHI/FDT expected products�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK5ubh?)��}�(h�+Esta es la documentación para mi programa.�h]�h�+Esta es la documentación para mi programa.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK7hj�  hhubh?)��}�(h�Lo que hay que ver�h]�h�Lo que hay que ver�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK9hj�  hhubeh}�(h!]��phi-fdt-expected-products�ah#]�h%]��phi/fdt expected products�ah']�h)]�uh+h
hhhhhh,hK5ubh)��}�(hhh]�(h)��}�(h�Pipeline description�h]�h�Pipeline description�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK<ubh?)��}�(h�ETwo fundamental blocks:
- Data preprocessing
- Instrument calibration�h]�h�ETwo fundamental blocks:
- Data preprocessing
- Instrument calibration�����}�(hj	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK>hj�  hhubj�  )��}�(hhh]�j�  )��}�(h��.. figure:: _static/modes.png
   :height: 300px
   :width: 900px
   :scale: 100 %
   :alt: alternate text
   :align: center
   :figwidth: 100 %
�h]�h}�(h!]�h#]�h%]�h']�h)]��height��300px��width��900px��scale�Kd�alt��alternate text��uri��_static/modes.png�j�  }�j�  j,  suh+j�  hj  hh,hNubah}�(h!]�h#]�h%]�h']�h)]�j�  �100%�j�  �center�uh+j�  hj�  hhhh,hNubh?)��}�(h�:The data preprocessing ... (whole process and corrections)�h]�h�:The data preprocessing … (whole process and corrections)�����}�(hj6  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKJhj�  hhubh?)��}�(h�9The instrument calibration deals with ... (flat and dark)�h]�h�9The instrument calibration deals with … (flat and dark)�����}�(hjD  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKLhj�  hhubh?)��}�(h�+Esta es la documentación para mi programa.�h]�h�+Esta es la documentación para mi programa.�����}�(hjR  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKOhj�  hhubh)��}�(hhh]�(h)��}�(h�
subseccion�h]�h�
subseccion�����}�(hjc  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj`  hhhh,hKRubh?)��}�(h�parts�h]�h�parts�����}�(hjq  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKThj`  hhubeh}�(h!]��
subseccion�ah#]�h%]��
subseccion�ah']�h)]�uh+h
hj�  hhhh,hKRubeh}�(h!]��pipeline-description�ah#]�h%]��pipeline description�ah']�h)]�uh+h
hhhhhh,hK<ubeh}�(h!]��the-phi-fdt-on-ground-pipeline�ah#]�h%]��the phi/fdt on-ground pipeline�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�J ���pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  hShPj�  j�  j�  j�  j�  j�  j�  j�  u�	nametypes�}�(j�  �hS�j�  �j�  �j�  �j�  �uh!}�(j�  hhPh-j�  hVj�  j�  j�  j�  j�  j`  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.